package org.ladhark.identity.util

import org.ladhark.identity.entity.application.Application
import org.ladhark.identity.entity.resource.ResourceAction
import org.slf4j.LoggerFactory
import org.springframework.http.HttpMethod
import org.springframework.http.server.PathContainer
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.web.util.pattern.PathPatternParser

object Logging {
    fun logger(): org.slf4j.Logger = LoggerFactory.getLogger("org.ladhark.identity.util.web")
}

private val log = Logging.logger()

fun pathMatches(pathInsideApplicationServicePattern: String, httpMethod: HttpMethod, resourceAction: ResourceAction): Boolean {

    if (log.isDebugEnabled) {
        log.debug("Matching path inside application service pattern {} with method {} to " +
                "resource action pattern {} with scope {}",
            pathInsideApplicationServicePattern,
            httpMethod,
            resourceAction.uriPattern,
            resourceAction.scope)
    }

    val scope = resourceAction.scope

    if (httpMethod.name().lowercase() != scope.name.lowercase()) {
        return false;
    }

    val pathPattern = resourceAction.uriPattern

    val pathPatternParser = PathPatternParser.defaultInstance.parse(pathPattern)

    return pathPatternParser.matches(PathContainer.parsePath(pathInsideApplicationServicePattern))
}

fun pathMatches(serverHttpRequest: ServerHttpRequest, application: Application): Boolean {

    if (log.isDebugEnabled) {
        log.debug("Matching server http request {} with method {} to application service pattern {}",
            serverHttpRequest.path.pathWithinApplication().value(),
            serverHttpRequest.method,
            application.servicePattern)
    }

    val requestPath = serverHttpRequest.path.pathWithinApplication()
    val pathPattern = application.servicePattern

    val pathPatternParser = PathPatternParser.defaultInstance.parse(pathPattern)

    return pathPatternParser.matches(requestPath)
}

