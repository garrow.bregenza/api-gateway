package org.ladhark.identity.api.resource

import org.ladhark.identity.entity.resource.ResourceAction
import org.ladhark.identity.service.application.cache.ApplicationCacheService
import org.ladhark.identity.service.resource.cache.ResourceActionCacheService
import org.ladhark.identity.util.pathMatches
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.server.PathContainer
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.stereotype.Component
import org.springframework.web.util.pattern.PathPatternParser

@Component
class ResourceActionProcessor {

    @Autowired
    lateinit var resourceActionCacheService: ResourceActionCacheService

    @Autowired
    lateinit var applicationCacheService: ApplicationCacheService

    suspend fun locateAndMatchResourceAction(serverHttpRequest: ServerHttpRequest): ResourceInformation {
        return try {

                    val application = applicationCacheService.allEntriesInUuidCache().first {
                        pathMatches(serverHttpRequest, it)
                    }

            val pathPatternParser = PathPatternParser.defaultInstance.parse(application.servicePattern)
            val pathInsideApplicationServicePattern = "/" + pathPatternParser
                .extractPathWithinPattern(PathContainer.parsePath(serverHttpRequest.path.pathWithinApplication().value()))
                .value()
                   val resourceAction = resourceActionCacheService.allEntriesInUuidCache().first {
                        // TODO cache matcher on pattern and method ??????
                        pathMatches(pathInsideApplicationServicePattern, serverHttpRequest.method, it)
                    }

                ResourceInformation(resourceAction, pathInsideApplicationServicePattern)
            } catch (e: NoSuchElementException) {
                return ResourceInformation(ResourceAction.EMPTY_RESOURCE, "")
            }
    }
}

data class ResourceInformation(val resourceAction: ResourceAction, val pathInsideApplicationServicePattern: String)