package org.ladhark.identity.api.controller.search


data class SearchParams(
    val offset: Int = 0,
    val pageSize: Int = 10,
    val sortBy: Map<String, SortMeta>?,
    val filterBy: Map<String, FilterMeta>?
)

data class SortMeta(
    val field: String,
    val order: SortOrder = SortOrder.DESC
)

enum class SortOrder {
    ASC,
    DESC;

    /*companion object {

        fun toNeo4j(order: SortOrder): org.neo4j.ogm.cypher.query.SortOrder.Direction? {
            return when(order) {
                ASC -> org.neo4j.ogm.cypher.query.SortOrder.Direction.ASC
                DESC -> org.neo4j.ogm.cypher.query.SortOrder.Direction.DESC
            }
        }
    }*/
}

data class FilterMeta(
    val value: Any,
    val matchMode: MatchMode = MatchMode.STARTS_WITH
)

enum class MatchMode(val operator: String) {

    STARTS_WITH("startsWith"),
    ENDS_WITH("endsWith"),
    CONTAINS("contains"),
    EXACT("exact"),
    LESS_THAN("lt"),
    LESS_THAN_EQUALS("lte"),
    GREATER_THAN("gt"),
    GREATER_THAN_EQUALS("gte"),
    EQUALS("equals"),
    IN("in"),
    RANGE("range"),
    GLOBAL("global");

    /*companion object {

        fun toNeo4j(mode: MatchMode): ComparisonOperator? {
            return when (mode) {
                STARTS_WITH -> ComparisonOperator.STARTING_WITH
                ENDS_WITH -> ComparisonOperator.ENDING_WITH
                CONTAINS -> ComparisonOperator.CONTAINING
                EXACT -> ComparisonOperator.EQUALS
                LESS_THAN -> ComparisonOperator.LESS_THAN
                LESS_THAN_EQUALS -> ComparisonOperator.LESS_THAN_EQUAL
                GREATER_THAN -> ComparisonOperator.GREATER_THAN
                GREATER_THAN_EQUALS -> ComparisonOperator.GREATER_THAN_EQUAL
                EQUALS -> ComparisonOperator.EQUALS
                IN -> ComparisonOperator.IN
                RANGE -> throw UnsupportedOperationException("RANGE")
                GLOBAL -> throw UnsupportedOperationException("GLOBAL")
            }
        }

    }*/
}

data class SearchResult<T>(val result: Set<T>, val totalCount: Long, val page: Int, val pageSize: Int)