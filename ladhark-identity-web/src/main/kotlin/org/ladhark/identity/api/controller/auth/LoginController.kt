package org.ladhark.identity.api.controller.auth

import org.ladhark.identity.properties.Constant
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Constant.API_TAG)
class LoginController {

    val log: Logger = LoggerFactory.getLogger(LoginController::class.java)

    companion object {
        const val LOGIN_REQUEST = "/login"
        const val REFRESH_ACCESS_TOKEN = "/refresh/access"
    }
}