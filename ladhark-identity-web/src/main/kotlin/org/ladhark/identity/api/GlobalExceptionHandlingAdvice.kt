package org.ladhark.identity.api

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.io.IOException

@ControllerAdvice
class GlobalExceptionHandlingAdvice {

    @ExceptionHandler
    fun handle(ex: IOException): ResponseEntity<*> {
        val errorDetails = ErrorDetails()
        errorDetails.errorMessage = ex.message
        return ResponseEntity<Any>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR)
    }
}

internal class ErrorDetails {
    var errorCode: String? = null
    var errorMessage: String? = null
}