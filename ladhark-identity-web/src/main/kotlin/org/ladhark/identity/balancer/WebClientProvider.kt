package org.ladhark.identity.balancer

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.ladhark.identity.entity.application.Application
import org.ladhark.identity.entity.application.BalancerType
import org.slf4j.LoggerFactory
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import reactor.netty.http.client.HttpClient
import reactor.netty.resources.ConnectionProvider
import java.time.Duration
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicReference


/**
 * A WebClient needs to change if
 *
 *  1. Service Pattern
 *  2. Pool size changes
 *  3. Balancer strategy changes
 *  4. Hosting location changes
 *  5. A property of balancer changes
 *
 */
@Service
class WebClientProvider {

    private val log = LoggerFactory.getLogger(WebClientProvider::class.java)

    private val mutex = Mutex()

    private val webClientHolderMap: MutableMap<WebClientApplication, WebClientHolder> = mutableMapOf()
    private val lastKnownApplicationHolderMap: MutableMap<WebClientApplication, LastKnownApplication> = mutableMapOf()

    suspend fun compareAndGet(application: Application): WebClient {

        with(application) {

            val webClientApplication = WebClientApplication(name)

            if (!webClientHolderMap.containsKey(webClientApplication)) {
                mutex.withLock {
                    if (!webClientHolderMap.containsKey(webClientApplication)) {
                        setUpApplicationWebClientHolder(application)
                    }
                }
            }

            val toCompareLastKnownApplication = lastKnownApplication(application)

            val webClientHolder = webClientHolderMap[webClientApplication]
                ?: throw IllegalStateException("Web client holder not present for application $name")

            if (isChangeDetected(application, toCompareLastKnownApplication)) {
                mutex.withLock {

                    log.info("Change in setting detected for application {}", webClientApplication)

                    if (isChangeDetected(application, toCompareLastKnownApplication)) {
                        webClientHolder.init(application)
                    }
                }
            }

            return webClientHolder.get(webClientApplication)
        }
    }

    private fun isChangeDetected(application: Application, toCompareLastKnownApplication: LastKnownApplication): Boolean {
        val webClientApplication = WebClientApplication(application.name)
        val lastKnownApplication = lastKnownApplicationHolderMap[webClientApplication]

        return (lastKnownApplication != toCompareLastKnownApplication).also {
            if (it) {
                log.info("Last known application {}", lastKnownApplication)
                log.info("Last known application to compare {}", toCompareLastKnownApplication)
            }
        }
    }

    private suspend fun setUpApplicationWebClientHolder(application: Application) {

        val webClientApplication = WebClientApplication(application.name)

        log.info("Setting up application web client holder for application {}", webClientApplication)

        val webClientHolder = WebClientHolder()
        webClientHolder.init(application)

        webClientHolderMap[webClientApplication] = webClientHolder
        lastKnownApplicationHolderMap[webClientApplication] = lastKnownApplication(application)
    }

    private fun lastKnownApplication(application: Application) =
        LastKnownApplication(
            application.servicePattern,
            application.hostingLocations.map { it.location }.toSet(),
            application.balancerConfig.type.name,
            application.balancerConfig.poolSize
        )
}

sealed interface BalancerStrategy {

    fun name(): String

    suspend fun initWebClients()

    suspend fun getWebClient(): WebClient

    suspend fun dispose()

}

internal class RoundRobinBalancerStrategy(private val application: Application) : BalancerStrategy {

    private val log = LoggerFactory.getLogger(RoundRobinBalancerStrategy::class.java)

    private val counter = AtomicInteger(0)
    private val webClients = mutableMapOf<Int, WebClient>()
    private val connectionProviderList = mutableListOf<ConnectionProvider>()

    private val name = "RoundRobinBalancerStrategy-${application.name}"

    override fun name(): String {
        return name
    }

    override suspend fun initWebClients() {
        with(application) {

            var count = 1

            with(balancerConfig) {

                hostingLocations.forEach { hostingLocation ->

                    val connectionProviderName = "connection-$name-$count"

                    log.info("Creating connection provider with name {} for hosting location {} and application {}",
                        connectionProviderName, hostingLocation.location, application.name)

                    val connectionProvider = ConnectionProvider
                        .builder(connectionProviderName)
                        .maxConnections(poolSize)
                        .maxIdleTime(Duration.ofSeconds(20))
                        .maxLifeTime(Duration.ofSeconds(60))
                        .pendingAcquireTimeout(Duration.ofSeconds(60))
                        .evictInBackground(Duration.ofSeconds(120))
                        .build()

                    connectionProviderList.add(connectionProvider)

                    val httpClient = HttpClient
                        .create(connectionProvider)
                        .baseUrl(hostingLocation.location)

                    val webClient = WebClient.builder()
                        .clientConnector(ReactorClientHttpConnector(httpClient))
                        .build()

                    webClients[count] = webClient

                    count += 1
                }
            }
        }
    }

    override suspend fun getWebClient(): WebClient {
        return nextWebClient()
    }

    override suspend fun dispose() {
        connectionProviderList.forEach {
            log.info("Disposing connection provider for application {} with connection provider instance name {}",
                application.name, it.name())
            it.dispose()
        }
    }

    private fun nextWebClient(): WebClient {
        val nextCounter = nextCounter()

        log.debug("Serving counter {} for application {}", nextCounter, application.name)

        return webClients[nextCounter]
            ?: throw IllegalStateException("Web Client cannot be fetched for counter $nextCounter")
    }

    private fun nextCounter() : Int {
        return with(application) {
            (counter.getAndIncrement() % hostingLocations.size) + 1
        }
    }

}

/**
 * Note: This class is not completely thread safe at present it is possible when strategy is being
 * initialized a request comes for a web client, we try best to not serve old web client on update
 * by immediately setting it to null on update and checking it when getting web client, but still there
 * is room for some escape, and also we are disposing old connection pool, well dispose logic
 * depends on implementation still calling it, it could be well no op.
 *
 * Atomic classes are used but still room is there for leaks.
 *
 * Leak could happen when 1st thread is getting a web client and in parallel request comes
 * to update the client, now one is fetching old web client and other in process of setting up new one.
 *
 * Let's see, knowledge of leak is intentional to be as lock free, this implementation may change
 * in latter stage if seen to cause issues, attempt is to be lock/mutex free when web client instance is fetched
 * for an application.
 *
 */
internal class WebClientHolder {

    private val log = LoggerFactory.getLogger(WebClientHolder::class.java)

    private val mutex = Mutex()
    private val lockHelper = AtomicBoolean(false)
    private val balancerStrategyAtomicReference: AtomicReference<BalancerStrategy> = AtomicReference()

    suspend fun init(application: Application) {
        mutex.withLock {
            try {
                lockHelper.set(true)

                log.info("Initializing balancer strategy for application {}", application.name)

                if(balancerStrategyAtomicReference.get() != null) {
                    // we have already registered one needs dispose ?
                    val balancerStrategy = balancerStrategyAtomicReference.get()
                    balancerStrategyAtomicReference.set(null)

                    coroutineScope {
                        launch {
                            balancerStrategy.dispose()
                        }
                    }
                }

                with(application) {
                    with(balancerConfig) {

                        log.info("Balancer strategy type is {} for application {}", type, application.name)

                        when (type) {
                            BalancerType.FAIL_OVER -> {
                                TODO("Not implemented !!!!")
                            }

                            BalancerType.RANDOM -> {
                                TODO("Not implemented !!!!")
                            }

                            BalancerType.ROUND_ROBIN -> {
                                balancerStrategyAtomicReference.set(RoundRobinBalancerStrategy(application))
                            }
                        }
                    }
                }

                balancerStrategyAtomicReference.get().initWebClients()

            } finally {
                lockHelper.set(false)
            }
        }
    }

    suspend fun get(webClientApplication: WebClientApplication): WebClient {

        val balancerStrategy = balancerStrategyAtomicReference.get()
            ?: throw IllegalStateException("Balancer strategy not ready for application $webClientApplication")

        return if (lockHelper.get()) {
            // locked
            mutex.withLock {
                log.info("Returning web client in locked mode for application {}", balancerStrategy.name())
                balancerStrategy.getWebClient()
            }
        } else {
            // normal
            log.debug("Returning web client in normal mode for application {}", balancerStrategy.name())
            balancerStrategy.getWebClient()
        }
    }

}

internal data class LastKnownApplication(private val servicePattern: String,
                                            private val hostingLocation: Set<String>,
                                            private val balancerConfigType: String,
                                            private val poolSize: Int)

@JvmInline
value class WebClientApplication(val name: String)