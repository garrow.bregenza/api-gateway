package org.ladhark.identity.security.filter

import kotlinx.coroutines.*
import org.ladhark.identity.api.controller.auth.LoginController
import org.ladhark.identity.api.resource.ResourceActionProcessor
import org.ladhark.identity.entity.application.Application
import org.ladhark.identity.entity.resource.ResourceAction
import org.ladhark.identity.entity.resource.policy.ResourcePolicy
import org.ladhark.identity.exception.AuthorizationFailedException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.messages.AppMessage
import org.ladhark.identity.properties.ApplicationProperties
import org.ladhark.identity.properties.Constant
import org.ladhark.identity.repository.extension.suspendedResultToMono
import org.ladhark.identity.repository.resource.policy.ResourcePolicyRepository
import org.ladhark.identity.security.AuthenticationToken
import org.ladhark.identity.security.JWTService
import org.ladhark.identity.security.LadharkAuthenticatedPrincipal
import org.ladhark.identity.security.context.*
import org.ladhark.identity.service.resource.ResourceService
import org.ladhark.identity.service.resource.policy.cache.ResourcePolicyCacheService
import org.ladhark.kotlin.extension.s
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.http.server.reactive.ServerHttpResponse
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono
import reactor.util.context.Context
import java.util.*


public const val Path_Inside_Application_Service_Pattern = "pathInsideApplicationServicePattern"

@Component
@Order(1)
class AuthorizationFilter : WebFilter {

    private val log = LoggerFactory.getLogger(AuthorizationFilter::class.java)

    @Autowired
    lateinit var applicationProperties: ApplicationProperties

    @Autowired
    lateinit var resourcePolicyRepository: ResourcePolicyRepository

    @Autowired
    lateinit var resourceService: ResourceService

    @Autowired
    lateinit var jwtService: JWTService

    @Autowired
    lateinit var appMessage: AppMessage

    @Autowired
    lateinit var resourceActionProcessor: ResourceActionProcessor

    @Autowired
    lateinit var resourcePolicyCacheService: ResourcePolicyCacheService

    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {

        val request: ServerHttpRequest = exchange.request
        val response: ServerHttpResponse = exchange.response

        val method = request.method
        val path = exchange.request.uri.path

        log.info("Processing URI {} with method {}", path, method)

        return suspendedResultToMono {
            val resourceInformation = resourceActionProcessor
                .locateAndMatchResourceAction(request)

            val resourceAction = resourceInformation.resourceAction
            var resourcePolicy: ResourcePolicy? = null
            var application: Application? = null

            if (ResourceAction.isNotEmpty(resourceAction)) {
                resourcePolicy = resourcePolicyCacheService
                    .getResourcePolicyByResourceActionUuidAsync(resourceAction.uuid.s) ?: run {
                        resourcePolicyRepository
                            .findByResourceActionUuid(resourceAction.uuid)?.also {
                                resourcePolicyCacheService.putResourcePolicyByResourceActionUuidAsync(resourceAction.uuid.s, it)
                            }
                    }

                application = resourceAction.resource.application
            }

            if (application == null) {
                throw IllegalStateException("Application not found")
            }

            var isOpen = false

            resourcePolicy?.let {
                isOpen = resourceService.isOpenPublic(it)
                if (isOpen) {
                    log.info("Action URI {} found public open", path)
                }
            }

            val serviceContext = if (isOpen) {
                val openLadharkPrincipal = OpenLadharkPrincipal()
                val ladharkAuthenticatedPrincipal = LadharkAuthenticatedPrincipal(openLadharkPrincipal)
                val authenticationToken = AuthenticationToken(ladharkAuthenticatedPrincipal, "__open__")
                ServiceContext(
                    false, authenticationToken,
                    Optional.ofNullable(application),
                    Optional.ofNullable(resourceAction),
                    Optional.ofNullable(resourcePolicy)
                )
            } else {
                val authorizationHeader = request.headers[applicationProperties.authorizationHeaderName()]?.first()

                if (authorizationHeader.isNullOrBlank()) {
                    throw AuthorizationFailedException("Authorization Token Invalid.")
                }

                val token = jwtService.extractTokenFromHeader(authorizationHeader)
                    ?: throw AuthorizationFailedException("Authorization Token Invalid.")

                val authenticationToken = getAuthenticationToken(token, request)

                ServiceContext(
                    true, authenticationToken,
                    Optional.ofNullable(application),
                    Optional.ofNullable(resourceAction),
                    Optional.ofNullable(resourcePolicy)
                )

                // TODO check validity of claims
                // if not valid throw exception
            }

            Pair(
                serviceContext,
                mapOf(Path_Inside_Application_Service_Pattern to resourceInformation
                    .pathInsideApplicationServicePattern)
            )

        }.flatMap { serviceContextPair ->
            Mono.deferContextual { context ->

                val serviceContextHolder = context.get<ServiceContextHolder>(SERVICE_CONTEXT_KEY)

                serviceContextHolder.serviceContext = Optional.of(serviceContextPair.first)
                serviceContextHolder.serviceContext
                    .get().requestInformation.properties.putAll(serviceContextPair.second)

                Mono.just(serviceContextPair)
            }
        }.flatMap {
            chain.filter(exchange)
        }.contextWrite {

            // optional empty as object storage, we start with empty
            // above map would fill it with appropriate setting
            Context.of(SERVICE_CONTEXT_KEY, ServiceContextHolder(Optional.empty()))

        }
    }


    private suspend fun getAuthenticationToken(token: String, request: ServerHttpRequest): AuthenticationToken {

        val claims = jwtService.parseJwtHeader(token)

        val tokenType = claims[Constant.JWT_TOKEN_TYPE] as String

        allowTokenProcessing(tokenType, request)

        val ladharkAuthenticatedPrincipal = when {
            claims[Constant.JWT_TOKEN_SUBJECT_TYPE] == Constant.JWT_TOKEN_SUBJECT_TYPE_CLIENT -> {
                jwtService.fetchClientForJwtSubject(claims)
            }

            claims[Constant.JWT_TOKEN_SUBJECT_TYPE] == Constant.JWT_TOKEN_SUBJECT_TYPE_USER -> {
                jwtService.fetchUserForJwtSubject(claims)
            }

            else -> {
                throw InvalidArgumentException(
                    appMessage.getMessage(
                        "error.jwt.token.invalid.data",
                        "Subject Type"
                    )
                )
            }
        }

        return AuthenticationToken(ladharkAuthenticatedPrincipal, token)
    }

    private suspend fun allowTokenProcessing(tokenType: String, request: ServerHttpRequest) {

        val isRefreshRequest = "${Constant.API_TAG}${LoginController.REFRESH_ACCESS_TOKEN}" == request.uri.toString()

        if (isRefreshRequest && Constant.JWT_TOKEN_TYPE_REFRESH != tokenType) {
            throw AuthorizationFailedException(
                appMessage.getMessage(
                    "error.jwt.token.type.mismatch",
                    "Refresh"
                )
            )
        }

        if (!isRefreshRequest && Constant.JWT_TOKEN_TYPE_ACCESS != tokenType) {
            throw AuthorizationFailedException(
                appMessage.getMessage(
                    "error.jwt.token.type.mismatch",
                    "Access"
                )
            )
        }

    }
}

class NameRequiredException(status: HttpStatus, message: String, e: Throwable) :
    ResponseStatusException(status, message, e)