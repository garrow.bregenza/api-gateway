package org.ladhark.identity.security.filter

import org.ladhark.identity.balancer.WebClientProvider
import org.ladhark.identity.repository.extension.suspendedResultToMono
import org.ladhark.identity.service.context.withUserContext
import org.slf4j.LoggerFactory
import org.slf4j.MDC
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.annotation.Order
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.http.server.reactive.ServerHttpResponse
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono
import java.net.ConnectException

@Component
@Order(4)
class ProxyFilter : WebFilter {

    private val log = LoggerFactory.getLogger(ProxyFilter::class.java)

    @Autowired
    private lateinit var webClientProvider: WebClientProvider

    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {

        val request: ServerHttpRequest = exchange.request
        val response: ServerHttpResponse = exchange.response

        val method = request.method
        val path = exchange.request.uri.path

        log.info("Processing URI {} with method {}", path, method)

        return suspendedResultToMono {
            withUserContext {
              application.orElseThrow {
                   throw IllegalStateException("Application not found")
               }
            }
        }.flatMap { application ->
            if (application.isHome()) {
                // is home no proxy needed

                val startTime = System.currentTimeMillis()

                chain.filter(exchange)
                    .doFirst {
                        println("!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                        // Don't think so anything useful here.

                        // throw AuthorizationFailedException("")
                    }
                    .doAfterTerminate {
                        exchange.response.headers.forEach { e ->
                            log.info(
                                "Response header '{}': {}",
                                e.key,
                                e.value
                            )
                        }
                        log.info(
                            "Served '{}' as {} in {} msec",
                            path,
                            exchange.response.statusCode,
                            System.currentTimeMillis() - startTime
                        )
                        println("!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                        MDC.remove("PRINCIPAL_UUID")
                        MDC.remove("REQUEST_UUID")
                    }
                    .onErrorResume { e ->
                        log.error("Error happened", e)
                        Mono.error(
                            NameRequiredException(
                                HttpStatus.BAD_REQUEST,
                                "username is required", e
                            )
                        )
                    }
            } else {
                // proxy it

                log.info("Serving '{}'", path)

                suspendedResultToMono {
                    withUserContext {
                        val webClient = webClientProvider.compareAndGet(application)
                        val proxyUri = requestInformation.properties[Path_Inside_Application_Service_Pattern]
                        Pair(webClient, proxyUri as String)
                    }
                }.flatMap { pair ->

                    val proxyClient = pair.first
                    val proxyUri = pair.second

                    log.info("Serving proxy path '{}'", proxyUri)

                    proxyClient
                        .method(method)
                        .uri { uriBuilder ->
                            uriBuilder
                                .path(proxyUri)
                                .queryParams(request.queryParams)
                                .build()
                        }
                        .headers { headers ->
                            val requestHeaders: HttpHeaders = request.headers
                            requestHeaders.forEach(headers::addAll)
                        }
                        .body(request.body, DataBuffer::class.java)
                        .exchangeToMono { clientResponse ->
                            response.statusCode = clientResponse.statusCode()
                            clientResponse.headers().asHttpHeaders().forEach { entry, values ->
                                if ("Content-Type".equals(entry, ignoreCase = true)) {
                                    // for text/event-stream
                                    response.headers.set(entry, values.first())
                                } else {
                                    response.headers.addAll(entry, values)
                                }
                            }
                            response.writeWith(clientResponse.bodyToFlux(DataBuffer::class.java))


                        }.doOnError { throwable ->
                            if (throwable.cause != null && throwable.cause is ConnectException) {
                                response.statusCode = HttpStatus.BAD_GATEWAY
                                response.headers.set("Content-Type", "text/plain")
                            } else {
                                response.statusCode = HttpStatus.INTERNAL_SERVER_ERROR
                                response.headers.set("Content-Type", "text/plain")
                            }

                            response.bufferFactory().wrap(ByteArray(0))
                        }
                }
            }
        }
    }
}