package org.ladhark.identity.security.filter

import jakarta.annotation.PostConstruct
import kotlinx.coroutines.flow.toList
import org.ladhark.identity.entity.aspect.Aspect
import org.ladhark.identity.entity.resource.ResourceAction
import org.ladhark.identity.repository.extension.suspendedResultToMono
import org.ladhark.identity.service.aspect.AspectService
import org.ladhark.identity.service.aspect.strategy.AspectStrategy
import org.ladhark.identity.service.aspect.strategy.cache.*
import org.ladhark.identity.service.context.withUserContext
import org.reactivestreams.Publisher
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.annotation.Order
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.http.server.reactive.ServerHttpResponse
import org.springframework.http.server.reactive.ServerHttpResponseDecorator
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.ServerWebExchangeDecorator
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.io.ByteArrayOutputStream
import java.nio.channels.Channels
import java.nio.charset.Charset


data class Request(val exchange: ServerWebExchange,
                   val context: MutableMap<String, Any> = mutableMapOf()) {

    companion object {
        // request object instance for handler chain held by this key
        const val REQUEST_FOR_THE_CHAIN = "REQUEST_FOR_THE_CHAIN"
    }

    init {
        context[REQUEST_FOR_THE_CHAIN] = this
    }
}

sealed interface Handler {
    suspend fun preHandle(request: Request, aspect: Aspect): Boolean
    suspend fun postHandle(request: Request, aspect: Aspect)
    fun responseInterceptor(responseBody: String, context: MutableMap<String, Any>)
}

/**
 * Each aspect is mapped to a handler
 */
internal abstract class BaseHandler(): Handler {

    private val log = LoggerFactory.getLogger(BaseHandler::class.java)

    companion object {
        // response is complete no need to go for filter chain ahead
        const val HANDLER_RESPONSE_COMPLETE = "HANDLER_RESPONSE_COMPLETE"
        // we add interceptor to get hold of remote api response
        // this key captures the body
        const val HANDLER_RESPONSE_BODY = "HANDLER_RESPONSE_BODY"
        // filter chain resulted in some error track error instance by this key
        const val SERVER_INTERNAL_ERROR_OBJECT = "SERVER_INTERNAL_ERROR_OBJECT"
    }

    open suspend fun pre(request: Request, aspect: Aspect): Boolean { return true }

    open suspend fun post(request: Request, aspect: Aspect) {}

    override suspend fun preHandle(request: Request, aspect: Aspect): Boolean {

        // pre logic
        return pre(request, aspect)

    }

    override suspend fun postHandle(request: Request, aspect: Aspect) {

        // post logic
        post(request, aspect)
    }

    override fun responseInterceptor(responseBody: String, context: MutableMap<String, Any>) {
        // no op
    }
}

internal class CacheAspectHandler(private val queryParamAspectStrategy: AspectStrategy): BaseHandler() {

    private val log = LoggerFactory.getLogger(CacheAspectHandler::class.java)

    companion object {
        // current resource action object in context map
        const val RESOURCE_ACTION_IN_CONTEXT = "RESOURCE_ACTION_IN_CONTEXT"
        // response from the cache
        const val CACHE_RESPONSE_OBJECT = "CACHE_RESPONSE_OBJECT"
        // cache hit miss push status
        const val CACHE_RESPONSE_OBJECT_STATUS = "CACHE_RESPONSE_OBJECT_STATUS"
    }

    override suspend fun pre(request: Request, aspect: Aspect): Boolean {

        val serverHttpRequest = request.exchange.request
        val context = request.context
        val responseComplete = context[HANDLER_RESPONSE_COMPLETE]

        if (responseComplete != null && responseComplete as Boolean) {
            return true
        }

        val resourceActionFromContext = (context[RESOURCE_ACTION_IN_CONTEXT]
            ?: throw IllegalStateException("Resource action not found in context.")) as ResourceAction

        val cacheResponse = queryParamAspectStrategy.action(
            mapOf(
                CONTEXT_ACTION to CONTEXT_ACTION_PULL,
                CONTEXT_PARAM_QUERY_PARAMS to serverHttpRequest.queryParams
            ),
            resourceActionFromContext,
            aspect
        )

        context[CACHE_RESPONSE_OBJECT_STATUS] = cacheResponse[CONTEXT_STATUS] as Any

        if (cacheResponse[CONTEXT_STATUS] == CONTEXT_ACTION_CACHE_HIT) {
            context[CACHE_RESPONSE_OBJECT] = cacheResponse[CONTEXT_ACTION_CACHED_DATA] as Any
            context[HANDLER_RESPONSE_COMPLETE] = true
        }

        return true
    }

    override suspend fun post(request: Request, aspect: Aspect) {

        val serverHttpRequest = request.exchange.request
        val serverHttpResponse = request.exchange.response
        val context = request.context

        val cacheStatus = context[CACHE_RESPONSE_OBJECT_STATUS]

        val resourceActionFromContext = (context[RESOURCE_ACTION_IN_CONTEXT]
            ?: throw IllegalStateException("Resource action not found in context.")) as ResourceAction

        // if miss add to cache
        // if hit send response body

        if (cacheStatus == CONTEXT_ACTION_CACHE_MISS) {
            val responseBody = context[HANDLER_RESPONSE_BODY]
            responseBody?.let {
                val data = it as String
                queryParamAspectStrategy.action(
                    mapOf(CONTEXT_ACTION to CONTEXT_ACTION_PUSH,
                        CONTEXT_ACTION_CACHE_TO_PUSH to data,
                        CONTEXT_PARAM_QUERY_PARAMS to serverHttpRequest.queryParams
                    ),
                    resourceActionFromContext,
                    aspect
                )
            }
        } else if (cacheStatus == CONTEXT_ACTION_CACHE_HIT) {
            val cache = context[CACHE_RESPONSE_OBJECT] as String
            val bytes: ByteArray = cache.toByteArray(Charset.forName("UTF-8"))
            val buffer = serverHttpResponse.bufferFactory().wrap(bytes)
            serverHttpResponse.writeWith(Flux.just(buffer))
        }
    }

    override fun responseInterceptor(responseBody: String, context: MutableMap<String, Any>) {

        val cacheStatus = context[CACHE_RESPONSE_OBJECT_STATUS]

        if (cacheStatus == CONTEXT_ACTION_CACHE_MISS) {
            context[HANDLER_RESPONSE_BODY] = responseBody
        }
    }

}

@Component
@Order(3)
class ApplicationAspectFilter : WebFilter {

    private val log = LoggerFactory.getLogger(ApplicationAspectFilter::class.java)

    companion object {
        // mapping from aspect to its handler instance
        const val ASPECT_TO_HANDLER_MAP = "ASPECT_TO_HANDLER_MAP"
    }

    @Autowired
    private lateinit var aspectService: AspectService

    @Autowired
    private lateinit var aspectHandlerFactory: AspectHandlerFactory

    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {

        val request = exchange.request
        val response = exchange.response

        return suspendedResultToMono {
            withUserContext {
                resourceAction.orElseThrow {
                    throw IllegalStateException("Resource Action not found")
                }
            }
        }.flatMap { resourceAction ->
            suspendedResultToMono {
                val aspects = aspectService
                    .findByResourceActionUuid(resourceAction.uuid)
                    .toList(mutableListOf())
                    .sortedBy {
                        it.order
                    }

                val aspectHandlerChainMap = aspectHandlerFactory.makeHandlerChain(aspects)

                val aspectChainRequest = Request(exchange,
                    mutableMapOf(
                        CacheAspectHandler.RESOURCE_ACTION_IN_CONTEXT to resourceAction,
                        ASPECT_TO_HANDLER_MAP to aspectHandlerChainMap
                    )
                )

                for (aspectHandlerEntry in aspectHandlerChainMap.entries) {
                    val aspect = aspectHandlerEntry.key
                    val aspectHandler = aspectHandlerEntry.value
                    if (!aspectHandler.preHandle(aspectChainRequest, aspect)) {
                        break
                    }
                }

                aspectChainRequest

            }.flatMap { aspectChainRequest ->

                val responseComplete = (aspectChainRequest
                    .context[BaseHandler.HANDLER_RESPONSE_COMPLETE] ?: false) as Boolean

                if (responseComplete) {
                    response.setComplete()
                } else {

                    val context = aspectChainRequest.context

                    val exchangeDecorator = object : ServerWebExchangeDecorator(exchange) {
                        override fun getResponse(): ServerHttpResponse {
                            return ApplicationResponseBodyInterceptor(super.getResponse(), context)
                        }
                    }

                    chain
                        .filter(exchangeDecorator)
                        .doOnSuccess {

                        }.doOnError {
                            context[BaseHandler.SERVER_INTERNAL_ERROR_OBJECT] = it
                        }.flatMap {
                            suspendedResultToMono {
                                AspectHandlerFactory.doSuspendedInReversedAspectHandlerChain(context) { handler, aspect ->
                                    handler.postHandle(aspectChainRequest, aspect)
                                }
                                it
                            }
                        }
                }
            }

        }
    }


}

@Component
class AspectHandlerFactory {

    private val log = LoggerFactory.getLogger(AspectHandlerFactory::class.java)

    @Autowired
    private lateinit var queryParamAspectStrategy: QueryParamAspectStrategy

    private val handlerInstanceMap = mutableMapOf<String, Handler>()

    @PostConstruct
    private fun postConstruct() {
        handlerInstanceMap[CACHE_BY_QUERY_PARAMS] = CacheAspectHandler(queryParamAspectStrategy)
    }

    suspend fun makeHandlerChain(aspects: List<Aspect>): LinkedHashMap<Aspect, Handler> {

        val chain = LinkedHashMap<Aspect, Handler>()

        aspects.forEach { aspect ->

            when(val name = aspect.type.name) {

                CACHE_BY_QUERY_PARAMS -> {
                    val handler = getHandlerInstance(CACHE_BY_QUERY_PARAMS)
                    chain[aspect] = handler
                }

                else -> {
                    throw IllegalArgumentException("No handler for $name")
                }
            }
        }

        return chain
    }

    private fun getHandlerInstance(key: String): Handler {
        return handlerInstanceMap[key] ?: throw IllegalStateException("No handler instance found.")
    }



    companion object {

        suspend fun doSuspendedInReversedAspectHandlerChain(context: Map<String, Any>,
                                                            block: suspend (handler: Handler, aspect: Aspect) -> Unit) {
            val aspectChainMap = aspectChainMap(context)
            val aspectChainKeysReversed = aspectChainKeysReversed(aspectChainMap)

            aspectChainKeysReversed.forEach { aspect ->
                val handler = aspectChainMap[aspect] ?: throw IllegalStateException("No handler instance found.")
                block(handler, aspect)
            }
        }

        fun doInReversedAspectHandlerChain(context: Map<String, Any>,
                                           block: (handler: Handler, aspect: Aspect) -> Unit) {
            val aspectChainMap = aspectChainMap(context)
            val aspectChainKeysReversed = aspectChainKeysReversed(aspectChainMap)

            aspectChainKeysReversed.forEach { aspect ->
                val handler = aspectChainMap[aspect] ?: throw IllegalStateException("No handler instance found.")
                block(handler, aspect)
            }
        }

        @Suppress("UNCHECKED_CAST")
        private fun aspectChainMap(context: Map<String, Any>): LinkedHashMap<Aspect, Handler> {
            return context[ApplicationAspectFilter.ASPECT_TO_HANDLER_MAP] as LinkedHashMap<Aspect, Handler>
        }

        private fun aspectChainKeysReversed(aspectChainMap: LinkedHashMap<Aspect, Handler>): List<Aspect> {
            return aspectChainMap.keys.reversed()
        }
    }
}


class ApplicationResponseBodyInterceptor(delegate: ServerHttpResponse, private val context: MutableMap<String, Any>) :
                     ServerHttpResponseDecorator(delegate) {

    private val log = LoggerFactory.getLogger(ApplicationResponseBodyInterceptor::class.java)

    override fun writeWith(body: Publisher<out DataBuffer>): Mono<Void> {

        val buffer: Flux<DataBuffer?> = Flux.from<DataBuffer?>(body)

        buffer.doOnNext { dataBuffer ->
            ByteArrayOutputStream().use { byteArray ->
                dataBuffer?.let {
                    Channels.newChannel(byteArray).write(it.toByteBuffer())
                    val responseBody = String(byteArray.toByteArray(), Charset.forName("UTF-8"))

                    AspectHandlerFactory.doInReversedAspectHandlerChain(context) { handler, _ ->
                        handler.responseInterceptor(responseBody, context)
                    }

                }
            }
        }

        return super.writeWith(buffer)
    }

}