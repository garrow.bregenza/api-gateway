package org.ladhark.identity.security.filter

import org.ladhark.identity.api.resource.ResourceActionProcessor
import org.ladhark.identity.exception.AuthorityEvaluationFailedException
import org.ladhark.identity.repository.extension.suspendedResultToMono
import org.ladhark.identity.service.context.withUserContext
import org.ladhark.identity.service.resource.ResourceService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.http.server.reactive.ServerHttpResponse
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono
import java.util.*

@Component
@Order(2)
class ResourcePolicyLoaderFilter : WebFilter {

    private val log = LoggerFactory.getLogger(ResourcePolicyLoaderFilter::class.java)

    @Autowired
    lateinit var resourceService: ResourceService

    @Autowired
    lateinit var resourceActionProcessor: ResourceActionProcessor

    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {
        val request: ServerHttpRequest = exchange.request
        val response: ServerHttpResponse = exchange.response

        val method = request.method
        val path = exchange.request.uri.path

        log.info("Processing URI {} with method {}", path, method)

        return suspendedResultToMono {
            withUserContext {
                var pass = true
                if (authorize) {
                    try {
                        val resourceActionOptional = resourceAction

                        if (resourceActionOptional.isEmpty) {
                            throw AuthorityEvaluationFailedException("Authority evaluation failed.")
                        }

                        val resourcePolicyOption = resourcePolicy

                        if (resourcePolicyOption.isEmpty) {
                            throw AuthorityEvaluationFailedException("Authority evaluation failed.")
                        }

                        val resourcePolicy = resourcePolicyOption.get()

                        val canAccess = resourceService.canAccessResource(resourcePolicy)

                        if (!canAccess) {
                            throw AuthorityEvaluationFailedException("Access denied.")
                        }

                        val applicationOptional = application

                        if (applicationOptional.isEmpty) {
                            throw AuthorityEvaluationFailedException("No service.")
                        }
                    } catch (e: Exception) {
                        log.error("Problem in request processing.", e)
                        pass = false
                    }
                }

                pass
            }
        }.flatMap { pass ->
            if (!pass) {
                with(response) {
                    statusCode = HttpStatus.UNAUTHORIZED
                    setComplete()
                }
            }
            chain.filter(exchange)
        }
    }

}