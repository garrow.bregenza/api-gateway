import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.http.server.PathContainer
import org.springframework.web.util.pattern.PathPatternParser

class PathPatternTest {

    @Test
    fun `it should match path matching a remote host`() {
        val path = PathPatternParser.defaultInstance.parse("/httpbin/**")
        val result = path.matches(PathContainer.parsePath("/httpbin/ball/product/1"))
        Assertions.assertThat(result).isTrue
    }

    @Test
    fun `it should not match path matching a remote host`() {
        val path = PathPatternParser.defaultInstance.parse("/somebin/**")
        val result = path.matches(PathContainer.parsePath("/httpbin/ball/product/1"))
        Assertions.assertThat(result).isFalse
    }

    @Test
    fun `it should extract path matching a remote host`() {
        val path = PathPatternParser.defaultInstance.parse("/httpbin/**")
        val extracted = path.extractPathWithinPattern(PathContainer.parsePath("/httpbin/path?abc.html?q=something"))
        Assertions.assertThat(extracted).isEqualTo(PathContainer.parsePath("path?abc.html?q=something"))
    }

    @Test
    fun `it should match path matching a self host`() {
        val path = PathPatternParser.defaultInstance.parse("/**")
        val result = path.matches(PathContainer.parsePath("/ball/product/1"))
        Assertions.assertThat(result).isTrue
    }

    @Test
    fun `it should not match path matching a self host`() {
        val path = PathPatternParser.defaultInstance.parse("/somebin/**")
        val result = path.matches(PathContainer.parsePath("/ball/product/1"))
        Assertions.assertThat(result).isFalse
    }

    @Test
    fun `it should extract path matching a self host`() {
        val path = PathPatternParser.defaultInstance.parse("/**")
        val extracted = path.extractPathWithinPattern(PathContainer.parsePath("/path?abc.html?q=something"))
        Assertions.assertThat(extracted).isEqualTo(PathContainer.parsePath("path?abc.html?q=something"))
    }

}