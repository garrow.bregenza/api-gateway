package org.ladhark.identity.policy.context

import org.ladhark.identity.policy.context.model.Client
import org.ladhark.identity.policy.context.model.HTTP
import org.ladhark.identity.policy.context.model.User

interface Context<T> {
    fun get(): T
    fun set(context: T)
}

class UserContext : Context<User> {
    lateinit var context: User

    override fun get(): User {
        return this.context
    }

    override fun set(context: User) {
        this.context = context
    }
}

class EnvironmentContext : Context<HTTP> {
    lateinit var context: HTTP

    override fun get(): HTTP {
        return this.context
    }

    override fun set(context: HTTP) {
        this.context = context
    }
}

class ClientContext : Context<Client> {
    lateinit var context: Client

    override fun get(): Client {
        return this.context
    }

    override fun set(context: Client) {
        this.context = context
    }
}