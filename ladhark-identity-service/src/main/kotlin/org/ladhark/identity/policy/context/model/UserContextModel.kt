package org.ladhark.identity.policy.context.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.ladhark.identity.entity.user.User
import org.ladhark.identity.repository.model.role.InferredAuthorities
import org.ladhark.identity.repository.model.role.RoleAuthorityResult
import org.ladhark.identity.repository.model.team.MappedInheritedTeam
import org.ladhark.identity.security.LadharkAuthority
import java.util.*

data class User(
    private val uuid: UUID,
    val email: String,
    val name: Name = Name(),
    val regions: Set<Region> = emptySet(),
    val selfTeam: Team? = null, // TODO can we avoid null here
    val exclusiveTeam: Team? = null, // TODO can we avoid null here
    val exclusiveRole: Role? = null,
    val exclusiveAuthorities: Set<Authority> = emptySet(), // from exclusive role
    val rolesInRegion: Set<RoleInRegion> = emptySet(), // direct roles attached to user
    val teamsInRegion: Set<TeamInRegion> = emptySet()
) : Principal { // has roles coming from team

    private fun allAuthorities(): List<Pair<String, Boolean>> {
        val exclusiveAuthorities = exclusiveAuthorities.flatMap { it.authorities() }
        val userAuthorities =
            rolesInRegion.flatMap { roleInRegion -> roleInRegion.role.authorities.flatMap { it.authorities() } }
        val teamAuthorities =
            teamsInRegion.flatMap { teamInRegion -> teamInRegion.rolesInRegion.flatMap { roleInRegion -> roleInRegion.role.authorities.flatMap { it.authorities() } } }

        return exclusiveAuthorities + userAuthorities + teamAuthorities
    }

    override fun getAuthorities(): Collection<LadharkAuthority> {
        return this.allAuthorities().map { LadharkAuthority(it.first, it.second) }
    }

    override fun getPrincipalName(): String {
        return name.fullName
    }

    override fun getUuid(): UUID {
        return uuid
    }

    override fun getPrincipalEmail(): String {
        return email
    }

    @JsonIgnore
    override fun isValid(): Boolean {
        TODO("Not yet implemented")
    }

    @JsonIgnore
    override fun getType(): Class<out Principal> {
        return this::class.java
    }

    companion object {
        @JvmStatic
        fun fromUserEntityModel(
            userEntityModel: User,
            roleAuthorityResultSet: Set<RoleAuthorityResult>,
            mappedTeam: Set<MappedInheritedTeam>,
            inferredUserAuthorities: Set<InferredAuthorities>,
            inferredTeamAuthorities: Set<InferredAuthorities>
        ): org.ladhark.identity.policy.context.model.User {

            val exclusiveAuthorities = roleAuthorityResultSet.filter { r -> r.authority != null }
                .map { RoleModel.mapToAuthority(it.authority!!, it.implies) }.toSet()

            val exclusiveRole = userEntityModel.exclusiveRole?.let { Role.fromRoleEntityModel(it) }

            val rolesInRegion = getRolesInRegion(inferredUserAuthorities.toList())

            val selfTeam = Team.fromTeamEntityModel(userEntityModel.selfTeam)

            val exclusiveTeam = Team.fromTeamEntityModel(userEntityModel.exclusiveTeam)

            // loop over each teams inferred authorities and convert to context model
            val teamsInRegion = getTeamsInRegion(mappedTeam, inferredTeamAuthorities)

            val regions = userEntityModel.hasRegions.map { re -> Region.fromRegionEntityModel(re.region) }.toSet()

            return User(
                userEntityModel.uuid,
                userEntityModel.email,
                Name.fromNameEntityModel(userEntityModel.name),
                regions,
                selfTeam = selfTeam,
                exclusiveTeam = exclusiveTeam,
                exclusiveRole = exclusiveRole,
                exclusiveAuthorities = exclusiveAuthorities,
                rolesInRegion = rolesInRegion,
                teamsInRegion = teamsInRegion
            )
        }

        private fun getTeamsInRegion(
            mappedTeam: Set<MappedInheritedTeam>,
            inferredTeamAuthorities: Set<InferredAuthorities>
        ): Set<TeamInRegion> {
            val mapOfInferredAuthoritiesByTeamUUID =
                inferredTeamAuthorities.groupBy { TeamRecord(it.source.uuid, it.region.uuid) }
            val mapOfTeam = mappedTeam.groupBy { TeamRecord(it.team.uuid, it.region.uuid) }

            // loop over each teams inferred authorities and convert to context model
            return mapOfInferredAuthoritiesByTeamUUID.map {
                val teamRecord = it.key
                val teamInferredAuthorities = it.value

                // convert role authority belonging to current iteration team into context model
                val rolesInRegion = getRolesInRegion(teamInferredAuthorities)

                // now we have role and its authorities converted into context model
                // we can construct TeamInRegion context model
                val teamInfoList = mapOfTeam[teamRecord]

                val teamInfo = teamInfoList!![0]

                val team = Team.fromTeamEntityModel(teamInfo.team)
                val region = Region.fromRegionEntityModel(teamInfo.region)
                val inheritedRegion = teamInfo.inheritedRegion

                TeamInRegion(Team(team.uuid, team.name, team.parent), region, inheritedRegion, rolesInRegion)
            }.toSet()
        }

        private fun getRolesInRegion(teamInferredAuthorities: List<InferredAuthorities>): Set<RoleInRegion> {
            val listOfTupleRoleRecordAndAuthority = convertInferredAuthoritiesToContextModel(teamInferredAuthorities)
            return listOfTupleRoleRecordAndAuthority.map { tuple ->
                val roleRecord = tuple[0] as RoleRecord
                val roleAuthorities = tuple[1] as Set<Authority>
                val roleRegionEntity = tuple[2] as org.ladhark.identity.entity.region.Region
                val region = Region.fromRegionEntityModel(roleRegionEntity)

                RoleInRegion(Role(roleRecord.uuid, roleRecord.name, roleAuthorities), region)
            }.toSet()
        }

        // we have a map of team/user and its inferred authorities
        // inferred authorities has list of each role and authorities it has
        // role1, school_create, [school_read]
        // role1, school_delete, [school_read]
        // role2, school_read, []
        // aim is to collect inferred authorities for each role
        // once you have (role, inferred authorities) map we need to convert that to context model
        // we still have (role, inferred authorities) in entity model form
        private fun convertInferredAuthoritiesToContextModel(teamInferredAuthorities: List<InferredAuthorities>): List<List<Any>> {
            // breaking list of inferred authorities belonging to this team mapped by role and list of inferred authorities it has
            val mapOfRoleRecordAndInferredAuthorities = teamInferredAuthorities.groupBy { ia ->
                RoleRecord(ia.role.uuid, ia.role.name, ia.region.uuid)
            }
            val regionMap = teamInferredAuthorities.map { a -> a.region }.groupBy { it.uuid }
            // role record inferred authorities gets converted into context authority
            return mapOfRoleRecordAndInferredAuthorities.map { r ->
                val roleRecord = r.key
                val roleRecordInferredAuthorities = r.value
                val roleAuthorities = roleRecordInferredAuthorities
                    .filter { inferredAuthorities -> inferredAuthorities.authority != null }
                    .map { inferredAuthorities ->
                        RoleModel.mapToAuthority(inferredAuthorities.authority!!, inferredAuthorities.implies)
                    }.toSet()
                listOf(
                    roleRecord,
                    roleAuthorities,
                    regionMap[roleRecord.regionUUID]?.get(0) ?: error("Region not found for ${roleRecord.uuid}")
                )
            }
        }
    }
}

data class Name(val first: String = "", val middle: String = "", val last: String = "", val fullName: String = "") {
    companion object {
        @JvmStatic
        fun fromNameEntityModel(nameEntityModel: org.ladhark.identity.entity.user.Name): Name {
            return Name(nameEntityModel.first, nameEntityModel.middle, nameEntityModel.last, nameEntityModel.fullName())
        }
    }
}

data class TeamInRegion(
    val team: Team,
    val inRegion: Region,
    val inheritedRegion: Boolean = false,
    val rolesInRegion: Set<RoleInRegion> = mutableSetOf()
)

data class Region(val uuid: UUID, val name: String, val parent: Region? = null) {
    companion object {
        @JvmStatic
        fun fromRegionEntityModel(regionEntityModel: org.ladhark.identity.entity.region.Region): Region {
            val parent = regionEntityModel.parentRegion
            var parentContextModel: Region? = null
            if (parent != null) {
                parentContextModel = fromRegionEntityModel(parent)
            }
            return Region(regionEntityModel.uuid, regionEntityModel.name, parentContextModel)
        }
    }
}

data class Team(val uuid: UUID, val name: String, val parent: Team? = null) {

    companion object {
        @JvmStatic
        fun fromTeamEntityModel(teamEntityModel: org.ladhark.identity.entity.team.Team): Team {
            val parent = teamEntityModel.parentTeam
            var parentContextModel: Team? = null
            if (parent != null) {
                parentContextModel = fromTeamEntityModel(parent)
            }
            return Team(teamEntityModel.uuid, teamEntityModel.name, parentContextModel)
        }
    }
}

data class RoleInRegion(val role: Role, val inRegion: Region)

internal data class RoleRecord(val uuid: UUID, val name: String, val regionUUID: UUID)
internal data class TeamRecord(val uuid: UUID, val regionUUID: UUID)