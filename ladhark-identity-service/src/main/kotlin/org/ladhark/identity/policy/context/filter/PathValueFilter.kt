package org.ladhark.identity.policy.context.filter

import org.ladhark.identity.policy.context.UserContext
import org.ladhark.identity.policy.context.model.Region
import org.ladhark.identity.policy.context.model.Role
import org.ladhark.identity.policy.context.model.RoleInRegion
import org.ladhark.identity.policy.context.model.TeamInRegion
import org.ladhark.identity.policy.context.token.PolicyRestrictionToken
import org.ladhark.kotlin.extension.s
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

/**
 * Filter predicate is present it will filter else return collection as it is.
 */
object FilterUtil {

    fun filterUserTeamsByTeam(team: List<String>, userTeam: Collection<TeamInRegion>): Collection<TeamInRegion> {
        return if (team.isNotEmpty()) {
            userTeam.filter { v -> team.contains(v.team.uuid.s) || team.contains(v.team.name) }
        } else {
            userTeam
        }
    }

    fun filterUserTeamsByRegion(region: List<String>, teamsInRegion: Set<TeamInRegion>): Collection<TeamInRegion> {
        return if (region.isNotEmpty()) {
            teamsInRegion.filter { v -> region.contains(v.inRegion.uuid.s) || region.contains(v.inRegion.name) }
        } else {
            teamsInRegion
        }
    }

    fun filterUserRolesByRegion(region: List<String>, rolesInRegion: Set<RoleInRegion>): Collection<RoleInRegion> {
        return if (region.isNotEmpty()) {
            rolesInRegion.filter { v -> region.contains(v.inRegion.uuid.s) || region.contains(v.inRegion.name) }
        } else {
            rolesInRegion
        }
    }

    fun filterRegionForUser(region: List<String>, regions: Set<Region>): Collection<Region> {
        return if (region.isNotEmpty()) {
            regions.filter { v -> region.contains(v.uuid.s) || region.contains(v.name) }
        } else {
            regions
        }
    }
}

interface PathValueFilter<C, R> {
    // key is list some will have more than one values to compare
    fun filter(token: PolicyRestrictionToken, context: C): Collection<R>
}

/**
 * User has two regional context, User can be assigned to more than one region.
 * But he will have roles in all of them is not guaranteed, which is mapped by other property.
 *
 * So User region context is as follows,
 * User can have access to many regions (one property)
 * User has roles in those regions (another property)
 *
 * Below targets User regional roles
 */
@Component
//region:user:role
class UserRegionalRolePathFilter : PathValueFilter<UserContext, RoleInRegion> {

    override fun filter(token: PolicyRestrictionToken, context: UserContext): Collection<RoleInRegion> {
        val user = context.get()
        return FilterUtil.filterUserRolesByRegion(token.regions, user.rolesInRegion)
    }

}

/**
 * User has two regional context, User can be assigned to more than one region.
 * But he will have roles in all of them is not guaranteed, which is mapped by other property.
 *
 * So User region context is as follows,
 * User can have access to many regions (one property)
 * User has roles in those regions (another property)
 *
 * Below targets User regions he has access to
 */
@Component
//region:user
class UserRegionPathFilter : PathValueFilter<UserContext, Region> {

    override fun filter(token: PolicyRestrictionToken, context: UserContext): Collection<Region> {
        val user = context.get()
        return FilterUtil.filterRegionForUser(token.regions, user.regions)
    }

}


@Component
//region:team
class TeamRegionPathFilter : PathValueFilter<UserContext, TeamInRegion> {

    override fun filter(token: PolicyRestrictionToken, context: UserContext): Collection<TeamInRegion> {
        val user = context.get()
        return FilterUtil.filterUserTeamsByRegion(token.regions, user.teamsInRegion)
    }

}

@Component
//region:team OR user:role
class RolePathFilter : PathValueFilter<UserContext, Role> {

    @Autowired
    lateinit var teamRegionPathExtractor: TeamRegionPathFilter

    @Autowired
    lateinit var userRegionalRolePathExtractor: UserRegionalRolePathFilter

    override fun filter(token: PolicyRestrictionToken, context: UserContext): Collection<Role> {
        val user = context.get()

        val rolesToProbe = token.roles

        val exclusiveRole = ArrayList<Role>()

        // no point in being exclusive if is filtered by criteria hence first in logic
        if (user.exclusiveRole != null && (rolesToProbe.contains(user.exclusiveRole.uuid.s) || rolesToProbe.contains(
                user.exclusiveRole.name
            ))
        )
            exclusiveRole.add(user.exclusiveRole)

        // if team tokens are present we don't need roles coming from user
        val userRoles = if (token.teams.isNotEmpty())
            emptyList()
        else
            userRegionalRolePathExtractor.filter(token, context)
                .map { it.role }


        // if user tokens are present we don't need roles coming from team
        val userRolesFromTeam = if (token.users.isNotEmpty())
            emptyList()
        else {
            val userTeam = FilterUtil.filterUserTeamsByTeam(
                token.teams,
                teamRegionPathExtractor.filter(token, context)
            )

            userTeam.flatMap { v -> v.rolesInRegion }
                .map { it.role }
        }

        return exclusiveRole + userRoles + userRolesFromTeam
    }

}
