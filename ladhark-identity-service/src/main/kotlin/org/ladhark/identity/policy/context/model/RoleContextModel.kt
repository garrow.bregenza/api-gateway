package org.ladhark.identity.policy.context.model

import java.util.*

data class Role(val uuid: UUID, val name: String, val authorities: Set<Authority> = emptySet()) {
    companion object {
        @JvmStatic
        fun fromRoleEntityModel(role: org.ladhark.identity.entity.role.Role): Role {
            return Role(role.uuid, role.name, role.authorities.map { Authority.fromAuthorityEntityModel(it) }.toSet())
        }
    }
}

object RoleModel {

    fun mapToAuthority(
        authority: org.ladhark.identity.entity.authority.Authority,
        implies: Set<org.ladhark.identity.entity.authority.Authority>
    ): Authority {
        val i = implies.map { im -> Authority.fromAuthorityEntityModel(im) }.toSet()
        return Authority(authority.uuid, authority.name, i)
    }
}

data class Authority(val uuid: UUID, val name: String, val implies: Set<Authority> = emptySet()) {

    fun authorities(): Set<Pair<String, Boolean>> {
        val set = HashSet<Pair<String, Boolean>>()
        collectAuthorities(set, false)
        return set
    }

    private fun collectAuthorities(set: HashSet<Pair<String, Boolean>>, implies: Boolean) {
        set.add(Pair(this.name, implies))
        this.implies.forEach { it.collectAuthorities(set, true) }
    }

    companion object {
        @JvmStatic
        fun fromAuthorityEntityModel(authority: org.ladhark.identity.entity.authority.Authority): Authority {
            return Authority(
                authority.uuid,
                authority.name,
                authority.implies.map { fromAuthorityEntityModel(it) }.toSet()
            )
        }
    }
}