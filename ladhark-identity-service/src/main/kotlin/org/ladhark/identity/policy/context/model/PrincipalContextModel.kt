package org.ladhark.identity.policy.context.model

import org.ladhark.identity.security.LadharkAuthority
import java.util.*

interface Principal {
    fun getAuthorities(): Collection<LadharkAuthority>
    fun getPrincipalName(): String
    fun getPrincipalEmail(): String?
    fun isValid(): Boolean
    fun getUuid(): UUID
    fun getType(): Class<out Principal>
}