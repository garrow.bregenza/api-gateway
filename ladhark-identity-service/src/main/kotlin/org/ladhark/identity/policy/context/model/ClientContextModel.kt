package org.ladhark.identity.policy.context.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.ladhark.identity.entity.client.Client
import org.ladhark.identity.repository.model.role.RoleAuthorityResult
import org.ladhark.identity.security.LadharkAuthority
import java.util.*

data class Client(private val uuid: UUID, val name: String, val email: String, val roles: Set<Role>) : Principal {

    override fun getAuthorities(): Collection<LadharkAuthority> {
        return this.roles.flatMap { r -> r.authorities }.flatMap { it.authorities() }
            .map { LadharkAuthority(it.first, it.second) }
    }

    override fun getPrincipalName(): String {
        return name
    }

    override fun getUuid(): UUID {
        return uuid
    }

    override fun getPrincipalEmail(): String {
        return email
    }

    @JsonIgnore
    override fun isValid(): Boolean {
        TODO("Not yet implemented")
    }

    @JsonIgnore
    override fun getType(): Class<out Principal> {
        return this::class.java
    }

    companion object {
        @JvmStatic
        fun fromClientEntityModel(
            clientEntityModel: Client,
            roleAuthorities: Set<RoleAuthorityResult>
        ): org.ladhark.identity.policy.context.model.Client {

            val roleMap = roleAuthorities.groupBy { it.role }

            val roles = roleMap.map { it ->

                val role = it.key
                val authorities = it.value

                Role(
                    role.uuid,
                    role.name,
                    authorities.filter { r -> r.authority != null }
                        .map { r -> RoleModel.mapToAuthority(r.authority!!, r.implies) }.toSet()
                )
            }.toSet()

            return Client(clientEntityModel.uuid, clientEntityModel.name, clientEntityModel.email, roles)
        }
    }

}