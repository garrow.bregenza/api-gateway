package org.ladhark.identity.policy.context.extractor

import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.exception.InvalidStateException
import org.ladhark.identity.policy.context.ClientContext
import org.ladhark.identity.policy.context.EnvironmentContext
import org.ladhark.identity.policy.context.UserContext
import org.ladhark.identity.policy.context.filter.RolePathFilter
import org.ladhark.identity.policy.context.filter.TeamRegionPathFilter
import org.ladhark.identity.policy.context.filter.UserRegionPathFilter
import org.ladhark.identity.policy.context.model.*
import org.ladhark.identity.policy.context.token.PolicyRestrictionToken
import org.ladhark.identity.policy.context.token.PolicyRestrictionTokenParser
import org.ladhark.kotlin.extension.s
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*


interface ValueExtractor<C, R> {
    // key is list some will have more than one values to compare
    fun extract(key: List<String>, context: C): Optional<R>
}

@Component
class IPValueExtractor : ValueExtractor<EnvironmentContext, String> {

    override fun extract(key: List<String>, context: EnvironmentContext): Optional<String> {
        val http = context.get()
        return if (key.contains(http.ip.address)) Optional.of(http.ip.address) else
            Optional.empty()
    }

}

@Component
//client
class ClientUUIDExtractor : ValueExtractor<ClientContext, Client> {

    override fun extract(key: List<String>, context: ClientContext): Optional<Client> {
        val client = context.get()
        return if (key.contains(client.getUuid().s) || key.contains(client.name)) Optional.of(client) else
            Optional.empty()
    }

}

@Component
//client role
class ClientRoleExtractor : ValueExtractor<ClientContext, Role> {

    @Autowired
    lateinit var policyRestrictionTokenParser: PolicyRestrictionTokenParser

    override fun extract(key: List<String>, context: ClientContext): Optional<Role> {

        val policyRestrictionToken = if (key.isNotEmpty() && key.first().contains(":")) {
            policyRestrictionTokenParser.parse(key.first())
        } else {
            Optional.of(PolicyRestrictionToken(users = key))
        }

        if (policyRestrictionToken.isPresent) {
            val token = policyRestrictionToken.get()
            val rolesToProbe = token.roles

            val client = context.get()

            val matchedRole = client.roles.find { r ->
                rolesToProbe.contains(r.name) || rolesToProbe.contains(r.uuid.s)
            }

            if (matchedRole != null) {
                return Optional.of(matchedRole)
            }
        }

        return Optional.empty()

    }

}

//region
@Component
class RegionExtractor : ValueExtractor<UserContext, Region> {

    @Autowired
    lateinit var policyRestrictionTokenParser: PolicyRestrictionTokenParser

    override fun extract(key: List<String>, context: UserContext): Optional<Region> {

        val policyRestrictionToken = if (key.isNotEmpty() && key.first().contains(":")) {
            policyRestrictionTokenParser.parse(key.first())
        } else {
            Optional.of(PolicyRestrictionToken(regions = key))
        }

        if (policyRestrictionToken.isPresent) {
            val token = policyRestrictionToken.get()
            val regionsToProbe = token.regions

            val user = context.get()

            val matchedRegion = user.regions.find { r ->
                regionsToProbe.contains(r.name) || regionsToProbe.contains(r.uuid.s)
            }

            if (matchedRegion != null) {
                return Optional.of(matchedRegion)
            }

        }

        return Optional.empty()
    }

}

@Component
//region:user
class UserExtractor : ValueExtractor<UserContext, User> {

    @Autowired
    lateinit var policyRestrictionTokenParser: PolicyRestrictionTokenParser

    @Autowired
    lateinit var userRegionPathFilter: UserRegionPathFilter

    override fun extract(key: List<String>, context: UserContext): Optional<User> {

        val policyRestrictionToken = if (key.isNotEmpty() && key.first().contains(":")) {
            policyRestrictionTokenParser.parse(key.first())
        } else {
            Optional.of(PolicyRestrictionToken(users = key))
        }

        if (policyRestrictionToken.isPresent) {
            val token = policyRestrictionToken.get()
            val user = context.get()

            val filteredRegions = userRegionPathFilter.filter(token, context)

            if (filteredRegions.isEmpty()) {
                return Optional.empty()
            }

            val usersToProbe = token.users

            return if (usersToProbe.contains(user.email) || usersToProbe.contains(user.getUuid().s)) Optional.of(user) else
                Optional.empty()
        }

        return Optional.empty()
    }

}

@Component
//region:team
class TeamExtractor : ValueExtractor<UserContext, Team> {

    @Autowired
    lateinit var policyRestrictionTokenParser: PolicyRestrictionTokenParser

    @Autowired
    lateinit var teamRegionPathFilter: TeamRegionPathFilter

    override fun extract(key: List<String>, context: UserContext): Optional<Team> {

        val policyRestrictionToken = if (key.isNotEmpty() && key.first().contains(":")) {
            policyRestrictionTokenParser.parse(key.first())
        } else {
            Optional.of(PolicyRestrictionToken(teams = key))
        }

        if (policyRestrictionToken.isPresent) {
            val token = policyRestrictionToken.get()
            val user = context.get()

            val teamsToProbe = token.teams

            if (user.selfTeam != null && (teamsToProbe.contains(user.selfTeam.uuid.s) || teamsToProbe.contains(user.selfTeam.name)))
                return Optional.of(user.selfTeam)
            if (user.exclusiveTeam != null && (teamsToProbe.contains(user.exclusiveTeam.uuid.s) || teamsToProbe.contains(
                    user.exclusiveTeam.name
                ))
            )
                return Optional.of(user.exclusiveTeam)

            val userTeamsInRegion = teamRegionPathFilter.filter(token, context)

            val matchedTeam =
                userTeamsInRegion.find { v -> teamsToProbe.contains(v.team.uuid.s) || teamsToProbe.contains(v.team.name) }

            if (matchedTeam != null) {
                return Optional.of(matchedTeam.team)
            }

        }

        return Optional.empty()
    }
}

//region:team OR user:role
@Component
class RoleExtractor : ValueExtractor<UserContext, Role> {

    @Autowired
    lateinit var policyRestrictionTokenParser: PolicyRestrictionTokenParser

    @Autowired
    lateinit var rolePathFilter: RolePathFilter

    override fun extract(key: List<String>, context: UserContext): Optional<Role> {

        val policyRestrictionToken = if (key.isNotEmpty() && key.first().contains(":")) {
            policyRestrictionTokenParser.parse(key.first())
        } else {
            Optional.of(PolicyRestrictionToken(roles = key))
        }

        // if team present role should come from team
        // if user present role should come from user
        // if nothing role can come from both user or team
        if (policyRestrictionToken.isPresent) {
            val token = policyRestrictionToken.get()
            val user = context.get()

            val rolesToProbe = token.roles

            // path extractor will execute this logic, but to short circuit logic needs to be here
            if (user.exclusiveRole != null && (rolesToProbe.contains(user.exclusiveRole.uuid.s) || rolesToProbe.contains(
                    user.exclusiveRole.name
                ))
            )
                return Optional.of(user.exclusiveRole)

            val allRolesInRegions = rolePathFilter.filter(token, context)

            val matchedRole = allRolesInRegions
                .find { v -> rolesToProbe.contains(v.uuid.s) || rolesToProbe.contains(v.name) }

            if (matchedRole != null) {
                return Optional.of(matchedRole)
            }

        }

        return Optional.empty()
    }

}

@Component
//region:team OR user:role:authority
class AuthorityExtractor : ValueExtractor<UserContext, Authority> {

    @Autowired
    lateinit var policyRestrictionTokenParser: PolicyRestrictionTokenParser

    @Autowired
    lateinit var rolePathExtractor: RolePathFilter

    override fun extract(key: List<String>, context: UserContext): Optional<Authority> {

        val policyRestrictionToken = if (key.isNotEmpty() && key.first().contains(":")) {
            policyRestrictionTokenParser.parse(key.first())
        } else {
            Optional.of(PolicyRestrictionToken(authorities = key))
        }

        if (policyRestrictionToken.isPresent) {

            val token = policyRestrictionToken.get()

            val allRoles = rolePathExtractor.filter(token, context)

            val allAuthorities = allRoles.flatMap { v -> v.authorities }

            val authoritiesToProbe = token.authorities

            return probeAuthorities(allAuthorities, authoritiesToProbe, true)

        }

        return Optional.empty()
    }

    fun probeAuthorities(
        authorities: List<Authority>,
        toMatchAuthorities: List<String>,
        direct: Boolean
    ): Optional<Authority> {
        val allAuthoritiesIterator = authorities.iterator()
        while (allAuthoritiesIterator.hasNext()) {
            val a = allAuthoritiesIterator.next()
            if (toMatchAuthorities.contains(a.uuid.s) || toMatchAuthorities.contains(a.name)) {
                return Optional.of(a)
            }

            val implies = a.implies.toList()
            if (implies.isNotEmpty()) probeAuthorities(implies, toMatchAuthorities, false)
        }

        return Optional.empty()
    }

}

@Component
class ValueExtractorFactory {

    @Autowired
    lateinit var regionExtractor: RegionExtractor

    @Autowired
    lateinit var teamExtractor: TeamExtractor

    @Autowired
    lateinit var userExtractor: UserExtractor

    @Autowired
    lateinit var roleExtractor: RoleExtractor

    @Autowired
    lateinit var authorityExtractor: AuthorityExtractor

    @Autowired
    lateinit var clientRoleExtractor: ClientRoleExtractor

    /**
     * In relation path region -> user or team -> role -> authority.
     * We start from end node, if that is present we use extractor linked to that node.
     *
     * If path has/ends authority we use it, if path has/ends with role we use it, so on.
     */
    fun get(policyRestrictionToken: PolicyRestrictionToken): ValueExtractor<UserContext, out Any> {

        val regionPresent = policyRestrictionToken.regions.isNotEmpty()
        val teamPresent = policyRestrictionToken.teams.isNotEmpty()
        val userPresent = policyRestrictionToken.users.isNotEmpty()
        val rolePresent = policyRestrictionToken.roles.isNotEmpty()
        val authorityPresent = policyRestrictionToken.authorities.isNotEmpty()

        if (teamPresent && userPresent) {
            throw InvalidArgumentException("Cannot evaluate Team and User path value extraction together.")
        }

        return when {
            authorityPresent -> {
                authorityExtractor
            }

            rolePresent -> {
                roleExtractor
            }

            teamPresent -> {
                teamExtractor
            }

            userPresent -> {
                userExtractor
            }

            regionPresent -> {
                regionExtractor
            }

            else -> throw InvalidStateException("Could not locate Extractor from provided PolicyRestrictionToken")
        }

    }

    fun getForClient(policyRestrictionToken: PolicyRestrictionToken): ValueExtractor<ClientContext, out Any> {
        return clientRoleExtractor
    }

}