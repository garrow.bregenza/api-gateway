package org.ladhark.identity.policy.context.model

data class HTTP(val headers: Set<HTTPHeader>, val cookie: Cookie, val ip: IP)

enum class IPVersion { V4, V6 }
data class IP(val address: String, val type: IPVersion)

data class HTTPHeader(val name: String, val values: Set<String>)

data class Cookie(val name: String, val value: String)