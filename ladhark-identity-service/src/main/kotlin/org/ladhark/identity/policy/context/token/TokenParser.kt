package org.ladhark.identity.policy.context.token

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Component
import java.util.*


@Component
class PolicyRestrictionTokenParser {
    private val objectMapper = ObjectMapper()

    fun parse(query: String): Optional<PolicyRestrictionToken> {
        val withQuotes = query.split(";").joinToString(",") { pair ->
            pair.split(":").joinToString(":") { token ->
                if (token.startsWith("[")) token
                else "\"${token}\""
            }
        }
        val json = "{${withQuotes}}"
        val token = objectMapper.readValue(json, PolicyRestrictionToken::class.java)
        return if (token != null) return Optional.of(token) else Optional.empty()
    }
}

data class PolicyRestrictionToken(
    val regions: List<String> = mutableListOf(),
    val teams: List<String> = mutableListOf(),
    val users: List<String> = mutableListOf(),
    val roles: List<String> = mutableListOf(),
    val authorities: List<String> = mutableListOf()
)