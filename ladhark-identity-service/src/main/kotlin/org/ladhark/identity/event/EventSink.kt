package org.ladhark.identity.event

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

@Component
class EventSink {

    private val log: Logger = LoggerFactory.getLogger(EventSink::class.java)

    @Async
    @EventListener
    fun sink(event: BaseEvent<*>) {
        log.info(
            "Captured event: \nCategory: {}  \n Action: {}  \n Summary: {}  \n Payload: {}",
            event.category(),
            event.action(),
            event.summary(),
            event.transferData()
        )
    }
}