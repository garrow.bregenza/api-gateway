package org.ladhark.identity.event

import org.springframework.context.ApplicationEvent
import java.time.Instant
import java.util.*

// May be can filter on category @EventListener(condition = "#blEvent.content == 'my-event'")
abstract class BaseEvent<T>(
    source: Any,
    val payload: T,
    private val category: EventCategory,
    private val action: EventAction
) : ApplicationEvent(source) {

    val id = UUID.randomUUID().toString()

    abstract fun summary(): String

    @Suppress("UNCHECKED_CAST")
    open fun transferData(): Map<String, Any> {
        return mapOf(
            "id" to id,
            "payload" to this.payload,
            "category" to category().type(),
            "action" to action().type(),
            "time" to Instant.now(),
            "summary" to summary()
        ) as Map<String, Any>
    }

    fun category(): EventCategory {
        return category
    }

    fun action(): EventAction {
        return action
    }
}

interface EventCategory {
    fun type(): String
}

interface EventAction {
    fun type(): String
}

enum class ApplicationEventAction : EventAction {
    CREATE {
        override fun type(): String {
            return CREATE.name
        }
    },
    READ {
        override fun type(): String {
            return READ.name
        }
    },
    UPDATE {
        override fun type(): String {
            return UPDATE.name
        }
    },
    DELETE {
        override fun type(): String {
            return DELETE.name
        }
    },
    SUCCESS {
        override fun type(): String {
            return SUCCESS.name
        }
    },
    FAILURE {
        override fun type(): String {
            return FAILURE.name
        }
    },
    CACHE_READ {
        override fun type(): String {
            return CACHE_READ.name
        }
    },
    CACHE_WRITE {
        override fun type(): String {
            return CACHE_WRITE.name
        }
    },
    CACHE_REMOVE {
        override fun type(): String {
            return CACHE_REMOVE.name
        }
    }
}