package org.ladhark.identity.aspect.cache.event

import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class CacheReadEvent(source: Any, val key: String, private val _object: BaseEntity,
                    val contextUser: AuthenticatedUserContextUUID) : BaseEvent<Any>(source, _object,
                    CacheHitEventCategory.CACHE, ApplicationEventAction.CACHE_READ) {
    override fun summary(): String {
        return "Entity with key $key and uuid ${_object.uuid} returned from cache of type ${_object::class.qualifiedName} " +
                " under User $contextUser"
    }
}

class CacheWriteEvent(source: Any, val key: String, private val _object: BaseEntity,
                    val contextUser: AuthenticatedUserContextUUID) : BaseEvent<Any>(source, _object,
                    CacheHitEventCategory.CACHE, ApplicationEventAction.CACHE_WRITE) {
    override fun summary(): String {
        return "Entity with key $key and uuid ${_object.uuid} stored value of type ${_object::class.qualifiedName} into cache" +
                " under User $contextUser"
    }
}

class CacheRemoveEvent(source: Any, val key: String, private val _object: BaseEntity,
                      val contextUser: AuthenticatedUserContextUUID) : BaseEvent<Any>(source, _object,
    CacheHitEventCategory.CACHE, ApplicationEventAction.CACHE_REMOVE) {
    override fun summary(): String {
        return "Entity with key $key and uuid ${_object.uuid} removed value of type ${_object::class.qualifiedName} from cache" +
                " under User $contextUser"
    }
}

enum class CacheHitEventCategory: EventCategory {
    CACHE {
        override fun type(): String {
            return CACHE.name
        }
    }
}