package org.ladhark.identity.aspect.cache

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.ladhark.identity.aspect.cache.event.CacheReadEvent
import org.ladhark.identity.aspect.cache.event.CacheRemoveEvent
import org.ladhark.identity.aspect.cache.event.CacheWriteEvent
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID
import org.ladhark.identity.service.base.CacheRegistry
import org.ladhark.identity.service.context.withUserContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Component
import java.lang.annotation.Inherited
import kotlin.coroutines.*

@Target(AnnotationTarget.TYPE, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class GetFromCache(val name: String)

@Target(AnnotationTarget.TYPE, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
annotation class PutInCache(val name: String)

@Target(AnnotationTarget.TYPE, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
annotation class PutInCaches(val caches: Array<PutInCache>)

@Target(AnnotationTarget.TYPE, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
annotation class RemoveFromCache(val name: String)

@Target(AnnotationTarget.TYPE, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@Inherited
@MustBeDocumented
annotation class RemoveFromCaches(val caches: Array<RemoveFromCache>)

@Aspect
@Component
class CacheAspect {

    val log: Logger = LoggerFactory.getLogger(CacheAspect::class.java)

    @Autowired
    lateinit var applicationEventPublisher: ApplicationEventPublisher

    @Autowired
    lateinit var cacheRegistry: CacheRegistry

    @Around("@annotation(getFromCache)")
    @Throws(Throwable::class)
    fun getFromCache(pjp: ProceedingJoinPoint, getFromCache: GetFromCache): Any? {

        val name = getFromCache.name
        val lookUpKey = pjp.args[0] as String

        log.debug("Cacheable get info -> {} {}", name, lookUpKey)

        val args = pjp.args

        val lastArg = args.lastOrNull()

        if(lastArg == null || lastArg !is Continuation<*>) {
            // it is not a suspended function, just invoke regularly
            return pjp.proceed()
        } else {


            // get the continuation.
            @Suppress("UNCHECKED_CAST")
            val originalContinuation = lastArg as Continuation<Any?>
            val originalContext = originalContinuation.context
            val newContext = Dispatchers.Unconfined + originalContext

            val wrappedContinuation = object : Continuation<Any?> {
                override val context: CoroutineContext get() = originalContinuation.context
                override fun resumeWith(result: Result<Any?>) {
                    if (result.isSuccess) {
                        result.getOrNull()?.let { value ->
                            // put in cache
                            if (value is BaseEntity) {
                                CoroutineScope(originalContext).launch(newContext) {
                                    withUserContext {
                                        cacheRegistry.putInCache(name, lookUpKey, value)
                                        applicationEventPublisher
                                            .publishEvent(CacheWriteEvent(this, lookUpKey, value,
                                                AuthenticatedUserContextUUID(principalUUID())))
                                    }
                                }
                            }
                        }
                    }
                    originalContinuation.resumeWith(result)
                }
            }

            // launch the suspend function with our logic continuation using the underlying scope and context,
            CoroutineScope(originalContext).launch(newContext) {
                    // get from cache
                    withUserContext {
                        val value = cacheRegistry.getFromCache(name, lookUpKey)

                        value?.let {
                            applicationEventPublisher
                                .publishEvent(CacheReadEvent(this, lookUpKey, it,
                                    AuthenticatedUserContextUUID(principalUUID())))
                            originalContinuation.resume(it)
                        }
                    } ?: run {
                        val argumentsWithoutContinuation = args.take(args.size - 1)
                        val newArgs = argumentsWithoutContinuation + wrappedContinuation
                        pjp.proceed(newArgs.toTypedArray())
                    }
            }
            return kotlin.coroutines.intrinsics.COROUTINE_SUSPENDED
        }
    }

    @Around("@annotation(putInCaches)")
    @Throws(Throwable::class)
    fun putInCaches(pjp: ProceedingJoinPoint, putInCaches: PutInCaches): Any? {

        val caches = putInCaches.caches
        val lookUpKey = pjp.args[0] as String

        if(log.isDebugEnabled) {
            caches.forEach {
                val name = it.name
                log.debug("Cacheable put info -> {} {}", name, lookUpKey)
            }
        }

        val args = pjp.args

        val lastArg = args.lastOrNull()

        if(lastArg == null || lastArg !is Continuation<*>) {
            // it is not a suspended function, just invoke regularly
            return pjp.proceed()
        } else {

            // get the continuation.
            @Suppress("UNCHECKED_CAST")
            val originalContinuation = lastArg as Continuation<Any?>
            val originalContext = originalContinuation.context
            val newContext = Dispatchers.Unconfined + originalContext

            val wrappedContinuation = object : Continuation<Any?> {
                override val context: CoroutineContext get() = originalContinuation.context
                override fun resumeWith(result: Result<Any?>) {
                    if (result.isSuccess) {
                        result.getOrNull()?.let { value ->
                            // put in cache
                            if (value is BaseEntity) {
                                CoroutineScope(originalContext).launch(newContext) {
                                    withUserContext {
                                        caches.forEach { putInCache ->
                                            val name = putInCache.name
                                            cacheRegistry.putInCache(name, lookUpKey, value)
                                            applicationEventPublisher
                                                .publishEvent(CacheWriteEvent(this, lookUpKey, value,
                                                    AuthenticatedUserContextUUID(principalUUID())))
                                        }
                                    }
                                }
                            }
                        }
                    }
                    originalContinuation.resumeWith(result)
                }
            }

            // launch the suspend function with our logic continuation using the underlying scope and context,
            CoroutineScope(originalContext).launch(newContext) {
                val argumentsWithoutContinuation = args.take(args.size - 1)
                val newArgs = argumentsWithoutContinuation + wrappedContinuation
                pjp.proceed(newArgs.toTypedArray())
            }
            return kotlin.coroutines.intrinsics.COROUTINE_SUSPENDED
        }
    }

    @Around("@annotation(removeFromCaches)")
    @Throws(Throwable::class)
    fun removeFromCaches(pjp: ProceedingJoinPoint, removeFromCaches: RemoveFromCaches): Any? {

        val caches = removeFromCaches.caches
        val lookUpKey = pjp.args[0] as String

        if(log.isDebugEnabled) {
            caches.forEach {
                val name = it.name
                log.debug("Cacheable remove info -> {} {}", name, lookUpKey)
            }
        }

        val args = pjp.args

        val lastArg = args.lastOrNull()

        if(lastArg == null || lastArg !is Continuation<*>) {
            // it is not a suspended function, just invoke regularly
            return pjp.proceed()
        } else {

            // get the continuation.
            @Suppress("UNCHECKED_CAST")
            val originalContinuation = lastArg as Continuation<Any?>
            val originalContext = originalContinuation.context
            val newContext = Dispatchers.Unconfined + originalContext

            val wrappedContinuation = object : Continuation<Any?> {
                override val context: CoroutineContext get() = originalContinuation.context
                override fun resumeWith(result: Result<Any?>) {
                    if (result.isSuccess) {
                        result.getOrNull()?.let { value ->
                            // put in cache
                            if (value is BaseEntity) {
                                CoroutineScope(originalContext).launch(newContext) {
                                    withUserContext {
                                        caches.forEach { removeFromCache ->
                                            val name = removeFromCache.name
                                            cacheRegistry.removeFromCache(name, lookUpKey)
                                            applicationEventPublisher
                                                .publishEvent(CacheRemoveEvent(this, lookUpKey, value,
                                                    AuthenticatedUserContextUUID(principalUUID())))
                                        }
                                    }
                                }
                            }
                        }
                    }
                    originalContinuation.resumeWith(result)
                }
            }

            // launch the suspend function with our logic continuation using the underlying scope and context,
            CoroutineScope(originalContext).launch(newContext) {
                val argumentsWithoutContinuation = args.take(args.size - 1)
                val newArgs = argumentsWithoutContinuation + wrappedContinuation
                pjp.proceed(newArgs.toTypedArray())
            }
            return kotlin.coroutines.intrinsics.COROUTINE_SUSPENDED
        }
    }
}