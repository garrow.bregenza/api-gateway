package org.ladhark.identity.security.context

import org.ladhark.identity.entity.application.Application
import org.ladhark.identity.entity.resource.ResourceAction
import org.ladhark.identity.entity.resource.policy.ResourcePolicy
import org.ladhark.identity.policy.context.model.Principal
import org.ladhark.identity.security.AuthenticationToken
import org.ladhark.identity.security.LadharkAuthority
import org.ladhark.kotlin.extension.s
import java.util.*

const val SERVICE_CONTEXT_KEY = "SERVICE_CONTEXT_KEY"
data class RequestInformation(val properties: MutableMap<String, Any>)
data class ServiceContext(
    val authorize: Boolean = true, val authenticationToken: AuthenticationToken,
    val application: Optional<Application> = Optional.empty(),
    val resourceAction: Optional<ResourceAction> = Optional.empty(),
    val resourcePolicy: Optional<ResourcePolicy> = Optional.empty(),
    val requestInformation: RequestInformation = RequestInformation(mutableMapOf())
) {

    fun serviceId(): Optional<String> {
        if (application.isPresent) {
            return Optional.of(application.get().name)
        }

        return Optional.empty()
    }

    fun principal() = authenticationToken.getPrincipal().principal

    fun principalName() = principal().getPrincipalName()

    fun principalUUID() = principal().getUuid()

    fun principalId() = principalUUID().s

    fun principalEmail() = principal().getPrincipalEmail()

    fun principalType() = principal().getType()
}

data class ServiceContextHolder(var serviceContext: Optional<ServiceContext>)

// data class SecurityContext(val authenticationToken: AuthenticationToken)

class OpenLadharkPrincipal : Principal {

    override fun getAuthorities(): Collection<LadharkAuthority> {
        return emptySet()
    }

    override fun getPrincipalName(): String {
        return "__open__"
    }

    override fun getPrincipalEmail(): String {
        return "__open@ladhark__"
    }

    override fun isValid(): Boolean {
        return true
    }

    override fun getUuid(): UUID {
        return UUID.randomUUID()
    }

    override fun getType(): Class<out Principal> {
        return OpenLadharkPrincipal::class.java
    }
}