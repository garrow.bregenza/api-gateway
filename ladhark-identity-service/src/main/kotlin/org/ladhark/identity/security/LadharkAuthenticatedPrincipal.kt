package org.ladhark.identity.security

import org.ladhark.identity.policy.context.model.Principal
import java.util.*

// TODO make private principal
class LadharkAuthenticatedPrincipal(val principal: Principal, val system: Boolean = false) {

    val authorities = mutableListOf<LadharkAuthority>()

    init {
        authorities.addAll(principal.getAuthorities())
    }

    fun getName(): String {
        return this.principal.getPrincipalName()
    }

    fun isValid(): Boolean {
        return principal.isValid()
    }

    fun authorities(): List<LadharkAuthority> {
        return authorities
    }

    fun uuid(): UUID {
        return this.principal.getUuid()
    }

    fun type(): Class<out Principal> {
        return principal.getType()
    }

}

object AnonymousUser : Principal {

    override fun getAuthorities(): Collection<LadharkAuthority> {
        return emptyList()
    }

    override fun getPrincipalName(): String {
        return "Anonymous"
    }

    override fun getPrincipalEmail(): String? {
        return null
    }

    override fun isValid(): Boolean {
        return true
    }

    override fun getUuid(): UUID {
        return UUID.randomUUID()
    }

    override fun getType(): Class<out Principal> {
        return this::class.java
    }

}

// TODO remove this we do not need this any more
class SystemUser private constructor() {

    // TODO HIGH need to evaluate this strategy is correct creating a static system user
    companion object Factory {
        var sysUser = org.ladhark.identity.policy.context.model.User(
            UUID.randomUUID(), "__system__@__ladhark__.org",
            org.ladhark.identity.policy.context.model.Name("__system__", "", "__user__")
        )


        private val INSTANCE = LadharkAuthenticatedPrincipal(sysUser, true)

        @JvmStatic
        fun get() = INSTANCE
    }
}

class LadharkAuthority(val appAuthority: String, val implied: Boolean)

class AuthenticationToken {
    private val principalStackLadhark: Stack<LadharkAuthenticatedPrincipal> = Stack()
    private var ladharkAuthenticatedPrincipal: LadharkAuthenticatedPrincipal
    private var ladharkCredentials: Any

    constructor(
        ladharkAuthenticatedPrincipal: LadharkAuthenticatedPrincipal,
        ladharkCredentials: Any
    ) {
        this.ladharkAuthenticatedPrincipal = ladharkAuthenticatedPrincipal
        this.ladharkCredentials = ladharkCredentials
        this.principalStackLadhark.push(this.ladharkAuthenticatedPrincipal)
    }

    // JWT token
    fun getCredentials(): Any {
        return this.ladharkCredentials
    }

    // User object instance
    fun getPrincipal(): LadharkAuthenticatedPrincipal {
        return this.principalStackLadhark.peek()!!
    }

    fun pushPrincipalToCurrentContext(principal: LadharkAuthenticatedPrincipal) {
        this.principalStackLadhark.push(principal)
    }

    fun popPrincipalFromCurrentContext(): LadharkAuthenticatedPrincipal? {
        return this.principalStackLadhark.pop()
    }

    fun isSystemUserContextOnly(): Boolean {
        return this.principalStackLadhark.size == 1 && this.principalStackLadhark.peek().system
    }

}