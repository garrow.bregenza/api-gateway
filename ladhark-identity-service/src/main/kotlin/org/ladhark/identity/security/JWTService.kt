package org.ladhark.identity.security

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.toCollection
import kotlinx.coroutines.flow.toSet
import org.ladhark.identity.api.dto.user.LoginResponse
import org.ladhark.identity.entity.user.UserHasRegion
import org.ladhark.identity.exception.AuthorizationFailedException
import org.ladhark.identity.exception.EntityNotFoundException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.exception.InvalidStateException
import org.ladhark.identity.policy.context.model.User
import org.ladhark.identity.properties.ApplicationProperties
import org.ladhark.identity.properties.Constant
import org.ladhark.identity.repository.client.ClientJWTRepository
import org.ladhark.identity.repository.model.role.InferredAuthorities
import org.ladhark.identity.repository.model.role.RoleAuthorityResult
import org.ladhark.identity.repository.model.team.MappedInheritedTeam
import org.ladhark.identity.repository.role.RoleRepository
import org.ladhark.identity.repository.team.TeamRoleRegionRepository
import org.ladhark.identity.repository.user.UserJWTRepository
import org.ladhark.identity.repository.user.UserTeamRegionRepository
import org.ladhark.identity.service.base.BaseService
import org.ladhark.kotlin.extension.s
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Service
class JWTService : BaseService() {

    private val log: Logger = LoggerFactory.getLogger(JWTService::class.java)

    @Autowired
    lateinit var userJWTRepository: UserJWTRepository

    @Autowired
    lateinit var clientJWTRepository: ClientJWTRepository

    @Autowired
    lateinit var applicationProperties: ApplicationProperties

    @Autowired
    lateinit var userTeamRegionRepository: UserTeamRegionRepository

    @Autowired
    lateinit var teamRoleRegionRepository: TeamRoleRegionRepository

    @Autowired
    lateinit var roleRepository: RoleRepository

    fun extractTokenFromHeader(header: String): String? {

        val headerPrefix = applicationProperties.authorizationHeaderPrefix()

        if (header.isBlank() || !header.startsWith(headerPrefix)) {
            return null
        }

        return header.replace(headerPrefix, "").trim()
    }

    fun parseJwtHeader(token: String): Claims {

        val secretKeyB64 = applicationProperties.jwtSecretKey()

        if (token.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Token"))
        }

        if (secretKeyB64.isBlank()) {
            throw InvalidStateException(appMessage.getMessage("error.value.required.is.empty", "Secret key"))
        }

        try {
            return Jwts.parserBuilder().setSigningKey(secretKeyB64).build().parseClaimsJws(token).body
                ?: throw AuthorizationFailedException(appMessage.getMessage("error.bad.credentials"))
        } catch (e: Exception) {
            throw AuthorizationFailedException(e.message.orEmpty(), e)
        }
    }

    fun createUserJwtAccessToken(userJwtDetails: UserJwtDetails): String {
        return createUserJwtToken(
            userJwtDetails,
            Constant.JWT_TOKEN_TYPE_ACCESS,
            applicationProperties.jwtAccessExpiration()
        )
    }

    fun createUserJwtRefreshToken(userJwtDetails: UserJwtDetails): String {
        return createUserJwtToken(
            userJwtDetails,
            Constant.JWT_TOKEN_TYPE_REFRESH,
            applicationProperties.jwtRefreshExpiration()
        )
    }

    fun createClientJwtAccessToken(clientJwtDetails: ClientJwtDetails): String {
        return createClientJwtToken(
            clientJwtDetails,
            Constant.JWT_TOKEN_TYPE_ACCESS,
            applicationProperties.jwtAccessExpiration()
        )
    }

    fun createClientJwtRefreshToken(clientJwtDetails: ClientJwtDetails): String {
        return createClientJwtToken(
            clientJwtDetails,
            Constant.JWT_TOKEN_TYPE_REFRESH,
            applicationProperties.jwtRefreshExpiration()
        )
    }

    private fun createUserJwtToken(userJwtDetails: UserJwtDetails, type: String, expiration: Date): String {

        val issuer = applicationProperties.jwtIssuer()

        if (userJwtDetails.subject.uuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Subject"))
        }

        if (userJwtDetails.region.uuid.isBlank()) {
            throw InvalidStateException(appMessage.getMessage("error.value.required.is.empty", "Region"))
        }

        if (issuer.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Issuer"))
        }

        val secretKeyB64 = applicationProperties.jwtSecretKey()

        if (secretKeyB64.isBlank()) {
            throw InvalidStateException(appMessage.getMessage("error.value.required.is.empty", "Secret key"))
        }

        val now = Instant.now()
        val dateNow = Date.from(now)

        if (expiration.before(dateNow)) {
            throw InvalidStateException(appMessage.getMessage("error.jwt.token.expiration.date.soon.invalid"))
        }

        val jwtId = UUID.randomUUID().toString()
        val secretKey = Base64.getDecoder().decode(secretKeyB64)
        val key = Keys.hmacShaKeyFor(secretKey)

        val builder = Jwts.builder()
            .setIssuer(issuer)
            .setExpiration(expiration)
            .setSubject(userJwtDetails.subject.uuid)
            .setIssuedAt(dateNow)
            .setNotBefore(dateNow)
            .setAudience(userJwtDetails.applications.joinToString(","))
            .setId(jwtId)
            .claim(Constant.JWT_TOKEN_TYPE, type)
            .claim(Constant.JWT_TOKEN_SUBJECT_TYPE, Constant.JWT_TOKEN_SUBJECT_TYPE_USER)
            .claim(Constant.JWT_REGION, userJwtDetails.region.uuid)
            .claim(Constant.JWT_REGION_CHILDREN_ACCESS, userJwtDetails.childrenAccess)
            .signWith(key)

        return builder.compact()
    }

    private fun createClientJwtToken(clientJwtDetails: ClientJwtDetails, type: String, expiration: Date): String {

        val issuer = applicationProperties.jwtIssuer()

        if (clientJwtDetails.client.uuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Subject"))
        }

        if (issuer.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Issuer"))
        }

        val secretKeyB64 = applicationProperties.jwtSecretKey()

        if (secretKeyB64.isBlank()) {
            throw InvalidStateException(appMessage.getMessage("error.value.required.is.empty", "Secret key"))
        }

        val now = Instant.now()
        val dateNow = Date.from(now)

        if (expiration.before(dateNow)) {
            throw InvalidStateException(appMessage.getMessage("error.jwt.token.expiration.date.soon.invalid"))
        }

        val jwtId = UUID.randomUUID().toString()
        val secretKey = Base64.getDecoder().decode(secretKeyB64)
        val key = Keys.hmacShaKeyFor(secretKey)

        val builder = Jwts.builder()
            .setIssuer(issuer)
            .setExpiration(expiration)
            .setSubject(clientJwtDetails.client.uuid)
            .setIssuedAt(dateNow)
            .setNotBefore(dateNow)
            .setAudience(clientJwtDetails.applications.joinToString(","))
            .setId(jwtId)
            .claim(Constant.JWT_TOKEN_TYPE, type)
            .claim(Constant.JWT_TOKEN_SUBJECT_TYPE, Constant.JWT_TOKEN_SUBJECT_TYPE_CLIENT)
            .signWith(key)

        return builder.compact()
    }

    @Transactional(readOnly = true)
    suspend fun fetchUserForJwtSubject(claims: Claims): LadharkAuthenticatedPrincipal {
        // Region goes down |
        //                  V
        // Team goes up ^
        //              |
        // for inheritance finding, I get teams above me, I access regions below current region passed
        val jwtId = claims.id
        val subject = claims.subject

        if (jwtId.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "JWT ID"))
        }

        if (subject.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Subject"))
        }

        if (claims[Constant.JWT_TOKEN_SUBJECT_TYPE] != Constant.JWT_TOKEN_SUBJECT_TYPE_USER) {
            throw InvalidArgumentException(appMessage.getMessage("error.jwt.token.invalid.data", "Subject Type"))
        }

        if (claims[Constant.JWT_REGION] == null) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Region"))
        }

        if (claims[Constant.JWT_REGION_CHILDREN_ACCESS] == null) {
            throw InvalidArgumentException(
                appMessage.getMessage(
                    "error.value.required.is.empty",
                    "Region Children Access"
                )
            )
        }

        val regionUuid = (claims[Constant.JWT_REGION] as String).uuid
        val regionChildrenAccess = claims[Constant.JWT_REGION_CHILDREN_ACCESS] as Boolean

        return LadharkAuthenticatedPrincipal(getUserContext(subject.uuid, regionUuid, regionChildrenAccess))
    }

    @Suppress("UNCHECKED_CAST")
    @Transactional(readOnly = true)
    suspend fun getUserContext(userUuid: UUID, regionUuid: UUID, regionChildrenAccess: Boolean): User {
        return userJWTRepository.findUserByUuid(userUuid)?.let { user ->
            // It may so happen JWT when given out, user had access to region, but now it was revoked better check
            val userRegion: UserHasRegion? = user.userRegion(regionUuid)

            // region access check
            if (userRegion == null) {
                log.error("Region {} not mapped to User {}.", regionUuid, user.uuid)
                //publishEvent(JwtInvalidClaimEvent(this, jwtId, "User Region"))
                // do not throw specific entity related exception, entity name not found gives hint about system, hence only bad cred
                throw InvalidStateException(appMessage.getMessage("error.jwt.token.invalid.data", "User Region"))
            } else {

                val records = coroutineScope {
                    awaitAll(
                        async {

                            // user direct authorities
                            if (regionChildrenAccess) {
                                userJWTRepository
                                    .findUserLocalAndInheritedAuthoritiesByRegionAndApplication(
                                        user.uuid,
                                        setOf(regionUuid)
                                    )
                                    .toSet()
                            } else {
                                userJWTRepository
                                    .findUserLocalOnlyAuthoritiesByRegionAndApplication(user.uuid, setOf(regionUuid))
                                    .toSet()
                            }
                        },
                        async {
                            val exclusiveRole = user.exclusiveRole

                            if (exclusiveRole != null) {
                                roleRepository
                                    .findAuthoritiesForRole(exclusiveRole.uuid)
                                    .toSet()
                            }
                        },
                        async {
                            // load teams
                            if (regionChildrenAccess) {
                                userTeamRegionRepository.findAllTeamsWithInheritanceForUserWithInheritedRegions(
                                    user.uuid,
                                    setOf(regionUuid)
                                ).toSet()
                            } else {
                                userTeamRegionRepository.findAllTeamsWithInheritanceForUserInRegions(
                                    user.uuid,
                                    setOf(regionUuid)
                                ).toSet()
                            }
                        }
                    )
                }

                val inferredAuthorities = records[0] as Set<InferredAuthorities>
                val exclusiveAuthorities = records[1] as Set<RoleAuthorityResult>
                val mappedInheritedTeams = records[2] as Set<MappedInheritedTeam>

                val teamAuthorities = mappedInheritedTeams.flatMap {
                    // load authorities mapping for teams
                    // TeamRoleRegion -> Role -> Authority
                    val team = it.team
                    teamRoleRegionRepository.findTeamLocalOnlyAuthoritiesByRegionAndApplication(
                        setOf(team.uuid),
                        setOf(regionUuid)
                    ).toSet()

                }.toSet()

                User.fromUserEntityModel(
                    user,
                    exclusiveAuthorities,
                    mappedInheritedTeams,
                    inferredAuthorities,
                    teamAuthorities
                )
            }

        } ?: throw EntityNotFoundException(
            appMessage.getMessage(
                "error.entity.not.found",
                userUuid.s
            )
        )
    }

    @Transactional(readOnly = true)
    suspend fun fetchClientForJwtSubject(claims: Claims): LadharkAuthenticatedPrincipal {
        val jwtId = claims.id
        val subject = claims.subject

        if (jwtId.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "JWT ID"))
        }

        if (subject.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Subject"))
        }

        if (claims[Constant.JWT_TOKEN_SUBJECT_TYPE] != Constant.JWT_TOKEN_SUBJECT_TYPE_CLIENT) {
            throw InvalidArgumentException(appMessage.getMessage("error.jwt.token.invalid.data", "Subject Type"))
        }

        val roleAuthorities = mutableSetOf<RoleAuthorityResult>()

        return clientJWTRepository.findClientByUuid(subject.uuid)?.let { client ->
            clientJWTRepository
                .findRoleAuthoritiesForClient(client.uuid)
                .toCollection(roleAuthorities)

            val clientContext = org.ladhark.identity.policy.context.model.Client.fromClientEntityModel(client, roleAuthorities)

            LadharkAuthenticatedPrincipal(clientContext)

        } ?: throw EntityNotFoundException(
            appMessage.getMessage(
                "error.entity.not.found",
                subject
            )
        )
    }

    @Transactional(readOnly = true)
    suspend fun refreshUserToken(authorizationHeader: String?): LoginResponse {

        if (authorizationHeader.isNullOrBlank()) {
            throw InvalidArgumentException(
                appMessage.getMessage(
                    "error.value.required.is.empty",
                    "Authorization header"
                )
            )
        }

        val receivedToken = extractTokenFromHeader(authorizationHeader)
            ?: throw InvalidArgumentException(
                appMessage.getMessage(
                    "error.value.required.is.empty",
                    "Authorization token"
                )
            )

        val claims = parseJwtHeader(receivedToken)

        val subject = claims.subject

        val tokenType = claims[Constant.JWT_TOKEN_TYPE]

        if (Constant.JWT_TOKEN_TYPE_REFRESH != tokenType) {
            throw InvalidStateException(appMessage.getMessage("error.jwt.token.type.mismatch", "Refresh"))
        }

        return when {

            claims[Constant.JWT_TOKEN_SUBJECT_TYPE] == Constant.JWT_TOKEN_SUBJECT_TYPE_CLIENT -> {
                createTokensForClient(claims, subject)
            }

            claims[Constant.JWT_TOKEN_SUBJECT_TYPE] == Constant.JWT_TOKEN_SUBJECT_TYPE_USER -> {
                createTokensForUser(claims, subject)
            }

            else -> {
                throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Subject Type"))
            }
        }

    }

    private suspend fun createTokensForUser(claims: Claims, subject: String): LoginResponse {
        val region = claims[Constant.JWT_REGION] as String

        val childrenAccess = claims[Constant.JWT_REGION_CHILDREN_ACCESS] as Boolean

        val applicationSet = getAudienceFromClaims(claims)

        return userJWTRepository.findUserByUuid(subject.uuid)?.let { user ->

            // validate user again what if locked !!! or something went wrong
            user.isValid()

            val access = createUserJwtAccessToken(UserJwtDetails(Subject(subject),
                Region(region), ChildrenAccess(childrenAccess), applicationSet))
            val refresh = createUserJwtRefreshToken(UserJwtDetails(Subject(subject),
                Region(region), ChildrenAccess(childrenAccess), applicationSet))
            log.info("Token created for user by uuid {} by refresh", user.uuid)

            LoginResponse(access, refresh)
        } ?: throw EntityNotFoundException(
            appMessage.getMessage(
                "error.entity.not.found",
                "User/$subject"
            )
        ) // TODO publish event over error
    }

    private suspend fun createTokensForClient(claims: Claims, subject: String): LoginResponse {

        val applicationSet = getAudienceFromClaims(claims)

        return clientJWTRepository.findClientByUuid(subject.uuid)?.let { client ->
            // validate client again what if inactive !!! or something went wrong
            client.isValid()

            val access = createClientJwtAccessToken(ClientJwtDetails(Client(subject), applicationSet))
            val refresh = createClientJwtRefreshToken(ClientJwtDetails(Client(subject), applicationSet))
            log.info("Token created for client by uuid {} by refresh", client.uuid)
            LoginResponse(access, refresh)
        } ?: throw EntityNotFoundException(appMessage.getMessage("error.entity.not.found", "Client/$subject"))

    }

    private fun getAudienceFromClaims(claims: Claims): Set<Application> {
        val applications = claims.audience as String

        return if (applications.isNotEmpty())
            applications.split(",")
                .map { Application(it) }
                .toSet()
        else
            emptySet()
    }
}

data class UserJwtDetails(val subject: Subject,
                          val region: Region,
                          val childrenAccess: ChildrenAccess = ChildrenAccess(false),
                          val applications: Set<Application> = emptySet())
data class ClientJwtDetails(val client: Client,  val applications: Set<Application> = emptySet())

@JvmInline
value class Subject(val uuid: String)
@JvmInline
value class Region(val uuid: String)
@JvmInline
value class ChildrenAccess(val childrenAccess: Boolean)
@JvmInline
value class Application(val uuid: String)
@JvmInline
value class Client(val uuid: String)

