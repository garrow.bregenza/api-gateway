package org.ladhark.identity.service.resource.cache

import com.github.benmanes.caffeine.cache.AsyncCache
import com.github.benmanes.caffeine.cache.Caffeine
import jakarta.annotation.PostConstruct
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.resource.ResourceAction
import org.ladhark.identity.repository.resource.ResourceActionRepository
import org.ladhark.identity.service.base.BaseCacheService
import org.ladhark.kotlin.extension.s
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

const val RESOURCE_ACTION_CACHE_BY_UUID = "RESOURCE_ACTION_CACHE_BY_UUID"
const val RESOURCE_ACTION_CACHE_BY_NAME_TO_UUID = "RESOURCE_ACTION_CACHE_BY_NAME_TO_UUID"
const val RESOURCE_ACTION_CACHE_BY_URI_AND_SCOPE_TO_UUID = "RESOURCE_ACTION_CACHE_BY_URI_AND_SCOPE_TO_UUID"

interface ResourceActionCacheService {
    suspend fun getResourceActionByUriAndScope(uri: String, scope: String): ResourceAction?
    suspend fun putResourceActionByUriAndScopeAsync(uri: String, scope: String, value: ResourceAction)
    suspend fun removeResourceActionByUriAndScopeAsync(uri: String, scope: String)
    suspend fun getResourceActionByNameToUuidAsync(key: String): ResourceAction?
    suspend fun putResourceActionByNameToUuidAsync(key: String, value: ResourceAction)
    suspend fun removeResourceActionByNameToUuidAsync(key: String)
    suspend fun getResourceActionByUuidAsync(key: String): ResourceAction?
    suspend fun putResourceActionByUuidAsync(key: String, value: ResourceAction)
    suspend fun removeResourceActionByUuidAsync(key: String)
    suspend fun allEntriesInUuidCache(): MutableCollection<ResourceAction>
}

@Service
class ResourceActionCacheServiceImpl : BaseCacheService(), ResourceActionCacheService {

    @Autowired
    private lateinit var resourceActionRepository: ResourceActionRepository

    private lateinit var resourceActionCacheByUUID: AsyncCache<String, ResourceAction>

    private lateinit var resourceActionCacheByNameToUUID: AsyncCache<String, String>

    private lateinit var resourceActionCacheByURIAndScopeToUUID: AsyncCache<String, String>

    @PostConstruct
    private fun postConstruct() {

        resourceActionCacheByUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        resourceActionCacheByNameToUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        // Note: key is uri + scope seperated by ->
        resourceActionCacheByURIAndScopeToUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        registerProvider(this, RESOURCE_ACTION_CACHE_BY_UUID,
            RESOURCE_ACTION_CACHE_BY_NAME_TO_UUID, RESOURCE_ACTION_CACHE_BY_URI_AND_SCOPE_TO_UUID)

        resourceActionRepository.subscribeAndDoOnAll {
            with(it) {
                resourceActionCacheByUUID.put(uuid.s, CompletableFuture.completedFuture(this))
                resourceActionCacheByNameToUUID.put(name, CompletableFuture.completedFuture(this.uuid.s))
            }
        }
    }

    override suspend fun getResourceActionByUriAndScope(uri: String, scope: String): ResourceAction? {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.getValueAsync(uriAndScopeKey(uri, scope),
                resourceActionCacheByURIAndScopeToUUID)
            return uuid?.let {
                caffeineCacheHelperService.getValueAsync(it, resourceActionCacheByUUID)
            }
        }
    }

    override suspend fun putResourceActionByUriAndScopeAsync(uri: String, scope: String, value: ResourceAction) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(uriAndScopeKey(uri, scope), value.uuid.s,
                resourceActionCacheByURIAndScopeToUUID)
            caffeineCacheHelperService.putValueAsync(value.uuid.s, value, resourceActionCacheByUUID)
        }
    }

    override suspend fun removeResourceActionByUriAndScopeAsync(uri: String, scope: String) {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.removeValueAsync(uriAndScopeKey(uri, scope),
                resourceActionCacheByURIAndScopeToUUID)
            uuid?.let {
                caffeineCacheHelperService.removeValueAsync(it, resourceActionCacheByUUID)
            }
        }
    }

    override suspend fun getResourceActionByNameToUuidAsync(key: String): ResourceAction? {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.getValueAsync(key, resourceActionCacheByNameToUUID)
            return uuid?.let {
                caffeineCacheHelperService.getValueAsync(it, resourceActionCacheByUUID)
            }
        }
    }

    override suspend fun putResourceActionByNameToUuidAsync(key: String, value: ResourceAction) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value.uuid.s, resourceActionCacheByNameToUUID)
            caffeineCacheHelperService.putValueAsync(value.uuid.s, value, resourceActionCacheByUUID)
        }
    }

    override suspend fun removeResourceActionByNameToUuidAsync(key: String) {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.removeValueAsync(key, resourceActionCacheByNameToUUID)
            uuid?.let {
                caffeineCacheHelperService.removeValueAsync(it, resourceActionCacheByUUID)
            }
        }
    }

    override suspend fun getResourceActionByUuidAsync(key: String): ResourceAction? {
        mutex.withLock {
            return caffeineCacheHelperService.getValueAsync(key, resourceActionCacheByUUID)
        }
    }

    override suspend fun putResourceActionByUuidAsync(key: String, value: ResourceAction) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value, resourceActionCacheByUUID)
        }
    }

    override suspend fun removeResourceActionByUuidAsync(key: String) {
        mutex.withLock {
            caffeineCacheHelperService.removeValueAsync(key, resourceActionCacheByUUID)
        }
    }


    override suspend fun allEntriesInUuidCache(): MutableCollection<ResourceAction> {

        return coroutineScope {
            withContext(Dispatchers.Unconfined) {
                resourceActionCacheByUUID.synchronous().asMap().values
            }
        }
    }

    private fun uriAndScopeKey(uri: String, scope: String) = "${uri}->${scope}"
    override suspend fun getFromCache(name: String, key: String): BaseEntity? {
        return when(name) {
            RESOURCE_ACTION_CACHE_BY_UUID -> getResourceActionByUuidAsync(key)
            RESOURCE_ACTION_CACHE_BY_NAME_TO_UUID -> getResourceActionByNameToUuidAsync(key)
            RESOURCE_ACTION_CACHE_BY_URI_AND_SCOPE_TO_UUID -> throw UnsupportedOperationException()
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun putInCache(name: String, key: String, value: BaseEntity) {

        if (value !is ResourceAction) {
            throw IllegalArgumentException("Cache value passed is not of type ${ResourceAction::class.qualifiedName}")
        }

        when(name) {
            RESOURCE_ACTION_CACHE_BY_UUID -> putResourceActionByUuidAsync(key, value)
            RESOURCE_ACTION_CACHE_BY_NAME_TO_UUID -> putResourceActionByNameToUuidAsync(key, value)
            RESOURCE_ACTION_CACHE_BY_URI_AND_SCOPE_TO_UUID -> throw UnsupportedOperationException()
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun removeFromCache(name: String, key: String) {
        when(name) {
            RESOURCE_ACTION_CACHE_BY_UUID -> removeResourceActionByUuidAsync(key)
            RESOURCE_ACTION_CACHE_BY_NAME_TO_UUID -> removeResourceActionByNameToUuidAsync(key)
            RESOURCE_ACTION_CACHE_BY_URI_AND_SCOPE_TO_UUID -> throw UnsupportedOperationException()
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }
}