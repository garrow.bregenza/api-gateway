package org.ladhark.identity.service.aspect

import org.ladhark.identity.api.dto.aspect.AspectTypeDetails
import org.ladhark.identity.aspect.cache.GetFromCache
import org.ladhark.identity.aspect.cache.PutInCache
import org.ladhark.identity.aspect.cache.PutInCaches
import org.ladhark.identity.entity.aspect.AspectType
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.repository.aspect.AspectTypeRepository
import org.ladhark.identity.service.aspect.cache.ASPECT_TYPE_CACHE_BY_NAME_TO_UUID
import org.ladhark.identity.service.aspect.cache.ASPECT_TYPE_CACHE_BY_UUID
import org.ladhark.identity.service.aspect.event.AspectTypeCreateEvent
import org.ladhark.identity.service.aspect.event.AspectTypeReadEvent
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID
import org.ladhark.identity.service.base.BaseService
import org.ladhark.identity.service.context.withUserContext
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

interface AspectTypeService {
    suspend fun createAspectType(aspectTypeDetails: AspectTypeDetails): AspectType
    suspend fun findByUuid(uuid: String): AspectType?
    suspend fun findByName(name: String): AspectType?
}

@Service
class AspectTypeServiceImpl: BaseService(), AspectTypeService {

    val log: Logger = LoggerFactory.getLogger(AspectTypeServiceImpl::class.java)

    @Autowired
    private lateinit var aspectTypeRepository: AspectTypeRepository

    @Transactional(rollbackFor = [Exception::class])
    @PutInCaches(caches = [
        PutInCache(ASPECT_TYPE_CACHE_BY_UUID),
        PutInCache(ASPECT_TYPE_CACHE_BY_NAME_TO_UUID),
    ])
    override suspend fun createAspectType(aspectTypeDetails: AspectTypeDetails): AspectType = withUserContext {

        if (aspectTypeDetails.name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        log.info("Saving aspect type details with name {}", aspectTypeDetails.name)

        val aspectType = AspectType().apply {
            name = aspectTypeDetails.name
            description = aspectTypeDetails.description
        }

        return aspectTypeRepository.save(aspectType).also { aspectTypeFromDb ->
            publishEvent(AspectTypeCreateEvent(this, aspectTypeFromDb, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    @Transactional(readOnly = true)
    @GetFromCache(ASPECT_TYPE_CACHE_BY_UUID)
    override suspend fun findByUuid(uuid: String): AspectType? = withUserContext {
        if (uuid.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching Aspect Type details for uuid {}", uuid)

        return aspectTypeRepository.findAspectTypeByUuid(uuid.uuid)?.also { aspectType ->
            publishEvent(AspectTypeReadEvent(this, aspectType, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    @Transactional(readOnly = true)
    @GetFromCache(ASPECT_TYPE_CACHE_BY_NAME_TO_UUID)
    override suspend fun findByName(name: String): AspectType? = withUserContext {

        if (name.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "Name"))
        }

        log.info("Fetching Aspect Type details for name {}", name)

        return aspectTypeRepository.findAspectTypeByName(name)?.also { aspectType ->
            publishEvent(AspectTypeReadEvent(this, aspectType, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

}