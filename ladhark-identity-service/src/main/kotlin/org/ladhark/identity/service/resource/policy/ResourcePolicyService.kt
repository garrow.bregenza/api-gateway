package org.ladhark.identity.service.resource.policy

import org.ladhark.identity.api.dto.policy.ResourcePolicyDetails
import org.ladhark.identity.entity.resource.policy.ResourcePolicy
import org.ladhark.identity.exception.EntityNotFoundException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.repository.resource.ResourceActionRepository
import org.ladhark.identity.repository.resource.policy.ResourcePolicyRepository
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID
import org.ladhark.identity.service.base.BaseService
import org.ladhark.identity.service.context.withUserContext
import org.ladhark.identity.service.resource.event.ResourceActionReadEvent
import org.ladhark.identity.service.resource.policy.event.ResourcePolicyCreateEvent
import org.ladhark.identity.service.resource.policy.event.ResourcePolicyReadEvent
import org.ladhark.kotlin.extension.s
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

interface ResourcePolicyService {
    suspend fun create(resourcePolicyDetails: ResourcePolicyDetails): ResourcePolicy
    suspend fun findByUuid(uuid: String): ResourcePolicy?
    suspend fun findByName(name: String): ResourcePolicy?
}

@Service
class ResourcePolicyServiceImpl : BaseService(), ResourcePolicyService {

    private val log: Logger = LoggerFactory.getLogger(ResourcePolicyServiceImpl::class.java)

    @Autowired
    private lateinit var resourcePolicyRepository: ResourcePolicyRepository

    @Autowired
    private lateinit var resourceActionRepository: ResourceActionRepository

    @Transactional(rollbackFor = [Exception::class])
    override suspend fun create(resourcePolicyDetails: ResourcePolicyDetails): ResourcePolicy = withUserContext {

        if (resourcePolicyDetails.name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        if (resourcePolicyDetails.resourceActionUuid.isBlank()) {
            throw InvalidArgumentException(
                appMessage.getMessage(
                    "error.value.required.is.empty",
                    "Resource Action UUID"
                )
            )
        }

        return resourceActionRepository
            .findResourceActionByUuid(resourcePolicyDetails.resourceActionUuid.uuid)?.let { resourceActionFromDb ->

                log.info("Saving resource policy details with name {}", resourcePolicyDetails.name)

                publishEvent(
                    ResourceActionReadEvent(
                        this, resourceActionFromDb, AuthenticatedUserContextUUID(principalUUID()),
                        appMessage.getMessage(
                            "message.resource.action.event.read.secondary.access",
                            arrayOf(
                                resourceActionFromDb.name,
                                resourceActionFromDb.uuid.s,
                                principalId(),
                                "create resource policy"
                            )
                        )
                    )
                )

                val resourcePolicy = ResourcePolicy().apply {
                    name = resourcePolicyDetails.name
                    description = resourcePolicyDetails.description
                    resourceAction = resourceActionFromDb
                }


                resourcePolicyRepository
                    .save(resourcePolicy)
                    .also { resourcePolicyFromDb ->
                        publishEvent(
                            ResourcePolicyCreateEvent(
                                this, resourcePolicyFromDb, AuthenticatedUserContextUUID(principalUUID())
                            )
                        )
                    }
            } ?: throw EntityNotFoundException(
            getMessage(
                "error.resource.action.not.found.by.uuid",
                resourcePolicyDetails.resourceActionUuid
            )
        )
    }

    @Transactional(readOnly = true)
    override suspend fun findByUuid(uuid: String): ResourcePolicy? = withUserContext {

        if (uuid.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching resource policy details for uuid {}", uuid)

        return resourcePolicyRepository
            .findResourcePolicyByUuid(uuid.uuid)?.also { resourcePolicyFromDb ->
                publishEvent(
                    ResourcePolicyReadEvent(
                        this, resourcePolicyFromDb, AuthenticatedUserContextUUID(principalUUID())
                    )
                )
            }
    }

    @Transactional(readOnly = true)
    override suspend fun findByName(name: String): ResourcePolicy? = withUserContext {

        if (name.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching resource policy details for name {}", name)

        return resourcePolicyRepository
            .findResourcePolicyByName(name)?.also { resourcePolicyFromDb ->
                publishEvent(
                    ResourcePolicyReadEvent(
                        this, resourcePolicyFromDb, AuthenticatedUserContextUUID(principalUUID())
                    )
                )
            }
    }
}