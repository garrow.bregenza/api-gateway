package org.ladhark.identity.service.cache

import com.github.benmanes.caffeine.cache.AsyncCache
import kotlinx.coroutines.future.await
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture

interface CaffeineCacheHelperService {
    suspend fun <K, V> getValueAsync(key: K, cache: AsyncCache<K, V>): V?
    suspend fun <K, V> putValueAsync(key: K, value: V, cache: AsyncCache<K, V>): V
    suspend fun <K, V> removeValueAsync(key: K, cache: AsyncCache<K, V>): V?
}

@Service
class CaffeineCacheHelperServiceImpl : CaffeineCacheHelperService {
    override suspend fun <K, V> getValueAsync(key: K, cache: AsyncCache<K, V>): V? = cache.getIfPresent(key)?.await()
    override suspend fun <K, V> putValueAsync(key: K, value: V, cache: AsyncCache<K, V>): V {
        val futureValueCompleted = CompletableFuture.completedFuture(value)
        cache.put(key, futureValueCompleted)
        return futureValueCompleted.await()
    }

    override suspend fun <K, V> removeValueAsync(key: K, cache: AsyncCache<K, V>): V? {
        return cache.asMap().remove(key)?.await()
    }
}