package org.ladhark.identity.service.application.event

import org.ladhark.identity.entity.application.Application
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class ApplicationCreateEvent(
    source: Any,
    private val application: Application,
    private val contextUser: AuthenticatedUserContextUUID
) :
    BaseEvent<Application>(source, application, ApplicationEventCategory.APPLICATION, ApplicationEventAction.CREATE) {
    override fun summary(): String {
        return "Application with name ${application.name} and id ${application.uuid} created by User $contextUser"
    }
}

class ApplicationReadEvent(
    source: Any,
    private val application: Application,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<Application>(source, application, ApplicationEventCategory.APPLICATION, ApplicationEventAction.READ) {
    override fun summary(): String {
        return summary.ifBlank {
            return "Application with name ${application.name} and id ${application.uuid} read by User $contextUser"
        }
    }
}

class ApplicationUpdateEvent(
    source: Any,
    private val application: Application,
    private val contextUser: AuthenticatedUserContextUUID
) :
    BaseEvent<Application>(source, application, ApplicationEventCategory.APPLICATION, ApplicationEventAction.UPDATE) {
    override fun summary(): String {
        return "Application with name ${application.name} and id ${application.uuid} updated by User $contextUser"
    }
}

class ApplicationDeleteEvent(source: Any, private val application: Application, private val contextUser: String) :
    BaseEvent<Application>(source, application, ApplicationEventCategory.APPLICATION, ApplicationEventAction.DELETE) {
    override fun summary(): String {
        return "Application with name ${application.name} and id ${application.uuid} deleted by User $contextUser"
    }
}

enum class ApplicationEventCategory : EventCategory {
    APPLICATION {
        override fun type(): String {
            return APPLICATION.name
        }
    }
}

