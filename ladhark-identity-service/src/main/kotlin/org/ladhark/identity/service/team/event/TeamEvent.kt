package org.ladhark.identity.service.team.event

import org.ladhark.identity.entity.team.Team
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class TeamCreateEvent(source: Any, private val team: Team, private val contextUser: AuthenticatedUserContextUUID)
    : BaseEvent<Team>(source, team, TeamEventCategory.TEAM, ApplicationEventAction.CREATE) {
    override fun summary(): String {
        return "Team with name ${team.name} and id ${team.uuid} created by User $contextUser"
    }
}

class TeamReadEvent(source: Any, private val team: Team, private val contextUser: AuthenticatedUserContextUUID, private val summary: String = "")
    : BaseEvent<Team>(source, team, TeamEventCategory.TEAM, ApplicationEventAction.READ) {
    override fun summary(): String {
        return summary.ifBlank {
            "Team with name ${team.name} and id ${team.uuid} read by User $contextUser"
        }
    }
}

class TeamUpdateEvent(source: Any, private val team: Team, private val contextUser: AuthenticatedUserContextUUID)
    : BaseEvent<Team>(source, team, TeamEventCategory.TEAM, ApplicationEventAction.UPDATE) {
    override fun summary(): String {
        return "Team with name ${team.name} and id ${team.uuid} updated by User $contextUser"
    }
}

class TeamDeleteEvent(source: Any, private val team: Team, private val contextUser: AuthenticatedUserContextUUID)
    : BaseEvent<Team>(source, team, TeamEventCategory.TEAM, ApplicationEventAction.DELETE) {
    override fun summary(): String {
        return "Team with name ${team.name} and id ${team.uuid} deleted by User $contextUser"
    }
}

enum class TeamEventCategory: EventCategory {
    TEAM {
        override fun type(): String {
            return TEAM.name
        }
    }
}

