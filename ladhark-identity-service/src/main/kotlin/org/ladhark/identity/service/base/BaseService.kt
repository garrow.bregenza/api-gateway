package org.ladhark.identity.service.base

import org.ladhark.identity.messages.AppMessage
import org.ladhark.identity.properties.ApplicationProperties
import org.ladhark.kotlin.extension.s
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEvent
import org.springframework.context.ApplicationEventPublisher
import java.util.*

@JvmInline
value class AuthenticatedUserContextUUID(private val userUUID: UUID) {
    override fun toString(): String {
        return userUUID.s
    }
}

abstract class BaseService {

    @Autowired
    protected lateinit var applicationEventPublisher: ApplicationEventPublisher

    @Autowired
    protected lateinit var properties: ApplicationProperties

    @Autowired
    protected lateinit var appMessage: AppMessage


    fun publishEvent(event: ApplicationEvent) {
        applicationEventPublisher.publishEvent(event)
    }

    fun getMessage(code: String): String {
        return appMessage.getMessage(code)
    }

    fun getMessage(code: String, arg: String): String {
        return appMessage.getMessage(code, arg)
    }

    fun getMessage(code: String, args: Array<String>): String {
        return appMessage.getMessage(code, args)
    }
    /*
        fun <T> doInUserContextWith(value: T, _function: Function<String, Unit>): Mono<T> {
            return fetchUserContext().zipWith(Mono.just(value)) { uuid: String, value: T ->
                _function.apply(uuid)
                value
            }

        }

        fun checkContext(): Mono<Boolean> {
            return fetchServiceContext().flatMap {
                true.toMono()
            }
        }

        fun fetchUserContext(): Mono<String> {
            return fetchServiceContext().flatMap { serviceContext ->
                val principalUUID = serviceContext.authenticationToken.getPrincipal().principal.getUuid()
                Mono.just(principalUUID.s)
            }
        }

        fun fetchServiceContext(): Mono<ServiceContext> {
            return Mono.deferContextual { context ->
                if (context != null) {

                    if (!context.hasKey("SERVICE_CONTEXT")) {
                        return@deferContextual IllegalStateException("Context has no value for key SERVICE_CONTEXT").toMono()
                    }
                    val serviceContextHolder = context.get<ServiceContextHolder>("SERVICE_CONTEXT")
                    if (serviceContextHolder.serviceContext.isPresent) {
                        val serviceContext = serviceContextHolder.serviceContext.get()
                        Mono.just(serviceContext)
                    } else {
                        IllegalStateException("Context is empty").toMono()
                    }
                } else {
                    IllegalStateException("Context is empty").toMono()
                }
            }
        }
    */

    /*
    /**
     * Caching operations notoriously change class loaders leading to class cast exception event for same entity instance and class;
     * hence better we subscribe on known thread pool started during app startup keeping class loader hierarchy in line.
     */
    fun <T> subscribeOnBoundedElastic(cacheMono: Supplier<Mono<T>>): Mono<T> {
        return cacheMono.get().subscribeOn(Schedulers.boundedElastic())
    }
    */


    /*protected open fun<T> search(searchParams: SearchParams, clazz: Class<T>): SearchResult<T> {

        val session = SessionFactoryUtils.getSession(sessionFactory)

        // filters
        val filters = Filters(searchParams.filterBy?.entries?.map {
            val f = Filter(it.key, MatchMode.toNeo4j(it.value.matchMode), it.value.value)
            if (f.comparisonOperator.isOneOf(
                    ComparisonOperator.EQUALS,
                    ComparisonOperator.CONTAINING,
                    ComparisonOperator.STARTING_WITH,
                    ComparisonOperator.ENDING_WITH) && f.function is PropertyComparison
            ) {

                f.ignoreCase()
            }
            f
        })

        val deletedFilter = Filter("deleted", ComparisonOperator.EQUALS, false)
        if (!filters.isEmpty) {
            deletedFilter.booleanOperator = BooleanOperator.AND
        }

        filters.add(deletedFilter)

        // sort
        val sortOrders = SortOrder()
        searchParams.sortBy?.entries?.forEach {
            sortOrders.add(
                org.ladhark.identity.api.controller.search.SortOrder.toNeo4j(it.value.order),
                it.value.field
            )
        }

        //pagination
        val pageNumber = searchParams.offset / searchParams.pageSize
        val pagination = Pagination(pageNumber, searchParams.pageSize)

        // page result
        val result = session.loadAll(clazz, filters, sortOrders, pagination, 2).toSet()

        // total count
        val totalCount = session.count(clazz, filters)

        return SearchResult(result, totalCount, searchParams.offset + 1, result.size)
    }*/
}