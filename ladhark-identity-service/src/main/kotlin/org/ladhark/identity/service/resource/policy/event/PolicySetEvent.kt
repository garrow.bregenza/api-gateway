package org.ladhark.identity.service.resource.policy.event

import org.ladhark.identity.entity.resource.policy.Policy
import org.ladhark.identity.entity.resource.policy.PolicySet
import org.ladhark.identity.entity.resource.policy.ResourcePolicy
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class PolicySetCreateEvent(
    source: Any,
    private val policySet: PolicySet,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<PolicySet>(source, policySet, PolicySetEventCategory.POLICYSET, ApplicationEventAction.CREATE) {
    override fun summary(): String {
        return "PolicySet with name ${policySet.name} and id ${policySet.uuid} created by User $contextUser"
    }
}

class PolicySetReadEvent(
    source: Any,
    private val policySet: PolicySet,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<PolicySet>(source, policySet, PolicySetEventCategory.POLICYSET, ApplicationEventAction.READ) {
    override fun summary(): String {
        return summary.ifBlank {
            return "PolicySet with name ${policySet.name} and id ${policySet.uuid} read by User $contextUser"
        }
    }
}

class PolicySetUpdateEvent(
    source: Any,
    private val policySet: PolicySet,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<PolicySet>(source, policySet, PolicySetEventCategory.POLICYSET, ApplicationEventAction.UPDATE) {
    override fun summary(): String {
        return summary.ifBlank {
            return "PolicySet with name ${policySet.name} and id ${policySet.uuid} updated by User $contextUser"
        }
    }
}

class PolicySetDeleteEvent(
    source: Any,
    private val policySet: PolicySet,
    private val contextUser: AuthenticatedUserContextUUID,
    val parent: ResourcePolicy?,
    val children: Set<Policy> = emptySet()
) : BaseEvent<PolicySet>(source, policySet, PolicySetEventCategory.POLICYSET, ApplicationEventAction.DELETE) {
    override fun summary(): String {
        return "PolicySet with name ${policySet.name} and id ${policySet.uuid} deleted by User $contextUser"
    }
}

enum class PolicySetEventCategory : EventCategory {
    POLICYSET {
        override fun type(): String {
            return POLICYSET.name
        }
    }
}

