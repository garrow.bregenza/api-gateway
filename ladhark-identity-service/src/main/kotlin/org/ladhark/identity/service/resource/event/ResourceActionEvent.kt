package org.ladhark.identity.service.resource.event

import org.ladhark.identity.entity.resource.ResourceAction
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class ResourceActionCreateEvent(
    source: Any,
    private val resourceAction: ResourceAction,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<ResourceAction>(
    source,
    resourceAction,
    ResourceActionEventCategory.RESOURCE_ACTION,
    ApplicationEventAction.CREATE
) {
    override fun summary(): String {
        return "ResourceAction with name ${resourceAction.name} and id ${resourceAction.uuid} created by User $contextUser"
    }
}

class ResourceActionReadEvent(
    source: Any,
    private val resourceAction: ResourceAction,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<ResourceAction>(
    source,
    resourceAction,
    ResourceActionEventCategory.RESOURCE_ACTION,
    ApplicationEventAction.READ
) {
    override fun summary(): String {
        return summary.ifBlank {
            return "ResourceAction with name ${resourceAction.name} and id ${resourceAction.uuid} read by User $contextUser"
        }
    }
}

class ResourceActionUpdateEvent(
    source: Any,
    private val resourceAction: ResourceAction,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<ResourceAction>(
    source,
    resourceAction,
    ResourceActionEventCategory.RESOURCE_ACTION,
    ApplicationEventAction.UPDATE
) {
    override fun summary(): String {
        return "ResourceAction with name ${resourceAction.name} and id ${resourceAction.uuid} updated by User $contextUser"
    }
}

class ResourceActionDeleteEvent(
    source: Any,
    private val resourceAction: ResourceAction,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<ResourceAction>(
    source,
    resourceAction,
    ResourceActionEventCategory.RESOURCE_ACTION,
    ApplicationEventAction.DELETE
) {
    override fun summary(): String {
        return "ResourceAction with name ${resourceAction.name} and id ${resourceAction.uuid} deleted by User $contextUser"
    }
}

enum class ResourceActionEventCategory : EventCategory {
    RESOURCE_ACTION {
        override fun type(): String {
            return RESOURCE_ACTION.name
        }
    }
}

