package org.ladhark.identity.service.aspect.cache

import com.github.benmanes.caffeine.cache.AsyncCache
import com.github.benmanes.caffeine.cache.Caffeine
import jakarta.annotation.PostConstruct
import kotlinx.coroutines.sync.withLock
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.aspect.AspectType
import org.ladhark.identity.repository.aspect.AspectTypeRepository
import org.ladhark.identity.service.base.BaseCacheService
import org.ladhark.kotlin.extension.s
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.concurrent.TimeUnit

const val ASPECT_TYPE_CACHE_BY_UUID = "ASPECT_TYPE_CACHE_BY_UUID"
const val ASPECT_TYPE_CACHE_BY_NAME_TO_UUID = "ASPECT_TYPE_CACHE_BY_NAME_TO_UUID"

interface AspectTypeCacheService {
    suspend fun getAspectTypeByNameToUuidAsync(key: String): AspectType?
    suspend fun putAspectTypeByNameToUuidAsync(key: String, value: AspectType)
    suspend fun removeAspectTypeByNameToUuidAsync(key: String)

    suspend fun getAspectTypeByUuidAsync(key: String): AspectType?
    suspend fun putAspectTypeByUuidAsync(key: String, value: AspectType)
    suspend fun removeAspectTypeByUuidAsync(key: String)
}

@Service
class AspectTypeCacheServiceImpl : BaseCacheService(), AspectTypeCacheService {

    @Autowired
    lateinit var aspectTypeRepository: AspectTypeRepository

    lateinit var aspectTypeCacheByUUID: AsyncCache<String, AspectType>

    lateinit var aspectTypeCacheByNameToUUID: AsyncCache<String, String>

    @PostConstruct
    private fun postConstruct() {

        aspectTypeCacheByUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        aspectTypeCacheByNameToUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        registerProvider(this, ASPECT_TYPE_CACHE_BY_UUID, ASPECT_TYPE_CACHE_BY_NAME_TO_UUID)

        aspectTypeRepository.subscribeAndDoOnAll {
            with(it) {
                aspectTypeCacheByUUID.put(uuid.s, java.util.concurrent.CompletableFuture.completedFuture(this))
                aspectTypeCacheByNameToUUID.put(name, java.util.concurrent.CompletableFuture.completedFuture(uuid.s))
            }
        }
    }

    override suspend fun getAspectTypeByNameToUuidAsync(key: String): AspectType? {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.getValueAsync(key, aspectTypeCacheByNameToUUID)
            return uuid?.let {
                caffeineCacheHelperService.getValueAsync(it, aspectTypeCacheByUUID)
            }
        }
    }

    override suspend fun putAspectTypeByNameToUuidAsync(key: String, value: AspectType) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value.uuid.s, aspectTypeCacheByNameToUUID)
            caffeineCacheHelperService.putValueAsync(value.uuid.s, value, aspectTypeCacheByUUID)
        }
    }

    override suspend fun removeAspectTypeByNameToUuidAsync(key: String) {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.removeValueAsync(key, aspectTypeCacheByNameToUUID)
            uuid?.let {
                caffeineCacheHelperService.removeValueAsync(it, aspectTypeCacheByUUID)
            }
        }
    }

    override suspend fun getAspectTypeByUuidAsync(key: String): AspectType? {
        mutex.withLock {
            return caffeineCacheHelperService.getValueAsync(key, aspectTypeCacheByUUID)
        }
    }

    override suspend fun putAspectTypeByUuidAsync(key: String, value: AspectType) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value, aspectTypeCacheByUUID)
        }
    }

    override suspend fun removeAspectTypeByUuidAsync(key: String) {
        mutex.withLock {
            caffeineCacheHelperService.removeValueAsync(key, aspectTypeCacheByUUID)
        }
    }

    override suspend fun getFromCache(name: String, key: String): BaseEntity? {
        return when(name) {
            ASPECT_TYPE_CACHE_BY_UUID -> getAspectTypeByUuidAsync(key)
            ASPECT_TYPE_CACHE_BY_NAME_TO_UUID -> getAspectTypeByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun putInCache(name: String, key: String, value: BaseEntity) {
        if (value !is AspectType) {
            throw IllegalArgumentException("Cache value passed is not of type ${AspectType::class.qualifiedName}")
        }

        when(name) {
            ASPECT_TYPE_CACHE_BY_UUID -> putAspectTypeByUuidAsync(key, value)
            ASPECT_TYPE_CACHE_BY_NAME_TO_UUID -> putAspectTypeByNameToUuidAsync(key, value)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun removeFromCache(name: String, key: String) {
        when(name) {
            ASPECT_TYPE_CACHE_BY_UUID -> removeAspectTypeByUuidAsync(key)
            ASPECT_TYPE_CACHE_BY_NAME_TO_UUID -> removeAspectTypeByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

}