package org.ladhark.identity.service.region.event

import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class RegionCreateEvent(
    source: Any,
    private val region: Region,
    private val contextUser: AuthenticatedUserContextUUID
) :
    BaseEvent<Region>(source, region, RegionEventCategory.REGION, ApplicationEventAction.CREATE) {
    override fun summary(): String {
        return "Region with name ${region.name} and id ${region.uuid} created by User $contextUser"
    }
}

class RegionReadEvent(
    source: Any,
    private val region: Region,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<Region>(source, region, RegionEventCategory.REGION, ApplicationEventAction.READ) {
    override fun summary(): String {
        return summary.ifBlank {
            "Region with name ${region.name} and id ${region.uuid} read by User $contextUser"
        }
    }
}

class RegionPageReadEvent(
    source: Any,
    private val regionPage: RegionPage,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<RegionPage>(source, regionPage, RegionEventCategory.REGION, ApplicationEventAction.READ) {
    override fun summary(): String {
        return summary.ifBlank {
            "Region with page ${regionPage.uuidsCsv()} read by User $contextUser"
        }
    }
}

class RegionUpdateEvent(
    source: Any,
    private val region: Region,
    private val contextUser: AuthenticatedUserContextUUID
) :
    BaseEvent<Region>(source, region, RegionEventCategory.REGION, ApplicationEventAction.UPDATE) {
    override fun summary(): String {
        return "Region with name ${region.name} and id ${region.uuid} updated by User $contextUser"
    }
}

class RegionDeleteEvent(
    source: Any,
    private val region: Region,
    private val contextUser: AuthenticatedUserContextUUID
) :
    BaseEvent<Region>(source, region, RegionEventCategory.REGION, ApplicationEventAction.DELETE) {
    override fun summary(): String {
        return "Region with name ${region.name} and id ${region.uuid} deleted by User $contextUser"
    }
}

enum class RegionEventCategory : EventCategory {
    REGION {
        override fun type(): String {
            return REGION.name
        }
    }
}

data class RegionPage(val uuids: Set<String>) {
    fun uuidsCsv(): String {
        return uuids.joinToString(",")
    }
}