package org.ladhark.identity.service.resource.policy

import org.ladhark.identity.api.dto.policy.PolicyConditionDetails
import org.ladhark.identity.api.dto.policy.PolicyDetails
import org.ladhark.identity.api.dto.policy.PolicySetDetails
import org.ladhark.identity.entity.resource.policy.*
import org.ladhark.identity.exception.EntityNotFoundException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.repository.resource.policy.PolicyConditionRepository
import org.ladhark.identity.repository.resource.policy.PolicyRepository
import org.ladhark.identity.repository.resource.policy.PolicySetRepository
import org.ladhark.identity.repository.resource.policy.ResourcePolicyRepository
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID
import org.ladhark.identity.service.base.BaseService
import org.ladhark.identity.service.context.withUserContext
import org.ladhark.identity.service.resource.policy.event.*
import org.ladhark.kotlin.extension.s
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

interface PolicyService {
    suspend fun create(policyDetails: PolicyDetails): Policy
    suspend fun findByUuid(uuid: String): Policy?
    suspend fun findByName(name: String): Policy?
    // TODO this needs depth of 3 !!!!!
    //fun findAll(): Flux<Policy>
}

@Service
class PolicyServiceImpl : BaseService(), PolicyService {

    private val log: Logger = LoggerFactory.getLogger(PolicyServiceImpl::class.java)

    @Autowired
    private lateinit var policyRepository: PolicyRepository

    @Autowired
    private lateinit var policySetRepository: PolicySetRepository

    @Transactional(rollbackFor = [Exception::class])
    override suspend fun create(policyDetails: PolicyDetails): Policy = withUserContext {

        if (policyDetails.name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        if (policyDetails.policySetUuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Policy Set UUID"))
        }

        val isPresentPolicyAccessType = PolicyAccessType.STRING_SET.contains(policyDetails.policyAccessType)

        if (!isPresentPolicyAccessType) {
            throw InvalidArgumentException(
                appMessage.getMessage(
                    "error.value.required.is.invalid",
                    "Policy Access Type"
                )
            )
        }

        return policySetRepository
            .findPolicySetByUuid(policyDetails.policySetUuid.uuid)?.let { policySetFromDb ->
                publishEvent(
                    PolicySetReadEvent(
                        this, policySetFromDb, AuthenticatedUserContextUUID(principalUUID()),
                        appMessage.getMessage(
                            "message.policy.set.event.read.secondary.access",
                            arrayOf(
                                policySetFromDb.name,
                                policySetFromDb.uuid.s,
                                principalId(),
                                "`create policy`"
                            )
                        )
                    )
                )

                log.info("Saving policy details with name {}", policyDetails.name)

                val policy = Policy().apply {
                    name = policyDetails.name
                    description = policyDetails.description
                    policyAccessType = PolicyAccessType.valueOf(policyDetails.policyAccessType)
                }


                policyRepository
                    .save(policy)
                    .also { policyFromDb ->
                        publishEvent(
                            PolicyCreateEvent(
                                this, policyFromDb, AuthenticatedUserContextUUID(principalUUID())
                            )
                        )

                        // this should happen on create event listener ????
                        policySetFromDb.hasPolicies.add(policyFromDb)

                        policySetRepository
                            .save(policySetFromDb)
                            .also { policySetUpdatedFromDb ->
                                publishEvent(
                                    PolicySetUpdateEvent(
                                        this, policySetUpdatedFromDb, AuthenticatedUserContextUUID(principalUUID()),
                                        appMessage.getMessage(
                                            "message.policy.set.event.update.policy.added",
                                            arrayOf(
                                                policySetUpdatedFromDb.name,
                                                policySetUpdatedFromDb.uuid.s,
                                                principalId(),
                                                "`create policy`"
                                            )
                                        )
                                    )
                                )
                            }
                    }
            } ?: throw EntityNotFoundException(
            getMessage(
                "error.resource.policy.set.not.found.by.uuid",
                policyDetails.policySetUuid
            )
        )

    }

    @Transactional(readOnly = true)
    override suspend fun findByUuid(uuid: String): Policy? = withUserContext {

        if (uuid.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching policy details for uuid {}", uuid)

        return policyRepository
            .findPolicyByUuid(uuid.uuid)?.also { policyFromDb ->
                publishEvent(
                    PolicyReadEvent(this, policyFromDb, AuthenticatedUserContextUUID(principalUUID()))
                )
            }
    }

    @Transactional(readOnly = true)
    override suspend fun findByName(name: String): Policy? = withUserContext {

        if (name.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "Name"))
        }

        log.info("Fetching policy details for name {}", name)

        return policyRepository
            .findPolicyByName(name)?.also { policyFromDb ->
                publishEvent(
                    PolicyReadEvent(this, policyFromDb, AuthenticatedUserContextUUID(principalUUID()))
                )
            }
    }
}


interface PolicySetService {
    suspend fun create(policySetDetails: PolicySetDetails): PolicySet
    suspend fun findByUuid(uuid: String): PolicySet?
    suspend fun findByName(name: String): PolicySet?
}

@Service
class PolicySetServiceImpl : BaseService(), PolicySetService {

    private val log: Logger = LoggerFactory.getLogger(PolicySetServiceImpl::class.java)

    @Autowired
    private lateinit var policySetRepository: PolicySetRepository

    @Autowired
    private lateinit var resourcePolicyRepository: ResourcePolicyRepository

    @Transactional(rollbackFor = [Exception::class])
    override suspend fun create(policySetDetails: PolicySetDetails): PolicySet = withUserContext {

        if (policySetDetails.name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        if (policySetDetails.resourcePolicyUuid.isBlank()) {
            throw InvalidArgumentException(
                appMessage.getMessage(
                    "error.value.required.is.empty",
                    "Resource Policy UUID"
                )
            )
        }

        val isPresentPolicyEvaluationType =
            PolicyEvaluationType.STRING_SET.contains(policySetDetails.policyEvaluationType)

        if (!isPresentPolicyEvaluationType) {
            throw InvalidArgumentException(
                appMessage.getMessage(
                    "error.value.required.is.invalid",
                    "Policy Evaluation Type"
                )
            )
        }

        return resourcePolicyRepository
            .findResourcePolicyByUuid(policySetDetails.resourcePolicyUuid.uuid)?.let { resourcePolicyFromDb ->
                publishEvent(
                    ResourcePolicyReadEvent(
                        this, resourcePolicyFromDb, AuthenticatedUserContextUUID(principalUUID()),
                        appMessage.getMessage(
                            "message.resource.policy.event.read.secondary.access",
                            arrayOf(
                                resourcePolicyFromDb.name,
                                resourcePolicyFromDb.uuid.s,
                                principalId(),
                                "`create policy set`"
                            )
                        )
                    )
                )

                log.info("Saving policy set details with name {}", policySetDetails.name)

                val policySet = PolicySet().apply {
                    name = policySetDetails.name
                    description = policySetDetails.description
                    policyEvaluationType = PolicyEvaluationType.valueOf(policySetDetails.policyEvaluationType)
                }


                policySetRepository
                    .save(policySet)
                    .also { policySetFromDb ->
                        publishEvent(
                            PolicySetCreateEvent(
                                this, policySetFromDb, AuthenticatedUserContextUUID(principalUUID())
                            )
                        )

                        resourcePolicyFromDb.policySet = policySetFromDb

                        resourcePolicyRepository
                            .save(resourcePolicyFromDb)
                            .also { resourcePolicyUpdatedFromDb ->
                                publishEvent(
                                    ResourcePolicyUpdateEvent(
                                        this, resourcePolicyUpdatedFromDb, AuthenticatedUserContextUUID(principalUUID()),
                                        appMessage.getMessage(
                                            "message.resource.policy.event.update.policy.set.added",
                                            arrayOf(
                                                resourcePolicyUpdatedFromDb.name,
                                                resourcePolicyUpdatedFromDb.uuid.s,
                                                principalId(),
                                                "`create policy set`"
                                            )
                                        )
                                    )
                                )
                            }
                    }
            } ?: throw EntityNotFoundException(
            appMessage.getMessage(
                "error.resource.policy.not.found.by.uuid", policySetDetails.resourcePolicyUuid
            )
        )
    }

    @Transactional(readOnly = true)
    override suspend fun findByUuid(uuid: String): PolicySet? = withUserContext {

        if (uuid.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching policy set details for uuid {}", uuid)

        return policySetRepository.findPolicySetByUuid(uuid.uuid)?.also { policySetFromDb ->
                publishEvent(PolicySetReadEvent(this, policySetFromDb, AuthenticatedUserContextUUID(principalUUID())))
            }
    }

    @Transactional(readOnly = true)
    override suspend fun findByName(name: String): PolicySet? = withUserContext {

        if (name.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "Name"))
        }

        log.info("Fetching policy set details for name {}", name)

        return policySetRepository.findPolicySetByName(name)?.also { policySetFromDb ->
                publishEvent(PolicySetReadEvent(this, policySetFromDb, AuthenticatedUserContextUUID(principalUUID())))
            }
    }

}

interface PolicyConditionService {
    suspend fun create(policyConditionDetails: PolicyConditionDetails): PolicyCondition
    suspend fun findByUuid(uuid: String): PolicyCondition?
}

@Service
class PolicyConditionServiceImpl: BaseService(), PolicyConditionService {

    val log: Logger = LoggerFactory.getLogger(PolicyConditionServiceImpl::class.java)

    @Autowired
    lateinit var policyConditionRepository: PolicyConditionRepository

    @Autowired
    lateinit var policyRepository: PolicyRepository

    @Transactional(rollbackFor = [Exception::class])
    override suspend fun create(policyConditionDetails: PolicyConditionDetails): PolicyCondition = withUserContext {

        if (policyConditionDetails.key.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Policy Condition Key"))
        }

        if (policyConditionDetails.value.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Policy Condition Value"))
        }

        if (policyConditionDetails.policyUuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Policy UUID"))
        }

        val isPresentPolicyConditionType = PolicyConditionType.STRING_SET.contains(policyConditionDetails.policyConditionType)

        if (!isPresentPolicyConditionType) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.invalid", "Policy Condition Type"))
        }

        return policyRepository
            .findPolicyByUuid(policyConditionDetails.policyUuid.uuid)?.let { policy ->
                publishEvent(PolicyReadEvent(this, policy, AuthenticatedUserContextUUID(principalUUID()),
                    appMessage.getMessage("message.policy.event.read.secondary.access",
                        arrayOf(policy.name, policy.uuid.s, principalId(), "`create policy condition`"))))

                val policyCondition = PolicyCondition()
                policyCondition.policyConditionType = PolicyConditionType.valueOf(policyConditionDetails.policyConditionType)
                policyCondition.key = policyConditionDetails.key
                policyCondition.value = policyConditionDetails.value

                policyConditionRepository.save(policyCondition).also { policyConditionFromDb ->

                    publishEvent(PolicyConditionCreateEvent(this, policyConditionFromDb, AuthenticatedUserContextUUID(principalUUID())))

                    policy.hasPolicyConditions.add(policyConditionFromDb)
                    policyRepository.save(policy)

                    publishEvent(PolicyUpdateEvent(this, policy, AuthenticatedUserContextUUID(principalUUID()),
                        appMessage.getMessage("message.policy.event.update.condition.added",
                            arrayOf(policy.name, policy.uuid.s, principalId(), "`create policy condition`"))))
                }



            }
            ?: throw EntityNotFoundException(getMessage("error.resource.policy.not.found.by.uuid", policyConditionDetails.policyUuid))

    }

    @Transactional(readOnly = true)
    override suspend fun findByUuid(uuid: String): PolicyCondition? = withUserContext {

        if (uuid.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching policy condition details for uuid {}", uuid)

        return policyConditionRepository.findPolicyConditionByUuid(uuid.uuid)?.also { policyCondition ->
                publishEvent(PolicyConditionReadEvent(this, policyCondition, AuthenticatedUserContextUUID(principalUUID())))
            }
    }
}