package org.ladhark.identity.service.team

import org.ladhark.identity.api.dto.team.TeamDetails
import org.ladhark.identity.aspect.cache.GetFromCache
import org.ladhark.identity.aspect.cache.PutInCache
import org.ladhark.identity.aspect.cache.PutInCaches
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.entity.team.Team
import org.ladhark.identity.exception.EntityNotFoundException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.repository.region.RegionRepository
import org.ladhark.identity.repository.team.TeamRepository
import org.ladhark.identity.service.application.cache.APPLICATION_CACHE_BY_NAME_TO_UUID
import org.ladhark.identity.service.application.cache.APPLICATION_CACHE_BY_UUID
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID
import org.ladhark.identity.service.base.BaseService
import org.ladhark.identity.service.context.withUserContext
import org.ladhark.identity.service.region.event.RegionReadEvent
import org.ladhark.identity.service.team.cache.TEAM_CACHE_BY_NAME_TO_UUID
import org.ladhark.identity.service.team.cache.TEAM_CACHE_BY_UUID
import org.ladhark.identity.service.team.event.TeamCreateEvent
import org.ladhark.identity.service.team.event.TeamReadEvent
import org.ladhark.kotlin.extension.s
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

interface TeamService {
    suspend fun createTeam(teamDetails: TeamDetails): Team
    suspend fun findByUuid(uuid: String): Team?
    suspend fun findByName(name: String): Team?
}

@Service
class TeamServiceImpl: BaseService(), TeamService {

    val log: Logger = LoggerFactory.getLogger(TeamServiceImpl::class.java)

    @Autowired
    lateinit var teamRepository: TeamRepository

    @Autowired
    lateinit var regionRepository: RegionRepository

    @Transactional(readOnly = true)
    @GetFromCache(APPLICATION_CACHE_BY_UUID)
    override suspend fun findByUuid(uuid: String): Team? = withUserContext {

        if (uuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching team details for uuid {}", uuid)

        return teamRepository.findTeamByUuid(uuid.uuid)?.also { team ->
            publishEvent(TeamReadEvent(this, team, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    @Transactional(readOnly = true)
    @GetFromCache(APPLICATION_CACHE_BY_NAME_TO_UUID)
    override suspend fun findByName(name: String): Team? = withUserContext {

        if (name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        log.info("Fetching team details for name {}", name)

        return teamRepository.findTeamByName(name)?.also { team ->
            publishEvent(TeamReadEvent(this, team, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    @Transactional(rollbackFor = [Exception::class])
    @PutInCaches(caches = [
        PutInCache(TEAM_CACHE_BY_UUID),
        PutInCache(TEAM_CACHE_BY_NAME_TO_UUID),
    ])
    override suspend fun createTeam(teamDetails: TeamDetails): Team = withUserContext {

        if (teamDetails.name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        var parentTeamFromDb: Team? = null
        if (teamDetails.parentTeamUuid.isBlank()) {
            parentTeamFromDb = teamRepository.findTeamByUuid(teamDetails.parentTeamUuid.uuid) ?:
                   throw EntityNotFoundException(appMessage.getMessage("error.parent.team.not.found.by.uuid", teamDetails.parentTeamUuid))

        }

        if (teamDetails.regionUuids.isEmpty()) {
            throw InvalidArgumentException(appMessage.getMessage("error.team.at.least.one.region.required"))
        }

        val regions: MutableSet<Region> = HashSet()
        teamDetails.regionUuids.forEach {
            regionRepository.findRegionByUuid(it.uuid)?.also { regionFromDb ->
                publishEvent(RegionReadEvent(this, regionFromDb, AuthenticatedUserContextUUID(principalUUID()),
                    appMessage.getMessage("message.region.event.read.secondary.access",
                        arrayOf(regionFromDb.name, regionFromDb.uuid.s, principalId(), "`create team`"))))

                regions.add(regionFromDb)
            } ?: throw EntityNotFoundException(appMessage.getMessage("error.region.not.found.by.uuid", it))
        }

        val team = Team().apply {
            name = teamDetails.name
            parentTeam = parentTeamFromDb
            regions.addAll(regions)
        }


        return teamRepository.save(team).also { teamFromDb ->
            publishEvent(TeamCreateEvent(this, teamFromDb, AuthenticatedUserContextUUID(principalUUID())))
        }
    }
}