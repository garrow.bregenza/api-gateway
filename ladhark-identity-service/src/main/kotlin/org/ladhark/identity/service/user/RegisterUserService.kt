package org.ladhark.identity.service.user

import org.ladhark.identity.api.dto.user.RegistrationDetails
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.user.Name
import org.ladhark.identity.entity.user.User
import org.ladhark.identity.entity.user.UserCredentials
import org.ladhark.identity.entity.user.UserHasRegion
import org.ladhark.identity.exception.EntityAlreadyPresentException
import org.ladhark.identity.exception.EntityNotFoundException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.repository.region.RegionRepository
import org.ladhark.identity.repository.user.UserCredentialsRepository
import org.ladhark.identity.repository.user.UserRepository
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID
import org.ladhark.identity.service.base.BaseService
import org.ladhark.identity.service.region.event.RegionReadEvent
import org.ladhark.identity.service.user.event.RegistrationFailEvent
import org.ladhark.identity.service.user.event.RegistrationSuccessEvent
import org.ladhark.identity.service.user.event.RegistrationSuccessEventPayload
import org.ladhark.kotlin.extension.s
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

interface RegisterUserService {
    suspend fun createUser(registrationDetails: RegistrationDetails): User
}

@Service
class RegisterUserServiceImpl : BaseService(), RegisterUserService {

    private val log: Logger = LoggerFactory.getLogger(RegisterUserServiceImpl::class.java)

    @Autowired
    lateinit var passwordEncoder: PasswordEncoder

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var userCredentialsRepository: UserCredentialsRepository

    @Autowired
    lateinit var regionRepository: RegionRepository

    @Transactional(rollbackFor = [Exception::class])
    override suspend fun createUser(registrationDetails: RegistrationDetails): User {

        log.info("Attempting user registration for email {}", registrationDetails.email)

        if (registrationDetails.inValid()) {
            val errorMsg = appMessage.getMessage("error.entity.invalid.data", "Registration Details")
            applicationEventPublisher.publishEvent(RegistrationFailEvent(this, registrationDetails, errorMsg))
            throw InvalidArgumentException(errorMsg)
        }

        return userRepository.findUserByEmail(registrationDetails.email)?.let { user ->
            val e = EntityAlreadyPresentException(appMessage.getMessage("error.entity.already.present", "User"))

            applicationEventPublisher.publishEvent(
                RegistrationFailEvent(
                    this,
                    registrationDetails,
                    e.message.orEmpty()
                )
            )
            throw e
        } ?: run {
            val userRegions = registrationDetails
                .regionUuids
                .map { userRegionDetails ->
                    regionRepository
                        .findRegionByUuid(userRegionDetails.regionUuid.uuid)?.let { regionFromDb ->

                            val userHasRegion = UserHasRegion().apply {
                                primary = userRegionDetails.primary
                                childrenAccess = userRegionDetails.childrenAccess
                                region = regionFromDb
                            }


                            publishEvent(
                                RegionReadEvent(
                                    this, regionFromDb, AuthenticatedUserContextUUID(UUID.randomUUID()),
                                    appMessage.getMessage(
                                        "message.region.event.read.secondary.access",
                                        arrayOf(
                                            regionFromDb.name,
                                            regionFromDb.uuid.s,
                                            this::javaClass.name,
                                            "`register user`"
                                        )
                                    )
                                )
                            )

                            userHasRegion

                        } ?: throw EntityNotFoundException(
                        appMessage.getMessage("error.region.not.found.by.uuid", userRegionDetails.regionUuid)
                    )
                }

            val user = User().apply {
                email = registrationDetails.email
                name = Name(
                    registrationDetails.name.first,
                    registrationDetails.name.middle,
                    registrationDetails.name.last
                )
                locked = false
                active = true
            }

            if (userRegions.isNotEmpty()) {
                user.hasRegions.addAll(userRegions)
            }

            val sefTeamUUID = BaseEntity.generateUuid()
            val exclusiveTeamUUID = BaseEntity.generateUuid()

            user.initOnCreateTeams(sefTeamUUID, exclusiveTeamUUID)

            userRepository
                .save(user)
                .also { userSaved ->

                    publishEvent(
                        RegistrationSuccessEvent(
                            this,
                            RegistrationSuccessEventPayload(registrationDetails, userSaved)
                        )
                    )

                    val userCredentials = UserCredentials().apply {
                        password = passwordEncoder.encode(registrationDetails.password)
                        this.user = userSaved
                    }

                    userCredentialsRepository.save(userCredentials)
                }
        }

    }

}