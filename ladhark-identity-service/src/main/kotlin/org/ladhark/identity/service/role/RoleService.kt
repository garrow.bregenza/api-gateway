package org.ladhark.identity.service.role

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import org.ladhark.identity.api.dto.role.RoleDetails
import org.ladhark.identity.aspect.cache.GetFromCache
import org.ladhark.identity.aspect.cache.PutInCache
import org.ladhark.identity.aspect.cache.PutInCaches
import org.ladhark.identity.entity.role.Role
import org.ladhark.identity.exception.EntityAlreadyPresentException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.repository.role.RoleRepository
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID
import org.ladhark.identity.service.base.BaseService
import org.ladhark.identity.service.context.withUserContext
import org.ladhark.identity.service.role.cache.ROLE_CACHE_BY_NAME_TO_UUID
import org.ladhark.identity.service.role.cache.ROLE_CACHE_BY_UUID
import org.ladhark.identity.service.role.cache.RoleCacheService
import org.ladhark.identity.service.role.event.RoleCreateEvent
import org.ladhark.identity.service.role.event.RolePage
import org.ladhark.identity.service.role.event.RolePageReadEvent
import org.ladhark.identity.service.role.event.RoleReadEvent
import org.ladhark.identity.service.user.event.UserRoleAddEvent
import org.ladhark.kotlin.extension.s
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.EventListener
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

interface RoleService {
    suspend fun findByUuid(uuid: String): Role?
    suspend fun findByName(name: String): Role?
    suspend fun createRole(roleDetails: RoleDetails): Role
    suspend fun findAll(): List<Role>
}

@Service
class RoleServiceImpl : BaseService(), RoleService {

    val log: Logger = LoggerFactory.getLogger(RoleServiceImpl::class.java)

    @Autowired
    lateinit var roleRepository: RoleRepository

    @Autowired
    lateinit var roleCacheService: RoleCacheService


    @Transactional(readOnly = true)
    @GetFromCache(ROLE_CACHE_BY_UUID)
    override suspend fun findByUuid(uuid: String): Role? = withUserContext {

        if (uuid.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching role details for uuid {}", uuid)

        return roleRepository.findRoleByUuid(uuid.uuid)?.also { role ->
                publishEvent(RoleReadEvent(this, role, AuthenticatedUserContextUUID(principalUUID())))
            }

    }

    @Transactional(readOnly = true)
    @GetFromCache(ROLE_CACHE_BY_NAME_TO_UUID)
    override suspend fun findByName(name: String): Role? = withUserContext {

        if (name.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "Name"))
        }

        log.info("Fetching role details for name {}", name)

        return roleRepository.findRoleByName(name)?.also { role ->
                publishEvent(RoleReadEvent(this, role, AuthenticatedUserContextUUID(principalUUID())))
            }
    }

    @Transactional(rollbackFor = [Exception::class])
    @PutInCaches(caches = [
        PutInCache(ROLE_CACHE_BY_UUID),
        PutInCache(ROLE_CACHE_BY_NAME_TO_UUID),
    ])
    override suspend fun createRole(roleDetails: RoleDetails): Role = withUserContext {

        log.info("Creating role by name {}", roleDetails.name)

        return roleRepository.findRoleByName(roleDetails.name)?.let {
            throw EntityAlreadyPresentException(
                appMessage.getMessage(
                    "error.entity.already.present.same.name",
                    roleDetails.name
                )
            )
        } ?: run {

            val role = Role().apply {
                name = roleDetails.name
                roleDetails.description?.let { descriptionFromDetails ->
                    description = descriptionFromDetails
                }
            }


            roleRepository.save(role).also { roleFromDb ->
                publishEvent(RoleCreateEvent(this, roleFromDb, AuthenticatedUserContextUUID(principalUUID())))
            }
        }
    }

    @Transactional(readOnly = true)
    override suspend fun findAll(): List<Role> = withUserContext {

        return roleRepository.findAll().toList().also { roles ->
            val regionPage = RolePage(roles.map { it.uuid.s }.toSet())

            publishEvent(
                RolePageReadEvent(
                    this, regionPage, AuthenticatedUserContextUUID(principalUUID()),
                    appMessage.getMessage(
                        "message.region.event.read.page.access",
                        arrayOf(regionPage.uuidsCsv(), principalId(), "`page region`")
                    )
                )
            )
        }
    }

    //@EventListener(UserRoleAddEvent::class)
    @Async
    @EventListener
    fun onEvent(event: UserRoleAddEvent) {
        val role = event.payload.second
        log.info("Removing from cache role with uuid {} as updated for user role region event", role.uuid)
        CoroutineScope(Dispatchers.Unconfined).launch {
            roleCacheService.removeRoleByNameToUuidAsync(role.name)
        }
    }

}