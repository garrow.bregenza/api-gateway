package org.ladhark.identity.service.region.cache

import com.github.benmanes.caffeine.cache.AsyncCache
import com.github.benmanes.caffeine.cache.Caffeine
import jakarta.annotation.PostConstruct
import kotlinx.coroutines.sync.withLock
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.repository.region.RegionRepository
import org.ladhark.identity.service.base.BaseCacheService
import org.ladhark.kotlin.extension.s
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

const val REGION_CACHE_BY_UUID = "REGION_CACHE_BY_UUID"
const val REGION_CACHE_BY_NAME_TO_UUID = "REGION_CACHE_BY_NAME_TO_UUID"

interface RegionCacheService {
    suspend fun getRegionByNameToUuidAsync(key: String): Region?
    suspend fun putRegionByNameToUuidAsync(key: String, value: Region)
    suspend fun removeRegionByNameToUuidAsync(key: String)
    suspend fun getRegionByUuidAsync(key: String): Region?
    suspend fun putRegionByUuidAsync(key: String, value: Region)
    suspend fun removeRegionByUuidAsync(key: String)
}

@Service
class RegionCacheServiceImpl : BaseCacheService(), RegionCacheService {

    @Autowired
    private lateinit var regionRepository: RegionRepository

    private lateinit var regionCacheByUUID: AsyncCache<String, Region>

    private lateinit var regionCacheByNameToUUID: AsyncCache<String, String>

    @PostConstruct
    private fun postConstruct() {

        regionCacheByUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.HOURS)
            .buildAsync()

        regionCacheByNameToUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.HOURS)
            .buildAsync()

        registerProvider(this, REGION_CACHE_BY_UUID, REGION_CACHE_BY_NAME_TO_UUID)

        regionRepository.subscribeAndDoOnAll {
            with(it) {
                regionCacheByUUID.put(uuid.s, CompletableFuture.completedFuture(this))
                regionCacheByNameToUUID.put(name, CompletableFuture.completedFuture(uuid.s))
            }
        }

    }

    override suspend fun getRegionByNameToUuidAsync(key: String): Region? {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.getValueAsync(key, regionCacheByNameToUUID)
            return uuid?.let {
                caffeineCacheHelperService.getValueAsync(it, regionCacheByUUID)
            }
        }
    }

    override suspend fun putRegionByNameToUuidAsync(key: String, value: Region) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value.uuid.s, regionCacheByNameToUUID)
            caffeineCacheHelperService.putValueAsync(value.uuid.s, value, regionCacheByUUID)
        }
    }

    override suspend fun removeRegionByNameToUuidAsync(key: String) {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.removeValueAsync(key, regionCacheByNameToUUID)
            uuid?.let {
                caffeineCacheHelperService.removeValueAsync(it, regionCacheByUUID)
            }
        }
    }

    override suspend fun getRegionByUuidAsync(key: String): Region? {
        mutex.withLock {
            return caffeineCacheHelperService.getValueAsync(key, regionCacheByUUID)
        }
    }

    override suspend fun putRegionByUuidAsync(key: String, value: Region) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value, regionCacheByUUID)
        }
    }

    override suspend fun removeRegionByUuidAsync(key: String) {
        mutex.withLock {
            caffeineCacheHelperService.removeValueAsync(key, regionCacheByUUID)
        }
    }

    override suspend fun getFromCache(name: String, key: String): BaseEntity? {
        return when(name) {
            REGION_CACHE_BY_UUID -> getRegionByUuidAsync(key)
            REGION_CACHE_BY_NAME_TO_UUID -> getRegionByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun putInCache(name: String, key: String, value: BaseEntity) {

        if (value !is Region) {
            throw IllegalArgumentException("Cache value passed is not of type ${Region::class.qualifiedName}")
        }

        when(name) {
            REGION_CACHE_BY_UUID -> putRegionByUuidAsync(key, value)
            REGION_CACHE_BY_NAME_TO_UUID -> putRegionByNameToUuidAsync(key, value)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun removeFromCache(name: String, key: String) {
        when(name) {
            REGION_CACHE_BY_UUID -> removeRegionByUuidAsync(key)
            REGION_CACHE_BY_NAME_TO_UUID -> removeRegionByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }
}