package org.ladhark.identity.service.user.event

import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.entity.role.Role
import org.ladhark.identity.entity.user.User
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class UserCreateEvent(source: Any, private val user: User, private val contextUser: AuthenticatedUserContextUUID) :
    BaseEvent<User>(source, user, UserEventCategory.USER, ApplicationEventAction.CREATE) {
    override fun summary(): String {
        return "User with email ${user.email} and id ${user.uuid} created by user $contextUser"
    }
}

class UserReadEvent(
    source: Any,
    private val user: User,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<User>(source, user, UserEventCategory.USER, ApplicationEventAction.READ) {
    override fun summary(): String {
        return summary.ifBlank {
            "Region with name ${user.email} and id ${user.uuid} read by User $contextUser"
        }
    }
}

class UserUpdateEvent(source: Any, private val user: User, private val contextUser: AuthenticatedUserContextUUID) :
    BaseEvent<User>(source, user, UserEventCategory.USER, ApplicationEventAction.UPDATE) {
    override fun summary(): String {
        return "User with email ${user.email} and id ${user.uuid} updated by user $contextUser"
    }
}

class UserDeleteEvent(source: Any, private val user: User, private val contextUser: AuthenticatedUserContextUUID) :
    BaseEvent<User>(source, user, UserEventCategory.USER, ApplicationEventAction.DELETE) {
    override fun summary(): String {
        return "User with email ${user.email} and id ${user.uuid} deleted by user $contextUser"
    }
}

class UserRoleAddEvent(
    source: Any,
    private val user: User,
    private val role: Role,
    private val region: Region?,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<Triple<User, Role, Region?>>(
    source,
    Triple(user, role, region),
    UserEventCategory.USER,
    ApplicationEventAction.SUCCESS
) {
    override fun summary(): String {
        return if (region != null) {
            "Role with name ${role.name}/${role.uuid} added to user in region ${region.name}/${region.uuid} with email ${user.email} and id ${user.uuid} by user $contextUser"
        } else {
            "Role with name ${role.name}/${role.uuid} added to user with email ${user.email} and id ${user.uuid} by user $contextUser"
        }
    }
}

class UserRoleRemoveEvent(
    source: Any,
    private val user: User,
    private val role: Role,
    private val region: Region?,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<Triple<User, Role, Region?>>(
    source,
    Triple(user, role, region),
    UserEventCategory.USER,
    ApplicationEventAction.FAILURE
) {
    override fun summary(): String {
        return if (region != null) {
            "Role with name ${role.name}/${role.uuid} removed from user in region ${region.name}/${region.uuid} with email ${user.email} and id ${user.uuid} by user $contextUser"
        } else {
            "Role with name ${role.name}/${role.uuid} removed from user with email ${user.email} and id ${user.uuid} by user $contextUser"
        }
    }
}

enum class UserEventCategory : EventCategory {
    USER {
        override fun type(): String {
            return USER.name
        }
    }
}

