package org.ladhark.identity.service.resource.event

import org.ladhark.identity.entity.resource.Resource
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class ResourceCreateEvent(
    source: Any,
    private val resource: Resource,
    private val contextUser: AuthenticatedUserContextUUID
) :
    BaseEvent<Resource>(source, resource, ResourceEventCategory.RESOURCE, ApplicationEventAction.CREATE) {
    override fun summary(): String {
        return "Resource with name ${resource.name} and id ${resource.uuid} created by User $contextUser"
    }
}

class ResourceReadEvent(
    source: Any,
    private val resource: Resource,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<Resource>(source, resource, ResourceEventCategory.RESOURCE, ApplicationEventAction.READ) {
    override fun summary(): String {
        return summary.ifBlank {
            return "Resource with name ${resource.name} and id ${resource.uuid} read by User $contextUser"
        }
    }
}

class ResourceUpdateEvent(
    source: Any,
    private val resource: Resource,
    private val contextUser: AuthenticatedUserContextUUID
) :
    BaseEvent<Resource>(source, resource, ResourceEventCategory.RESOURCE, ApplicationEventAction.UPDATE) {
    override fun summary(): String {
        return "Resource with name ${resource.name} and id ${resource.uuid} updated by User $contextUser"
    }
}

class ResourceDeleteEvent(
    source: Any,
    private val resource: Resource,
    private val contextUser: AuthenticatedUserContextUUID
) :
    BaseEvent<Resource>(source, resource, ResourceEventCategory.RESOURCE, ApplicationEventAction.DELETE) {
    override fun summary(): String {
        return "Resource with name ${resource.name} and id ${resource.uuid} deleted by User $contextUser"
    }
}

enum class ResourceEventCategory : EventCategory {
    RESOURCE {
        override fun type(): String {
            return RESOURCE.name
        }
    }
}

