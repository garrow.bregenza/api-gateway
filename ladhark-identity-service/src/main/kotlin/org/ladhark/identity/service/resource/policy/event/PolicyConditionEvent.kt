package org.ladhark.identity.service.resource.policy.event

import org.ladhark.identity.entity.resource.policy.PolicyCondition
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class PolicyConditionCreateEvent(
    source: Any,
    private val policyCondition: PolicyCondition,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<PolicyCondition>(
    source,
    policyCondition,
    PolicyConditionEventCategory.POLICY_CONDITION,
    ApplicationEventAction.CREATE
) {
    override fun summary(): String {
        return "PolicyCondition with id ${policyCondition.uuid} created by User $contextUser"
    }
}

class PolicyConditionReadEvent(
    source: Any,
    private val policyCondition: PolicyCondition,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<PolicyCondition>(
    source,
    policyCondition,
    PolicyConditionEventCategory.POLICY_CONDITION,
    ApplicationEventAction.READ
) {
    override fun summary(): String {
        return summary.ifBlank {
            return "PolicyCondition with id ${policyCondition.uuid} read by User $contextUser"
        }
    }
}

class PolicyConditionUpdateEvent(
    source: Any,
    private val policyCondition: PolicyCondition,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<PolicyCondition>(
    source,
    policyCondition,
    PolicyConditionEventCategory.POLICY_CONDITION,
    ApplicationEventAction.UPDATE
) {
    override fun summary(): String {
        return "PolicyCondition with id ${policyCondition.uuid} updated by User $contextUser"
    }
}

class PolicyConditionDeleteEvent(
    source: Any,
    private val policyCondition: PolicyCondition,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<PolicyCondition>(
    source,
    policyCondition,
    PolicyConditionEventCategory.POLICY_CONDITION,
    ApplicationEventAction.DELETE
) {
    override fun summary(): String {
        return "PolicyCondition with id ${policyCondition.uuid} deleted by User $contextUser"
    }
}

enum class PolicyConditionEventCategory : EventCategory {
    POLICY_CONDITION {
        override fun type(): String {
            return POLICY_CONDITION.name
        }
    }
}

