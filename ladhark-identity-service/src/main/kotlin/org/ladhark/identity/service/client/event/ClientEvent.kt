package org.ladhark.identity.service.client.event

import org.ladhark.identity.entity.client.Client
import org.ladhark.identity.entity.role.Role
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class ClientCreateEvent(
    source: Any,
    private val client: Client,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<Client>(source, client, ClientEventCategory.CLIENT, ApplicationEventAction.CREATE) {
    override fun summary(): String {
        return "Client with name ${client.name} and id ${client.uuid} created by User $contextUser"
    }
}

class ClientReadEvent(
    source: Any,
    private val client: Client,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<Client>(source, client, ClientEventCategory.CLIENT, ApplicationEventAction.READ) {
    override fun summary(): String {
        return summary.ifBlank {
            return "Client with name ${client.name} and id ${client.uuid} read by User $contextUser"
        }
    }
}

class ClientPageReadEvent(
    source: Any,
    private val clientPage: ClientPage,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<ClientPage>(source, clientPage, ClientEventCategory.CLIENT, ApplicationEventAction.READ) {
    override fun summary(): String {
        return summary.ifBlank {
            return "Client with page ${clientPage.uuidsCsv()} read by User $contextUser"
        }
    }
}

class ClientUpdateEvent(
    source: Any,
    private val client: Client,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<Client>(source, client, ClientEventCategory.CLIENT, ApplicationEventAction.UPDATE) {
    override fun summary(): String {
        return "Client with name ${client.name} and id ${client.uuid} updated by User $contextUser"
    }
}

class ClientDeleteEvent(
    source: Any,
    private val client: Client,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<Client>(source, client, ClientEventCategory.CLIENT, ApplicationEventAction.DELETE) {
    override fun summary(): String {
        return "Client with name ${client.name} and id ${client.uuid} deleted by User $contextUser"
    }
}

class ClientRoleAddEvent(
    source: Any,
    private val client: Client,
    private val role: Role,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<Pair<Client, Role>>(
    source,
    Pair(client, role),
    ClientEventCategory.CLIENT,
    ApplicationEventAction.SUCCESS
) {
    override fun summary(): String {
        return "Role with name ${role.name}/${role.uuid} added to client with email ${client.email} and id ${client.uuid} by user $contextUser"
    }
}

enum class ClientEventCategory : EventCategory {
    CLIENT {
        override fun type(): String {
            return CLIENT.name
        }
    }
}

data class ClientPage(val uuids: Set<String>) {
    fun uuidsCsv(): String {
        return uuids.joinToString(",")
    }
}
