package org.ladhark.identity.service.user

import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.entity.role.Role
import org.ladhark.identity.entity.user.User
import org.ladhark.identity.entity.user.UserRoleRegion
import org.ladhark.identity.exception.EntityAlreadyPresentException
import org.ladhark.identity.exception.EntityNotFoundException
import org.ladhark.identity.repository.region.RegionRepository
import org.ladhark.identity.repository.role.RoleRepository
import org.ladhark.identity.repository.user.UserJWTRepository
import org.ladhark.identity.repository.user.UserRoleRegionRepository
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID
import org.ladhark.identity.service.base.BaseService
import org.ladhark.identity.service.context.withUserContext
import org.ladhark.identity.service.region.event.RegionReadEvent
import org.ladhark.identity.service.role.event.RoleReadEvent
import org.ladhark.identity.service.user.event.UserReadEvent
import org.ladhark.identity.service.user.event.UserRoleAddEvent
import org.ladhark.kotlin.extension.s
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

interface UserService {
    suspend fun findByEmail(email: String): User?
    suspend fun findByUuid(uuid: String): User?
    suspend fun addRoleForRegion(userUUID: String, roleUUID: String, regionUUID: String): UserRoleRegion
}

@Service
class UserServiceImpl : BaseService(), UserService {

    private val log: Logger = LoggerFactory.getLogger(UserServiceImpl::class.java)

    @Autowired
    private lateinit var userJWTRepository: UserJWTRepository

    @Autowired
    private lateinit var regionRepository: RegionRepository

    @Autowired
    private lateinit var roleRepository: RoleRepository

    @Autowired
    private lateinit var userRoleRegionRepository: UserRoleRegionRepository

    @Transactional(readOnly = true)
    override suspend fun findByEmail(email: String): User? = withUserContext {

        if (email.isBlank()) {
            throw IllegalArgumentException(appMessage.getMessage("error.value.required.is.empty", "Email"))
        }

        log.info("Fetching user details for email {}", email)

        return userJWTRepository.findUserByEmail(email)?.also { user ->
            publishEvent(UserReadEvent(this, user, AuthenticatedUserContextUUID(principalUUID())))
        }

    }

    @Transactional(readOnly = true)
    override suspend fun findByUuid(uuid: String): User? = withUserContext {

        if (uuid.isBlank()) {
            throw IllegalArgumentException(appMessage.getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching user details for uuid {}", uuid)

        return userJWTRepository.findUserByUuid(uuid.uuid)?.also { user ->
            publishEvent(UserReadEvent(this, user, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    @Transactional
    override suspend fun addRoleForRegion(userUUID: String, roleUUID: String, regionUUID: String): UserRoleRegion = withUserContext {

        if (userUUID.isBlank()) {
            throw IllegalArgumentException(appMessage.getMessage("error.value.required.is.empty", "User UUID"))
        }

        if (roleUUID.isBlank()) {
            throw IllegalArgumentException(appMessage.getMessage("error.value.required.is.empty", "Role UUID"))
        }

        if (regionUUID.isBlank()) {
            throw IllegalArgumentException(appMessage.getMessage("error.value.required.is.empty", "Region UUID"))
        }

        // TODO IMP the links would increase version, need to clear cache for all three post op i.e linking

        val records = coroutineScope {
            awaitAll(
                async {
                    userJWTRepository.findUserByUuid(userUUID.uuid)?.also { user ->
                        publishEvent(
                            UserReadEvent(
                                this, user, AuthenticatedUserContextUUID(principalUUID()),
                                appMessage.getMessage(
                                    "message.user.event.read.secondary.access",
                                    arrayOf(
                                        user.email,
                                        user.uuid.s,
                                        this::javaClass.name,
                                        "`add role to user`"
                                    )
                                )
                            )
                        )
                    } ?: throw EntityNotFoundException(appMessage.getMessage("error.user.not.found.by.uuid", userUUID))
                },
                async {
                    roleRepository.findRoleByUuid(roleUUID.uuid)?.also { role ->
                        RoleReadEvent(
                            this, role, AuthenticatedUserContextUUID(principalUUID()),
                            appMessage.getMessage(
                                "message.role.event.read.secondary.access",
                                arrayOf(
                                    role.name,
                                    role.uuid.s,
                                    this::javaClass.name,
                                    "`add role to user`"
                                )
                            )
                        )
                    } ?: throw EntityNotFoundException(appMessage.getMessage("error.role.not.found.by.uuid", roleUUID))
                },
                async {
                    regionRepository.findRegionByUuid(regionUUID.uuid)?.also { region ->
                        RegionReadEvent(
                            this, region, AuthenticatedUserContextUUID(principalUUID()),
                            appMessage.getMessage(
                                "message.region.event.read.secondary.access",
                                arrayOf(
                                    region.name,
                                    region.uuid.s,
                                    this::javaClass.name,
                                    "`add role to user`"
                                )
                            )
                        )
                    } ?: throw EntityNotFoundException(
                        appMessage.getMessage(
                            "error.region.not.found.by.uuid",
                            regionUUID
                        )
                    )
                }
            )
        }

        val userFromDb = records[0] as User
        val roleFromDb = records[1] as Role
        val regionFromDb = records[2] as Region

        return userRoleRegionRepository.findByUserUuidAndRoleUuidAndRegionUuid(
            userUUID.uuid,
            roleUUID.uuid,
            regionUUID.uuid
        )?.let {
            throw EntityAlreadyPresentException(
                appMessage.getMessage(
                    "error.user.role.region.already.present",
                    arrayOf(userFromDb.email, roleFromDb.name, regionFromDb.name)
                )
            )
        } ?: run {
            val userRoleRegion = UserRoleRegion().apply {
                user = userFromDb
                region = regionFromDb
                role = roleFromDb
            }

            userRoleRegionRepository
                .save(userRoleRegion)
                .also {
                    publishEvent(UserRoleAddEvent(this, userFromDb, roleFromDb, regionFromDb, AuthenticatedUserContextUUID(principalUUID())))
                }
        }
    }
}