package org.ladhark.identity.service.user.event

import org.ladhark.identity.api.dto.user.RegistrationDetails
import org.ladhark.identity.entity.user.User
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory

class RegistrationSuccessEvent(source: Any, payload: RegistrationSuccessEventPayload) :
    BaseEvent<RegistrationSuccessEventPayload>(
        source,
        payload,
        RegisterEventCategory.REGISTER,
        ApplicationEventAction.SUCCESS
    ) {
    override fun summary(): String {
        return "User registered successfully with email ${payload.registrationDetails.email}"
    }
}

class RegistrationFailEvent(source: Any, val user: RegistrationDetails, var reason: String = "") :
    BaseEvent<RegistrationDetails>(source, user, RegisterEventCategory.REGISTER, ApplicationEventAction.FAILURE) {
    override fun summary(): String {
        return "User registration failed for user with email ${user.email}, reason => $reason"
    }

    override fun transferData(): Map<String, Any> {
        return super.transferData() + mapOf("reason" to reason)
    }
}

enum class RegisterEventCategory(val category: String) : EventCategory {
    REGISTER("Register") {
        override fun type(): String {
            return REGISTER.category
        }
    }
}

data class RegistrationSuccessEventPayload(val registrationDetails: RegistrationDetails, val user: User)