package org.ladhark.identity.service.resource.cache

import com.github.benmanes.caffeine.cache.AsyncCache
import com.github.benmanes.caffeine.cache.Caffeine
import jakarta.annotation.PostConstruct
import kotlinx.coroutines.sync.withLock
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.resource.Resource
import org.ladhark.identity.repository.resource.ResourceRepository
import org.ladhark.identity.service.base.BaseCacheService
import org.ladhark.kotlin.extension.s
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

const val RESOURCE_CACHE_BY_UUID = "RESOURCE_CACHE_BY_UUID"
const val RESOURCE_CACHE_BY_NAME_TO_UUID = "RESOURCE_CACHE_BY_NAME_TO_UUID"

interface ResourceCacheService {
    suspend fun getResourceByNameToUuidAsync(key: String): Resource?
    suspend fun putResourceByNameToUuidAsync(key: String, value: Resource)
    suspend fun removeResourceByNameToUuidAsync(key: String)
    suspend fun getResourceByUuidAsync(key: String): Resource?
    suspend fun putResourceByUuidAsync(key: String, value: Resource)
    suspend fun removeResourceByUuidAsync(key: String)
}

@Service
class ResourceCacheServiceImpl : BaseCacheService(), ResourceCacheService {

    @Autowired
    lateinit var resourceRepository: ResourceRepository

    lateinit var resourceCacheByUUID: AsyncCache<String, Resource>

    lateinit var resourceCacheByNameToUUID: AsyncCache<String, String>

    @PostConstruct
    private fun postConstruct() {

        resourceCacheByUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        resourceCacheByNameToUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        registerProvider(this, RESOURCE_CACHE_BY_UUID, RESOURCE_CACHE_BY_NAME_TO_UUID)

        resourceRepository.subscribeAndDoOnAll {
            with(it) {
                resourceCacheByUUID.put(uuid.s, CompletableFuture.completedFuture(this))
                resourceCacheByNameToUUID.put(name, CompletableFuture.completedFuture(uuid.s))
            }
        }
    }

    override suspend fun getResourceByNameToUuidAsync(key: String): Resource? {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.getValueAsync(key, resourceCacheByNameToUUID)
            return uuid?.let {
                caffeineCacheHelperService.getValueAsync(it, resourceCacheByUUID)
            }
        }
    }

    override suspend fun putResourceByNameToUuidAsync(key: String, value: Resource) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value.uuid.s, resourceCacheByNameToUUID)
            caffeineCacheHelperService.putValueAsync(value.uuid.s, value, resourceCacheByUUID)
        }
    }

    override suspend fun removeResourceByNameToUuidAsync(key: String) {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.removeValueAsync(key, resourceCacheByNameToUUID)
            uuid?.let {
                caffeineCacheHelperService.removeValueAsync(it, resourceCacheByUUID)
            }
        }
    }

    override suspend fun getResourceByUuidAsync(key: String): Resource? {
        mutex.withLock {
            return caffeineCacheHelperService.getValueAsync(key, resourceCacheByUUID)
        }
    }

    override suspend fun putResourceByUuidAsync(key: String, value: Resource) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value, resourceCacheByUUID)
        }
    }

    override suspend fun removeResourceByUuidAsync(key: String) {
        mutex.withLock {
            caffeineCacheHelperService.removeValueAsync(key, resourceCacheByUUID)
        }
    }

    override suspend fun getFromCache(name: String, key: String): BaseEntity? {
        return when(name) {
            RESOURCE_CACHE_BY_UUID -> getResourceByUuidAsync(key)
            RESOURCE_CACHE_BY_NAME_TO_UUID -> getResourceByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun putInCache(name: String, key: String, value: BaseEntity) {

        if (value !is Resource) {
            throw IllegalArgumentException("Cache value passed is not of type ${Resource::class.qualifiedName}")
        }

        when(name) {
            RESOURCE_CACHE_BY_UUID -> putResourceByUuidAsync(key, value)
            RESOURCE_CACHE_BY_NAME_TO_UUID -> putResourceByNameToUuidAsync(key, value)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun removeFromCache(name: String, key: String) {
        when(name) {
            RESOURCE_CACHE_BY_UUID -> removeResourceByUuidAsync(key)
            RESOURCE_CACHE_BY_NAME_TO_UUID -> removeResourceByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

}