package org.ladhark.identity.service.aspect.event

import org.ladhark.identity.entity.aspect.AspectType
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class AspectTypeCreateEvent(source: Any, private val aspectType: AspectType, private val contextUser: AuthenticatedUserContextUUID) : BaseEvent<AspectType>(source, aspectType, AspectTypeEventCategory.ASPECT_TYPE, ApplicationEventAction.CREATE) {
    override fun summary(): String {
        return "AspectType with name ${aspectType.name} and id ${aspectType.uuid} created by User $contextUser"
    }
}

class AspectTypeReadEvent(source: Any, private val aspectType: AspectType, private val contextUser: AuthenticatedUserContextUUID, private val summary: String = "") : BaseEvent<AspectType>(source, aspectType, AspectTypeEventCategory.ASPECT_TYPE, ApplicationEventAction.READ) {
    override fun summary(): String {
        return summary.ifBlank {
            return "AspectType with name ${aspectType.name} and id ${aspectType.uuid} read by User $contextUser"
        }
    }
}

class AspectTypeUpdateEvent(source: Any, private val aspectType: AspectType, private val contextUser: AuthenticatedUserContextUUID) : BaseEvent<AspectType>(source, aspectType, AspectTypeEventCategory.ASPECT_TYPE, ApplicationEventAction.UPDATE) {
    override fun summary(): String {
        return "AspectType with name ${aspectType.name} and id ${aspectType.uuid} updated by User $contextUser"
    }
}

class AspectTypeDeleteEvent(source: Any, private val aspectType: AspectType, private val contextUser: AuthenticatedUserContextUUID) : BaseEvent<AspectType>(source, aspectType, AspectTypeEventCategory.ASPECT_TYPE, ApplicationEventAction.DELETE) {
    override fun summary(): String {
        return "AspectType with name ${aspectType.name} and id ${aspectType.uuid} deleted by User $contextUser"
    }
}

enum class AspectTypeEventCategory: EventCategory {
    ASPECT_TYPE {
        override fun type(): String {
            return ASPECT_TYPE.name
        }
    }
}

