package org.ladhark.identity.service.resource.policy.event

import org.ladhark.identity.entity.resource.policy.Policy
import org.ladhark.identity.entity.resource.policy.PolicyCondition
import org.ladhark.identity.entity.resource.policy.PolicySet
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class PolicyCreateEvent(
    source: Any,
    private val policy: Policy,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<Policy>(source, policy, PolicyEventCategory.POLICY, ApplicationEventAction.CREATE) {
    override fun summary(): String {
        return "Policy with name ${policy.name} and id ${policy.uuid} created by User $contextUser"
    }
}

class PolicyReadEvent(
    source: Any,
    private val policy: Policy,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<Policy>(source, policy, PolicyEventCategory.POLICY, ApplicationEventAction.READ) {
    override fun summary(): String {
        return summary.ifBlank {
            return "Policy with name ${policy.name} and id ${policy.uuid} read by User $contextUser"
        }
    }
}

class PolicyUpdateEvent(
    source: Any,
    private val policy: Policy,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<Policy>(source, policy, PolicyEventCategory.POLICY, ApplicationEventAction.UPDATE) {
    override fun summary(): String {
        return summary.ifBlank {
            return "Policy with name ${policy.name} and id ${policy.uuid} updated by User $contextUser"
        }
    }
}

class PolicyDeleteEvent(
    source: Any,
    private val policy: Policy,
    private val contextUser: AuthenticatedUserContextUUID,
    val parent: PolicySet?,
    val children: Set<PolicyCondition> = emptySet()
) : BaseEvent<Policy>(source, policy, PolicyEventCategory.POLICY, ApplicationEventAction.DELETE) {
    override fun summary(): String {
        return "Policy with name ${policy.name} and id ${policy.uuid} deleted by User $contextUser"
    }
}

enum class PolicyEventCategory : EventCategory {
    POLICY {
        override fun type(): String {
            return POLICY.name
        }
    }
}

