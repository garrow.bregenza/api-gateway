package org.ladhark.identity.service.resource.policy.event

import org.ladhark.identity.entity.resource.policy.ResourcePolicy
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class ResourcePolicyCreateEvent(
    source: Any,
    private val resourcePolicy: ResourcePolicy,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<ResourcePolicy>(
    source,
    resourcePolicy,
    ResourcePolicyEventCategory.RESOURCE_POLICY,
    ApplicationEventAction.CREATE
) {
    override fun summary(): String {
        return "ResourcePolicy with name ${resourcePolicy.name} and id ${resourcePolicy.uuid} created by User $contextUser"
    }
}

class ResourcePolicyReadEvent(
    source: Any,
    private val resourcePolicy: ResourcePolicy,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<ResourcePolicy>(
    source,
    resourcePolicy,
    ResourcePolicyEventCategory.RESOURCE_POLICY,
    ApplicationEventAction.READ
) {
    override fun summary(): String {
        return summary.ifBlank {
            return "ResourcePolicy with name ${resourcePolicy.name} and id ${resourcePolicy.uuid} read by User $contextUser"
        }
    }
}

class ResourcePolicyUpdateEvent(
    source: Any,
    private val resourcePolicy: ResourcePolicy,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<ResourcePolicy>(
    source,
    resourcePolicy,
    ResourcePolicyEventCategory.RESOURCE_POLICY,
    ApplicationEventAction.UPDATE
) {
    override fun summary(): String {
        return summary.ifBlank {
            return "ResourcePolicy with name ${resourcePolicy.name} and id ${resourcePolicy.uuid} updated by User $contextUser"
        }
    }
}

class ResourcePolicyDeleteEvent(
    source: Any,
    private val resourcePolicy: ResourcePolicy,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<ResourcePolicy>(
    source,
    resourcePolicy,
    ResourcePolicyEventCategory.RESOURCE_POLICY,
    ApplicationEventAction.DELETE
) {
    override fun summary(): String {
        return "ResourcePolicy with name ${resourcePolicy.name} and id ${resourcePolicy.uuid} deleted by User $contextUser"
    }
}

enum class ResourcePolicyEventCategory : EventCategory {
    RESOURCE_POLICY {
        override fun type(): String {
            return RESOURCE_POLICY.name
        }
    }
}

