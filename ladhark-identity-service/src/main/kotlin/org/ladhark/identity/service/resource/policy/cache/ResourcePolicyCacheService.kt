package org.ladhark.identity.service.resource.policy.cache

import com.github.benmanes.caffeine.cache.AsyncCache
import com.github.benmanes.caffeine.cache.Caffeine
import jakarta.annotation.PostConstruct
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.resource.policy.ResourcePolicy
import org.ladhark.identity.repository.resource.policy.ResourcePolicyRepository
import org.ladhark.identity.service.base.BaseCacheService
import org.ladhark.kotlin.extension.s
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

const val RESOURCE_POLICY_CACHE_BY_UUID = "RESOURCE_POLICY_CACHE_BY_UUID"
const val RESOURCE_POLICY_CACHE_BY_NAME_TO_UUID = "RESOURCE_POLICY_CACHE_BY_NAME_TO_UUID"
const val RESOURCE_POLICY_CACHE_BY_RESOURCE_ACTION_UUID = "RESOURCE_POLICY_CACHE_BY_RESOURCE_ACTION_UUID"

interface ResourcePolicyCacheService {

    suspend fun getResourcePolicyByResourceActionUuidAsync(key: String): ResourcePolicy?
    suspend fun putResourcePolicyByResourceActionUuidAsync(key: String, value: ResourcePolicy)
    suspend fun removeResourcePolicyByResourceActionUuidAsync(key: String)
    suspend fun getResourcePolicyByNameToUuidAsync(key: String): ResourcePolicy?
    suspend fun putResourcePolicyByNameToUuidAsync(key: String, value: ResourcePolicy)
    suspend fun removeResourcePolicyByNameToUuidAsync(key: String)
    suspend fun getResourcePolicyByUuidAsync(key: String): ResourcePolicy?
    suspend fun putResourcePolicyByUuidAsync(key: String, value: ResourcePolicy)
    suspend fun removeResourcePolicyByUuidAsync(key: String)
    suspend fun allEntriesInUuidCache(): MutableCollection<ResourcePolicy>
}

@Service
class ResourcePolicyCacheServiceImpl : BaseCacheService(), ResourcePolicyCacheService {

    @Autowired
    private lateinit var resourcePolicyRepository: ResourcePolicyRepository

    private lateinit var resourcePolicyCacheByUUID: AsyncCache<String, ResourcePolicy>

    private lateinit var resourcePolicyCacheByNameToUUID: AsyncCache<String, String>

    private lateinit var resourcePolicyCacheByResourceActionUUID: AsyncCache<String, String>

    @PostConstruct
    private fun postConstruct() {

        resourcePolicyCacheByUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        resourcePolicyCacheByNameToUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        resourcePolicyCacheByResourceActionUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        registerProvider(this, RESOURCE_POLICY_CACHE_BY_UUID,
            RESOURCE_POLICY_CACHE_BY_NAME_TO_UUID, RESOURCE_POLICY_CACHE_BY_RESOURCE_ACTION_UUID)

        resourcePolicyRepository.subscribeAndDoOnAll {
            with(it) {
                resourcePolicyCacheByUUID.put(uuid.s, CompletableFuture.completedFuture(this))
                resourcePolicyCacheByNameToUUID.put(name, CompletableFuture.completedFuture(this.uuid.s))
            }
        }
    }

    override suspend fun getResourcePolicyByResourceActionUuidAsync(key: String): ResourcePolicy? {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.getValueAsync(key, resourcePolicyCacheByResourceActionUUID)
            return uuid?.let {
                caffeineCacheHelperService.getValueAsync(it, resourcePolicyCacheByUUID)
            }
        }
    }

    override suspend fun putResourcePolicyByResourceActionUuidAsync(key: String, value: ResourcePolicy) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value.uuid.s, resourcePolicyCacheByResourceActionUUID)
            caffeineCacheHelperService.putValueAsync(value.uuid.s, value, resourcePolicyCacheByUUID)
        }
    }

    override suspend fun removeResourcePolicyByResourceActionUuidAsync(key: String) {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.removeValueAsync(key, resourcePolicyCacheByResourceActionUUID)
            uuid?.let {
                caffeineCacheHelperService.removeValueAsync(it, resourcePolicyCacheByUUID)
            }
        }
    }

    override suspend fun getResourcePolicyByNameToUuidAsync(key: String): ResourcePolicy? {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.getValueAsync(key, resourcePolicyCacheByNameToUUID)
            return uuid?.let {
                caffeineCacheHelperService.getValueAsync(it, resourcePolicyCacheByUUID)
            }
        }
    }

    override suspend fun putResourcePolicyByNameToUuidAsync(key: String, value: ResourcePolicy) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value.uuid.s, resourcePolicyCacheByNameToUUID)
            caffeineCacheHelperService.putValueAsync(value.uuid.s, value, resourcePolicyCacheByUUID)
        }
    }

    override suspend fun removeResourcePolicyByNameToUuidAsync(key: String) {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.removeValueAsync(key, resourcePolicyCacheByNameToUUID)
            uuid?.let {
                caffeineCacheHelperService.removeValueAsync(it, resourcePolicyCacheByUUID)
            }
        }
    }

    override suspend fun getResourcePolicyByUuidAsync(key: String): ResourcePolicy? {
        mutex.withLock {
            return caffeineCacheHelperService.getValueAsync(key, resourcePolicyCacheByUUID)
        }
    }

    override suspend fun putResourcePolicyByUuidAsync(key: String, value: ResourcePolicy) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value, resourcePolicyCacheByUUID)
        }
    }

    override suspend fun removeResourcePolicyByUuidAsync(key: String) {
        mutex.withLock {
            caffeineCacheHelperService.removeValueAsync(key, resourcePolicyCacheByUUID)
        }
    }


    override suspend fun allEntriesInUuidCache(): MutableCollection<ResourcePolicy> {

        return coroutineScope {
            withContext(Dispatchers.Unconfined) {
                resourcePolicyCacheByUUID.synchronous().asMap().values
            }
        }
    }

    private fun uriAndScopeKey(uri: String, scope: String) = "${uri}->${scope}"
    override suspend fun getFromCache(name: String, key: String): BaseEntity? {
        return when(name) {
            RESOURCE_POLICY_CACHE_BY_UUID -> getResourcePolicyByUuidAsync(key)
            RESOURCE_POLICY_CACHE_BY_NAME_TO_UUID -> getResourcePolicyByNameToUuidAsync(key)
            RESOURCE_POLICY_CACHE_BY_RESOURCE_ACTION_UUID -> throw UnsupportedOperationException()
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun putInCache(name: String, key: String, value: BaseEntity) {

        if (value !is ResourcePolicy) {
            throw IllegalArgumentException("Cache value passed is not of type ${ResourcePolicy::class.qualifiedName}")
        }

        when(name) {
            RESOURCE_POLICY_CACHE_BY_UUID -> putResourcePolicyByUuidAsync(key, value)
            RESOURCE_POLICY_CACHE_BY_NAME_TO_UUID -> putResourcePolicyByNameToUuidAsync(key, value)
            RESOURCE_POLICY_CACHE_BY_RESOURCE_ACTION_UUID -> throw UnsupportedOperationException()
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun removeFromCache(name: String, key: String) {
        when(name) {
            RESOURCE_POLICY_CACHE_BY_UUID -> removeResourcePolicyByUuidAsync(key)
            RESOURCE_POLICY_CACHE_BY_NAME_TO_UUID -> removeResourcePolicyByNameToUuidAsync(key)
            RESOURCE_POLICY_CACHE_BY_RESOURCE_ACTION_UUID -> throw UnsupportedOperationException()
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }
}