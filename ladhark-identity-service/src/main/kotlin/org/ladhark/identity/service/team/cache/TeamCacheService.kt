package org.ladhark.identity.service.team.cache

import com.github.benmanes.caffeine.cache.AsyncCache
import com.github.benmanes.caffeine.cache.Caffeine
import jakarta.annotation.PostConstruct
import kotlinx.coroutines.sync.withLock
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.team.Team
import org.ladhark.identity.repository.team.TeamRepository
import org.ladhark.identity.service.base.BaseCacheService
import org.ladhark.kotlin.extension.s
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

const val TEAM_CACHE_BY_UUID = "TEAM_CACHE_BY_UUID"
const val TEAM_CACHE_BY_NAME_TO_UUID = "TEAM_CACHE_BY_NAME_TO_UUID"

interface TeamCacheService {
    suspend fun getTeamByNameToUuidAsync(key: String): Team?
    suspend fun putTeamByNameToUuidAsync(key: String, value: Team)
    suspend fun removeTeamByNameToUuidAsync(key: String)
    suspend fun getTeamByUuidAsync(key: String): Team?
    suspend fun putTeamByUuidAsync(key: String, value: Team)
    suspend fun removeTeamByUuidAsync(key: String)
}

@Service
class TeamCacheServiceImpl : BaseCacheService(), TeamCacheService {

    @Autowired
    lateinit var teamRepository: TeamRepository

    lateinit var teamCacheByUUID: AsyncCache<String, Team>

    lateinit var teamCacheByNameToUUID: AsyncCache<String, String>

    @PostConstruct
    private fun postConstruct() {

        teamCacheByUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        teamCacheByNameToUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        registerProvider(this, TEAM_CACHE_BY_UUID, TEAM_CACHE_BY_NAME_TO_UUID)

        teamRepository.subscribeAndDoOnAll {
            with(it) {
                teamCacheByUUID.put(uuid.s, CompletableFuture.completedFuture(this))
                teamCacheByNameToUUID.put(name, CompletableFuture.completedFuture(uuid.s))
            }
        }
    }

    override suspend fun getTeamByNameToUuidAsync(key: String): Team? {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.getValueAsync(key, teamCacheByNameToUUID)
            return uuid?.let {
                caffeineCacheHelperService.getValueAsync(it, teamCacheByUUID)
            }
        }
    }

    override suspend fun putTeamByNameToUuidAsync(key: String, value: Team) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value.uuid.s, teamCacheByNameToUUID)
            caffeineCacheHelperService.putValueAsync(value.uuid.s, value, teamCacheByUUID)
        }
    }

    override suspend fun removeTeamByNameToUuidAsync(key: String) {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.removeValueAsync(key, teamCacheByNameToUUID)
            uuid?.let {
                caffeineCacheHelperService.removeValueAsync(it, teamCacheByUUID)
            }
        }
    }

    override suspend fun getTeamByUuidAsync(key: String): Team? {
        mutex.withLock {
            return caffeineCacheHelperService.getValueAsync(key, teamCacheByUUID)
        }
    }

    override suspend fun putTeamByUuidAsync(key: String, value: Team) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value, teamCacheByUUID)
        }
    }

    override suspend fun removeTeamByUuidAsync(key: String) {
        mutex.withLock {
            caffeineCacheHelperService.removeValueAsync(key, teamCacheByUUID)
        }
    }

    override suspend fun getFromCache(name: String, key: String): BaseEntity? {
        return when(name) {
            TEAM_CACHE_BY_UUID -> getTeamByUuidAsync(key)
            TEAM_CACHE_BY_NAME_TO_UUID -> getTeamByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun putInCache(name: String, key: String, value: BaseEntity) {

        if (value !is Team) {
            throw IllegalArgumentException("Cache value passed is not of type ${Team::class.qualifiedName}")
        }

        when(name) {
            TEAM_CACHE_BY_UUID -> putTeamByUuidAsync(key, value)
            TEAM_CACHE_BY_NAME_TO_UUID -> putTeamByNameToUuidAsync(key, value)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun removeFromCache(name: String, key: String) {
        when(name) {
            TEAM_CACHE_BY_UUID -> removeTeamByUuidAsync(key)
            TEAM_CACHE_BY_NAME_TO_UUID -> removeTeamByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

}