package org.ladhark.identity.service.base

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.ladhark.identity.entity.BaseEntity
import org.springframework.stereotype.Service

interface CacheRegistry {
    suspend fun registerCacheProvider(name: String, provider: BaseCacheService)
    suspend fun getFromCache(name: String, key: String): BaseEntity?
    suspend fun putInCache(name: String, key: String, value: BaseEntity)
    suspend fun removeFromCache(name: String, key: String)
}

@Service
class CacheRegistryImpl: CacheRegistry {

    private val mutex = Mutex()
    private val cacheInfoHolder = mutableMapOf<String, BaseCacheService>()

    override suspend fun registerCacheProvider(name: String, provider: BaseCacheService) {
        mutex.withLock {
            require(name.isNotBlank()) { "Name cannot be blank" }
            cacheInfoHolder[name] = provider
        }
    }

    override suspend fun getFromCache(name: String, key: String): BaseEntity? {
        require(name.isNotBlank()) { "Name cannot be blank" }

        val cacheInfo = cacheInfoHolder[name]
        return cacheInfo?.getFromCache(name, key)
    }

    override suspend fun putInCache(name: String, key: String, value: BaseEntity) {
        require(name.isNotBlank()) { "Name cannot be blank" }

        val cacheInfo = cacheInfoHolder[name]
        cacheInfo?.putInCache(name, key, value)
    }

    override suspend fun removeFromCache(name: String, key: String) {
        require(name.isNotBlank()) { "Name cannot be blank" }

        val cacheInfo = cacheInfoHolder[name]
        cacheInfo?.removeFromCache(name, key)
    }
}