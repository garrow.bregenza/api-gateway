package org.ladhark.identity.service.client.event

import org.ladhark.identity.entity.client.ClientCredentials
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class ClientCredentialsCreateEvent(
    source: Any,
    private val clientCredentials: ClientCredentials,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<ClientCredentials>(
    source,
    clientCredentials,
    ClientCredentialsEventCategory.CLIENT_CREDENTIALS,
    ApplicationEventAction.CREATE
) {
    override fun summary(): String {
        return "ClientCredentials with uuid ${clientCredentials.uuid} created by User $contextUser"
    }
}

class ClientCredentialsReadEvent(
    source: Any,
    private val clientCredentials: ClientCredentials,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<ClientCredentials>(
    source,
    clientCredentials,
    ClientCredentialsEventCategory.CLIENT_CREDENTIALS,
    ApplicationEventAction.READ
) {
    override fun summary(): String {
        return summary.ifBlank {
            return "ClientCredentials with uuid ${clientCredentials.uuid} read by User $contextUser"
        }
    }
}

class ClientCredentialsUpdateEvent(
    source: Any,
    private val clientCredentials: ClientCredentials,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<ClientCredentials>(
    source,
    clientCredentials,
    ClientCredentialsEventCategory.CLIENT_CREDENTIALS,
    ApplicationEventAction.UPDATE
) {
    override fun summary(): String {
        return "ClientCredentials with uuid ${clientCredentials.uuid} updated by User $contextUser"
    }
}

class ClientCredentialsDeleteEvent(
    source: Any,
    private val clientCredentials: ClientCredentials,
    private val contextUser: AuthenticatedUserContextUUID
) : BaseEvent<ClientCredentials>(
    source,
    clientCredentials,
    ClientCredentialsEventCategory.CLIENT_CREDENTIALS,
    ApplicationEventAction.DELETE
) {
    override fun summary(): String {
        return "ClientCredentials with uuid ${clientCredentials.uuid} deleted by User $contextUser"
    }
}

enum class ClientCredentialsEventCategory : EventCategory {
    CLIENT_CREDENTIALS {
        override fun type(): String {
            return CLIENT_CREDENTIALS.name
        }
    }
}

