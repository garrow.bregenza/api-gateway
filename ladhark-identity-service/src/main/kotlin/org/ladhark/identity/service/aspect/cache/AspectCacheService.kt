package org.ladhark.identity.service.aspect.cache

import com.github.benmanes.caffeine.cache.AsyncCache
import com.github.benmanes.caffeine.cache.Caffeine
import jakarta.annotation.PostConstruct
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.sync.withLock
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.aspect.Aspect
import org.ladhark.identity.repository.aspect.AspectRepository
import org.ladhark.identity.repository.extension.suspendedResultToMono
import org.ladhark.identity.repository.resource.ResourceActionRepository
import org.ladhark.identity.service.base.BaseCacheService
import org.ladhark.kotlin.extension.s
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

const val ASPECT_CACHE_BY_UUID = "ASPECT_CACHE_BY_UUID"
const val ASPECT_CACHE_BY_NAME_TO_UUID = "ASPECT_CACHE_BY_NAME_TO_UUID"
const val ASPECT_CACHE_BY_RESOURCE_ACTION_UUID_TO_UUID = "ASPECT_CACHE_BY_RESOURCE_ACTION_UUID_TO_UUID"

interface AspectCacheService {
    suspend fun getAspectByNameToUuidAsync(key: String): Aspect?
    suspend fun putAspectByNameToUuidAsync(key: String, value: Aspect)
    suspend fun removeAspectByNameToUuidAsync(key: String)

    suspend fun getAspectByUuidAsync(key: String): Aspect?
    suspend fun putAspectByUuidAsync(key: String, value: Aspect)
    suspend fun removeAspectByUuidAsync(key: String)

    suspend fun getAspectsByResourceActionUuidToUuidAsync(key: String): List<Aspect>
    suspend fun putAspectByResourceActionUuidToUuidAsync(key: String, value: List<Aspect>)
    suspend fun removeAspectByResourceActionUuidToUuidAsync(key: String)
}

@Service
class AspectCacheServiceImpl : BaseCacheService(), AspectCacheService {

    @Autowired
    lateinit var aspectRepository: AspectRepository

    @Autowired
    lateinit var resourceActionRepository: ResourceActionRepository

    lateinit var aspectCacheByUUID: AsyncCache<String, Aspect>

    lateinit var aspectCacheByNameToUUID: AsyncCache<String, String>

    lateinit var aspectCacheByResourceActionUUIDToUUID: AsyncCache<String, List<String>>

    @PostConstruct
    private fun postConstruct() {

        aspectCacheByUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        aspectCacheByNameToUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        aspectCacheByResourceActionUUIDToUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        registerProvider(this, ASPECT_CACHE_BY_UUID,
            ASPECT_CACHE_BY_NAME_TO_UUID, ASPECT_CACHE_BY_RESOURCE_ACTION_UUID_TO_UUID)

        aspectRepository.subscribeAndDoOnAll {
            with(it) {
                aspectCacheByUUID.put(uuid.s, CompletableFuture.completedFuture(this))
                aspectCacheByNameToUUID.put(name, CompletableFuture.completedFuture(uuid.s))

            }
        }

        resourceActionRepository
            .findAllFlux()
            .map { resourceAction ->
                suspendedResultToMono {
                    val aspectUuids = mutableListOf<String>()
                    aspectRepository
                        .findAspectsByResourceActionUuid(resourceAction.uuid)
                        .map { aspect ->
                            aspect.uuid.s
                        }.toList(aspectUuids)
                }.map { aspects ->
                    aspectCacheByResourceActionUUIDToUUID
                        .put(resourceAction.uuid.s, CompletableFuture.completedFuture(aspects))
                }
            }.subscribe()
    }

    override suspend fun getAspectByNameToUuidAsync(key: String): Aspect? {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.getValueAsync(key, aspectCacheByNameToUUID)
            return uuid?.let {
                caffeineCacheHelperService.getValueAsync(it, aspectCacheByUUID)
            }
        }
    }

    override suspend fun putAspectByNameToUuidAsync(key: String, value: Aspect) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value.uuid.s, aspectCacheByNameToUUID)
            caffeineCacheHelperService.putValueAsync(value.uuid.s, value, aspectCacheByUUID)
        }
    }

    override suspend fun removeAspectByNameToUuidAsync(key: String) {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.removeValueAsync(key, aspectCacheByNameToUUID)
            uuid?.let {
                caffeineCacheHelperService.removeValueAsync(it, aspectCacheByUUID)
            }
        }
    }

    override suspend fun getAspectByUuidAsync(key: String): Aspect? {
        mutex.withLock {
            return caffeineCacheHelperService.getValueAsync(key, aspectCacheByUUID)
        }
    }

    override suspend fun putAspectByUuidAsync(key: String, value: Aspect) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value, aspectCacheByUUID)
        }
    }

    override suspend fun removeAspectByUuidAsync(key: String) {
        mutex.withLock {
            caffeineCacheHelperService.removeValueAsync(key, aspectCacheByUUID)
        }
    }

    override suspend fun getAspectsByResourceActionUuidToUuidAsync(key: String): List<Aspect> {
        mutex.withLock {
            val uuids = caffeineCacheHelperService.getValueAsync(key, aspectCacheByResourceActionUUIDToUUID)
            val aspects = mutableListOf<Aspect>()
            uuids?.forEach {
                val aspect = caffeineCacheHelperService.getValueAsync(it, aspectCacheByUUID)
                if (aspect != null) {
                    aspects.add(aspect)
                }
            }

            return aspects
        }
    }

    override suspend fun putAspectByResourceActionUuidToUuidAsync(key: String, value: List<Aspect>) {
        mutex.withLock {
            value.onEach { aspect ->
                caffeineCacheHelperService.putValueAsync(aspect.uuid.s, aspect, aspectCacheByUUID)
            }

            val uuids = value.map {
                it.uuid.s
            }

            caffeineCacheHelperService.putValueAsync(key, uuids, aspectCacheByResourceActionUUIDToUUID)
        }
    }

    override suspend fun removeAspectByResourceActionUuidToUuidAsync(key: String) {
        mutex.withLock {
            val uuids = caffeineCacheHelperService.removeValueAsync(key, aspectCacheByResourceActionUUIDToUUID)
            uuids?.forEach {
                caffeineCacheHelperService.removeValueAsync(it, aspectCacheByUUID)
            }
        }
    }

    override suspend fun getFromCache(name: String, key: String): BaseEntity? {
        return when(name) {
            ASPECT_CACHE_BY_UUID -> getAspectByUuidAsync(key)
            ASPECT_CACHE_BY_NAME_TO_UUID -> getAspectByNameToUuidAsync(key)
            ASPECT_CACHE_BY_RESOURCE_ACTION_UUID_TO_UUID -> throw UnsupportedOperationException()
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun putInCache(name: String, key: String, value: BaseEntity) {

        if (value !is Aspect) {
            throw IllegalArgumentException("Cache value passed is not of type ${Aspect::class.qualifiedName}")
        }

        when(name) {
            ASPECT_CACHE_BY_UUID -> putAspectByUuidAsync(key, value)
            ASPECT_CACHE_BY_NAME_TO_UUID -> putAspectByNameToUuidAsync(key, value)
            ASPECT_CACHE_BY_RESOURCE_ACTION_UUID_TO_UUID -> throw UnsupportedOperationException()
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun removeFromCache(name: String, key: String) {
        when(name) {
            ASPECT_CACHE_BY_UUID -> removeAspectByUuidAsync(key)
            ASPECT_CACHE_BY_NAME_TO_UUID -> removeAspectByNameToUuidAsync(key)
            ASPECT_CACHE_BY_RESOURCE_ACTION_UUID_TO_UUID -> throw UnsupportedOperationException()
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

}