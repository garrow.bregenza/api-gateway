package org.ladhark.identity.service.aspect.strategy.cache

import com.github.benmanes.caffeine.cache.AsyncCache
import com.github.benmanes.caffeine.cache.Caffeine
import jakarta.annotation.PostConstruct
import org.ladhark.identity.entity.aspect.Aspect
import org.ladhark.identity.entity.resource.ResourceAction
import org.ladhark.identity.service.aspect.strategy.AspectStrategy
import org.ladhark.identity.service.cache.CaffeineCacheHelperService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.util.MultiValueMap
import java.math.BigInteger
import java.security.MessageDigest
import java.util.concurrent.TimeUnit

const val CACHE_BY_QUERY_PARAMS = "CACHE_BY_QUERY_PARAMS"
const val PROPERTY_QUERY_PARAM_CSV_NAME = "query_param_name_csv"

const val CONTEXT_PARAM_QUERY_PARAMS = "queryParams"
const val CONTEXT_ACTION = "action"
const val CONTEXT_ACTION_PULL = "pull"
const val CONTEXT_ACTION_PUSH = "push"
const val CONTEXT_ACTION_CACHE_TO_PUSH = "cacheToPush"
const val CONTEXT_ACTION_CACHED_DATA = "cachedData"
const val CONTEXT_ACTION_NO_OP = "noOp"
const val CONTEXT_ACTION_CACHE_HIT = "cacheHit"
const val CONTEXT_ACTION_CACHE_MISS = "cacheMiss"
const val CONTEXT_STATUS = "status"

@Service
class QueryParamAspectStrategy: AspectStrategy {

    @Autowired
    lateinit var caffeineCacheHelperService: CaffeineCacheHelperService

    lateinit var cache: AsyncCache<String, MutableMap<String, String>>

    @PostConstruct
    private fun postConstruct() {
        cache = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()
    }

    override suspend fun action(context: Map<String, Any>, resourceAction: ResourceAction, aspect: Aspect): Map<String, Any> {

        val keyAsDigestHex = makeKeyForCache(context, aspect)

        val cache = getCache(resourceAction)

        when {

            context[CONTEXT_ACTION] == CONTEXT_ACTION_PULL -> {

                val cachedResponse = cache[keyAsDigestHex]

                if (cachedResponse != null) {
                    return mapOf(CONTEXT_STATUS to CONTEXT_ACTION_CACHE_HIT,
                        CONTEXT_ACTION_CACHED_DATA to cachedResponse)
                }
                return mapOf(CONTEXT_STATUS to CONTEXT_ACTION_CACHE_MISS)
            }

            context[CONTEXT_ACTION] == CONTEXT_ACTION_PUSH -> {
                val cacheToPush = context[CONTEXT_ACTION_CACHE_TO_PUSH]
                if (cacheToPush != null) {
                    cache[keyAsDigestHex] = cacheToPush as String
                    return mapOf(CONTEXT_STATUS to CONTEXT_ACTION_CACHE_HIT)
                }

                return mapOf(CONTEXT_STATUS to CONTEXT_ACTION_CACHE_MISS)
            }

            else -> return mapOf(CONTEXT_STATUS to CONTEXT_ACTION_NO_OP)
        }

    }

    override suspend fun clear(context: Map<String, Any>, resourceAction: ResourceAction, aspect: Aspect) {
        val cacheName = "RESOURCE_ACTION_CACHE_${resourceAction.uuid}"
        caffeineCacheHelperService.removeValueAsync(cacheName, cache)
    }


    private suspend fun getCache(resourceAction: ResourceAction): MutableMap<String, String> {
        val cacheName = "RESOURCE_ACTION_CACHE_${resourceAction.uuid}"

        val queryParamCache = caffeineCacheHelperService.getValueAsync(cacheName, cache)

        return queryParamCache ?: run {
            caffeineCacheHelperService.putValueAsync(cacheName, mutableMapOf(), cache)
        }

    }


    private fun makeKeyForCache(
        context: Map<String, Any>,
        aspect: Aspect
    ): String {
        @Suppress("UNCHECKED_CAST")
        val queryParams = context[CONTEXT_PARAM_QUERY_PARAMS] as MultiValueMap<String, String>
        val aspectQueryParamsCsv = aspect.properties[PROPERTY_QUERY_PARAM_CSV_NAME]
        val aspectQueryParams = mutableListOf<String>()
        if (aspectQueryParamsCsv != null) {
            aspectQueryParams.addAll(aspectQueryParamsCsv.toString().split(","))
        }

        val values = StringBuilder()
        aspectQueryParams.forEach {
            val paramValue = queryParams[it]?.firstOrNull()
            paramValue?.let { value ->
                if (value.isNotBlank()) {
                    values.append(paramValue.trim().lowercase())
                }
            }
        }

        return md5(values.toString())
    }

    private fun md5(text: String): String {
        val crypt = MessageDigest.getInstance("MD5");
        crypt.update(text.toByteArray());
        return BigInteger(1, crypt.digest()).toString(16)
    }

}