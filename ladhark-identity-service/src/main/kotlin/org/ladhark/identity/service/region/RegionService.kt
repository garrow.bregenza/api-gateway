package org.ladhark.identity.service.region

import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.toList
import org.ladhark.identity.api.dto.region.RegionDetails
import org.ladhark.identity.aspect.cache.*
import org.ladhark.identity.aspect.cache.event.CacheRemoveEvent
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.exception.EntityAlreadyPresentException
import org.ladhark.identity.exception.EntityNotFoundException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.exception.InvalidStateException
import org.ladhark.identity.repository.region.RegionRepository
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID
import org.ladhark.identity.service.base.BaseService
import org.ladhark.identity.service.context.withUserContext
import org.ladhark.identity.service.region.cache.REGION_CACHE_BY_NAME_TO_UUID
import org.ladhark.identity.service.region.cache.REGION_CACHE_BY_UUID
import org.ladhark.identity.service.region.cache.RegionCacheService
import org.ladhark.identity.service.region.event.*
import org.ladhark.kotlin.extension.s
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

interface RegionService {
    suspend fun findByUuid(uuid: String): Region?
    suspend fun findByName(name: String): Region?
    suspend fun createRegion(regionDetails: RegionDetails): Region
    suspend fun findPublicRegions(): List<Region>
    suspend fun findAllRegions(): List<Region>

    // fun search(searchParams: SearchParams): SearchResult<Region>
    suspend fun updateRegion(uuid: String, regionDetails: RegionDetails): Region
    suspend fun deleteRegion(uuid: String): Region
}

@Service
class RegionServiceImpl : BaseService(), RegionService {

    val log: Logger = LoggerFactory.getLogger(RegionServiceImpl::class.java)

    @Autowired
    private lateinit var regionRepository: RegionRepository

    @Autowired
    private lateinit var regionCacheService: RegionCacheService

    @Transactional(readOnly = true)
    @GetFromCache(REGION_CACHE_BY_UUID)
    override suspend fun findByUuid(uuid: String): Region? = withUserContext {

        if (uuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching region details for uuid {}", uuid)

        return regionRepository
            .findRegionByUuid(uuid.uuid)?.also { region ->
                publishEvent(RegionReadEvent(this, region, AuthenticatedUserContextUUID(principalUUID())))
            }
    }

    @Transactional(readOnly = true)
    @GetFromCache(REGION_CACHE_BY_NAME_TO_UUID)
    override suspend fun findByName(name: String): Region? = withUserContext {

        if (name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        log.info("Fetching region details for name {}", name)

        return regionRepository
            .findRegionByName(name)?.also { region ->
                publishEvent(RegionReadEvent(this, region, AuthenticatedUserContextUUID(principalUUID())))
            }
    }

    @Transactional(readOnly = true)
    override suspend fun findPublicRegions(): List<Region> = withUserContext {
        log.info("Fetching public regions details.")

        val regions = regionRepository
            .findAllPublicRegions()
            .toList()

        val regionPage = RegionPage(regions.map { it.uuid.s }.toSet())

        publishEvent(
            RegionPageReadEvent(
                this, regionPage, AuthenticatedUserContextUUID(principalUUID()),
                appMessage.getMessage(
                    "message.region.event.read.page.access",
                    arrayOf(regionPage.uuidsCsv(), principalId(), "`page region`")
                )
            )
        )

        return regions
    }

    @Transactional(readOnly = true)
    override suspend fun findAllRegions(): List<Region> = withUserContext {
        log.info("Fetching all regions details.")

        val regions = regionRepository
            .findAllWithParentRegion()
            .toList()

        val regionPage = RegionPage(regions.map { it.uuid.s }.toSet())

        publishEvent(
            RegionPageReadEvent(
                this, regionPage, AuthenticatedUserContextUUID(principalUUID()),
                appMessage.getMessage(
                    "message.region.event.read.page.access",
                    arrayOf(regionPage.uuidsCsv(), principalId(), "`page region`")
                )
            )
        )

        return regions
    }

    /*@Transactional(readOnly = true)
    override fun search(searchParams: SearchParams): SearchResult<Region> {
        return super.search(searchParams, Region::class.java)
    }*/

    @Transactional(rollbackFor = [Exception::class])
    @PutInCaches(caches = [
        PutInCache(REGION_CACHE_BY_UUID),
        PutInCache(REGION_CACHE_BY_NAME_TO_UUID),
    ])
    override suspend fun createRegion(regionDetails: RegionDetails): Region = withUserContext {

        if (regionDetails.name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        validateGlobalRegionState(regionDetails)

        val inferredParentRegion = inferParentRegion(regionDetails)

        val region = Region().apply {
            name = regionDetails.name
            description = regionDetails.description
            global = regionDetails.global
            public = regionDetails.public

            if (inferredParentRegion.isPresent) parentRegion = inferredParentRegion.get()

            log.info("Saving region with name {}", name)
        }

        return regionRepository.save(region).also { regionFromDb ->
            publishEvent(RegionCreateEvent(this, regionFromDb, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    @Suppress("UNCHECKED_CAST")
    @Transactional(rollbackFor = [Exception::class])
    @PutInCaches(caches = [
        PutInCache(REGION_CACHE_BY_UUID),
        PutInCache(REGION_CACHE_BY_NAME_TO_UUID),
    ])
    override suspend fun updateRegion(uuid: String, regionDetails: RegionDetails): Region = withUserContext {

        if (uuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "UUID"))
        }

        validateGlobalRegionState(regionDetails)

        val records = coroutineScope {
            awaitAll(
                async {
                    inferParentRegion(regionDetails)
                },
                async {
                    log.info("Fetching region details for uuid {}", uuid)
                    regionRepository
                        .findRegionByUuid(uuid.uuid) ?: throw EntityNotFoundException(
                        appMessage.getMessage("error.region.not.found.by.uuid", uuid)
                    )
                }
            )
        }

        val parentRegion = records[0] as Optional<Region>
        val regionToUpdate = records[1] as Region

        regionRepository
            .findRegionExistsForUpdateWithName(regionDetails.name, regionToUpdate.name)
            ?.let {
                throw EntityAlreadyPresentException(appMessage.getMessage("error.global.region.already.present"))
            }

        regionToUpdate.apply {
            log.info("Updating region with name {}", name)

            if (regionDetails.name != name) {
                log.info(
                    "Region details clearing on update by name as name change detected from {} to {}.",
                    name,
                    regionDetails.name
                )
                regionCacheService.removeRegionByNameToUuidAsync(name).also {
                    publishEvent(CacheRemoveEvent(this, name, regionToUpdate, AuthenticatedUserContextUUID(principalUUID())))
                }
            }

            name = regionDetails.name
            description = regionDetails.description
            global = regionDetails.global
            public = regionDetails.public

            if (parentRegion.isPresent) this.parentRegion = parentRegion.get()
        }

        return regionRepository.save(regionToUpdate).also { regionFromDb ->
            publishEvent(RegionUpdateEvent(this, regionFromDb, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    @Transactional(rollbackFor = [Exception::class])
    @RemoveFromCaches(caches = [
        RemoveFromCache(REGION_CACHE_BY_UUID),
        RemoveFromCache(REGION_CACHE_BY_NAME_TO_UUID),
    ])
    override suspend fun deleteRegion(uuid: String): Region = withUserContext {

        if (uuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "UUID"))
        }

        return regionRepository
            .findRegionByUuid(uuid.uuid)
            ?.let { region ->
                region.deleted = true

                log.info("Deleting region with uuid {}", region.uuid)

                regionRepository
                    .save(region)
                    .also { regionFromDb ->
                        publishEvent(RegionDeleteEvent(this, regionFromDb, AuthenticatedUserContextUUID(principalUUID())))
                    }
            }
            ?: throw EntityNotFoundException(appMessage.getMessage("error.region.not.found.by.uuid", uuid))
    }


    private suspend fun inferParentRegion(regionDetails: RegionDetails): Optional<Region> {

        val regionOptional = if (!regionDetails.parentRegionUuid.isNullOrBlank()) {
            regionRepository
                .findRegionByUuid(regionDetails.parentRegionUuid!!.uuid)?.let { regionFromDb ->
                    Optional.of(regionFromDb)
                } ?: run {
                throw EntityNotFoundException(
                    appMessage.getMessage(
                        "error.parent.region.not.found.by.uuid",
                        regionDetails.parentRegionUuid!!
                    )
                )
            }

        } else {
            Optional.empty()
        }

        if (regionOptional.isPresent) {

            if (regionDetails.name == regionOptional.get().name) {
                throw InvalidStateException("Child and parent is same.")
            }

            regionRepository
                .findChildInChainByName(regionOptional.get().name, regionDetails.name)
                ?.let {
                    throw InvalidStateException("Child and parent relation already exists.")
                }
        }

        return regionOptional
    }

    private suspend fun validateGlobalRegionState(regionDetails: RegionDetails): Boolean {

        if (regionDetails.global && !regionDetails.parentRegionUuid.isNullOrBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.global.region.cannot.have.parent"))
        }

        return if (regionDetails.global) {
            regionRepository
                .findRegionByGlobal(true)
                ?.let {
                    throw EntityAlreadyPresentException(appMessage.getMessage("error.global.region.already.present"))
                } ?: true

        } else {
            true
        }
    }

}