package org.ladhark.identity.service.aspect.event

import org.ladhark.identity.entity.aspect.Aspect
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class AspectCreateEvent(source: Any, private val aspect: Aspect, private val contextUser: AuthenticatedUserContextUUID) : BaseEvent<Aspect>(source, aspect, AspectEventCategory.ASPECT, ApplicationEventAction.CREATE) {
    override fun summary(): String {
        return "Aspect with name ${aspect.name} and id ${aspect.uuid} created by User $contextUser"
    }
}

class AspectReadEvent(source: Any, private val aspect: Aspect, private val contextUser: AuthenticatedUserContextUUID, private val summary: String = "") : BaseEvent<Aspect>(source, aspect, AspectEventCategory.ASPECT, ApplicationEventAction.READ) {
    override fun summary(): String {
        return summary.ifBlank {
            return "Aspect with name ${aspect.name} and id ${aspect.uuid} read by User $contextUser"
        }
    }
}

class AspectUpdateEvent(source: Any, private val aspect: Aspect, private val contextUser: AuthenticatedUserContextUUID) : BaseEvent<Aspect>(source, aspect, AspectEventCategory.ASPECT, ApplicationEventAction.UPDATE) {
    override fun summary(): String {
        return "Aspect with name ${aspect.name} and id ${aspect.uuid} updated by User $contextUser"
    }
}

class AspectDeleteEvent(source: Any, private val aspect: Aspect, private val contextUser: AuthenticatedUserContextUUID) : BaseEvent<Aspect>(source, aspect, AspectEventCategory.ASPECT, ApplicationEventAction.DELETE) {
    override fun summary(): String {
        return "Aspect with name ${aspect.name} and id ${aspect.uuid} deleted by User $contextUser"
    }
}

enum class AspectEventCategory: EventCategory {
    ASPECT {
        override fun type(): String {
            return ASPECT.name
        }
    }
}

