package org.ladhark.identity.service.aspect.strategy

import org.ladhark.identity.entity.aspect.Aspect
import org.ladhark.identity.entity.resource.ResourceAction

interface AspectStrategy {
    suspend fun action(context: Map<String, Any>, resourceAction: ResourceAction, aspect: Aspect): Map<String, Any>
    suspend fun clear(context: Map<String, Any>, resourceAction: ResourceAction, aspect: Aspect)
}