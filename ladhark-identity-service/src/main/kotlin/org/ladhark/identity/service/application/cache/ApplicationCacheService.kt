package org.ladhark.identity.service.application.cache

import com.github.benmanes.caffeine.cache.AsyncCache
import com.github.benmanes.caffeine.cache.Caffeine
import jakarta.annotation.PostConstruct
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.application.Application
import org.ladhark.identity.repository.application.ApplicationRepository
import org.ladhark.identity.service.base.BaseCacheService
import org.ladhark.kotlin.extension.s
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

const val APPLICATION_CACHE_BY_UUID = "APPLICATION_CACHE_BY_UUID"
const val APPLICATION_CACHE_BY_NAME_TO_UUID = "APPLICATION_CACHE_BY_NAME_TO_UUID"

interface ApplicationCacheService {
    suspend fun getApplicationByNameToUuidAsync(key: String): Application?
    suspend fun putApplicationByNameToUuidAsync(key: String, value: Application)
    suspend fun removeApplicationByNameToUuidAsync(key: String)
    suspend fun getApplicationByUuidAsync(key: String): Application?
    suspend fun putApplicationByUuidAsync(key: String, value: Application)
    suspend fun removeApplicationByUuidAsync(key: String)
    suspend fun allEntriesInUuidCache(): MutableCollection<Application>
}

@Service
class ApplicationCacheServiceImpl : BaseCacheService(), ApplicationCacheService {

    @Autowired
    lateinit var applicationRepository: ApplicationRepository

    lateinit var applicationCacheByUUID: AsyncCache<String, Application>

    lateinit var applicationCacheByNameToUUID: AsyncCache<String, String>

    @PostConstruct
    private fun postConstruct() {

        applicationCacheByUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        applicationCacheByNameToUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        registerProvider(this, APPLICATION_CACHE_BY_UUID, APPLICATION_CACHE_BY_NAME_TO_UUID)

        applicationRepository.subscribeAndDoOnAll {
            with(it) {
                applicationCacheByUUID.put(uuid.s, CompletableFuture.completedFuture(this))
                applicationCacheByNameToUUID.put(name, CompletableFuture.completedFuture(uuid.s))
            }
        }
    }

    override suspend fun getApplicationByNameToUuidAsync(key: String): Application? {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.getValueAsync(key, applicationCacheByNameToUUID)
            return uuid?.let {
                caffeineCacheHelperService.getValueAsync(it, applicationCacheByUUID)
            }
        }
    }

    override suspend fun putApplicationByNameToUuidAsync(key: String, value: Application) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value.uuid.s, applicationCacheByNameToUUID)
            caffeineCacheHelperService.putValueAsync(value.uuid.s, value, applicationCacheByUUID)
        }
    }

    override suspend fun removeApplicationByNameToUuidAsync(key: String) {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.removeValueAsync(key, applicationCacheByNameToUUID)
            uuid?.let {
                caffeineCacheHelperService.removeValueAsync(it, applicationCacheByUUID)
            }
        }
    }

    override suspend fun getApplicationByUuidAsync(key: String): Application? {
        mutex.withLock {
            return caffeineCacheHelperService.getValueAsync(key, applicationCacheByUUID)
        }
    }

    override suspend fun putApplicationByUuidAsync(key: String, value: Application) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value, applicationCacheByUUID)
        }
    }

    override suspend fun removeApplicationByUuidAsync(key: String) {
        mutex.withLock {
            caffeineCacheHelperService.removeValueAsync(key, applicationCacheByUUID)
        }
    }

    override suspend fun allEntriesInUuidCache(): MutableCollection<Application> {
        return coroutineScope {
            withContext(Dispatchers.Unconfined) {
                applicationCacheByUUID.synchronous().asMap().values
            }
        }
    }

    override suspend fun getFromCache(name: String, key: String): BaseEntity? {
        return when(name) {
            APPLICATION_CACHE_BY_UUID -> getApplicationByUuidAsync(key)
            APPLICATION_CACHE_BY_NAME_TO_UUID -> getApplicationByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun putInCache(name: String, key: String, value: BaseEntity) {

        if (value !is Application) {
            throw IllegalArgumentException("Cache value passed is not of type ${Application::class.qualifiedName}")
        }

        when(name) {
            APPLICATION_CACHE_BY_UUID -> putApplicationByUuidAsync(key, value)
            APPLICATION_CACHE_BY_NAME_TO_UUID -> putApplicationByNameToUuidAsync(key, value)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun removeFromCache(name: String, key: String) {
        when(name) {
            APPLICATION_CACHE_BY_UUID -> removeApplicationByUuidAsync(key)
            APPLICATION_CACHE_BY_NAME_TO_UUID -> removeApplicationByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

}