package org.ladhark.identity.service.client.cache

import com.github.benmanes.caffeine.cache.AsyncCache
import com.github.benmanes.caffeine.cache.Caffeine
import jakarta.annotation.PostConstruct
import kotlinx.coroutines.sync.withLock
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.client.Client
import org.ladhark.identity.repository.client.ClientRepository
import org.ladhark.identity.service.base.BaseCacheService
import org.ladhark.kotlin.extension.s
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

const val CLIENT_CACHE_BY_UUID = "CLIENT_CACHE_BY_UUID"
const val CLIENT_CACHE_BY_NAME_TO_UUID = "CLIENT_CACHE_BY_NAME_TO_UUID"

interface ClientCacheService {

    suspend fun getClientByUuidAsync(key: String): Client?
    suspend fun putClientByUuidAsync(key: String, value: Client)
    suspend fun removeClientByUuidAsync(key: String)
    suspend fun getClientByNameToUuidAsync(key: String): Client?
    suspend fun putClientByNameToUuidAsync(key: String, value: Client)
    suspend fun removeClientByNameToUuidAsync(key: String)

}

@Service
class ClientCacheServiceImpl : BaseCacheService(), ClientCacheService {

    @Autowired
    private lateinit var clientRepository: ClientRepository

    private lateinit var clientCacheByUUID: AsyncCache<String, Client>

    private lateinit var clientCacheByNameToUUID: AsyncCache<String, String>

    @PostConstruct
    private fun postConstruct() {

        clientCacheByUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.HOURS)
            .buildAsync()

        clientCacheByNameToUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.HOURS)
            .buildAsync()

        registerProvider(this, CLIENT_CACHE_BY_UUID, CLIENT_CACHE_BY_NAME_TO_UUID)

        clientRepository.subscribeAndDoOnAll {
            with(it) {
                clientCacheByUUID.put(uuid.s, CompletableFuture.completedFuture(this))
                clientCacheByNameToUUID.put(name, CompletableFuture.completedFuture(uuid.s))
            }
        }

    }

    override suspend fun getClientByNameToUuidAsync(key: String): Client? {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.getValueAsync(key, clientCacheByNameToUUID)
            return uuid?.let {
                caffeineCacheHelperService.getValueAsync(it, clientCacheByUUID)
            }
        }
    }

    override suspend fun putClientByNameToUuidAsync(key: String, value: Client) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value.uuid.s, clientCacheByNameToUUID)
            caffeineCacheHelperService.putValueAsync(value.uuid.s, value, clientCacheByUUID)
        }
    }

    override suspend fun removeClientByNameToUuidAsync(key: String) {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.removeValueAsync(key, clientCacheByNameToUUID)
            uuid?.let {
                caffeineCacheHelperService.removeValueAsync(it, clientCacheByUUID)
            }
        }
    }

    override suspend fun getClientByUuidAsync(key: String): Client? {
        mutex.withLock {
            return caffeineCacheHelperService.getValueAsync(key, clientCacheByUUID)
        }
    }

    override suspend fun putClientByUuidAsync(key: String, value: Client) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value, clientCacheByUUID)
        }
    }

    override suspend fun removeClientByUuidAsync(key: String) {
        mutex.withLock {
            caffeineCacheHelperService.removeValueAsync(key, clientCacheByUUID)
        }
    }

    override suspend fun getFromCache(name: String, key: String): BaseEntity? {
        return when(name) {
            CLIENT_CACHE_BY_UUID -> getClientByUuidAsync(key)
            CLIENT_CACHE_BY_NAME_TO_UUID -> getClientByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun putInCache(name: String, key: String, value: BaseEntity) {

        if (value !is Client) {
            throw IllegalArgumentException("Cache value passed is not of type ${Client::class.qualifiedName}")
        }

        when(name) {
            CLIENT_CACHE_BY_UUID -> putClientByUuidAsync(key, value)
            CLIENT_CACHE_BY_NAME_TO_UUID -> putClientByNameToUuidAsync(key, value)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun removeFromCache(name: String, key: String) {
        when(name) {
            CLIENT_CACHE_BY_UUID -> removeClientByUuidAsync(key)
            CLIENT_CACHE_BY_NAME_TO_UUID -> removeClientByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

}