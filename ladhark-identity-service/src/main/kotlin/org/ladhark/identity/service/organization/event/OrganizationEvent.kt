package org.ladhark.identity.service.organization.event

import org.ladhark.identity.entity.organization.Organization
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class OrganizationCreateEvent(source: Any, private val organization: Organization, private val contextUser: AuthenticatedUserContextUUID) : BaseEvent<Organization>(source, organization, OrganizationEventCategory.ORGANIZATION, ApplicationEventAction.CREATE) {
    override fun summary(): String {
        return "Organization with name ${organization.name} and id ${organization.uuid} created by User $contextUser"
    }
}

class OrganizationReadEvent(source: Any, private val organization: Organization, private val contextUser: AuthenticatedUserContextUUID) : BaseEvent<Organization>(source, organization, OrganizationEventCategory.ORGANIZATION, ApplicationEventAction.READ) {
    override fun summary(): String {
        return "Organization with name ${organization.name} and id ${organization.uuid} read by User $contextUser"
    }
}

class OrganizationUpdateEvent(source: Any, private val organization: Organization, private val contextUser: AuthenticatedUserContextUUID) : BaseEvent<Organization>(source, organization, OrganizationEventCategory.ORGANIZATION, ApplicationEventAction.UPDATE) {
    override fun summary(): String {
        return "Organization with name ${organization.name} and id ${organization.uuid} updated by User $contextUser"
    }
}

class OrganizationDeleteEvent(source: Any, private val organization: Organization, private val contextUser: AuthenticatedUserContextUUID) : BaseEvent<Organization>(source, organization, OrganizationEventCategory.ORGANIZATION, ApplicationEventAction.DELETE) {
    override fun summary(): String {
        return "Organization with name ${organization.name} and id ${organization.uuid} deleted by User $contextUser"
    }
}

enum class OrganizationEventCategory: EventCategory {
    ORGANIZATION {
        override fun type(): String {
            return ORGANIZATION.name
        }
    }
}

