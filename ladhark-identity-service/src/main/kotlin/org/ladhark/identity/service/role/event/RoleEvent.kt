package org.ladhark.identity.service.role.event

import org.ladhark.identity.entity.role.Role
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID

class RoleCreateEvent(source: Any, private val role: Role, private val contextUser: AuthenticatedUserContextUUID) :
    BaseEvent<Role>(source, role, RoleEventCategory.ROLE, ApplicationEventAction.CREATE) {
    override fun summary(): String {
        return "Role with name ${role.name} and id ${role.uuid} created by User $contextUser"
    }
}

class RoleReadEvent(
    source: Any,
    private val role: Role,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<Role>(source, role, RoleEventCategory.ROLE, ApplicationEventAction.READ) {
    override fun summary(): String {
        return summary.ifBlank {
            "Role with name ${role.name} and id ${role.uuid} read by User $contextUser"
        }
    }
}

class RolePageReadEvent(
    source: Any,
    private val rolePage: RolePage,
    private val contextUser: AuthenticatedUserContextUUID,
    private val summary: String = ""
) : BaseEvent<RolePage>(source, rolePage, RoleEventCategory.ROLE, ApplicationEventAction.READ) {
    override fun summary(): String {
        return summary.ifBlank {
            "Role with page ${rolePage.uuidsCsv()} read by User $contextUser"
        }
    }
}

class RoleUpdateEvent(source: Any, private val role: Role, private val contextUser: AuthenticatedUserContextUUID) :
    BaseEvent<Role>(source, role, RoleEventCategory.ROLE, ApplicationEventAction.UPDATE) {
    override fun summary(): String {
        return "Role with name ${role.name} and id ${role.uuid} updated by User $contextUser"
    }
}

class RoleDeleteEvent(source: Any, private val role: Role, private val contextUser: AuthenticatedUserContextUUID) :
    BaseEvent<Role>(source, role, RoleEventCategory.ROLE, ApplicationEventAction.DELETE) {
    override fun summary(): String {
        return "Role with name ${role.name} and id ${role.uuid} deleted by User $contextUser"
    }
}

enum class RoleEventCategory : EventCategory {
    ROLE {
        override fun type(): String {
            return ROLE.name
        }
    }
}


data class RolePage(val uuids: Set<String>) {
    fun uuidsCsv(): String {
        return uuids.joinToString(",")
    }
}
