package org.ladhark.identity.service.base

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.service.cache.CaffeineCacheHelperService
import org.springframework.beans.factory.annotation.Autowired

abstract class BaseCacheService {

    @Autowired
    lateinit var caffeineCacheHelperService: CaffeineCacheHelperService

    @Autowired
    lateinit var cacheRegistry: CacheRegistry

    protected val mutex = Mutex()
    abstract suspend fun getFromCache(name: String, key: String): BaseEntity?

    abstract suspend fun putInCache(name: String, key: String, value: BaseEntity)

    abstract suspend fun removeFromCache(name: String, key: String)

    fun registerProvider(provider: BaseCacheService, vararg names: String) {
        CoroutineScope(Dispatchers.Unconfined).launch {
            names.forEach {
                cacheRegistry.registerCacheProvider(it, provider)
            }
        }
    }

}