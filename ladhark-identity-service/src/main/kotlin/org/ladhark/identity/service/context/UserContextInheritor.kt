package org.ladhark.identity.service.context

import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.reactor.ReactorContext
import org.ladhark.identity.security.context.SERVICE_CONTEXT_KEY
import org.ladhark.identity.security.context.ServiceContext
import org.ladhark.identity.security.context.ServiceContextHolder

suspend inline fun <R> withUserContext(block: ServiceContext.() -> R): R {
    return block(appServiceContext())
}

suspend fun appServiceContext(): ServiceContext {
    val context = currentCoroutineContext()[ReactorContext]

    context?.let {
        with(it.context) {
            if (!hasKey(SERVICE_CONTEXT_KEY)) {
                throw IllegalStateException("Context has no value for key $SERVICE_CONTEXT_KEY")
            }

            val serviceContextHolder = get<ServiceContextHolder>(SERVICE_CONTEXT_KEY)

            if (serviceContextHolder.serviceContext.isPresent) {
                return serviceContextHolder.serviceContext.get()
            } else {
                throw IllegalStateException("Context is empty")
            }
        }
    }

    throw IllegalStateException("Context is empty")
}