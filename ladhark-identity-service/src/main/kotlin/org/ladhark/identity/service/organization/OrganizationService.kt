package org.ladhark.identity.service.organization

import org.ladhark.identity.api.dto.organization.OrganizationDetails
import org.ladhark.identity.aspect.cache.GetFromCache
import org.ladhark.identity.aspect.cache.PutInCache
import org.ladhark.identity.aspect.cache.PutInCaches
import org.ladhark.identity.entity.organization.Organization
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.repository.organization.OrganizationRepository
import org.ladhark.identity.repository.region.RegionRepository
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID
import org.ladhark.identity.service.base.BaseService
import org.ladhark.identity.service.context.withUserContext
import org.ladhark.identity.service.organization.cache.ORGANIZATION_CACHE_BY_NAME_TO_UUID
import org.ladhark.identity.service.organization.cache.ORGANIZATION_CACHE_BY_UUID
import org.ladhark.identity.service.organization.event.OrganizationCreateEvent
import org.ladhark.identity.service.organization.event.OrganizationReadEvent
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

interface OrganizationService {
    suspend fun findByUuid(uuid: String): Organization?
    suspend fun findByName(name: String): Organization?
    suspend fun createOrganization(organizationDetails: OrganizationDetails): Organization
}

@Service
class OrganizationServiceImpl: BaseService(), OrganizationService {

    val log: Logger = LoggerFactory.getLogger(OrganizationServiceImpl::class.java)

    @Autowired
    private lateinit var organizationRepository: OrganizationRepository

    @Autowired
    private lateinit var regionRepository: RegionRepository

    @Transactional(readOnly = true)
    @GetFromCache(ORGANIZATION_CACHE_BY_UUID)
    override suspend fun findByUuid(uuid: String): Organization? = withUserContext {

        if (uuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching organization details for uuid {}", uuid)

        return organizationRepository.findOrganizationByUuid(uuid.uuid)?.also { organization ->
            publishEvent(OrganizationReadEvent(this, organization, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    @Transactional(readOnly = true)
    @GetFromCache(ORGANIZATION_CACHE_BY_NAME_TO_UUID)
    override suspend fun findByName(name: String): Organization? = withUserContext {

        if (name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        log.info("Fetching organization details for name {}", name)

        return organizationRepository.findOrganizationByName(name)?.also { organization ->
            publishEvent(OrganizationReadEvent(this, organization, AuthenticatedUserContextUUID(principalUUID())))
        }
    }


    @Transactional(rollbackFor = [Exception::class])
    @PutInCaches(caches = [
        PutInCache(ORGANIZATION_CACHE_BY_UUID),
        PutInCache(ORGANIZATION_CACHE_BY_NAME_TO_UUID),
    ])
    override suspend fun createOrganization(organizationDetails: OrganizationDetails): Organization = withUserContext {

        if (organizationDetails.name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        log.info("Saving organization details with name {}", organizationDetails.name)

        val organization = Organization().apply {
            name = organizationDetails.name
        }


        return organizationRepository.save(organization).also { organizationFromDb ->
            publishEvent(OrganizationCreateEvent(this, organizationFromDb, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

}