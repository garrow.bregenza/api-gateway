package org.ladhark.identity.service.role.cache

import com.github.benmanes.caffeine.cache.AsyncCache
import com.github.benmanes.caffeine.cache.Caffeine
import jakarta.annotation.PostConstruct
import kotlinx.coroutines.sync.withLock
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.role.Role
import org.ladhark.identity.repository.role.RoleRepository
import org.ladhark.identity.service.application.cache.APPLICATION_CACHE_BY_NAME_TO_UUID
import org.ladhark.identity.service.application.cache.APPLICATION_CACHE_BY_UUID
import org.ladhark.identity.service.base.BaseCacheService
import org.ladhark.kotlin.extension.s
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

const val ROLE_CACHE_BY_UUID = "ROLE_CACHE_BY_UUID"
const val ROLE_CACHE_BY_NAME_TO_UUID = "ROLE_CACHE_BY_NAME_TO_UUID"

interface RoleCacheService {
    suspend fun getRoleByNameToUuidAsync(key: String): Role?
    suspend fun putRoleByNameToUuidAsync(key: String, value: Role)
    suspend fun removeRoleByNameToUuidAsync(key: String)
    suspend fun getRoleByUuidAsync(key: String): Role?
    suspend fun putRoleByUuidAsync(key: String, value: Role)
    suspend fun removeRoleByUuidAsync(key: String)
}

@Service
class RoleCacheServiceImpl : BaseCacheService(), RoleCacheService {

    @Autowired
    lateinit var roleRepository: RoleRepository

    lateinit var roleCacheByUUID: AsyncCache<String, Role>

    lateinit var roleCacheByNameToUUID: AsyncCache<String, String>

    @PostConstruct
    private fun postConstruct() {

        roleCacheByUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.HOURS)
            .buildAsync()

        roleCacheByNameToUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.HOURS)
            .buildAsync()

        registerProvider(this, ROLE_CACHE_BY_UUID, ROLE_CACHE_BY_NAME_TO_UUID)

        roleRepository.subscribeAndDoOnAll {
            with(it) {
                roleCacheByUUID.put(uuid.s, CompletableFuture.completedFuture(this))
                roleCacheByNameToUUID.put(name, CompletableFuture.completedFuture(uuid.s))
            }
        }
    }

    override suspend fun getRoleByNameToUuidAsync(key: String): Role? {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.getValueAsync(key, roleCacheByNameToUUID)
            return uuid?.let {
                caffeineCacheHelperService.getValueAsync(it, roleCacheByUUID)
            }
        }
    }

    override suspend fun putRoleByNameToUuidAsync(key: String, value: Role) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value.uuid.s, roleCacheByNameToUUID)
            caffeineCacheHelperService.putValueAsync(value.uuid.s, value, roleCacheByUUID)
        }
    }

    override suspend fun removeRoleByNameToUuidAsync(key: String) {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.removeValueAsync(key, roleCacheByNameToUUID)
            uuid?.let {
                caffeineCacheHelperService.removeValueAsync(it, roleCacheByUUID)
            }
        }
    }

    override suspend fun getRoleByUuidAsync(key: String): Role? {
        mutex.withLock {
            return caffeineCacheHelperService.getValueAsync(key, roleCacheByUUID)
        }
    }

    override suspend fun putRoleByUuidAsync(key: String, value: Role) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value, roleCacheByUUID)
        }
    }

    override suspend fun removeRoleByUuidAsync(key: String) {
        mutex.withLock {
            caffeineCacheHelperService.removeValueAsync(key, roleCacheByUUID)
        }
    }

    override suspend fun getFromCache(name: String, key: String): BaseEntity? {
        return when(name) {
            APPLICATION_CACHE_BY_UUID -> getRoleByUuidAsync(key)
            APPLICATION_CACHE_BY_NAME_TO_UUID -> getRoleByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun putInCache(name: String, key: String, value: BaseEntity) {

        if (value !is Role) {
            throw IllegalArgumentException("Cache value passed is not of type ${Role::class.qualifiedName}")
        }

        when(name) {
            APPLICATION_CACHE_BY_UUID -> putRoleByUuidAsync(key, value)
            APPLICATION_CACHE_BY_NAME_TO_UUID -> putRoleByNameToUuidAsync(key, value)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun removeFromCache(name: String, key: String) {
        when(name) {
            APPLICATION_CACHE_BY_UUID -> removeRoleByUuidAsync(key)
            APPLICATION_CACHE_BY_NAME_TO_UUID -> removeRoleByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }
}