package org.ladhark.identity.service.aspect

import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.onEach
import org.ladhark.identity.api.dto.aspect.AspectDetails
import org.ladhark.identity.aspect.cache.GetFromCache
import org.ladhark.identity.aspect.cache.PutInCache
import org.ladhark.identity.aspect.cache.PutInCaches
import org.ladhark.identity.aspect.cache.event.CacheReadEvent
import org.ladhark.identity.entity.aspect.Aspect
import org.ladhark.identity.entity.aspect.AspectType
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.entity.resource.ResourceAction
import org.ladhark.identity.exception.EntityAlreadyPresentException
import org.ladhark.identity.exception.EntityNotFoundException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.repository.aspect.AspectRepository
import org.ladhark.identity.repository.aspect.AspectTypeRepository
import org.ladhark.identity.repository.region.RegionRepository
import org.ladhark.identity.repository.resource.ResourceActionRepository
import org.ladhark.identity.service.aspect.cache.ASPECT_CACHE_BY_NAME_TO_UUID
import org.ladhark.identity.service.aspect.cache.ASPECT_CACHE_BY_RESOURCE_ACTION_UUID_TO_UUID
import org.ladhark.identity.service.aspect.cache.ASPECT_CACHE_BY_UUID
import org.ladhark.identity.service.aspect.cache.AspectCacheService
import org.ladhark.identity.service.aspect.event.AspectCreateEvent
import org.ladhark.identity.service.aspect.event.AspectReadEvent
import org.ladhark.identity.service.aspect.event.AspectTypeReadEvent
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID
import org.ladhark.identity.service.base.BaseService
import org.ladhark.identity.service.context.withUserContext
import org.ladhark.identity.service.region.event.RegionReadEvent
import org.ladhark.identity.service.resource.event.ResourceActionReadEvent
import org.ladhark.kotlin.extension.s
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

interface AspectService {
    suspend fun createAspect(aspectDetails: AspectDetails): Aspect
    suspend fun findByUuid(uuid: String): Aspect?
    suspend fun findByName(name: String): Aspect?
    suspend fun findByResourceActionUuid(uuid: UUID): Flow<Aspect>
}

@Service
class AspectServiceImpl: BaseService(), AspectService {

    val log: Logger = LoggerFactory.getLogger(AspectServiceImpl::class.java)

    @Autowired
    private lateinit var aspectRepository: AspectRepository

    @Autowired
    private lateinit var aspectTypeRepository: AspectTypeRepository

    @Autowired
    private lateinit var regionRepository: RegionRepository

    @Autowired
    private lateinit var resourceActionRepository: ResourceActionRepository

    @Autowired
    private lateinit var aspectCacheService: AspectCacheService

    @Transactional(rollbackFor = [Exception::class])
    @PutInCaches(caches = [
        PutInCache(ASPECT_CACHE_BY_UUID),
        PutInCache(ASPECT_CACHE_BY_NAME_TO_UUID),
    ])
    override suspend fun createAspect(aspectDetails: AspectDetails): Aspect = withUserContext {

        if (aspectDetails.name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        if (aspectDetails.typeUuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Type"))
        }

        if (aspectDetails.resourceActionUuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Resource Action"))
        }

        if (aspectDetails.enforceRegion &&
            aspectDetails.regionUuid.isNullOrBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Region (Enforced)"))
        }

        findByName(aspectDetails.name)?.let {
            throw EntityAlreadyPresentException(
                appMessage.getMessage(
                    "error.entity.already.present.same.name",
                    aspectDetails.name
                )
            )
        } ?: run {

            val records = coroutineScope {
                awaitAll(async {
                    aspectTypeRepository
                        .findAspectTypeByUuid(aspectDetails.typeUuid.uuid)?.also { aspectType ->
                            publishEvent(
                                AspectTypeReadEvent(this, aspectType, AuthenticatedUserContextUUID(principalUUID()),
                                    appMessage.getMessage("message.application.event.read.secondary.access",
                                        arrayOf(aspectType.name, aspectType.uuid.s, principalId(), "`create resource`")))
                            )
                        }
                        ?: throw EntityNotFoundException(getMessage("error.application.not.found.by.uuid",
                            aspectDetails.typeUuid))
                },
                async {
                    resourceActionRepository
                        .findResourceActionByUuid(aspectDetails.resourceActionUuid.uuid)?.also { resourceAction ->
                            publishEvent(
                                ResourceActionReadEvent(
                                    this, resourceAction, AuthenticatedUserContextUUID(principalUUID()),
                                    appMessage.getMessage(
                                        "message.application.event.read.secondary.access",
                                        arrayOf(resourceAction.name, resourceAction.uuid.s, principalId(), "`create resource`")
                                    )
                                )
                            )
                        } ?: throw EntityNotFoundException(
                                getMessage(
                                    "error.application.not.found.by.uuid",
                                    aspectDetails.resourceActionUuid
                                )
                            )
                },
                async {
                    var region: Region? = null

                    if (aspectDetails.enforceRegion
                        && !aspectDetails.regionUuid.isNullOrBlank()) {
                        region = regionRepository
                            .findRegionByUuid(aspectDetails.regionUuid!!.uuid) ?:
                                throw EntityNotFoundException(
                                    getMessage(
                                        "error.resource.action.not.found.by.uuid",
                                        aspectDetails.resourceActionUuid
                                    )
                                )

                        publishEvent(
                            RegionReadEvent(
                                this, region, AuthenticatedUserContextUUID(principalUUID()),
                                appMessage.getMessage(
                                    "message.application.event.read.secondary.access",
                                    arrayOf(region.name, region.uuid.s, principalId(), "`create resource`")
                                )
                            )
                        )
                    }

                    region
                })
            }

            val aspectTypeFromDb = records[0] as AspectType
            val resourceActionFromDb = records[1] as ResourceAction
            val regionFromDb = records[2] as Region?

            log.info("Saving aspect details with name {}", aspectDetails.name)

            val aspect = Aspect().apply {
                name = aspectDetails.name
                type = aspectTypeFromDb
                region = regionFromDb
                resourceAction = resourceActionFromDb
                enforceRegion = aspectDetails.enforceRegion
                order = aspectDetails.order
                properties.putAll(aspectDetails.properties)
            }


            return aspectRepository.save(aspect).also { aspectFromDb ->
                publishEvent(AspectCreateEvent(this, aspectFromDb, AuthenticatedUserContextUUID(principalUUID())))
            }

        }
    }

    @Transactional(readOnly = true)
    @GetFromCache(ASPECT_CACHE_BY_UUID)
    override suspend fun findByUuid(uuid: String): Aspect? = withUserContext {
        if (uuid.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching aspect details for uuid {}", uuid)

        return aspectRepository.findAspectByUuid(uuid.uuid)?.also { aspect ->
            publishEvent(AspectReadEvent(this, aspect, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    @Transactional(readOnly = true)
    @GetFromCache(ASPECT_CACHE_BY_NAME_TO_UUID)
    override suspend fun findByName(name: String): Aspect? = withUserContext {
        if (name.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "Name"))
        }

        log.info("Fetching aspect details for name {}", name)

        return aspectRepository.findAspectByName(name)?.also { aspect ->
            publishEvent(AspectReadEvent(this, aspect, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    @Transactional(readOnly = true)
    override suspend fun findByResourceActionUuid(uuid: UUID): Flow<Aspect> = withUserContext {

        log.info("Fetching aspect details for resource action uuid {}", uuid)

        val aspectsFromCache = aspectCacheService.getAspectsByResourceActionUuidToUuidAsync(uuid.s)

        return if (aspectsFromCache.isEmpty()) {
                resourceActionRepository
                .findResourceActionByUuid(uuid)?.let { resourceAction ->
                    publishEvent(
                        ResourceActionReadEvent(
                            this, resourceAction, AuthenticatedUserContextUUID(principalUUID()),
                            appMessage.getMessage(
                                "message.application.event.read.secondary.access",
                                arrayOf(resourceAction.name, resourceAction.uuid.s, principalId(), "`create resource`")
                            )
                        )
                    )

                    aspectRepository
                        .findAspectsByResourceActionUuid(resourceAction.uuid).also { aspects ->
                            aspects.onEach { aspect ->
                                publishEvent(AspectReadEvent(this, aspect, AuthenticatedUserContextUUID(principalUUID())))
                            }
                        }
                } ?: emptyFlow()
        } else {

            aspectsFromCache.forEach {
                publishEvent(
                    CacheReadEvent(this, ASPECT_CACHE_BY_RESOURCE_ACTION_UUID_TO_UUID, it,
                        AuthenticatedUserContextUUID(principalUUID()))
                )
            }

            aspectsFromCache.asFlow()
        }
    }

}