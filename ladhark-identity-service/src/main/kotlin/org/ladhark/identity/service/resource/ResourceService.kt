package org.ladhark.identity.service.resource

import org.ladhark.identity.api.dto.resource.ResourceDetails
import org.ladhark.identity.aspect.cache.GetFromCache
import org.ladhark.identity.aspect.cache.PutInCache
import org.ladhark.identity.aspect.cache.PutInCaches
import org.ladhark.identity.entity.application.Application
import org.ladhark.identity.entity.resource.Resource
import org.ladhark.identity.entity.resource.ResourceAction
import org.ladhark.identity.entity.resource.Scope
import org.ladhark.identity.entity.resource.policy.*
import org.ladhark.identity.exception.EntityAlreadyPresentException
import org.ladhark.identity.exception.EntityNotFoundException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.exception.InvalidStateException
import org.ladhark.identity.policy.context.ClientContext
import org.ladhark.identity.policy.context.UserContext
import org.ladhark.identity.policy.context.extractor.ValueExtractorFactory
import org.ladhark.identity.policy.context.model.Client
import org.ladhark.identity.policy.context.model.User
import org.ladhark.identity.policy.context.token.PolicyRestrictionTokenParser
import org.ladhark.identity.repository.application.ApplicationRepository
import org.ladhark.identity.repository.resource.ResourceActionRepository
import org.ladhark.identity.repository.resource.ResourceRepository
import org.ladhark.identity.repository.resource.policy.ResourcePolicyRepository
import org.ladhark.identity.security.context.ServiceContext
import org.ladhark.identity.service.application.event.ApplicationReadEvent
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID
import org.ladhark.identity.service.base.BaseService
import org.ladhark.identity.service.context.withUserContext
import org.ladhark.identity.service.resource.cache.RESOURCE_CACHE_BY_NAME_TO_UUID
import org.ladhark.identity.service.resource.cache.RESOURCE_CACHE_BY_UUID
import org.ladhark.identity.service.resource.event.ResourceCreateEvent
import org.ladhark.identity.service.resource.event.ResourceReadEvent
import org.ladhark.kotlin.extension.s
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

interface ResourceService {

    suspend fun findResourcePolicyByResourceAction(resourceAction: ResourceAction): ResourcePolicy?
    suspend fun findApplicationForUriPatternAndScope(uriPattern: String, scope: Scope): Application?
    suspend fun canAccessResource(resourcePolicy: ResourcePolicy): Boolean
    fun isOpenPublic(resourcePolicy: ResourcePolicy): Boolean
    suspend fun createResource(resourceDetails: ResourceDetails): Resource
    suspend fun findByUuid(uuid: String): Resource?
    suspend fun findByName(name: String): Resource?
}

@Service
class ResourceServiceImpl : BaseService(), ResourceService {

    val log: Logger = LoggerFactory.getLogger(ResourceServiceImpl::class.java)

    @Autowired
    lateinit var applicationRepository: ApplicationRepository

    @Autowired
    lateinit var resourceActionRepository: ResourceActionRepository

    @Autowired
    lateinit var resourcePolicyRepository: ResourcePolicyRepository

    @Autowired
    lateinit var resourceRepository: ResourceRepository

    @Autowired
    lateinit var policyRestrictionTokenParser: PolicyRestrictionTokenParser

    @Autowired
    lateinit var valueExtractorFactory: ValueExtractorFactory

    @Transactional(readOnly = true)
    override suspend fun findResourcePolicyByResourceAction(resourceAction: ResourceAction): ResourcePolicy? = withUserContext {
        return resourcePolicyRepository.findByResourceActionUuid(resourceAction.uuid)
    }

    @Transactional(readOnly = true)
    override suspend fun findApplicationForUriPatternAndScope(uriPattern: String, scope: Scope): Application? = withUserContext {
        return resourceActionRepository.findApplicationForUriPatternAndScope(uriPattern, scope)
    }

    override fun isOpenPublic(resourcePolicy: ResourcePolicy): Boolean {
        val policySet = resourcePolicy.policySet

        val policies = policySet.hasPolicies
        val results = policies.map { policy ->
            val conditions = policy.hasPolicyConditions
            val conditionTypes = conditions.map { c -> c.policyConditionType }
            conditionTypes.contains(PolicyConditionType.PUBLIC_OPEN) && conditions.find {
                it.key == "isOpen" && it.value == "true"
            } != null
        }

        return results.firstOrNull {
            it
        } != null
    }

    @Transactional(readOnly = true)
    override suspend fun canAccessResource(resourcePolicy: ResourcePolicy): Boolean = withUserContext {
        // TODO assert authority, event firing, logs

        val policySet = resourcePolicy.policySet

        /**
         * 1. Map User into evaluator user context model
         * 2. Build policy evaluators according to policy rule specified
         * 3. Evaluate each policy according evaluators defined
         * 4. According Resource Policy strategy compute final result from all policies result
         * 5. If true let it pass
         *
         * TODO::
         *
         * Spring model to Context evaluator model
         * Evaluator factory from each policy defined.
         *
         */
        /**
         * 1. Map User into evaluator user context model
         * 2. Build policy evaluators according to policy rule specified
         * 3. Evaluate each policy according evaluators defined
         * 4. According Resource Policy strategy compute final result from all policies result
         * 5. If true let it pass
         *
         * TODO::
         *
         * Spring model to Context evaluator model
         * Evaluator factory from each policy defined.
         *
         */
        val policies = policySet.hasPolicies
        val results = policies.map { policy ->
            val conditions = policy.hasPolicyConditions
            val conditionTypes = conditions.map { c -> c.policyConditionType }

            validateConditionTypes(conditionTypes)

            if (conditionTypes.contains(PolicyConditionType.REGION_UUID)
                || conditionTypes.contains(PolicyConditionType.TEAM_UUID)
                || conditionTypes.contains(PolicyConditionType.ROLE_UUID)
                || conditionTypes.contains(PolicyConditionType.AUTHORITY_UUID)
                || conditionTypes.contains(PolicyConditionType.REGION_NAME)
                || conditionTypes.contains(PolicyConditionType.TEAM_NAME)
                || conditionTypes.contains(PolicyConditionType.ROLE_NAME)
                || conditionTypes.contains(PolicyConditionType.AUTHORITY_NAME)
            ) {
                val result = processPolicyConditions(conditions, this)
                if (policy.policyAccessType == PolicyAccessType.ALLOW) result else !result
            } else {
                false
            }

            /*when {
                conditionTypes.contains(PolicyConditionType.IP) -> {
                    TODO()
                }
                conditionTypes.contains(PolicyConditionType.HTTP_HEADERS) -> {
                    TODO()
                }
                conditionTypes.contains(PolicyConditionType.TIME) -> {
                    TODO()
                }
                conditionTypes.contains(PolicyConditionType.SCRIPT) -> {
                    TODO()
                }
                conditionTypes.contains(PolicyConditionType.CLIENT) -> {
                    TODO()
                }
                conditionTypes.contains(PolicyConditionType.USER_UUID) -> {
                    TODO()
                }
                conditionTypes.contains(PolicyConditionType.USER_EMAIL) -> {
                    TODO()
                }
                else -> {
                    result = process(conditions)
                }
            }

            return result*/

        }

        // allow according to evaluation type
        return when (policySet.policyEvaluationType) {

            PolicyEvaluationType.UNANIMOUS -> {

                val falsePresent = results.firstOrNull {
                    !it
                }

                falsePresent == null
            }

            PolicyEvaluationType.AFFIRMATIVE -> {
                val truePresent = results.firstOrNull {
                    it
                }

                truePresent != null
            }

            PolicyEvaluationType.CONSENSUS -> {
                val tfCount = results.groupBy { it.toString() }
                (tfCount[true.toString()]?.size ?: 0) >= (tfCount[false.toString()]?.size ?: 0)
            }

            else -> {
                false
            }
        }


    }

    @Transactional(rollbackFor = [Exception::class])
    @PutInCaches(caches = [
        PutInCache(RESOURCE_CACHE_BY_UUID),
        PutInCache(RESOURCE_CACHE_BY_NAME_TO_UUID),
    ])
    override suspend fun createResource(resourceDetails: ResourceDetails): Resource = withUserContext {

        if (resourceDetails.name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        if (resourceDetails.applicationUuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Application UUID"))
        }

        return resourceRepository.findResourceByName(resourceDetails.name)?.let {
            throw EntityAlreadyPresentException(
                appMessage.getMessage(
                    "error.entity.already.present.same.name",
                    resourceDetails.name
                )
            )
        } ?: run {
            val applicationToLink = applicationRepository
                .findApplicationByUuid(resourceDetails.applicationUuid.uuid)?.also { application ->
                    publishEvent(
                        ApplicationReadEvent(
                            this, application, AuthenticatedUserContextUUID(principalUUID()),
                            appMessage.getMessage(
                                "message.application.event.read.secondary.access",
                                arrayOf(application.name, application.uuid.s, principalId(), "`create resource`")
                            )
                        )
                    )
                } ?: throw EntityNotFoundException(
                getMessage(
                    "error.application.not.found.by.uuid",
                    resourceDetails.applicationUuid
                )
            )

            log.info("Saving resource details with name {}", resourceDetails.name)

            val resource = Resource().apply {
                application = applicationToLink
                name = resourceDetails.name
                description = resourceDetails.description
            }

            resourceRepository.save(resource).also { resourceFromDb ->
                publishEvent(ResourceCreateEvent(this, resourceFromDb, AuthenticatedUserContextUUID(principalUUID())))
            }
        }

    }

    @Transactional(readOnly = true)
    @GetFromCache(RESOURCE_CACHE_BY_UUID)
    override suspend fun findByUuid(uuid: String): Resource? = withUserContext {

        if (uuid.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching resource details for uuid {}", uuid)

        return resourceRepository.findResourceByUuid(uuid.uuid)?.also { resource ->
                publishEvent(ResourceReadEvent(this, resource, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    @Transactional(readOnly = true)
    @GetFromCache(RESOURCE_CACHE_BY_NAME_TO_UUID)
    override suspend fun findByName(name: String): Resource? = withUserContext {

        if (name.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "Name"))
        }

        log.info("Fetching resource details for name {}", name)

        return resourceRepository.findResourceByName(name)?.also { resource ->
                publishEvent(ResourceReadEvent(this, resource, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    private fun processPolicyConditions(
        conditions: MutableSet<PolicyCondition>,
        securityContext: ServiceContext
    ): Boolean {
        // region team role authority
        // region user role authority
        val conditionMap = HashMap<PolicyConditionType, MutableSet<String>>()
        conditions.forEach { condition ->
            val type = condition.policyConditionType
            val value = condition.value

            if (!conditionMap.containsKey(type)) {
                conditionMap[type] = HashSet()
            }

            if (value.isBlank()) {
                throw InvalidStateException("Value is empty")
            }

            conditionMap[type]?.add(value)
        }

        val conditionQueryTokens = ArrayList<String>()

        val regionSetting = computeRegionSetting(conditionMap)
        if (regionSetting.isNotBlank()) {
            conditionQueryTokens.add(regionSetting)
        }

        val teamSetting = computeTeamSetting(conditionMap)
        if (teamSetting.isNotBlank()) {
            conditionQueryTokens.add(teamSetting)
        }

        val userSetting = computeUserSetting(conditionMap)
        if (userSetting.isNotBlank()) {
            conditionQueryTokens.add(userSetting)
        }

        val roleSetting = computeRoleSetting(conditionMap)
        if (roleSetting.isNotBlank()) {
            conditionQueryTokens.add(roleSetting)
        }

        val authoritySetting = computeAuthoritySetting(conditionMap)
        if (authoritySetting.isNotBlank()) {
            conditionQueryTokens.add(authoritySetting)
        }

        val conditionQuery = conditionQueryTokens.joinToString(";")
        // conditionQuery something like => region:["Agra"];team:["Agra_Team"];role:["ROLE_ADMIN_AGRA"]

        val policyRestrictionTokenOptional = policyRestrictionTokenParser.parse(conditionQuery)

        if (policyRestrictionTokenOptional.isPresent) {
            val policyRestrictionToken = policyRestrictionTokenOptional.get()


            // we have the extractor now map X to user context model
            val authenticationToken = securityContext.authenticationToken

            val principal = authenticationToken.getPrincipal()
            if (principal.principal is User) {
                val user = principal.principal

                val valueExtractor = valueExtractorFactory.get(policyRestrictionToken)

                val userContext = UserContext()
                userContext.set(user)

                val result = valueExtractor.extract(listOf(conditionQuery), userContext)

                return result.isPresent
            } else if (principal.principal is Client) {
                val client = principal.principal

                val valueExtractor = valueExtractorFactory.getForClient(policyRestrictionToken)

                val clientContext = ClientContext()
                clientContext.set(client)

                val result = valueExtractor.extract(listOf(conditionQuery), clientContext)

                return result.isPresent
            }

        }

        return false
    }

    private fun validateConditionTypes(conditionTypes: List<PolicyConditionType>) {
        val size = conditionTypes.size
        val msg = "Condition type '%s' cannot be coupled with other conditions."

        if (conditionTypes.contains(PolicyConditionType.IP) && size > 1) {
            throw InvalidStateException(String.format(msg, PolicyConditionType.IP))
        }

        if (conditionTypes.contains(PolicyConditionType.HTTP_HEADERS) && size > 1) {
            throw InvalidStateException(String.format(msg, PolicyConditionType.HTTP_HEADERS))
        }

        if (conditionTypes.contains(PolicyConditionType.CLIENT) && size > 1) {
            throw InvalidStateException(String.format(msg, PolicyConditionType.CLIENT))
        }

        if (conditionTypes.contains(PolicyConditionType.SCRIPT) && size > 1) {
            throw InvalidStateException(String.format(msg, PolicyConditionType.SCRIPT))
        }

        if (conditionTypes.contains(PolicyConditionType.TIME) && size > 1) {
            throw InvalidStateException(String.format(msg, PolicyConditionType.TIME))
        }

        if (conditionTypes.contains(PolicyConditionType.USER_EMAIL) && size > 1) {
            throw InvalidStateException(String.format(msg, PolicyConditionType.USER_EMAIL))
        }
    }

    private fun computeRegionSetting(conditionMap: HashMap<PolicyConditionType, MutableSet<String>>): String {
        return computeSetting(conditionMap, "regions", PolicyConditionType.REGION_NAME, PolicyConditionType.REGION_UUID)
    }

    private fun computeTeamSetting(conditionMap: HashMap<PolicyConditionType, MutableSet<String>>): String {
        return computeSetting(conditionMap, "teams", PolicyConditionType.TEAM_NAME, PolicyConditionType.TEAM_UUID)
    }

    private fun computeUserSetting(conditionMap: HashMap<PolicyConditionType, MutableSet<String>>): String {
        return computeSetting(conditionMap, "users", PolicyConditionType.USER_EMAIL, PolicyConditionType.USER_UUID)
    }

    private fun computeRoleSetting(conditionMap: HashMap<PolicyConditionType, MutableSet<String>>): String {
        return computeSetting(conditionMap, "roles", PolicyConditionType.ROLE_NAME, PolicyConditionType.ROLE_UUID)
    }

    private fun computeAuthoritySetting(conditionMap: HashMap<PolicyConditionType, MutableSet<String>>): String {
        return computeSetting(
            conditionMap,
            "authorities",
            PolicyConditionType.AUTHORITY_NAME,
            PolicyConditionType.AUTHORITY_UUID
        )
    }

    private fun computeSetting(
        conditionMap: HashMap<PolicyConditionType, MutableSet<String>>, token: String,
        conditionTypeName: PolicyConditionType, conditionTypeUUID: PolicyConditionType
    ): String {
        if (conditionMap.containsKey(conditionTypeName) || conditionMap.containsKey(conditionTypeUUID)) {
            val conditionValues = conditionMap[conditionTypeName].orEmpty() + conditionMap[conditionTypeUUID].orEmpty()
            val joined = conditionValues.joinToString(",") { v -> "\"${v}\"" }
            return "${token}:[${joined}]"
        }

        return ""
    }
}