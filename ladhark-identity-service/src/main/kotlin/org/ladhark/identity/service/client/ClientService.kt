package org.ladhark.identity.service.client

import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.toList
import org.ladhark.identity.api.dto.client.ClientCredentialDetailsResponse
import org.ladhark.identity.api.dto.client.ClientDetails
import org.ladhark.identity.aspect.cache.GetFromCache
import org.ladhark.identity.aspect.cache.PutInCache
import org.ladhark.identity.aspect.cache.PutInCaches
import org.ladhark.identity.entity.client.Client
import org.ladhark.identity.entity.client.ClientCredentials
import org.ladhark.identity.entity.role.Role
import org.ladhark.identity.exception.EntityAlreadyPresentException
import org.ladhark.identity.exception.EntityNotFoundException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.repository.client.ClientCredentialsRepository
import org.ladhark.identity.repository.client.ClientRepository
import org.ladhark.identity.repository.role.RoleRepository
import org.ladhark.identity.security.ClientJwtDetails
import org.ladhark.identity.security.JWTService
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID
import org.ladhark.identity.service.base.BaseService
import org.ladhark.identity.service.client.event.*
import org.ladhark.identity.service.context.withUserContext
import org.ladhark.identity.service.role.event.RoleReadEvent
import org.ladhark.kotlin.extension.s
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

const val CLIENT_CACHE_BY_UUID = "ClientCacheByUUID"
const val CLIENT_CACHE_BY_NAME_TO_UUID = "ClientCacheByNameToUUID"

interface ClientService {

    suspend fun createClient(clientDetails: ClientDetails): Client
    suspend fun findByUuid(uuid: String): Client?
    suspend fun findByName(name: String): Client?
    suspend fun createKeyForClient(uuid: String): ClientCredentialDetailsResponse
    suspend fun addRole(clientUuid: String, roleUuid: String)
    suspend fun findAll(): List<Client>

}

@Service
class ClientServiceImpl : BaseService(), ClientService {

    val log: Logger = LoggerFactory.getLogger(ClientServiceImpl::class.java)

    @Autowired
    private lateinit var clientRepository: ClientRepository

    @Autowired
    private lateinit var clientCredentialsRepository: ClientCredentialsRepository

    @Autowired
    private lateinit var roleRepository: RoleRepository

    @Autowired
    private lateinit var jwtService: JWTService

    @Transactional(rollbackFor = [Exception::class])
    @PutInCaches(caches = [
        PutInCache(CLIENT_CACHE_BY_UUID),
        PutInCache(CLIENT_CACHE_BY_NAME_TO_UUID),
    ])
    override suspend fun createClient(clientDetails: ClientDetails): Client = withUserContext {

        if (clientDetails.name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        if (clientDetails.email.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Email"))
        }

        clientRepository.findClientByName(clientDetails.name)?.let {

            throw EntityAlreadyPresentException(
                appMessage.getMessage(
                    "error.entity.already.present.same.name",
                    clientDetails.name
                )
            )

        } ?: run {
            val client = Client().apply {
                name = clientDetails.name
                email = clientDetails.email
                description = clientDetails.description
                active = true

                log.info("Saving client with name {}", name)
            }

            return clientRepository
                .save(client)
                .also { clientFromDb ->
                    publishEvent(ClientCreateEvent(this, clientFromDb, AuthenticatedUserContextUUID(principalUUID())))
                }
        }

    }

    @Transactional(readOnly = true)
    @GetFromCache(CLIENT_CACHE_BY_UUID)
    override suspend fun findByUuid(uuid: String): Client? = withUserContext {

        if (uuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching client details for uuid {}", uuid)

        return clientRepository.findClientByUuid(uuid.uuid)?.also { client ->
                publishEvent(ClientReadEvent(this, client, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    @Transactional(readOnly = true)
    @GetFromCache(CLIENT_CACHE_BY_NAME_TO_UUID)
    override suspend fun findByName(name: String): Client? = withUserContext {

        if (name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        log.info("Fetching client details for name {}", name)

        return clientRepository.findClientByName(name)?.also { client ->
                publishEvent(ClientReadEvent(this, client, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    @Transactional(rollbackFor = [Exception::class])
    override suspend fun createKeyForClient(uuid: String): ClientCredentialDetailsResponse = withUserContext {

        if (uuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "UUID"))
        }

        val clientCredentialsFromDb = clientRepository
            .findClientByUuid(uuid.uuid)
            ?.let { client ->
                publishEvent(ClientReadEvent(this, client, AuthenticatedUserContextUUID(principalUUID())))

                clientCredentialsRepository
                    .findLatestActiveCredentials(true)
                    ?.let { clientCredentialsFromDb ->

                        publishEvent(
                            ClientCredentialsReadEvent(
                                this, clientCredentialsFromDb,
                                AuthenticatedUserContextUUID(principalUUID()),
                                appMessage.getMessage(
                                    "message.client.credentials.event.read.secondary.access",
                                    arrayOf(clientCredentialsFromDb.uuid.s, uuid, "`create client key`")
                                )
                            )
                        )

                        clientCredentialsFromDb.active = false
                        clientCredentialsRepository
                            .save(clientCredentialsFromDb)
                            .let {
                                saveClientCredentials(client)
                            }
                    } ?: saveClientCredentials(client)
            }
            ?: throw EntityNotFoundException(appMessage.getMessage("error.client.not.found.by.uuid", uuid))

        return with(clientCredentialsFromDb) {
                ClientCredentialDetailsResponse(
                    this.uuid.s,
                    accessToken,
                    refreshToken
                )
        }
    }

    @Transactional(rollbackFor = [Exception::class])
    override suspend fun addRole(clientUuid: String, roleUuid: String): Unit = withUserContext {

        if (clientUuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Client UUID"))
        }

        if (roleUuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Role UUID"))
        }

        val records = coroutineScope {
            awaitAll(
                async {
                    clientRepository
                        .findClientByUuid(clientUuid.uuid)
                        ?.also { client ->
                            publishEvent(ClientReadEvent(this, client, AuthenticatedUserContextUUID(principalUUID())))
                        } ?: throw EntityNotFoundException(
                        appMessage.getMessage(
                            "error.client.not.found.by.uuid",
                            clientUuid
                        )
                    )
                },
                async {
                    roleRepository
                        .findRoleByUuid(roleUuid.uuid)
                        ?.also { role ->
                            publishEvent(RoleReadEvent(this, role, AuthenticatedUserContextUUID(principalUUID())))
                        } ?: throw EntityNotFoundException(
                        appMessage.getMessage(
                            "error.role.not.found.by.uuid",
                            roleUuid
                        )
                    )
                }
            )
        }

        val activeClient = records[0] as Client
        val roleToAdd = records[1] as Role

        activeClient.hasRoles.add(roleToAdd)

        clientRepository
            .save(activeClient)
            .also { activeClientFromDb ->
                publishEvent(ClientRoleAddEvent(this, activeClientFromDb, roleToAdd, AuthenticatedUserContextUUID(principalUUID())))
            }
    }

    @Transactional(readOnly = true)
    override suspend fun findAll(): List<Client> = withUserContext {
        return clientRepository.findAll().toList()
            .also { clients ->
                val clientPage = ClientPage(clients.map { it.uuid.s }.toSet())

                publishEvent(
                    ClientPageReadEvent(
                        this, clientPage, AuthenticatedUserContextUUID(principalUUID()),
                        appMessage.getMessage(
                            "message.client.event.read.page.access",
                            arrayOf(clientPage.uuidsCsv(), principalId(), "`page region`")
                        )
                    )
                )
            }
    }

    private suspend fun saveClientCredentials(client: Client): ClientCredentials = withUserContext {

        val clientJwtDetails = ClientJwtDetails(org.ladhark.identity.security.Client(client.uuid.s))

        val accessToken = jwtService.createClientJwtAccessToken(clientJwtDetails)
        val refreshToken = jwtService.createClientJwtRefreshToken(clientJwtDetails)

        val clientCredentials = ClientCredentials()
        clientCredentials.accessToken = accessToken
        clientCredentials.refreshToken = refreshToken
        clientCredentials.client = client
        clientCredentials.active = true

        return clientCredentialsRepository
            .save(clientCredentials)
            .also { clientCredentialsFromDb ->
                publishEvent(
                    ClientCredentialsCreateEvent(
                        this,
                        clientCredentialsFromDb,
                        AuthenticatedUserContextUUID(principalUUID())
                    )
                )
            }
    }

}