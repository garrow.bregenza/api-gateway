package org.ladhark.identity.service.resource

import org.ladhark.identity.api.dto.resource.ResourceActionDetails
import org.ladhark.identity.aspect.cache.GetFromCache
import org.ladhark.identity.aspect.cache.PutInCache
import org.ladhark.identity.aspect.cache.PutInCaches
import org.ladhark.identity.entity.resource.ResourceAction
import org.ladhark.identity.entity.resource.Scope
import org.ladhark.identity.exception.EntityAlreadyPresentException
import org.ladhark.identity.exception.EntityNotFoundException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.repository.resource.ResourceActionRepository
import org.ladhark.identity.repository.resource.ResourceRepository
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID
import org.ladhark.identity.service.base.BaseService
import org.ladhark.identity.service.context.withUserContext
import org.ladhark.identity.service.resource.cache.RESOURCE_ACTION_CACHE_BY_NAME_TO_UUID
import org.ladhark.identity.service.resource.cache.RESOURCE_ACTION_CACHE_BY_UUID
import org.ladhark.identity.service.resource.event.ResourceActionCreateEvent
import org.ladhark.identity.service.resource.event.ResourceActionReadEvent
import org.ladhark.identity.service.resource.event.ResourceReadEvent
import org.ladhark.kotlin.extension.s
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

interface ResourceActionService {

    //fun locateAndMatchResourceAction(httpServletRequest: ServerHttpRequest): Mono<ResourceAction>
    suspend fun createResourceAction(resourceActionDetails: ResourceActionDetails): ResourceAction
    suspend fun findByUuid(uuid: String): ResourceAction?
    suspend fun findByName(name: String): ResourceAction?
}

@Service
class ResourceActionServiceImpl : BaseService(), ResourceActionService {

    val log: Logger = LoggerFactory.getLogger(ResourceActionServiceImpl::class.java)

    @Autowired
    lateinit var resourceActionRepository: ResourceActionRepository

    @Autowired
    lateinit var resourceRepository: ResourceRepository

    @Transactional(rollbackFor = [Exception::class])
    @PutInCaches(caches = [
        PutInCache(RESOURCE_ACTION_CACHE_BY_UUID),
        PutInCache(RESOURCE_ACTION_CACHE_BY_NAME_TO_UUID),
    ])
    override suspend fun createResourceAction(resourceActionDetails: ResourceActionDetails): ResourceAction = withUserContext {

        if (resourceActionDetails.name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        if (resourceActionDetails.scope.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Scope"))
        }

        val isPresentScope = Scope.STRING_SET.contains(resourceActionDetails.scope)

        if (!isPresentScope) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.invalid", "Scope"))
        }

        if (resourceActionDetails.uriPattern.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "URI Pattern"))
        }

        if (resourceActionDetails.resourceUuid.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Resource UUID"))
        }

        return resourceActionRepository.findResourceActionByName(resourceActionDetails.name)?.let {
            throw EntityAlreadyPresentException(
                appMessage.getMessage(
                    "error.entity.already.present.same.name",
                    resourceActionDetails.name
                )
            )
        } ?: run {
            resourceActionRepository
                .findByUriPatternAndScope(resourceActionDetails.uriPattern, Scope.valueOf(resourceActionDetails.scope))
                ?.let {
                    throw EntityAlreadyPresentException(
                        appMessage.getMessage(
                            "error.entity.already.present.same.uri.scope",
                            arrayOf(resourceActionDetails.uriPattern, resourceActionDetails.scope)
                        )
                    )
                }

            resourceRepository
                .findResourceByUuid(resourceActionDetails.resourceUuid.uuid)?.let { resourceFromDb ->
                    publishEvent(
                        ResourceReadEvent(
                            this, resourceFromDb, AuthenticatedUserContextUUID(principalUUID()),
                            appMessage.getMessage(
                                "message.application.event.read.secondary.access",
                                arrayOf(resourceFromDb.name, resourceFromDb.uuid.s, principalId(), "`create resource action`")
                            )
                        )
                    )

                    val resourceAction = ResourceAction().apply {
                        resource = resourceFromDb
                        name = resourceActionDetails.name
                        scope = Scope.valueOf(resourceActionDetails.scope)
                        uriPattern = resourceActionDetails.uriPattern
                        description = resourceActionDetails.description
                    }

                    resourceActionRepository
                        .save(resourceAction)
                        .also { resourceActionFromDb ->
                            publishEvent(ResourceActionCreateEvent(this, resourceActionFromDb, AuthenticatedUserContextUUID(principalUUID())))
                        }
                } ?: throw EntityNotFoundException(getMessage("error.resourceAction.not.found.by.uuid",
                    resourceActionDetails.resourceUuid))
        }

    }

    @Transactional(readOnly = true)
    @GetFromCache(RESOURCE_ACTION_CACHE_BY_UUID)
    override suspend fun findByUuid(uuid: String): ResourceAction? = withUserContext {

        if (uuid.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching resource action details for uuid {}", uuid)

        return resourceActionRepository.findResourceActionByUuid(uuid.uuid)?.also { resourceAction ->
                publishEvent(ResourceActionReadEvent(this, resourceAction, AuthenticatedUserContextUUID(principalUUID())))
            }

    }

    @Transactional(readOnly = true)
    @GetFromCache(RESOURCE_ACTION_CACHE_BY_NAME_TO_UUID)
    override suspend fun findByName(name: String): ResourceAction? = withUserContext {

        if (name.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "Name"))
        }

        log.info("Fetching resource action details for name {}", name)

        return resourceActionRepository.findResourceActionByName(name)?.also { resourceAction ->
            publishEvent(ResourceActionReadEvent(this, resourceAction, AuthenticatedUserContextUUID(principalUUID())))
        }

    }

}