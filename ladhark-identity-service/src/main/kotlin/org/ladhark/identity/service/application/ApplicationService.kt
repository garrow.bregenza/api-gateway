package org.ladhark.identity.service.application

import org.ladhark.identity.api.dto.application.ApplicationDetails
import org.ladhark.identity.aspect.cache.GetFromCache
import org.ladhark.identity.aspect.cache.PutInCache
import org.ladhark.identity.aspect.cache.PutInCaches
import org.ladhark.identity.entity.application.Application
import org.ladhark.identity.entity.application.HostingLocation
import org.ladhark.identity.entity.credential.CredentialStore
import org.ladhark.identity.entity.credential.CredentialStoreType
import org.ladhark.identity.exception.EntityAlreadyPresentException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.repository.application.ApplicationRepository
import org.ladhark.identity.service.application.cache.APPLICATION_CACHE_BY_NAME_TO_UUID
import org.ladhark.identity.service.application.cache.APPLICATION_CACHE_BY_UUID
import org.ladhark.identity.service.application.event.ApplicationCreateEvent
import org.ladhark.identity.service.application.event.ApplicationReadEvent
import org.ladhark.identity.service.base.AuthenticatedUserContextUUID
import org.ladhark.identity.service.base.BaseService
import org.ladhark.identity.service.context.withUserContext
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

interface ApplicationService {
    suspend fun findByUuid(uuid: String): Application?
    suspend fun findByName(name: String): Application?
    suspend fun createApplication(applicationDetails: ApplicationDetails): Application
    //fun search(searchParams: SearchParams): Mono<SearchResult<Application>>
}

@Service("applicationService")
class ApplicationServiceImpl : BaseService(), ApplicationService {

    private val log: Logger = LoggerFactory.getLogger(ApplicationServiceImpl::class.java)

    @Autowired
    lateinit var applicationRepository: ApplicationRepository


    /*@Transactional(rollbackFor = [Exception::class])
    internal fun checkAndSaveApplication() {
        doAsSystem<Unit, Unit>({
            lockAndPerform<Unit, Unit>("save_application", {

                val applicationName = properties.applicationName()

                if (StringUtils.isBlank(applicationName)) {
                    throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
                }

                val applicationFromDb = applicationRepository.findApplicationByName(applicationName);

                if (applicationFromDb.isEmpty) {
                    val applicationDetails = ApplicationDetails(applicationName, false)
                    createApplication(applicationDetails)
                }
            })
        })
    }*/

    @Transactional(readOnly = true)
    @GetFromCache(APPLICATION_CACHE_BY_UUID)
    override suspend fun findByUuid(uuid: String): Application? = withUserContext {

        if (uuid.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "UUID"))
        }

        log.info("Fetching application details for uuid {}", uuid)

        return applicationRepository.findApplicationByUuid(uuid.uuid)?.also { application ->
            publishEvent(ApplicationReadEvent(this, application, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    @Transactional(readOnly = true)
    @GetFromCache(APPLICATION_CACHE_BY_NAME_TO_UUID)
    override suspend fun findByName(name: String): Application? = withUserContext {

        if (name.isBlank()) {
            throw InvalidArgumentException(getMessage("error.value.required.is.empty", "Name"))
        }

        log.info("Fetching application details for name {}", name)

        return applicationRepository.findApplicationByName(name)?.also { application ->
            publishEvent(ApplicationReadEvent(this, application, AuthenticatedUserContextUUID(principalUUID())))
        }
    }

    @Transactional(rollbackFor = [Exception::class])
    @PutInCaches(caches = [
        PutInCache(APPLICATION_CACHE_BY_UUID),
        PutInCache(APPLICATION_CACHE_BY_NAME_TO_UUID),
    ])
    override suspend fun createApplication(applicationDetails: ApplicationDetails): Application = withUserContext {

        if (applicationDetails.name.isBlank()) {
            throw InvalidArgumentException(appMessage.getMessage("error.value.required.is.empty", "Name"))
        }

        log.info("Saving application details with name {}", applicationDetails.name)

        findByName(applicationDetails.name)?.let {
            throw EntityAlreadyPresentException(
                appMessage.getMessage(
                    "error.entity.already.present.same.name",
                    applicationDetails.name
                )
            )
        } ?: run {

            val application = Application().apply {
                name = applicationDetails.name
                description = applicationDetails.description
                external = applicationDetails.external
                servicePattern = applicationDetails.servicePattern

                applicationDetails.hostingLocations.forEach { hostingLocation ->
                    val hl = HostingLocation()
                    hl.location = hostingLocation

                    hostingLocations.add(hl)

                    val credentialStoreDetails = applicationDetails.credentialStore

                    if (credentialStoreDetails != null) {
                        val credentialStoreToSave = CredentialStore().apply {
                            name = credentialStoreDetails.name
                            type = CredentialStoreType.valueOf(credentialStoreDetails.type)
                            properties.putAll(credentialStoreDetails.properties)
                        }


                        log.info(
                            "Mapping credential store type {} to application {}",
                            credentialStoreToSave.type,
                            name
                        )

                        credentialStore = credentialStoreToSave
                    }
                }

                log.info("Saving application details with name {} after all hosting location setting.", name)
            }

            return applicationRepository.save(application).also { applicationSaved ->
                publishEvent(ApplicationCreateEvent(this, applicationSaved, AuthenticatedUserContextUUID(principalUUID())))
            }
        }
    }

    /*@Transactional(readOnly = true)
    override fun search(searchParams: SearchParams): SearchResult<Application> {
        return super.search(searchParams, Application::class.java)
    }*/

}