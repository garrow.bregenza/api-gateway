package org.ladhark.identity.service.user

import org.ladhark.identity.api.dto.user.LoginDetails
import org.ladhark.identity.api.dto.user.LoginResponse
import org.ladhark.identity.entity.user.UserHasRegion
import org.ladhark.identity.exception.AuthorityEvaluationFailedException
import org.ladhark.identity.repository.user.UserCredentialsRepository
import org.ladhark.identity.repository.user.UserJWTRepository
import org.ladhark.identity.security.*
import org.ladhark.identity.service.base.BaseService
import org.ladhark.identity.service.user.event.LoginFailureEvent
import org.ladhark.identity.service.user.event.LoginSuccessEvent
import org.ladhark.kotlin.extension.uuid
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

interface LoginService {
    suspend fun login(loginDetails: LoginDetails): LoginResponse
}

@Service
class LoginServiceImpl : BaseService(), LoginService {

    private val log: Logger = LoggerFactory.getLogger(LoginServiceImpl::class.java)

    @Autowired
    lateinit var passwordEncoder: PasswordEncoder

    @Autowired
    lateinit var jwtService: JWTService

    @Autowired
    lateinit var userRepository: UserJWTRepository

    @Autowired
    lateinit var userCredentialsRepository: UserCredentialsRepository

    @Transactional(readOnly = true)
    override suspend fun login(loginDetails: LoginDetails): LoginResponse {

        log.info("Performing login for user with email {}", loginDetails.email)

        if (loginDetails.inValid()) {
            val errorMsg = appMessage.getMessage("error.entity.invalid.data", "Login Details")
            publishEvent(LoginFailureEvent(this, loginDetails, errorMsg))
            throw AuthorityEvaluationFailedException(errorMsg)
        }

        val email = loginDetails.email
        // TODO move region and application validation to jwt generation logic
        return userRepository
            .findUserByEmail(email) // TODO this has to fetch regions as well
            ?.let { user ->
                log.info("Found user by uuid {}", user.uuid)

                val regionUuid = loginDetails.regionUuid
                val userRegion: UserHasRegion? = if (regionUuid.isBlank()) {
                    user.primaryRegion()
                } else {
                    user.userRegion(regionUuid.uuid)
                }

                if (userRegion == null) {
                    log.error("No region found for user uuid {} during log in process.", user.uuid)
                    publishEvent(
                        LoginFailureEvent(
                            this,
                            loginDetails,
                            appMessage.getMessage("error.entity.invalid.data", "User Region")
                        )
                    )

                    // do not throw specific entity related exception, entity name not found gives hint about system, hence only bad cred
                    throw AuthorityEvaluationFailedException(appMessage.getMessage("error.bad.credentials"))


                }

                // TODO support applications for audience

                // TODO add cred read event
                userCredentialsRepository
                    .findByUserUuid(user.uuid)?.let { userCredentials ->
                        val password = userCredentials.password

                        if (!passwordEncoder.matches(loginDetails.password, password)) {
                            publishEvent(
                                LoginFailureEvent(
                                    this,
                                    loginDetails,
                                    appMessage.getMessage("error.credentials.match.failed")
                                )
                            )

                            // do not throw specific entity related exception, entity name not found gives hint about system, hence only bad cred
                            throw AuthorityEvaluationFailedException(appMessage.getMessage("error.bad.credentials"))
                        }

                        val access = jwtService.createUserJwtAccessToken(
                            UserJwtDetails(
                                Subject(user.uuid.toString()),
                                Region(userRegion.region.uuid.toString()),
                                ChildrenAccess(userRegion.childrenAccess)
                            )
                        )
                        val refresh = jwtService.createUserJwtRefreshToken(
                            UserJwtDetails(
                                Subject(user.uuid.toString()),
                                Region(userRegion.region.uuid.toString()),
                                ChildrenAccess(userRegion.childrenAccess)
                            )
                        )
                        log.info("Token created for user by uuid {}", user.uuid)
                        publishEvent(LoginSuccessEvent(this, loginDetails))


                        LoginResponse(access, refresh)

                    } ?: run {
                    publishEvent(
                        LoginFailureEvent(
                            this,
                            loginDetails,
                            appMessage.getMessage("error.entity.not.found", "UserCredentials")
                        )
                    )

                    // do not throw specific entity related exception, entity name not found gives hint about system, hence only bad cred
                    throw AuthorityEvaluationFailedException(appMessage.getMessage("error.bad.credentials"))
                }


            } ?: run {

            publishEvent(
                LoginFailureEvent(
                    this,
                    loginDetails,
                    appMessage.getMessage("error.entity.not.found", "User")
                )
            )

            // do not throw specific entity related exception, entity name not found gives hint about system, hence only bad cred
            throw AuthorityEvaluationFailedException(appMessage.getMessage("error.bad.credentials"))
        }
    }
}