package org.ladhark.identity.service.organization.cache

import com.github.benmanes.caffeine.cache.AsyncCache
import com.github.benmanes.caffeine.cache.Caffeine
import jakarta.annotation.PostConstruct
import kotlinx.coroutines.sync.withLock
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.organization.Organization
import org.ladhark.identity.repository.organization.OrganizationRepository
import org.ladhark.identity.service.base.BaseCacheService
import org.ladhark.kotlin.extension.s
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

const val ORGANIZATION_CACHE_BY_UUID = "ORGANIZATION_CACHE_BY_UUID"
const val ORGANIZATION_CACHE_BY_NAME_TO_UUID = "ORGANIZATION_CACHE_BY_NAME_TO_UUID"

interface OrganizationCacheService {
    suspend fun getOrganizationByNameToUuidAsync(key: String): Organization?
    suspend fun putOrganizationByNameToUuidAsync(key: String, value: Organization)
    suspend fun removeOrganizationByNameToUuidAsync(key: String)
    suspend fun getOrganizationByUuidAsync(key: String): Organization?
    suspend fun putOrganizationByUuidAsync(key: String, value: Organization)
    suspend fun removeOrganizationByUuidAsync(key: String)
}

@Service
class OrganizationCacheServiceImpl : BaseCacheService(), OrganizationCacheService {

    @Autowired
    lateinit var organizationRepository: OrganizationRepository

    lateinit var organizationCacheByUUID: AsyncCache<String, Organization>

    lateinit var organizationCacheByNameToUUID: AsyncCache<String, String>

    @PostConstruct
    private fun postConstruct() {

        organizationCacheByUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        organizationCacheByNameToUUID = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .buildAsync()

        registerProvider(this, ORGANIZATION_CACHE_BY_UUID, ORGANIZATION_CACHE_BY_NAME_TO_UUID)

        organizationRepository.subscribeAndDoOnAll {
            with(it) {
                organizationCacheByUUID.put(uuid.s, CompletableFuture.completedFuture(this))
                organizationCacheByNameToUUID.put(name, CompletableFuture.completedFuture(uuid.s))
            }
        }
    }

    override suspend fun getOrganizationByNameToUuidAsync(key: String): Organization? {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.getValueAsync(key, organizationCacheByNameToUUID)
            return uuid?.let {
                caffeineCacheHelperService.getValueAsync(it, organizationCacheByUUID)
            }
        }
    }

    override suspend fun putOrganizationByNameToUuidAsync(key: String, value: Organization) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value.uuid.s, organizationCacheByNameToUUID)
            caffeineCacheHelperService.putValueAsync(value.uuid.s, value, organizationCacheByUUID)
        }
    }

    override suspend fun removeOrganizationByNameToUuidAsync(key: String) {
        mutex.withLock {
            val uuid = caffeineCacheHelperService.removeValueAsync(key, organizationCacheByNameToUUID)
            uuid?.let {
                caffeineCacheHelperService.removeValueAsync(it, organizationCacheByUUID)
            }
        }
    }

    override suspend fun getOrganizationByUuidAsync(key: String): Organization? {
        mutex.withLock {
            return caffeineCacheHelperService.getValueAsync(key, organizationCacheByUUID)
        }
    }

    override suspend fun putOrganizationByUuidAsync(key: String, value: Organization) {
        mutex.withLock {
            caffeineCacheHelperService.putValueAsync(key, value, organizationCacheByUUID)
        }
    }

    override suspend fun removeOrganizationByUuidAsync(key: String) {
        mutex.withLock {
            caffeineCacheHelperService.removeValueAsync(key, organizationCacheByUUID)
        }
    }

    override suspend fun getFromCache(name: String, key: String): BaseEntity? {
        return when(name) {
            ORGANIZATION_CACHE_BY_UUID -> getOrganizationByUuidAsync(key)
            ORGANIZATION_CACHE_BY_NAME_TO_UUID -> getOrganizationByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun putInCache(name: String, key: String, value: BaseEntity) {

        if (value !is Organization) {
            throw IllegalArgumentException("Cache value passed is not of type ${Organization::class.qualifiedName}")
        }

        when(name) {
            ORGANIZATION_CACHE_BY_UUID -> putOrganizationByUuidAsync(key, value)
            ORGANIZATION_CACHE_BY_NAME_TO_UUID -> putOrganizationByNameToUuidAsync(key, value)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

    override suspend fun removeFromCache(name: String, key: String) {
        when(name) {
            ORGANIZATION_CACHE_BY_UUID -> removeOrganizationByUuidAsync(key)
            ORGANIZATION_CACHE_BY_NAME_TO_UUID -> removeOrganizationByNameToUuidAsync(key)
            else -> throw IllegalArgumentException("No cache by name $name found.")
        }
    }

}