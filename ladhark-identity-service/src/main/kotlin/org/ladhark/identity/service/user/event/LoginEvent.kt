package org.ladhark.identity.service.user.event

import org.ladhark.identity.api.dto.user.LoginDetails
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.event.BaseEvent
import org.ladhark.identity.event.EventCategory

class JwtLoginSuccessEvent(source: Any, private val token: String) :
    BaseEvent<String>(source, token, JwtEventCategory.JWT, ApplicationEventAction.SUCCESS) {
    override fun summary(): String {
        return "User granted Jwt token."
    }
}

class JwtLoginFailureEvent(source: Any, private val token: String, private var reason: String = "") :
    BaseEvent<String>(source, token, JwtEventCategory.JWT, ApplicationEventAction.FAILURE) {
    override fun summary(): String {
        return "User Jwt token grant failed, reason => $reason."
    }
}

class JwtInvalidClaimEvent(source: Any, private val jwtId: String, private var claim: String = "") :
    BaseEvent<String>(source, jwtId, JwtEventCategory.JWT, ApplicationEventAction.FAILURE) {
    override fun summary(): String {
        return "User Jwt invalid claim, claim => $claim."
    }

    override fun transferData(): Map<String, Any> {
        return super.transferData() + mapOf("claim" to claim)
    }
}

class LoginSuccessEvent(source: Any, private val loginDetails: LoginDetails) :
    BaseEvent<LoginDetails>(source, loginDetails, LoginEventCategory.LOGIN, ApplicationEventAction.SUCCESS) {
    override fun summary(): String {
        return "User logged in with email ${loginDetails.email}"
    }
}

class LoginFailureEvent(source: Any, private val loginDetails: LoginDetails, private var reason: String = "") :
    BaseEvent<LoginDetails>(source, loginDetails, LoginEventCategory.LOGIN, ApplicationEventAction.FAILURE) {
    override fun summary(): String {
        return "User login failed for user with email ${loginDetails.email}, reason => $reason"
    }
}

enum class JwtEventCategory : EventCategory {
    JWT {
        override fun type(): String {
            return JWT.name
        }
    }
}

enum class LoginEventCategory : EventCategory {
    LOGIN {
        override fun type(): String {
            return LOGIN.name
        }
    }
}