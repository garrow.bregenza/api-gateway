package org.ladhark.identity.properties

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import java.time.Duration
import java.time.Instant
import java.util.*

@Service
class ApplicationProperties {

    @Autowired
    lateinit var environment: Environment

    fun applicationName(): String {
        return validate(environment.getProperty("spring.application.name"))
    }

    fun applicationUrl(): String {
        return validate(environment.getProperty("spring.application.url"))
    }

    fun applicationIcon(): String {
        return validate(environment.getProperty("spring.application.icon.base64"))
    }

    fun authorizationHeaderName(): String {
        return environment.getProperty("ladhark.authorization.token.header", "Authorization")
    }

    fun clientAuthorizationHeaderName(): String {
        return environment.getProperty("ladhark.client.authorization.token.header", "X-Ladhark-Client-Authorization")
    }

    fun authorizationHeaderPrefix(): String {
        return environment.getProperty("ladhark.authorization.token.header.prefix", "Bearer")
    }

    fun jwtSecretKey(): String {
        return validate(environment.getProperty("ladhark.jwt.secret.key.base64"))
    }

    fun jwtIssuer(): String {
        return validate(environment.getProperty("ladhark.jwt.issuer"))
    }

    fun jwtAccessExpiration(): Date {
        val value = validate(environment.getProperty("ladhark.jwt.access.token.expiration.minutes", Long::class.java))
        return Date.from(Instant.now().plus(Duration.ofMinutes(value)))
    }

    fun jwtRefreshExpiration(): Date {
        val value = validate(environment.getProperty("ladhark.jwt.refresh.token.expiration.minutes", Long::class.java))
        return Date.from(Instant.now().plus(Duration.ofMinutes(value)))
    }

    fun neo4jURI(): String {
        return validate(environment.getProperty("spring.data.neo4j.uri"))
    }

    fun encryptionMasterKey(): String {
        return validate(environment.getProperty("ladhark.encryption.master.key"))
    }

    fun encryptionMasterKeySalt(): String {
        return validate(environment.getProperty("ladhark.encryption.master.key.salt"))
    }

    fun bcryptPasswordStrength(): Int {
        return Integer.parseInt(validate(environment.getProperty("ladhark.bcrypt.password.strength")))
    }

    fun <T> validate(property: T?): T {
        if (property == null) {
            throw IllegalStateException("Missing property.")
        }

        if (property is String && property.trim() == "") {
            throw IllegalStateException("Missing property.")
        }

        return property
    }
}