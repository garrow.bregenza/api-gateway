package org.ladhark.identity.properties

object Constant {

    const val API_TAG = "/api/v1"

    const val JWT_TOKEN_TYPE = "token_type"

    const val JWT_TOKEN_TYPE_ACCESS = "access"

    const val JWT_TOKEN_TYPE_REFRESH = "refresh"

    const val JWT_REGION = "region"

    const val JWT_REGION_CHILDREN_ACCESS = "region_children_access"

    const val JWT_TOKEN_SUBJECT_TYPE = "token_subject_type"

    const val JWT_TOKEN_SUBJECT_TYPE_USER = "token_subject_type_user"

    const val JWT_TOKEN_SUBJECT_TYPE_CLIENT = "token_subject_type_client"

    const val ANONYMOUS_TOKEN = "anonymous_token"
}