package org.ladhark.identity.messages

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.context.NoSuchMessageException
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.stereotype.Component

interface AppMessage {
    fun getMessage(code: String): String
    fun getMessage(code: String, arg: String): String
    fun getMessage(code: String, args: Array<String>): String
}

@Component
class AppMessageImpl : AppMessage {

    val log: Logger = LoggerFactory.getLogger(AppMessageImpl::class.java)

    @Autowired
    lateinit var messageSource: MessageSource

    override fun getMessage(code: String): String {
        return try {
            messageSource.getMessage(code, emptyArray(), LocaleContextHolder.getLocale())
        } catch (e: NoSuchMessageException) {
            "i18n[$code]"
        } catch (e: Exception) {
            log.error("Problem in fetching i18n message", e)
            "i18n[$code]"
        }
    }

    override fun getMessage(code: String, arg: String): String {
        return try {
            messageSource.getMessage(code, arrayOf(arg), LocaleContextHolder.getLocale())
        } catch (e: NoSuchMessageException) {
            "i18n[$code]"
        } catch (e: Exception) {
            log.error("Problem in fetching i18n message", e)
            "i18n[$code]"
        }
    }

    override fun getMessage(code: String, args: Array<String>): String {
        return try {
            return messageSource.getMessage(code, args, LocaleContextHolder.getLocale())
        } catch (e: NoSuchMessageException) {
            "i18n[$code]"
        } catch (e: Exception) {
            log.error("Problem in fetching i18n message", e)
            "i18n[$code]"
        }
    }
}