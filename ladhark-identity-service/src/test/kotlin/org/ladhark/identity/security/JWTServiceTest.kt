package org.ladhark.identity.security

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.impl.DefaultClaims
import io.jsonwebtoken.security.Keys
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test
import org.ladhark.identity.entity.authority.Authority
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.entity.resource.Resource
import org.ladhark.identity.entity.role.Role
import org.ladhark.identity.entity.team.Team
import org.ladhark.identity.entity.user.Name
import org.ladhark.identity.entity.user.User
import org.ladhark.identity.entity.user.UserHasRegion
import org.ladhark.identity.exception.AuthorizationFailedException
import org.ladhark.identity.exception.EntityNotFoundException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.exception.InvalidStateException
import org.ladhark.identity.messages.AppMessage
import org.ladhark.identity.properties.ApplicationProperties
import org.ladhark.identity.properties.Constant
import org.ladhark.identity.repository.client.ClientJWTRepository
import org.ladhark.identity.repository.model.role.InferredAuthorities
import org.ladhark.identity.repository.model.team.MappedInheritedTeam
import org.ladhark.identity.repository.role.RoleRepository
import org.ladhark.identity.repository.team.TeamRoleRegionRepository
import org.ladhark.identity.repository.user.UserJWTRepository
import org.ladhark.identity.repository.user.UserTeamRegionRepository
import org.ladhark.kotlin.extension.s
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoSettings
import java.time.Duration
import java.time.Instant
import java.util.*
import javax.crypto.SecretKey

@MockitoSettings
class JWTServiceTest {


    private val secretKey: SecretKey = Keys.secretKeyFor(SignatureAlgorithm.HS512)

    @Mock
    private lateinit var userJWTRepository: UserJWTRepository

    @Mock
    private lateinit var clientJWTRepository: ClientJWTRepository

    @Mock
    private lateinit var applicationProperties: ApplicationProperties

    @Mock
    private lateinit var userTeamRegionRepository: UserTeamRegionRepository

    @Mock
    private lateinit var teamRoleRegionRepository: TeamRoleRegionRepository

    @Mock
    private lateinit var roleRepository: RoleRepository

    @Mock
    lateinit var appMessage: AppMessage

    @InjectMocks
    private lateinit var jwtService: JWTService

    @Test
    fun `it should extract token from http header`() {
        `when`(applicationProperties.authorizationHeaderPrefix()).thenReturn("Bearer")

        val processedHeaderValue = jwtService.extractTokenFromHeader("Bearer MyTestingToken")

        assertThat(processedHeaderValue).isEqualTo("MyTestingToken")
    }

    @Test
    fun `it should not extract token from http header if header value is empty`() {
        `when`(applicationProperties.authorizationHeaderPrefix()).thenReturn("Bearer")

        val processedHeaderValue = jwtService.extractTokenFromHeader("")

        assertThat(processedHeaderValue).isEqualTo(null)
    }

    @Test
    fun `it should not extract token from http header if header value does not starts with header prefix value`() {
        `when`(applicationProperties.authorizationHeaderPrefix()).thenReturn("Bearer")

        val processedHeaderValue = jwtService.extractTokenFromHeader("MyTestingToken")

        assertThat(processedHeaderValue).isEqualTo(null)
    }

    @Test
    fun `it should parse a valid token to claims`() {

        `when`(applicationProperties.applicationName()).thenReturn("test@ladhark.com")
        `when`(applicationProperties.jwtIssuer()).thenReturn("test@ladhark.com")
        `when`(applicationProperties.jwtSecretKey()).thenReturn(Base64.getEncoder().encodeToString(secretKey.encoded))

        val token = createToken(Constant.JWT_TOKEN_TYPE_ACCESS)

        val claims = jwtService.parseJwtHeader(token)

        assertThat(claims.subject).isEqualTo("test@ladhark.com")
        assertThat(claims.audience).isEqualTo("test@ladhark.com")
        assertThat(claims.issuer).isEqualTo("test@ladhark.com")
        assertThat(claims[Constant.JWT_TOKEN_TYPE]).isEqualTo(Constant.JWT_TOKEN_TYPE_ACCESS)
        assertThat(claims.id).isNotEmpty
        assertThat(claims.issuedAt).isNotNull
        assertThat(claims.notBefore).isNotNull
        assertThat(claims.expiration).isNotNull

    }

    @Test
    fun `it should not parse a token if secret key is empty`() {
        `when`(applicationProperties.jwtSecretKey()).thenReturn(null)
        `when`(appMessage.getMessage("error.value.required.is.empty", "Secret key")).thenReturn("`Secret key` value is empty")

        val token = createToken(Constant.JWT_TOKEN_TYPE_ACCESS)

        assertThatExceptionOfType(InvalidStateException::class.java)
            .isThrownBy { jwtService.parseJwtHeader(token) }
            .withMessage("LA-500: `Secret key` value is empty")
    }

    @Test
    fun `it should not parse a token if token is invalid (signed by different key)`() {
        `when`(applicationProperties.jwtSecretKey()).thenReturn(Base64.getEncoder().encodeToString(secretKey.encoded))

        val token = createToken(Constant.JWT_TOKEN_TYPE_ACCESS, Keys.secretKeyFor(SignatureAlgorithm.HS512))

        assertThatExceptionOfType(AuthorizationFailedException::class.java)
            .isThrownBy { jwtService.parseJwtHeader(token) }
            .withMessage("SA-403: JWT signature does not match locally computed signature. JWT validity cannot be asserted and should not be trusted.")
    }

    @Test
    fun `it should not parse a token if token is empty`() {
        `when`(appMessage.getMessage("error.value.required.is.empty", "Token")).thenReturn("`Token` value is empty")

        assertThatExceptionOfType(InvalidArgumentException::class.java)
            .isThrownBy { jwtService.parseJwtHeader("") }
            .withMessage("LA-400: `Token` value is empty")
    }

    @Test
    fun `it should not create an access token if subject(user uuid) is empty`() {

        `when`(appMessage.getMessage("error.value.required.is.empty", "Subject")).thenReturn("`Subject` value is empty")
        `when`(applicationProperties.applicationName()).thenReturn("Testing")

        val userJwtDetails = UserJwtDetails(Subject(""), org.ladhark.identity.security.Region(""))
        assertThatExceptionOfType(InvalidArgumentException::class.java)
            .isThrownBy { jwtService.createUserJwtAccessToken(userJwtDetails) }
            .withMessage("LA-400: `Subject` value is empty")
    }

    @Test
    fun `it should not create a refresh token if subject(user uuid) is empty`() {

        `when`(appMessage.getMessage("error.value.required.is.empty", "Subject")).thenReturn("`Subject` value is empty")
        `when`(applicationProperties.applicationName()).thenReturn("Testing")

        val userJwtDetails = UserJwtDetails(Subject(""), org.ladhark.identity.security.Region(""))
        assertThatExceptionOfType(InvalidArgumentException::class.java)
            .isThrownBy { jwtService.createUserJwtRefreshToken(userJwtDetails) }
            .withMessage("LA-400: `Subject` value is empty")
    }

    @Test
    fun `it should not create an access token if region is empty`() {

        `when`(appMessage.getMessage("error.value.required.is.empty", "Subject")).thenReturn("`Subject` value is empty")
        `when`(applicationProperties.applicationName()).thenReturn("Testing")

        val userJwtDetails = UserJwtDetails(Subject(""), org.ladhark.identity.security.Region(""))
        assertThatExceptionOfType(InvalidArgumentException::class.java)
            .isThrownBy { jwtService.createUserJwtAccessToken(userJwtDetails) }
            .withMessage("LA-400: `Subject` value is empty")
    }

    @Test
    fun `it should not create a refresh token if region is empty`() {

        `when`(appMessage.getMessage("error.value.required.is.empty", "Subject")).thenReturn("`Subject` value is empty")
        `when`(applicationProperties.applicationName()).thenReturn("Testing")

        val userJwtDetails = UserJwtDetails(Subject(""), org.ladhark.identity.security.Region(""))
        assertThatExceptionOfType(InvalidArgumentException::class.java)
            .isThrownBy { jwtService.createUserJwtRefreshToken(userJwtDetails) }
            .withMessage("LA-400: `Subject` value is empty")
    }

    @Test
    fun `it should not create an access token if secret key is empty`() {

        `when`(applicationProperties.applicationName()).thenReturn("ladhark.com")
        `when`(applicationProperties.jwtIssuer()).thenReturn("admin@ladhark.com")
        `when`(applicationProperties.jwtSecretKey()).thenReturn(null)
        `when`(appMessage.getMessage("error.value.required.is.empty", "Secret key")).thenReturn("`Secret key` value is empty")
        `when`(applicationProperties.applicationName()).thenReturn("Testing")

        val userJwtDetails = UserJwtDetails(Subject(UUID.randomUUID().s),
            org.ladhark.identity.security.Region(UUID.randomUUID().s))
        assertThatExceptionOfType(InvalidStateException::class.java)
            .isThrownBy { jwtService.createUserJwtAccessToken(userJwtDetails) }
            .withMessage("LA-500: `Secret key` value is empty")
    }

    @Test
    fun `it should not create a refresh token if secret key is empty`() {

        `when`(applicationProperties.applicationName()).thenReturn("ladhark.com")
        `when`(applicationProperties.jwtIssuer()).thenReturn("admin@ladhark.com")
        `when`(applicationProperties.jwtSecretKey()).thenReturn(null)
        `when`(appMessage.getMessage("error.value.required.is.empty", "Secret key")).thenReturn("`Secret key` value is empty")
        `when`(applicationProperties.applicationName()).thenReturn("Testing")

        val userJwtDetails = UserJwtDetails(Subject(UUID.randomUUID().s),
            org.ladhark.identity.security.Region(UUID.randomUUID().s))
        assertThatExceptionOfType(InvalidStateException::class.java)
            .isThrownBy { jwtService.createUserJwtRefreshToken(userJwtDetails) }
            .withMessage("LA-500: `Secret key` value is empty")
    }

    @Test
    fun `it should not create an access token if issuer is empty`() {

        `when`(applicationProperties.jwtIssuer()).thenReturn(null)
        `when`(appMessage.getMessage("error.value.required.is.empty", "Issuer")).thenReturn("`Issuer` value is empty")
        `when`(applicationProperties.applicationName()).thenReturn("Testing")

        val userJwtDetails = UserJwtDetails(Subject(UUID.randomUUID().s),
            org.ladhark.identity.security.Region(UUID.randomUUID().s))
        assertThatExceptionOfType(InvalidArgumentException::class.java)
            .isThrownBy { jwtService.createUserJwtAccessToken(userJwtDetails) }
            .withMessage("LA-400: `Issuer` value is empty")
    }

    @Test
    fun `it should not create a refresh token if issuer is empty`() {

        `when`(applicationProperties.jwtIssuer()).thenReturn(null)
        `when`(appMessage.getMessage("error.value.required.is.empty", "Issuer")).thenReturn("`Issuer` value is empty")
        `when`(applicationProperties.applicationName()).thenReturn("Testing")

        val userJwtDetails = UserJwtDetails(Subject(UUID.randomUUID().s),
            org.ladhark.identity.security.Region(UUID.randomUUID().s))
        assertThatExceptionOfType(InvalidArgumentException::class.java)
            .isThrownBy { jwtService.createUserJwtRefreshToken(userJwtDetails) }
            .withMessage("LA-400: `Issuer` value is empty")
    }

    @Test
    fun `it should create an access token for valid details`() {

        `when`(applicationProperties.jwtIssuer()).thenReturn("admin@ladhark.com")
        `when`(applicationProperties.applicationName()).thenReturn("ladhark.com")
        `when`(applicationProperties.jwtAccessExpiration()).thenReturn(`10MinutesFromNow`())
        `when`(applicationProperties.jwtSecretKey()).thenReturn(Base64.getEncoder().encodeToString(secretKey.encoded))
        `when`(applicationProperties.applicationName()).thenReturn("Testing")

        val userJwtDetails = UserJwtDetails(Subject(UUID.randomUUID().s),
            org.ladhark.identity.security.Region(UUID.randomUUID().s))
        val token = jwtService.createUserJwtAccessToken(userJwtDetails)

        assertThat(token).isNotEmpty

        val claims = parseToken(token)!!

        assertThat(claims.subject).isEqualTo(userJwtDetails.subject.uuid)
        assertThat(claims.issuer).isEqualTo("admin@ladhark.com")
        assertThat(claims[Constant.JWT_TOKEN_TYPE]).isEqualTo(Constant.JWT_TOKEN_TYPE_ACCESS)
        assertThat(claims[Constant.JWT_REGION]).isEqualTo(userJwtDetails.region.uuid)
        assertThat(claims[Constant.JWT_REGION_CHILDREN_ACCESS]).isEqualTo(userJwtDetails.childrenAccess)
        assertThat(claims.id).isNotEmpty
        assertThat(claims.issuedAt).isNotNull
        assertThat(claims.notBefore).isNotNull
        assertThat(claims.expiration).isNotNull
    }

    @Test
    fun `it should not create an access token if expiration is before or equal now`() {
        `when`(applicationProperties.jwtIssuer()).thenReturn("admin@ladhark.com")
        `when`(applicationProperties.applicationName()).thenReturn("ladhark.com")
        `when`(applicationProperties.jwtAccessExpiration()).thenReturn(now())
        `when`(applicationProperties.jwtSecretKey()).thenReturn(Base64.getEncoder().encodeToString(secretKey.encoded))
        `when`(applicationProperties.applicationName()).thenReturn("Testing")


        `when`(appMessage.getMessage("error.jwt.token.expiration.date.soon.invalid")).thenReturn("Expiration date has already expired")

        val userJwtDetails = UserJwtDetails(Subject(UUID.randomUUID().s),
            org.ladhark.identity.security.Region(UUID.randomUUID().s))
        assertThatExceptionOfType(InvalidStateException::class.java)
            .isThrownBy { jwtService.createUserJwtAccessToken(userJwtDetails) }
            .withMessage("LA-500: Expiration date has already expired")
    }

    @Test
    fun `it should create a refresh token for valid details`() {

        `when`(applicationProperties.jwtIssuer()).thenReturn("admin@ladhark.com")
        `when`(applicationProperties.applicationName()).thenReturn("ladhark.com")
        `when`(applicationProperties.jwtRefreshExpiration()).thenReturn(`10MinutesFromNow`())
        `when`(applicationProperties.jwtSecretKey()).thenReturn(Base64.getEncoder().encodeToString(secretKey.encoded))
        `when`(applicationProperties.applicationName()).thenReturn("Testing")

        val userJwtDetails = UserJwtDetails(Subject(UUID.randomUUID().s),
            org.ladhark.identity.security.Region(UUID.randomUUID().s))
        val token = jwtService.createUserJwtRefreshToken(userJwtDetails)

        assertThat(token).isNotEmpty

        val claims = parseToken(token)!!

        assertThat(claims.subject).isEqualTo(userJwtDetails.subject.uuid)
        assertThat(claims.issuer).isEqualTo("admin@ladhark.com")
        assertThat(claims[Constant.JWT_TOKEN_TYPE]).isEqualTo(Constant.JWT_TOKEN_TYPE_REFRESH)
        assertThat(claims[Constant.JWT_REGION]).isEqualTo(userJwtDetails.region.uuid)
        assertThat(claims[Constant.JWT_REGION_CHILDREN_ACCESS]).isEqualTo(userJwtDetails.childrenAccess)
        assertThat(claims.id).isNotEmpty
        assertThat(claims.issuedAt).isNotNull
        assertThat(claims.notBefore).isNotNull
        assertThat(claims.expiration).isNotNull
    }

    @Test
    fun `it should not create a refresh token if expiration is before or equal now`() {

        `when`(applicationProperties.jwtIssuer()).thenReturn("admin@ladhark.com")
        `when`(applicationProperties.applicationName()).thenReturn("ladhark.com")
        `when`(applicationProperties.jwtRefreshExpiration()).thenReturn(now())
        `when`(applicationProperties.jwtSecretKey()).thenReturn(Base64.getEncoder().encodeToString(secretKey.encoded))
        `when`(applicationProperties.applicationName()).thenReturn("Testing")

        `when`(appMessage.getMessage("error.jwt.token.expiration.date.soon.invalid")).thenReturn("Expiration date has already expired")

        val userJwtDetails = UserJwtDetails(Subject(UUID.randomUUID().s),
            org.ladhark.identity.security.Region(UUID.randomUUID().s))
        assertThatExceptionOfType(InvalidStateException::class.java)
            .isThrownBy { jwtService.createUserJwtRefreshToken(userJwtDetails) }
            .withMessage("LA-500: Expiration date has already expired")
    }

    @Test
    fun `it should return ladhark authenticated principal by jwt token subject`() {

        runBlocking {

            val roleAdmin = Role()
            roleAdmin.name = "Admin"

            val authorityRead = Authority()
            authorityRead.name = "read"

            val authorityCreate = Authority()
            authorityCreate.name = "create"
            authorityCreate.implies.add(authorityRead)

            val resource = Resource()
            resource.name = "school"

            val user = User()
            user.email = "a@a.com"
            user.uuid = UUID.randomUUID()

            `when`(userJWTRepository.findUserByUuid(user.uuid)).thenReturn(user)

            val region = Region()
            region.name = "Paris"

            val userRegion = UserHasRegion()
            userRegion.childrenAccess = false
            userRegion.primary = true
            userRegion.region = region

            user.hasRegions.add(userRegion)

            val claims = DefaultClaims()
            claims.subject = user.uuid.toString()
            claims[Constant.JWT_REGION] = region.uuid.toString()
            claims[Constant.JWT_REGION_CHILDREN_ACCESS] = userRegion.childrenAccess

            val inferredPermissions = flowOf(
                InferredAuthorities(
                    roleAdmin,
                    authorityCreate,
                    setOf(authorityRead),
                    resource,
                    region,
                    application = null,
                    user
                )
            )

            `when`(
                userJWTRepository.findUserLocalOnlyAuthoritiesByRegionAndApplication(
                    user.uuid,
                    setOf(region.uuid)
                )
            ).thenReturn(inferredPermissions)

            val ladharkAuthenticatedPrincipal = jwtService.fetchUserForJwtSubject(claims)

            assertThat(ladharkAuthenticatedPrincipal).isNotNull
            assertThat(ladharkAuthenticatedPrincipal.getName()).isEqualTo(user.email)
        }
    }

    @Test
    fun `it should not return ladhark authenticated principal by jwt token subject if subject token is empty`() {
        `when`(
            appMessage.getMessage(
                "error.value.required.is.empty",
                "Subject"
            )
        ).thenReturn("`Subject` value is empty")

        assertThatExceptionOfType(InvalidArgumentException::class.java)
            .isThrownBy { runBlocking { jwtService.fetchUserForJwtSubject(DefaultClaims()) } }
            .withMessage("LA-400: `Subject` value is empty")
    }

    @Test
    fun `it should not return ladhark authenticated principal by jwt token subject if subject does not exists in the system`() {
        runBlocking {
            val region = Region()
            region.name = "Paris"

            val userUuid = UUID.randomUUID()

            `when`(
                appMessage.getMessage(
                    "error.entity.not.found",
                    userUuid.toString()
                )
            ).thenReturn("Entity not found for `$userUuid`")

            `when`(userJWTRepository.findUserByUuid(userUuid)).thenReturn(null)

            val claims = DefaultClaims()
            claims.subject = userUuid.toString()
            claims[Constant.JWT_REGION] = region.uuid.toString()
            claims[Constant.JWT_REGION_CHILDREN_ACCESS] = false

            assertThatExceptionOfType(EntityNotFoundException::class.java)
                .isThrownBy {  runBlocking { jwtService.fetchUserForJwtSubject(claims) } }
                .withMessage("LA-404: Entity not found for `$userUuid`")
        }
    }

    @Test
    fun `it should create user jwt token`() {

        val userJwtDetails = UserJwtDetails(Subject(UUID.randomUUID().s),
            org.ladhark.identity.security.Region(UUID.randomUUID().s))
        val expiresIn = Date.from(Instant.now().plus(Duration.ofMinutes(30)))

        `when`(applicationProperties.jwtIssuer()).thenReturn("Test")
        `when`(applicationProperties.jwtAccessExpiration()).thenReturn(expiresIn)
        `when`(applicationProperties.jwtSecretKey()).thenReturn(Base64.getEncoder().encodeToString(secretKey.encoded))

        val token = jwtService.createUserJwtAccessToken(userJwtDetails)

        assertThat(token).isNotBlank

        val claims = jwtService.parseJwtHeader(token)

        assertThat(claims.subject).isEqualTo(userJwtDetails.subject.uuid)
        assertThat(claims.id).isNotBlank

        assertThat(claims[Constant.JWT_TOKEN_TYPE]).isEqualTo(Constant.JWT_TOKEN_TYPE_ACCESS)
        assertThat(claims[Constant.JWT_TOKEN_SUBJECT_TYPE]).isEqualTo(Constant.JWT_TOKEN_SUBJECT_TYPE_USER)
        assertThat(claims[Constant.JWT_REGION]).isEqualTo(userJwtDetails.region.uuid)
        assertThat(claims[Constant.JWT_REGION_CHILDREN_ACCESS]).isEqualTo(userJwtDetails.childrenAccess)
    }

    @Test
    fun `it should fetch user context for valid user and region details`() {
        runBlocking {
            val userUUID = UUID.randomUUID()

            val user = User()
            user.uuid = userUUID
            user.email = "test@ladhark.org"
            user.name = Name("test", "", "user")

            val regionIndiaUUID = UUID.randomUUID()

            val regionIndia = Region()
            regionIndia.uuid = regionIndiaUUID
            regionIndia.name = "India"


            val userHasRegion = UserHasRegion()
            userHasRegion.region = regionIndia

            user.hasRegions.add(userHasRegion)

            user.initOnCreateTeams(UUID.randomUUID(), UUID.randomUUID())

            val roleUUID = UUID.randomUUID()

            val role = Role()
            role.name = "Everyone"
            role.uuid = roleUUID

            val inferredAuthorityUser = InferredAuthorities(
                role,
                null,
                emptySet(),
                null,
                region = regionIndia,
                null,
                source = user,
                inherited = false
            )

            `when`(userJWTRepository.findUserByUuid(userUUID)).thenReturn(user)
            `when`(
                userJWTRepository.findUserLocalOnlyAuthoritiesByRegionAndApplication(
                    userUUID,
                    setOf(regionIndiaUUID)
                )
            ).thenReturn(flowOf(inferredAuthorityUser))

            val teamUUID = UUID.randomUUID()

            val team = Team()
            team.name = "Kotlin"
            team.uuid = teamUUID


            val mappedInheritedTeam = MappedInheritedTeam(team, null, regionIndia, false)

            `when`(
                userTeamRegionRepository.findAllTeamsWithInheritanceForUserInRegions(
                    user.uuid,
                    setOf(regionIndiaUUID)
                )
            ).thenReturn(flowOf(mappedInheritedTeam))

            val inferredAuthorityTeam = InferredAuthorities(
                role,
                null,
                emptySet(),
                null,
                region = regionIndia,
                null,
                source = team,
                inherited = false
            )

            `when`(
                teamRoleRegionRepository.findTeamLocalOnlyAuthoritiesByRegionAndApplication(
                    setOf(team.uuid),
                    setOf(regionIndiaUUID)
                )
            ).thenReturn(flowOf(inferredAuthorityTeam))

            val userModel = jwtService.getUserContext(userUUID, regionIndiaUUID, false)
            assertThat(userModel.email).isEqualTo("test@ladhark.org")
        }
    }

    private fun createToken(type: String): String {
        return	createToken(type, secretKey)
    }

    private fun createToken(type: String, secretKey: SecretKey): String {
        val jwtId = UUID.randomUUID().toString()

        val now = Instant.now()

        val builder =  Jwts.builder()
            .setIssuer(applicationProperties.jwtIssuer())
            .setExpiration(`10MinutesFromNow`())
            .setSubject("test@ladhark.com")
            .setIssuedAt(Date.from(now))
            .setNotBefore(Date.from(now))
            .setId(jwtId)
            .setAudience(applicationProperties.applicationName())
            .claim(Constant.JWT_TOKEN_TYPE,type)
            .signWith(secretKey)

        return	builder.compact()
    }
    private fun `10MinutesFromNow`() = Date.from(Instant.now().plus(Duration.ofMinutes(10)))

    private fun now() = Date.from(Instant.now())

    private fun parseToken(token: String): Claims? {
        val secretKeyB64 = Base64.getEncoder().encodeToString(secretKey.encoded)
        return Jwts.parserBuilder().setSigningKey(secretKeyB64).build().parseClaimsJws(token).body
    }

}