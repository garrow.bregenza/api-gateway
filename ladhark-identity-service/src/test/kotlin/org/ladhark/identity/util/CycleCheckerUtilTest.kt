package org.ladhark.identity.util

import org.assertj.core.api.Assertions.assertThatCode
import org.junit.jupiter.api.Test
import org.ladhark.identity.entity.authority.Authority
import org.ladhark.identity.exception.InvalidStateException

class CycleCheckerUtilTest {

    // A B C D
    // X Y Z

    @Test
    fun `it should not throw exception for cycle in own chain`() {
        val authorityA = Authority()
        authorityA.name = "A"

        val authorityB = Authority()
        authorityB.name = "B"

        val authorityC = Authority()
        authorityC.name = "C"

        val authorityD = Authority()
        authorityD.name = "D"

        authorityA.implies.add(authorityB)

        authorityB.implies.add(authorityC)

        authorityC.implies.add(authorityD)


        assertThatCode {
            CycleCheckerUtil.checkCycleAndDepth(authorityA)
        }.doesNotThrowAnyException()
    }

    // A B C D
    @Test
    fun `it should throw exception for cycle in own chain`() {
        val authorityA = Authority()
        authorityA.name = "A"

        val authorityB = Authority()
        authorityB.name = "B"

        val authorityC = Authority()
        authorityC.name = "C"

        val authorityD = Authority()
        authorityD.name = "D"

        authorityA.implies.add(authorityB)

        authorityB.implies.add(authorityC)

        authorityC.implies.add(authorityD)

        authorityD.implies.add(authorityA)

        assertThatCode {
            CycleCheckerUtil.checkCycleAndDepth(authorityA)
        }.isInstanceOf(InvalidStateException::class.java)
            .hasMessage("LA-500: Path cycle detected with A")
    }

    // A B C
    //    \
    //     X Y
    @Test
    fun `it should not throw exception for cycle in sibling chain`() {
        val authorityA = Authority()
        authorityA.name = "A"

        val authorityB = Authority()
        authorityB.name = "B"

        val authorityC = Authority()
        authorityC.name = "C"

        val authorityD = Authority()
        authorityD.name = "D"

        val authorityX = Authority()
        authorityX.name = "X"

        val authorityY = Authority()
        authorityY.name = "Y"

        authorityA.implies.add(authorityB)

        authorityB.implies.add(authorityC)

        authorityB.implies.add(authorityX)

        authorityX.implies.add(authorityY)

        assertThatCode {
            CycleCheckerUtil.checkCycleAndDepth(authorityA)
        }.doesNotThrowAnyException()
    }

    // A B C
    //    \
    //     X Y C <-- though in parent chain A, is not in current processing chain of B
    @Test
    fun `it should not throw exception for cycle in sibling chain if parent implies a node but it is not in current processing node`() {
        val authorityA = Authority()
        authorityA.name = "A"

        val authorityB = Authority()
        authorityB.name = "B"

        val authorityC = Authority()
        authorityC.name = "C"

        val authorityD = Authority()
        authorityD.name = "D"

        val authorityX = Authority()
        authorityX.name = "X"

        val authorityY = Authority()
        authorityY.name = "Y"

        authorityA.implies.add(authorityB)

        authorityB.implies.add(authorityC)

        authorityB.implies.add(authorityX)

        authorityX.implies.add(authorityY)

        authorityY.implies.add(authorityC)

        assertThatCode {
            CycleCheckerUtil.checkCycleAndDepth(authorityA)
        }.doesNotThrowAnyException()
    }

    // A B C
    //    \
    //     X Y A
    @Test
    fun `it should throw exception for cycle in sibling chain`() {
        val authorityA = Authority()
        authorityA.name = "A"

        val authorityB = Authority()
        authorityB.name = "B"

        val authorityC = Authority()
        authorityC.name = "C"

        val authorityD = Authority()
        authorityD.name = "D"

        val authorityX = Authority()
        authorityX.name = "X"

        val authorityY = Authority()
        authorityY.name = "Y"

        authorityA.implies.add(authorityB)

        authorityB.implies.add(authorityC)

        authorityB.implies.add(authorityX)

        authorityX.implies.add(authorityY)

        authorityY.implies.add(authorityA)

        assertThatCode {
            CycleCheckerUtil.checkCycleAndDepth(authorityA)
        }.isInstanceOf(InvalidStateException::class.java)
            .hasMessage("LA-500: Path cycle detected with A")
    }

    // A B C
    //    \
    //     X
    //      \
    //       Z Y
    //        \
    //         A
    @Test
    fun `it should throw exception for cycle in child chain`() {
        val authorityA = Authority()
        authorityA.name = "A"

        val authorityB = Authority()
        authorityB.name = "B"

        val authorityC = Authority()
        authorityC.name = "C"

        val authorityD = Authority()
        authorityD.name = "D"

        val authorityX = Authority()
        authorityX.name = "X"

        val authorityY = Authority()
        authorityY.name = "Y"

        val authorityZ = Authority()
        authorityZ.name = "Z"

        authorityA.implies.add(authorityB)

        authorityB.implies.add(authorityC)

        authorityB.implies.add(authorityX)

        authorityX.implies.add(authorityZ)

        authorityZ.implies.add(authorityY)

        authorityZ.implies.add(authorityA)

        assertThatCode {
            CycleCheckerUtil.checkCycleAndDepth(authorityA)
        }.isInstanceOf(InvalidStateException::class.java)
            .hasMessage("LA-500: Path cycle detected with A")
    }
}