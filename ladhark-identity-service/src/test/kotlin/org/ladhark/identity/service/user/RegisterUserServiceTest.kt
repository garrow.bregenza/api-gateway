package org.ladhark.identity.service.user

import org.mockito.junit.jupiter.MockitoSettings

@MockitoSettings
class RegisterUserServiceTest {
    /*
        @Mock
        private lateinit var passwordEncoder: PasswordEncoder

        @Mock
        private lateinit var userRepository: UserRepository

        @Mock
        private lateinit var userCredentialsRepository: UserCredentialsRepository

        @Mock
        private lateinit var regionRepository: RegionRepository

        @Mock
        private lateinit var applicationEventPublisher: ApplicationEventPublisher

        @Mock
        private lateinit var appMessage: AppMessage

        @InjectMocks
        private lateinit var registerUserServiceImpl: RegisterUserServiceImpl

        @Test
        fun `it should register a new user`() {
            val uuidIndia = UUID.randomUUID()
            val uuidDelhi = UUID.randomUUID()

            val userRegionDetailsIndia = UserRegionDetails(true, false, uuidIndia.s)
            val userRegionDetailsDelhi = UserRegionDetails(true, false, uuidDelhi.s)

            val regionIndia = Region()
            regionIndia.name = "India"
            regionIndia.uuid = uuidIndia

            val regionDelhi = Region()
            regionDelhi.name = "Delhi"
            regionDelhi.uuid = uuidDelhi

            val registrationDetails = RegistrationDetails(
                "test@ladhark.org",
                "secret",
                Name("test", "", "user"),
                arrayOf(userRegionDetailsIndia, userRegionDetailsDelhi)
            )

            val user = User(
                registrationDetails.email,
                org.ladhark.identity.entity.user.Name(
                    registrationDetails.name.first,
                    registrationDetails.name.middle,
                    registrationDetails.name.last
                ),
                locked = false, active = true
            )


            user.hasRegions.addAll(arrayOf(regionIndia, regionDelhi).map {
                val userHasRegion = UserHasRegion()
                userHasRegion.region = it
                userHasRegion
            })

            user.initOnCreateTeams(UUID.randomUUID(), UUID.randomUUID())

            val userCredentials = UserCredentials()
            userCredentials.password = "234o234oi234uo234"
            userCredentials.user = user

            `when`(userRepository.findUserByEmail(registrationDetails.email)).thenReturn(Mono.empty())
            `when`(regionRepository.findRegionByUuid(uuidIndia)).thenReturn(regionIndia.toMono())
            `when`(regionRepository.findRegionByUuid(uuidDelhi)).thenReturn(regionDelhi.toMono())

            `when`(
                appMessage.getMessage(
                    eq("message.region.event.read.secondary.access"),
                    anyArray()
                )
            ).thenReturn("Region read.")

            `when`(passwordEncoder.encode(registrationDetails.password)).thenReturn("234o234oi234uo234")

            `when`(userRepository.save(any())).thenReturn(user.toMono())
            `when`(userCredentialsRepository.save(any())).thenReturn(userCredentials.toMono())

            registerUserServiceImpl.createUser(registrationDetails)
                .test().expectNext(user).verifyComplete()

            val userArg = argumentCaptor<User>()
            verify(userRepository, times(1)).save(userArg.capture())

            val event = argumentCaptor<RegistrationSuccessEvent>()
            verify(applicationEventPublisher, times(3)).publishEvent(event.capture())


            Assertions.assertThat(event.thirdValue.category()).isInstanceOf(RegisterEventCategory::class.java)
            Assertions.assertThat(event.thirdValue.category()).isEqualTo(RegisterEventCategory.REGISTER)
            Assertions.assertThat(event.thirdValue.action()).isEqualTo(ApplicationEventAction.SUCCESS)
            Assertions.assertThat(event.thirdValue.payload)
                .isEqualTo(RegistrationSuccessEventPayload(registrationDetails, user))
        }

     */
}