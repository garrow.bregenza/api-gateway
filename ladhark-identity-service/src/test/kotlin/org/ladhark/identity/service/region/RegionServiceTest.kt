package org.ladhark.identity.service.region

import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test
import org.ladhark.identity.api.dto.region.RegionDetails
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.exception.EntityAlreadyPresentException
import org.ladhark.identity.exception.EntityNotFoundException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.exception.InvalidStateException
import org.ladhark.identity.messages.AppMessage
import org.ladhark.identity.repository.region.RegionRepository
import org.ladhark.identity.security.context.SERVICE_CONTEXT_KEY
import org.ladhark.identity.service.region.cache.RegionCacheService
import org.ladhark.identity.service.region.event.RegionCreateEvent
import org.ladhark.identity.service.region.event.RegionEventCategory
import org.ladhark.identity.util.TestUtil
import org.ladhark.kotlin.extension.s
import org.mockito.ArgumentCaptor
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.any
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoSettings
import org.mockito.kotlin.argumentCaptor
import org.springframework.context.ApplicationEventPublisher
import java.util.*

@MockitoSettings
class RegionServiceTest {

        @Mock
        private lateinit var regionRepository: RegionRepository

        @Mock
        lateinit var appMessage: AppMessage

        @Mock
        lateinit var regionCacheService: RegionCacheService

        @Mock
        lateinit var applicationEventPublisher: ApplicationEventPublisher

        @InjectMocks
        lateinit var regionService: RegionServiceImpl

        @Test
        fun `it should save a region as public`() {
            runBlocking {
                // default public
                val regionDetails = RegionDetails("India")

                val region = Region()
                region.name = "India"

                `when`(regionRepository.save(any())).thenReturn(region)

                withContext(TestUtil.prepareTestContext()) {
                    regionService.createRegion(regionDetails)
                }

                val regionArg = ArgumentCaptor.forClass(Region::class.java)
                verify(regionRepository, times(1)).save(regionArg.capture())

                val regionFromSource = regionArg.value
                assertThat(regionFromSource.name).isEqualTo(region.name)

                val event = ArgumentCaptor.forClass(RegionCreateEvent::class.java)

                verify(applicationEventPublisher, times(1)).publishEvent(event.capture())

                assertThat(event.value.category()).isInstanceOf(RegionEventCategory::class.java)
                assertThat(event.value.category()).isEqualTo(RegionEventCategory.REGION)
                assertThat(event.value.action()).isEqualTo(ApplicationEventAction.CREATE)
                assertThat(event.value.payload).isEqualTo(region)
            }
        }

        @Test
        fun `it should not create a global region if a parent region is specified`() {

            runBlocking {
                val regionDetails = RegionDetails(
                    "India",
                    global = true,
                    public = false,
                    parentRegionUuid = UUID.randomUUID().s,
                    description = "Test Global"
                )

                `when`(appMessage.getMessage("error.global.region.cannot.have.parent")).thenReturn("Global region cannot have parent.")

                withContext(TestUtil.prepareTestContext()) {
                    assertThatExceptionOfType(InvalidArgumentException::class.java)
                        .isThrownBy { runBlocking { regionService.createRegion(regionDetails) } }
                        .withMessage("LA-400: Global region cannot have parent.")
                }

                verify(regionRepository, never()).save(any())
                verify(regionCacheService, never()).putRegionByNameToUuidAsync(any(), any())
                verify(regionCacheService, never()).putRegionByUuidAsync(any(), any())
                verify(applicationEventPublisher, never()).publishEvent(any())
            }
        }

        @Test
        fun `it should not create a global region if a global region is already present`() {
            runBlocking {
                val regionDetails = RegionDetails(
                    "India",
                    global = true,
                    public = false,
                    description = "Test Global"
                )

                val regionGlobalPresent = Region()
                regionGlobalPresent.name = "Global"
                regionGlobalPresent.global = true
                regionGlobalPresent.public = false

                `when`(regionRepository.findRegionByGlobal(true)).thenReturn(regionGlobalPresent)
                `when`(appMessage.getMessage("error.global.region.already.present")).thenReturn("Global region already present.")

                withContext(TestUtil.prepareTestContext()) {
                    assertThatExceptionOfType(EntityAlreadyPresentException::class.java)
                        .isThrownBy { runBlocking { regionService.createRegion(regionDetails) } }
                        .withMessage("LA-412: Global region already present.")
                }

                verify(regionRepository, never()).save(any())
                verify(regionCacheService, never()).putRegionByNameToUuidAsync(any(), any())
                verify(regionCacheService, never()).putRegionByUuidAsync(any(), any())
                verify(applicationEventPublisher, never()).publishEvent(any())
            }
        }

        @Test
        fun `it should not create a region if parent provided does not exists`() {
            runBlocking {
                val parentRegionUuid = UUID.randomUUID()

                val regionDetails = RegionDetails(
                    "India",
                    global = false,
                    public = false,
                    parentRegionUuid = parentRegionUuid.s,
                    description = "Test Global"
                )

                `when`(regionRepository.findRegionByUuid(parentRegionUuid)).thenReturn(null)
                `when`(
                    appMessage.getMessage(
                        "error.parent.region.not.found.by.uuid",
                        parentRegionUuid.s
                    )
                ).thenReturn("Parent region not found.")

                withContext(TestUtil.prepareTestContext()) {
                    assertThatExceptionOfType(EntityNotFoundException::class.java)
                        .isThrownBy { runBlocking { regionService.createRegion(regionDetails) } }
                        .withMessage("LA-404: Parent region not found.")
                }

                verify(regionRepository, never()).save(any())
                verify(regionCacheService, never()).putRegionByNameToUuidAsync(any(), any())
                verify(regionCacheService, never()).putRegionByUuidAsync(any(), any())
                verify(applicationEventPublisher, never()).publishEvent(any())
            }
        }

        @Test
        fun `it should not create a region if parent child relation already exists`() {
            runBlocking {
                val parentRegion = Region()
                parentRegion.name = "India"
                parentRegion.global = false
                parentRegion.public = false

                val regionDetails = RegionDetails(
                    "Delhi",
                    global = false,
                    public = false,
                    parentRegionUuid = parentRegion.uuid.s,
                    description = "Test Global"
                )

                `when`(regionRepository.findRegionByUuid(parentRegion.uuid)).thenReturn(parentRegion)
                `when`(
                    regionRepository.findChildInChainByName(
                        parentRegion.name,
                        regionDetails.name
                    )
                ).thenReturn(parentRegion)

                withContext(TestUtil.prepareTestContext()) {
                    assertThatExceptionOfType(InvalidStateException::class.java)
                        .isThrownBy { runBlocking { regionService.createRegion(regionDetails) } }
                        .withMessage("LA-500: Child and parent relation already exists.")
                }

                verify(regionRepository, never()).save(any())
                verify(regionCacheService, never()).putRegionByNameToUuidAsync(any(), any())
                verify(regionCacheService, never()).putRegionByUuidAsync(any(), any())
                verify(applicationEventPublisher, never()).publishEvent(any())
            }
        }

        @Test
        fun `it should not create a region if parent and child are same`() {

            runBlocking {
                val parentRegion = Region()
                parentRegion.name = "India"
                parentRegion.global = false
                parentRegion.public = false

                val regionDetails = RegionDetails(
                    "India",
                    global = false,
                    public = false,
                    parentRegionUuid = parentRegion.uuid.s,
                    description = "Test Global"
                )

                `when`(regionRepository.findRegionByUuid(parentRegion.uuid)).thenReturn(parentRegion)

                withContext(TestUtil.prepareTestContext()) {
                    assertThatExceptionOfType(InvalidStateException::class.java)
                        .isThrownBy { runBlocking { regionService.createRegion(regionDetails) } }
                        .withMessage("LA-500: Child and parent is same.")
                }

                verify(regionRepository, never()).save(any())
                verify(regionCacheService, never()).putRegionByNameToUuidAsync(any(), any())
                verify(regionCacheService, never()).putRegionByUuidAsync(any(), any())
                verify(applicationEventPublisher, never()).publishEvent(any())
            }
        }

        @Test
        fun `it should not create a region if context is empty`() {

            runBlocking {
                val regionDetails = RegionDetails(
                    "India",
                    global = true,
                    public = false,
                    description = "Test Global"
                )

                val region = Region()
                region.global = true
                region.public = false
                region.name = "India"
                region.description = "Test Global"

                assertThatExceptionOfType(IllegalStateException::class.java)
                    .isThrownBy { runBlocking { regionService.createRegion(regionDetails) } }
                    .withMessage("Context has no value for key $SERVICE_CONTEXT_KEY")

                verify(regionCacheService, never()).putRegionByNameToUuidAsync(region.name, region)
                verify(regionCacheService, never()).putRegionByUuidAsync(region.uuid.s, region)
                verify(regionRepository, never()).save(region)
                verify(applicationEventPublisher, never()).publishEvent(any())
            }
        }

        @Test
        fun `it should create a valid global region`() {

            runBlocking {
                val regionDetails = RegionDetails(
                    "India",
                    global = true,
                    public = false,
                    description = "Test Global"
                )

                val region = Region()
                region.global = true
                region.public = false
                region.name = "India"
                region.description = "Test Global"

                `when`(regionRepository.findRegionByGlobal(true)).thenReturn(null)
                `when`(regionRepository.save(any())).thenReturn(region)

                withContext(TestUtil.prepareTestContext()) {
                    regionService.createRegion(regionDetails)
                }

                val regionCaptor = argumentCaptor<Region>()
                verify(regionRepository, times(1)).save(regionCaptor.capture())

                val regionFromSource = regionCaptor.firstValue
                assertThat(regionFromSource.name).isEqualTo(region.name)

                val regionCreateEventCaptor = argumentCaptor<RegionCreateEvent>()

                verify(applicationEventPublisher, times(1)).publishEvent(regionCreateEventCaptor.capture())


                val eventValue = regionCreateEventCaptor.firstValue

                assertThat(eventValue.category()).isInstanceOf(RegionEventCategory::class.java)
                assertThat(eventValue.category()).isEqualTo(RegionEventCategory.REGION)
                assertThat(eventValue.action()).isEqualTo(ApplicationEventAction.CREATE)
                assertThat(eventValue.payload).isEqualTo(region)
            }
        }

        @Test
        fun `it should create region with valid parent relation`() {

            runBlocking {
                val parentRegion = Region()
                parentRegion.name = "India"
                parentRegion.global = false
                parentRegion.public = false

                val regionDetails = RegionDetails(
                    "Delhi",
                    global = false,
                    public = false,
                    parentRegionUuid = parentRegion.uuid.s,
                    description = "Test Global"
                )

                val region = Region()
                region.global = false
                region.public = false
                region.name = "Delhi"
                region.description = "Test Global"
                region.parentRegion = parentRegion

                `when`(regionRepository.findRegionByUuid(parentRegion.uuid)).thenReturn(parentRegion)
                `when`(
                    regionRepository.findChildInChainByName(
                        parentRegion.name,
                        regionDetails.name
                    )
                ).thenReturn(null)

                `when`(regionRepository.save(any())).thenReturn(region)

                withContext(TestUtil.prepareTestContext()) {
                    regionService.createRegion(regionDetails)
                }

                val regionCaptor = argumentCaptor<Region>()
                verify(regionRepository, times(1)).save(regionCaptor.capture())

                val regionFromSource = regionCaptor.firstValue
                assertThat(regionFromSource.name).isEqualTo(region.name)

                val regionCreateEventCaptor = argumentCaptor<RegionCreateEvent>()

                verify(applicationEventPublisher, times(1)).publishEvent(regionCreateEventCaptor.capture())


                val eventValue = regionCreateEventCaptor.firstValue

                assertThat(eventValue.category()).isInstanceOf(RegionEventCategory::class.java)
                assertThat(eventValue.category()).isEqualTo(RegionEventCategory.REGION)
                assertThat(eventValue.action()).isEqualTo(ApplicationEventAction.CREATE)
                assertThat(eventValue.payload).isEqualTo(region)
            }
        }

        @Test
        fun `it should create valid region when no parent region is provided`() {

            runBlocking {
                val regionDetails = RegionDetails(
                    "Delhi",
                    global = false,
                    public = false,
                    description = "Test Global"
                )

                val region = Region()
                region.global = false
                region.public = false
                region.name = "Delhi"
                region.description = "Test Global"

                `when`(regionRepository.save(any())).thenReturn(region)

                withContext(TestUtil.prepareTestContext()) {
                    regionService.createRegion(regionDetails)
                }

                val regionCaptor = argumentCaptor<Region>()
                verify(regionRepository, times(1)).save(regionCaptor.capture())

                val regionFromSource = regionCaptor.firstValue
                assertThat(regionFromSource.name).isEqualTo(region.name)

                val regionCreateEventCaptor = argumentCaptor<RegionCreateEvent>()

                verify(applicationEventPublisher, times(1)).publishEvent(regionCreateEventCaptor.capture())


                val eventValue = regionCreateEventCaptor.firstValue

                assertThat(eventValue.category()).isInstanceOf(RegionEventCategory::class.java)
                assertThat(eventValue.category()).isEqualTo(RegionEventCategory.REGION)
                assertThat(eventValue.action()).isEqualTo(ApplicationEventAction.CREATE)
                assertThat(eventValue.payload).isEqualTo(region)
            }
        }

        @Test
        fun `it should update a valid region if passed region is not present and involves special case of name update`() {

            runBlocking {
                val regionDetails = RegionDetails(
                    "Paris",
                    global = false,
                    public = false,
                    description = "Test Global"
                )

                val regionBeforeUpdate = Region()
                regionBeforeUpdate.global = false
                regionBeforeUpdate.public = false
                regionBeforeUpdate.name = "Delhi"
                regionBeforeUpdate.description = "Test Global"

                val regionUpdated = Region()
                regionUpdated.global = false
                regionUpdated.public = false
                regionUpdated.name = "Paris"
                regionUpdated.description = "Test Global"

                `when`(regionRepository.findRegionByUuid(regionBeforeUpdate.uuid)).thenReturn(regionBeforeUpdate)
                `when`(
                    regionRepository.findRegionExistsForUpdateWithName(
                        regionDetails.name,
                        regionBeforeUpdate.name
                    )
                ).thenReturn(null)

                val regionCaptor = argumentCaptor<Region>()

                `when`(regionRepository.save(any())).thenReturn(regionUpdated)


                withContext(TestUtil.prepareTestContext()) {
                    regionService.updateRegion(regionBeforeUpdate.uuid.s, regionDetails)
                }

                verify(regionRepository, times(1)).save(regionCaptor.capture())
                val region = regionCaptor.firstValue

                assertThat(region.name).isEqualTo(regionDetails.name)
                assertThat(region.public).isEqualTo(regionDetails.public)
                assertThat(region.global).isEqualTo(regionDetails.global)
                assertThat(region.description).isEqualTo(regionDetails.description)


                verify(regionCacheService, times(1)).removeRegionByNameToUuidAsync("Delhi")
            }
        }

        @Test
        fun `it should not update a region if passed region is not present`() {

        }

        @Test
        fun `it should not update a region if passed new name for update is already existing`() {

        }


}