package org.ladhark.identity.service.application

import org.ladhark.identity.messages.AppMessage
import org.ladhark.identity.repository.application.ApplicationRepository
import org.ladhark.identity.service.application.cache.ApplicationCacheService
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoSettings
import org.springframework.context.ApplicationEventPublisher

@MockitoSettings
class ApplicationServiceTest {

    @Mock
    private lateinit var applicationRepository: ApplicationRepository

    @Mock
    lateinit var appMessage: AppMessage

    @Mock
    lateinit var applicationCacheService: ApplicationCacheService

    @Mock
    lateinit var applicationEventPublisher: ApplicationEventPublisher

    @InjectMocks
    lateinit var applicationService: ApplicationServiceImpl


}