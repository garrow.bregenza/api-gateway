package org.ladhark.identity.service.role

import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test
import org.ladhark.identity.api.dto.role.RoleDetails
import org.ladhark.identity.entity.role.Role
import org.ladhark.identity.event.ApplicationEventAction
import org.ladhark.identity.exception.EntityNotFoundException
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.messages.AppMessage
import org.ladhark.identity.repository.role.RoleRepository
import org.ladhark.identity.security.context.SERVICE_CONTEXT_KEY
import org.ladhark.identity.service.role.cache.RoleCacheService
import org.ladhark.identity.service.role.event.RoleCreateEvent
import org.ladhark.identity.service.role.event.RoleEventCategory
import org.ladhark.identity.service.role.event.RolePageReadEvent
import org.ladhark.identity.service.role.event.RoleReadEvent
import org.ladhark.identity.util.TestUtil
import org.ladhark.kotlin.extension.s
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoSettings
import org.mockito.kotlin.argumentCaptor
import org.springframework.context.ApplicationEventPublisher
import java.util.*


@MockitoSettings
class RoleServiceTest {

        @Mock
        private lateinit var roleRepository: RoleRepository

        @Mock
        private lateinit var appMessage: AppMessage

        @Mock
        private lateinit var roleCacheService: RoleCacheService

        @Mock
        private lateinit var applicationEventPublisher: ApplicationEventPublisher

        @InjectMocks
        private lateinit var roleService: RoleServiceImpl

        @Test
        fun `it should find role by name from database when not present in cache`() {
            runBlocking {
                val role = Role()
                role.name = "test"

                `when`(roleRepository.findRoleByName("test")).thenReturn(role)
                `when`(roleCacheService.getRoleByNameToUuidAsync("test")).thenReturn(null)

                withContext(TestUtil.prepareTestContext()) {
                    roleService.findByName("test")
                }

                val event = argumentCaptor<RoleReadEvent>()

                verify(roleCacheService, times(1)).getRoleByNameToUuidAsync("test")
                verify(roleRepository, times(1)).findRoleByName("test")
                verify(applicationEventPublisher, times(1)).publishEvent(event.capture())

                assertThat(event.firstValue.category()).isInstanceOf(RoleEventCategory::class.java)
                assertThat(event.firstValue.category()).isEqualTo(RoleEventCategory.ROLE)
                assertThat(event.firstValue.action()).isEqualTo(ApplicationEventAction.READ)
                assertThat(event.firstValue.payload).isEqualTo(role)
            }

        }

        @Test
        fun `it should throw error when role by name is not in database and cache`() {
            runBlocking {
                `when`(roleRepository.findRoleByName("test")).thenReturn(null)
                `when`(roleCacheService.getRoleByNameToUuidAsync("test")).thenReturn(null)
                `when`(appMessage.getMessage("error.resource.not.found.by.name", "test"))
                    .thenReturn("Missing entity")

                    assertThatExceptionOfType(EntityNotFoundException::class.java)
                        .isThrownBy { runBlocking {  withContext(TestUtil.prepareTestContext()) {
                                roleService.findByName("test")
                            }
                        }
                    }


                verify(roleCacheService, times(1)).getRoleByNameToUuidAsync("test")
                verify(roleRepository, times(1)).findRoleByName("test")
                verify(applicationEventPublisher, never()).publishEvent(any())
            }
        }

        @Test
        fun `it should find role by name from cache and not make repository call`() {
            runBlocking {
                val role = Role()
                role.name = "test"

                `when`(roleCacheService.getRoleByNameToUuidAsync("test")).thenReturn(role)
                `when`(
                    appMessage.getMessage(
                        "message.entity.event.read.cache.access", arrayOf(
                            Role::javaClass.name,
                            role.name, role.uuid.s
                        )
                    )
                ).thenReturn("Entity from cache")

                withContext(TestUtil.prepareTestContext()) {
                    roleService.findByName("test")
                }

                val event = argumentCaptor<RoleReadEvent>()

                verify(roleCacheService, times(1)).getRoleByNameToUuidAsync("test")
                verify(roleRepository, never()).findRoleByName("test")
                verify(applicationEventPublisher, times(1)).publishEvent(event.capture())

                assertThat(event.firstValue.category()).isInstanceOf(RoleEventCategory::class.java)
                assertThat(event.firstValue.category()).isEqualTo(RoleEventCategory.ROLE)
                assertThat(event.firstValue.action()).isEqualTo(ApplicationEventAction.READ)
                assertThat(event.firstValue.payload).isEqualTo(role)
            }
        }

        @Test
        fun `it should throw exception if argument is empty for find by name`() {

            runBlocking {
                `when`(appMessage.getMessage("error.value.required.is.empty", "Name"))
                    .thenReturn("Empty argument")

                withContext(TestUtil.prepareTestContext()) {
                    assertThatExceptionOfType(InvalidArgumentException::class.java)
                        .isThrownBy { runBlocking { roleService.findByName("") } }
                }

                verify(roleCacheService, never()).getRoleByNameToUuidAsync(any())
                verify(roleRepository, never()).findRoleByName(any())
                verify(applicationEventPublisher, never()).publishEvent(any())
            }

        }

        @Test
        fun `it should throw exception if context is empty for find by name by cache scenario`() {
            runBlocking {
                val role = Role()
                role.name = "test"

                assertThatExceptionOfType(IllegalStateException::class.java)
                    .isThrownBy { runBlocking { roleService.findByName("test") } }
                    .withMessage("Context is empty")


                verify(roleCacheService, never()).getRoleByNameToUuidAsync("test")
                verify(roleRepository, never()).findRoleByName("test")
                verify(applicationEventPublisher, never()).publishEvent(any())
            }

        }

        @Test
        fun `it should throw exception if context is empty for find by name by repository scenario`() {
            runBlocking {
                val role = Role()
                role.name = "test"

                assertThatExceptionOfType(IllegalStateException::class.java)
                    .isThrownBy { runBlocking { roleService.findByName("test") } }
                    .withMessage("Context is empty")


                verify(roleCacheService, never()).getRoleByNameToUuidAsync("test")
                verify(roleRepository, never()).findRoleByName("test")
                verify(applicationEventPublisher, never()).publishEvent(any())
            }

        }

        @Test
        fun `it should find role by uuid from database when not present in cache`() {
            runBlocking {
                val role = Role()
                role.name = "test"

                `when`(roleRepository.findRoleByUuid(role.uuid)).thenReturn(role)
                `when`(roleCacheService.getRoleByUuidAsync(role.uuid.s)).thenReturn(null)

                withContext(TestUtil.prepareTestContext()) {
                    roleService.findByUuid(role.uuid.s)
                }

                val event = argumentCaptor<RoleReadEvent>()

                verify(roleCacheService, times(1)).getRoleByUuidAsync(role.uuid.s)
                verify(roleRepository, times(1)).findRoleByUuid(role.uuid)
                verify(applicationEventPublisher, times(1)).publishEvent(event.capture())

                assertThat(event.firstValue.category()).isInstanceOf(RoleEventCategory::class.java)
                assertThat(event.firstValue.category()).isEqualTo(RoleEventCategory.ROLE)
                assertThat(event.firstValue.action()).isEqualTo(ApplicationEventAction.READ)
                assertThat(event.firstValue.payload).isEqualTo(role)
            }
        }

        @Test
        fun `it should throw error when role by uuid is not in database and cache`() {
            runBlocking {
                val uuid = UUID.randomUUID()
                val uuids = uuid.s

                `when`(roleRepository.findRoleByUuid(uuid)).thenReturn(null)
                `when`(roleCacheService.getRoleByUuidAsync(uuids)).thenReturn(null)
                `when`(appMessage.getMessage("error.resource.not.found.by.uuid", uuids)).thenReturn("Missing entity")

                assertThatExceptionOfType(EntityNotFoundException::class.java)
                    .isThrownBy { runBlocking {
                        withContext(TestUtil.prepareTestContext()) {
                            roleService.findByUuid(uuids)
                        }
                    }
                }

                verify(roleCacheService, times(1)).getRoleByUuidAsync(uuids)
                verify(roleRepository, times(1)).findRoleByUuid(uuid)
                verify(applicationEventPublisher, never()).publishEvent(any())
            }
        }

        @Test
        fun `it should find role by uuid from cache and not make repository call`() {
            runBlocking {
                val role = Role()
                role.name = "test"

                `when`(roleCacheService.getRoleByUuidAsync(role.uuid.s)).thenReturn(role)
                `when`(
                    appMessage.getMessage(
                        "message.entity.event.read.cache.access", arrayOf(
                            Role::javaClass.name,
                            role.name, role.uuid.s
                        )
                    )
                ).thenReturn("Entity from cache")

                withContext(TestUtil.prepareTestContext()) {
                    roleService.findByUuid(role.uuid.s)
                }

                val event = argumentCaptor<RoleReadEvent>()

                verify(roleCacheService, times(1)).getRoleByUuidAsync(role.uuid.s)
                verify(roleRepository, never()).findRoleByUuid(role.uuid)
                verify(applicationEventPublisher, times(1)).publishEvent(event.capture())

                assertThat(event.firstValue.category()).isInstanceOf(RoleEventCategory::class.java)
                assertThat(event.firstValue.category()).isEqualTo(RoleEventCategory.ROLE)
                assertThat(event.firstValue.action()).isEqualTo(ApplicationEventAction.READ)
                assertThat(event.firstValue.payload).isEqualTo(role)
            }
        }

        @Test
        fun `it should throw exception if argument is empty for find by uuid`() {
            runBlocking {
                `when`(appMessage.getMessage("error.value.required.is.empty", "UUID"))
                    .thenReturn("Empty argument")

                withContext(TestUtil.prepareTestContext()) {
                    assertThatExceptionOfType(InvalidArgumentException::class.java)
                        .isThrownBy { runBlocking { roleService.findByUuid("") } }
                }

                verify(roleCacheService, never()).getRoleByUuidAsync(any())
                verify(roleRepository, never()).findRoleByUuid(any())
                verify(applicationEventPublisher, never()).publishEvent(any())
            }
        }

        @Test
        fun `it should throw exception if context is empty for find by uuid by cache scenario`() {
            runBlocking {
                val role = Role()
                role.name = "test"

                assertThatExceptionOfType(IllegalStateException::class.java)
                    .isThrownBy { runBlocking { roleService.findByUuid(role.uuid.s) } }
                    .withMessage("Context has no value for key $SERVICE_CONTEXT_KEY")


                verify(roleCacheService, never()).getRoleByUuidAsync(role.uuid.s)
                verify(roleRepository, never()).findRoleByUuid(role.uuid)
                verify(applicationEventPublisher, never()).publishEvent(any())
            }
        }

        @Test
        fun `it should save role and put it in cache`() {
            runBlocking {
                val roleDetails = RoleDetails("test")
                val role = Role()
                role.name = "test"

                `when`(roleRepository.findRoleByName("test")).thenReturn(null)
                `when`(roleCacheService.getRoleByNameToUuidAsync("test")).thenReturn(null)
                `when`(
                    appMessage.getMessage(
                        "error.resource.not.found.by.name",
                        roleDetails.name
                    )
                ).thenReturn("Entity not found.")

                `when`(roleRepository.save(any())).thenReturn(role)


                withContext(TestUtil.prepareTestContext()) {
                    roleService.createRole(roleDetails)
                }


                val roleArg = argumentCaptor<Role>()
                verify(roleRepository, times(1)).save(roleArg.capture())

                val roleFromSource = roleArg.firstValue
                assertThat(roleFromSource.name).isEqualTo(role.name)

                val event = argumentCaptor<RoleCreateEvent>()

                verify(roleRepository, times(1)).findRoleByName("test")
                verify(roleCacheService, times(1)).getRoleByNameToUuidAsync("test")

                verify(applicationEventPublisher, times(1)).publishEvent(event.capture())

                assertThat(event.firstValue.category()).isInstanceOf(RoleEventCategory::class.java)
                assertThat(event.firstValue.category()).isEqualTo(RoleEventCategory.ROLE)
                assertThat(event.firstValue.action()).isEqualTo(ApplicationEventAction.CREATE)
                assertThat(event.firstValue.payload).isEqualTo(role)
            }
        }

        @Test
        fun `it should not save role and put it in cache if context is empty`() {
            runBlocking {
                val roleDetails = RoleDetails("test")
                val role = Role()
                role.name = "test"

                assertThatExceptionOfType(IllegalStateException::class.java)
                    .isThrownBy { runBlocking { roleService.createRole(roleDetails) } }
                    .withMessage("Context has no value for key $SERVICE_CONTEXT_KEY")

                verify(roleCacheService, never()).putRoleByNameToUuidAsync(role.name, role)
                verify(roleCacheService, never()).putRoleByUuidAsync(role.uuid.s, role)
                verify(roleRepository, never()).save(role)
                verify(applicationEventPublisher, never()).publishEvent(any())
            }
        }

        @Test
        fun `it should find all roles`() {
            runBlocking {
                val userUuid = UUID.randomUUID()

                val role1 = Role()
                role1.name = "role1"

                val role2 = Role()
                role2.name = "role2"

                val roleListFlow = flowOf(role1, role2)

                `when`(roleRepository.findAll()).thenReturn(roleListFlow)
                `when`(
                    appMessage.getMessage(
                        "message.region.event.read.page.access",
                        arrayOf("${role1.uuid},${role2.uuid}", userUuid.s, "`page region`")
                    )
                ).thenReturn("Role page read.")

                withContext(TestUtil.prepareTestContext(userUuid)) {
                    roleService.findAll()
                }

                val event = argumentCaptor<RolePageReadEvent>()
                verify(applicationEventPublisher, times(1)).publishEvent(event.capture())
                verify(roleRepository, times(1)).findAll()

                assertThat(event.firstValue.category()).isInstanceOf(RoleEventCategory::class.java)
                assertThat(event.firstValue.category()).isEqualTo(RoleEventCategory.ROLE)
                assertThat(event.firstValue.action()).isEqualTo(ApplicationEventAction.READ)
                assertThat(event.firstValue.payload.uuidsCsv()).isEqualTo("${role1.uuid},${role2.uuid}")
            }
        }

}