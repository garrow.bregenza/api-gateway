package org.ladhark.identity.entity.region

import org.ladhark.identity.entity.BaseEntity
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Relationship

@Node
class Region() : BaseEntity() {

    // @Index(unique = true)
    lateinit var name: String

    var description: String? = null

    // Marker for root
    var global = false

    // to be visible during public actions such as register new user
    var public = true

    @Relationship(type = "PARENT_REGION", direction = Relationship.Direction.OUTGOING)
    var parentRegion: Region? = null

    constructor(name: String) : this() {
        this.name = name
    }

    constructor(name: String, parentRegion: Region) : this(name) {
        this.parentRegion = parentRegion
    }
}