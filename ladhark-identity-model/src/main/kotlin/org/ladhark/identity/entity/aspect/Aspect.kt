package org.ladhark.identity.entity.aspect

import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.entity.resource.ResourceAction
import org.springframework.data.neo4j.core.schema.CompositeProperty
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Relationship

@Node
class AspectType : BaseEntity() {

    // @Index(unique = true)
    lateinit var name: String

    var description: String? = null
}

@Node
class Aspect : BaseEntity() {

    // @Index(unique = true)
    lateinit var name: String

    var description: String? = null

    @Relationship(type = "HAS_ASPECT_TYPE", direction = Relationship.Direction.OUTGOING)
    lateinit var type: AspectType

    var enforceRegion = false

    // if multiple filter qualify for a route how to sort
    var order = 0

    @Relationship(type = "BELONGS_TO_REGION", direction = Relationship.Direction.OUTGOING)
    var region: Region? = null

    @Relationship(type = "BELONGS_TO_RESOURCE_ACTION", direction = Relationship.Direction.OUTGOING)
    lateinit var resourceAction: ResourceAction

    @CompositeProperty
    val properties: MutableMap<String, Any> = HashMap()

}