package org.ladhark.identity.entity.user.converter

import org.ladhark.identity.entity.user.Name
import org.neo4j.driver.Value
import org.neo4j.driver.Values
import org.springframework.data.neo4j.core.convert.Neo4jConversionService
import org.springframework.data.neo4j.core.convert.Neo4jPersistentPropertyToMapConverter


class NameConverter : Neo4jPersistentPropertyToMapConverter<String, Name> {

    override fun compose(source: MutableMap<String, Value>, neo4jConversionService: Neo4jConversionService): Name {
        val firstName = source["firstName"]
        val middleName = source["middleName"]
        val lastName = source["lastName"]

        return Name(firstName!!.asString(), middleName!!.asString(), lastName!!.asString())
    }

    override fun decompose(property: Name?, neo4jConversionService: Neo4jConversionService): MutableMap<String, Value> {
        val properties: MutableMap<String, Value> = HashMap()

        if (property != null) {
            properties["firstName"] = Values.value(property.first)
            properties["middleName"] = Values.value(property.middle)
            properties["lastName"] = Values.value(property.last)
        }

        return properties
    }

}