package org.ladhark.identity.entity.user

import com.fasterxml.jackson.annotation.JsonIgnore
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.entity.role.Role
import org.ladhark.identity.entity.team.Team
import org.ladhark.identity.entity.team.TeamType
import org.ladhark.identity.entity.user.converter.NameConverter
import org.ladhark.identity.exception.AuthorityEvaluationFailedException
import org.ladhark.identity.exception.InvalidStateException
import org.springframework.data.neo4j.core.schema.*
import java.util.*

@Node
class User() : BaseEntity() {

    // @Index(unique = true)
    lateinit var email: String

    var locked = false
    var active = false
    var system = false

    @CompositeProperty(converter = NameConverter::class)
    var name: Name = Name()

    // Self team for sharing record with no one but self
    // Personal private profile page may not be shared with anyone, such records would come here
    // Artifacts are shareable, a document of personal nature would fit here
    @Relationship(type = "HAS_SELF_TEAM", direction = Relationship.Direction.OUTGOING)
    lateinit var selfTeam: Team

    // Exclusive team for this user irrespective of region
    // An artifact record for this user would be sharable with members in this team
    // Typical example a student record can be shared with parents
    @Relationship(type = "HAS_EXCLUSIVE_TEAM", direction = Relationship.Direction.OUTGOING)
    lateinit var exclusiveTeam: Team

    // Exclusive role for this user irrespective of region
    @Relationship(type = "HAS_EXCLUSIVE_ROLE", direction = Relationship.Direction.OUTGOING)
    var exclusiveRole: Role? = null

    // Regions to which this user belongs
    @Relationship(type = "HAS_REGIONS", direction = Relationship.Direction.OUTGOING)
    var hasRegions: MutableSet<UserHasRegion> = HashSet()

    constructor(email: String, name: Name = Name(), locked: Boolean, active: Boolean, system: Boolean = false) : this() {
        this.email = email
        this.name = name
        this.locked = locked
        this.active = active
        this.system = system
    }

    fun isValid() {
        if (locked) {
            throw AuthorityEvaluationFailedException("User is locked.")
        }

        if (!active) {
            throw AuthorityEvaluationFailedException("User is inactive.")
        }
    }

    fun primaryRegion(): UserHasRegion? {
        return hasRegions.find { it.primary }
    }

    fun userRegion(regionUuid: UUID): UserHasRegion? {
        return hasRegions.find { it.region.uuid == regionUuid }
    }

    fun initOnCreateTeams(sefTeamUUID: UUID, exclusiveTeamUUID: UUID) {

        if (email.isBlank()) {
            throw InvalidStateException("Email not set.")
        }

        selfTeam = Team()
        selfTeam.uuid = sefTeamUUID
        selfTeam.type = TeamType.SELF
        selfTeam.name = "_self_${email}"

        exclusiveTeam = Team()
        exclusiveTeam.uuid = exclusiveTeamUUID
        exclusiveTeam.type = TeamType.EXCLUSIVE
        exclusiveTeam.name = "_exclusive_${email}"
    }
}

@Node
class UserCredentials : BaseEntity() {

    // JsonIgnore as entities are notoriously linked to lot of objects and can make out as JSON accidentally better be explicit on export
    @JsonIgnore
    @Transient
    lateinit var password: String

    @Relationship(type = "BELONGS_TO_USER", direction = Relationship.Direction.OUTGOING)
    lateinit var user: User

    // cannot risk accidental printing of password
    override fun toString(): String {
        return "UserCredentials@${hashCode()}"
    }
}

@RelationshipProperties // (type = "HAS_REGIONS")
class UserHasRegion {

    @Id
    @GeneratedValue
    var id: Long? = null

    // User will be logged into this reason by default
    var primary = false

    // When a User is assigned to a region, can he access the children regions.
    // For instance India has Delhi and Agra as two children.
    // If user is assigned to India with this field as true he can access data from
    // Delhi and Agra, else data local to India can only be accessed
    var childrenAccess = false

    @TargetNode
    lateinit var region: Region


}

class Name(val first: String = "", val middle: String = "", val last: String = "") {

    fun fullName(): String {
        return "$first ${middle.trim()} $last"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Name

        if (first != other.first) return false
        if (middle != other.middle) return false
        if (last != other.last) return false

        return true
    }

    override fun hashCode(): Int {
        var result = first.hashCode()
        result = 31 * result + middle.hashCode()
        result = 31 * result + last.hashCode()
        return result
    }

}