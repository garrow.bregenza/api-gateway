package org.ladhark.identity.entity.authority

import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.resource.Resource
import org.ladhark.identity.util.CycleChecker
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Relationship

@Node
class Authority() : BaseEntity(), CycleChecker {

    // @Index(unique = true)
    lateinit var name: String

    @Relationship(type = "BELONGS_TO_RESOURCE", direction = Relationship.Direction.OUTGOING)
    lateinit var resource: Resource

    @Relationship(type = "IMPLIES_AUTHORITIES", direction = Relationship.Direction.OUTGOING)
    var implies = HashSet<Authority>()

    constructor(name: String, resource: Resource) : this() {
        this.name = name
        this.resource = resource
    }

    constructor(name: String, resource: Resource, vararg impliesAuthorities: Authority) : this(name, resource) {
        this.implies.addAll(impliesAuthorities)
    }

    override fun checkAgainst(): String {
        return this.name
    }

    override fun children(): Iterator<CycleChecker> {
        return this.implies.iterator()
    }

}