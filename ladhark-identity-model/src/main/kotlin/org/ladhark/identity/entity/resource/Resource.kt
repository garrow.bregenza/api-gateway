package org.ladhark.identity.entity.resource

import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.application.Application
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Relationship

@Node
class Resource : BaseEntity() {

    //@Index(unique = true)
    lateinit var name: String
    var description: String? = null

    @Relationship(type = "BELONGS_TO_APPLICATION", direction = Relationship.Direction.OUTGOING)
    lateinit var application: Application
}

@Node
class ResourceAction : BaseEntity() {

    //@Index(unique = true)
    lateinit var name: String
    lateinit var scope: Scope
    var description: String? = null

    //@Index
    lateinit var uriPattern: String

    @Relationship(type = "BELONGS_TO_RESOURCE", direction = Relationship.Direction.OUTGOING)
    lateinit var resource: Resource

    companion object {
        private const val EMPTY_RESOURCE_NAME = "EMPTY_RESOURCE"
        val EMPTY_RESOURCE = empty()

        fun empty(): ResourceAction {
            val empty = ResourceAction()
            empty.id = -1
            empty.name = EMPTY_RESOURCE_NAME
            return empty
        }

        fun isEmpty(resourceAction: ResourceAction): Boolean {
            return resourceAction.name == EMPTY_RESOURCE_NAME
        }

        fun isNotEmpty(resourceAction: ResourceAction): Boolean {
            return !isEmpty(resourceAction)
        }
    }
}

enum class Scope {

    GET, POST, PUT, PATCH, DELETE, ANY;

    companion object {
        val STRING_SET = values().map { v -> v.toString() }.toSet()
    }
}