package org.ladhark.identity.entity.application

import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.credential.CredentialStore
import org.springframework.data.neo4j.core.schema.CompositeProperty
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Relationship


@Node
class Application : BaseEntity() {
    //@Index(unique = true)
    lateinit var name: String

    lateinit var description: String

    // is this application supported and is external service
    var external = true

    // route.path = "/httpbin/**"
    lateinit var servicePattern: String

    var logo: ByteArray? = null

    @Relationship(type = "HAS_HOSTING_LOCATIONS", direction = Relationship.Direction.OUTGOING)
    var hostingLocations: MutableSet<HostingLocation> = HashSet()

    @Relationship(type = "HAS_CREDENTIAL_STORE", direction = Relationship.Direction.OUTGOING)
    var credentialStore: CredentialStore? = null

    @Relationship(type = "HAS_BALANCER_CONFIG", direction = Relationship.Direction.OUTGOING)
    var balancerConfig = BalancerConfig()

    // root url/home url

    fun isHome() = !external

}

@Node
class HostingLocation : BaseEntity() {

    lateinit var location: String

}

@Node
class BalancerConfig : BaseEntity() {

    var type = BalancerType.ROUND_ROBIN

    var poolSize = 50

    @CompositeProperty
    val properties: MutableMap<String, Any> = HashMap()
}

enum class BalancerType {

    RANDOM, FAIL_OVER, ROUND_ROBIN;
    companion object {
        val STRING_SET = BalancerType.values().map { v -> v.toString() }.toSet()
    }
}