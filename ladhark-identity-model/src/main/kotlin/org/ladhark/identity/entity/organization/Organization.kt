package org.ladhark.identity.entity.organization

import org.ladhark.identity.entity.BaseEntity
import org.springframework.data.neo4j.core.schema.Node

@Node
class Organization() : BaseEntity() {

    // @Index(unique = true)
    lateinit var name: String

    constructor(name: String) : this() {
        this.name = name
    }
}