package org.ladhark.identity.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.*
import org.springframework.data.neo4j.core.schema.GeneratedValue
import org.springframework.data.neo4j.core.schema.Id
import org.springframework.data.neo4j.core.schema.Node
import java.io.Serializable
import java.time.Instant
import java.util.*

@Node
abstract class BaseEntity : Serializable {
    @Id
    @GeneratedValue
    @JsonIgnore
    var id: Long? = null

    //@Index(unique = true)
    var uuid: UUID = generateUuid()

    //@Version
    //@JsonIgnore
    //var version: Long = -1

    @CreatedBy
    var createdBy: UUID? = null

    @CreatedDate
    var createdAt: Instant = Instant.now()

    @LastModifiedBy
    var modifiedBy: UUID? = null

    @LastModifiedDate
    var modifiedAt: Instant? = null

    var deleted: Boolean = false

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BaseEntity

        if (uuid != other.uuid) return false

        return true
    }

    override fun hashCode(): Int {
        return uuid.hashCode()
    }

    companion object {

        //private val tag = AtomicLong()

        /**
         * UUID is a blocking call we need to fetch value on a worker thread
         *
         * TODO: to check block hound still complains for random call to be a blocking call
         */
        fun generateUuid(): UUID {
            /*val instantNow = Instant.now()
            val millis = instantNow.toEpochMilli()
            val nano = instantNow.nano
            val tag = tag.incrementAndGet()


            val msb = millis + millis.hashCode() + nano + tag + Objects.hashCode(Object())
            val lsb = millis + millis.hashCode() + nano + tag + Objects.hashCode(Object())*/

            return UUID.randomUUID()
        }

    }
}