package org.ladhark.identity.entity.user

import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.entity.team.Team
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Relationship

/**
 * In a given region, User has access to Teams
 */
@Node
class UserTeamRegion : BaseEntity() {

    @Relationship(type = "HAS_USER", direction = Relationship.Direction.OUTGOING)
    var user: User = User()

    @Relationship(type = "HAS_TEAM", direction = Relationship.Direction.OUTGOING)
    var team: Team = Team()

    @Relationship(type = "HAS_REGION", direction = Relationship.Direction.OUTGOING)
    var region: Region = Region()
}