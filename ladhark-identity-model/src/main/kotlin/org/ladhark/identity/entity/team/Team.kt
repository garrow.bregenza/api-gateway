package org.ladhark.identity.entity.team

import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.entity.role.Role
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Relationship

@Node
class Team() : BaseEntity() {
    // @Index(unique = true)
    lateinit var name: String

    var description: String? = null

    // TODO extract to entity
    var type: TeamType = TeamType.OPERATION

    // Parent team if present, you inherit roles from your parent team
    @Relationship(type = "PARENT_TEAM", direction = Relationship.Direction.OUTGOING)
    var parentTeam: Team? = null

    // Roles supported by this team
    @Relationship(type = "HAS_ROLES", direction = Relationship.Direction.OUTGOING)
    var roles: MutableSet<Role> = HashSet()

    // Team will be available to allotted regions only, team can be multi regional
    @Relationship(type = "BELONGS_TO_REGIONS", direction = Relationship.Direction.OUTGOING)
    var regions: MutableSet<Region> = HashSet()

    // Exclusive role for this team irrespective of region
    @Relationship(type = "HAS_EXCLUSIVE_ROLE", direction = Relationship.Direction.OUTGOING)
    var exclusiveRole: Role? = null

    constructor(name: String) : this() {
        this.name = name
    }

    constructor(name: String, parentTeam: Team) : this(name) {
        this.parentTeam = parentTeam
    }
}

enum class TeamType {
    SELF, EXCLUSIVE, OPERATION
}