package org.ladhark.identity.entity.credential

import org.ladhark.identity.entity.BaseEntity
import org.springframework.data.neo4j.core.schema.CompositeProperty
import org.springframework.data.neo4j.core.schema.Node

enum class CredentialStoreType {

    PART_OF_URL_QUERY_PARAM, PART_OF_URL_PATH_VARIABLE, CUSTOM_HEADER, AUTHORIZATION_BEARER_HEADER;

    companion object {
        val STRING_SET = values().map { v -> v.toString() }.toSet()
    }
}

@Node
class CredentialStore : BaseEntity() {

    // @Index(unique = true)
    lateinit var name: String

    lateinit var type: CredentialStoreType

    @CompositeProperty(prefix = "")
    val properties: MutableMap<String, Any> = HashMap()

}