package org.ladhark.identity.entity.user

import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.entity.role.Role
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Relationship

/**
 * In a given region, User has been assigned Roles
 */
@Node
class UserRoleRegion : BaseEntity() {

    @Relationship(type = "HAS_USER", direction = Relationship.Direction.OUTGOING)
    lateinit var user: User

    @Relationship(type = "HAS_ROLE", direction = Relationship.Direction.OUTGOING)
    lateinit var role: Role

    @Relationship(type = "HAS_REGION", direction = Relationship.Direction.OUTGOING)
    lateinit var region: Region
}