package org.ladhark.identity.entity.resource.policy

import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.resource.ResourceAction
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Relationship

@Node
class ResourcePolicy : BaseEntity() {

    //@Index(unique = true)
    lateinit var name: String

    lateinit var description: String

    @Relationship(type = "FOR_RESOURCE_ACTION", direction = Relationship.Direction.OUTGOING)
    lateinit var resourceAction: ResourceAction
    @Relationship(type = "HAS_POLICY_SET", direction = Relationship.Direction.OUTGOING)
    lateinit var policySet: PolicySet
}

@Node
class PolicySet : BaseEntity() {

    // @Index(unique = true)
    lateinit var name: String

    var description: String? = null
    lateinit var policyEvaluationType: PolicyEvaluationType

    @Relationship(type = "AGGREGATE_POLICY_SETS", direction = Relationship.Direction.OUTGOING)
    var aggregatePolicySets: MutableSet<PolicySet> = HashSet()
    @Relationship(type = "HAS_POLICIES", direction = Relationship.Direction.OUTGOING)
    var hasPolicies: MutableSet<Policy> = HashSet()
}

enum class PolicyEvaluationType {

    UNANIMOUS, // All true
    AFFIRMATIVE, // At least one
    CONSENSUS; // No. of allow > deny

    companion object {
        val STRING_SET = values().map { v -> v.toString() }.toSet()
    }
}

@Node
class Policy : BaseEntity() {

    // @Index(unique = true)
    lateinit var name: String
    var description: String? = null

    lateinit var policyAccessType: PolicyAccessType

    // Multiple values are for some special cases where you cannot define using single condition
    @Relationship(type = "HAS_POLICY_CONDITIONS", direction = Relationship.Direction.OUTGOING)
    var hasPolicyConditions: MutableSet<PolicyCondition> = HashSet()
}

enum class PolicyAccessType {

    ALLOW, DENY;

    companion object {
        val STRING_SET = values().map { v -> v.toString() }.toSet()
    }
}

@Node
class PolicyCondition : BaseEntity() {

    lateinit var policyConditionType: PolicyConditionType
    var description: String? = null
    lateinit var key: String
    lateinit var value: String

}

enum class PolicyConditionType {

    PUBLIC_OPEN,
    USER_UUID,
    USER_EMAIL,
    TEAM_UUID,
    TEAM_NAME,
    ROLE_UUID,
    ROLE_NAME,
    REGION_UUID,
    REGION_NAME,
    AUTHORITY_UUID,
    AUTHORITY_NAME,
    SCRIPT,
    TIME,
    CLIENT,
    IP,
    HTTP_HEADERS;

    companion object {
        val STRING_SET = values().map { v -> v.toString() }.toSet()
    }
}