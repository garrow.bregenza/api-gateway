package org.ladhark.identity.entity.role

import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.authority.Authority
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Relationship

@Node
class Role() : BaseEntity() {

    // @Index(unique = true)
    lateinit var name: String

    var description: String? = null

    // Authorities supported by this role
    @Relationship(type = "HAS_AUTHORITIES", direction = Relationship.Direction.OUTGOING)
    var authorities = HashSet<Authority>()

    constructor(name: String) : this() {
        this.name = name
    }
}