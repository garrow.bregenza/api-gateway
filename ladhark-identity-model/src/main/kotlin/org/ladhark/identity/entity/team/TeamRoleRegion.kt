package org.ladhark.identity.entity.team

import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.entity.role.Role
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Relationship

/**
 * In a given region, Team has been assigned Roles
 */
@Node
class TeamRoleRegion : BaseEntity() {

    @Relationship(type = "HAS_TEAM", direction = Relationship.Direction.OUTGOING)
    var team: Team = Team()

    @Relationship(type = "HAS_ROLE", direction = Relationship.Direction.OUTGOING)
    var role: Role = Role()

    @Relationship(type = "HAS_REGION", direction = Relationship.Direction.OUTGOING)
    var region: Region = Region()
}