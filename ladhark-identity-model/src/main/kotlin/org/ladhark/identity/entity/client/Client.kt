package org.ladhark.identity.entity.client

import com.fasterxml.jackson.annotation.JsonIgnore
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.role.Role
import org.ladhark.identity.exception.AuthorityEvaluationFailedException
import org.springframework.data.neo4j.core.schema.Node
import org.springframework.data.neo4j.core.schema.Relationship

@Node
class Client : BaseEntity() {

    //@Index(unique = true)
    lateinit var name: String

    //@Index(unique = true)
    lateinit var email: String
    var active = false
    lateinit var description: String

    @Relationship(type = "HAS_ROLES", direction = Relationship.Direction.OUTGOING)
    var hasRoles: MutableSet<Role> = HashSet()

    fun isValid() {
        if (!active) {
            throw AuthorityEvaluationFailedException("User is inactive.")
        }
    }
}

@Node
class ClientCredentials : BaseEntity() {

    // this could be well another JWT token with all the details included to probe which client it is
    // JsonIgnore as entities are notoriously linked to lot of objects and can make out as JSON accidentally better be explicit on export
    @JsonIgnore
    lateinit var accessToken: String

    @JsonIgnore
    lateinit var refreshToken: String

    var active = false

    @Relationship(type = "BELONGS_TO_CLIENT", direction = Relationship.Direction.OUTGOING)
    lateinit var client: Client

    // cannot risk accidental printing of password
    override fun toString(): String {
        return "ClientCredentials@${this.hashCode()}"
    }
}