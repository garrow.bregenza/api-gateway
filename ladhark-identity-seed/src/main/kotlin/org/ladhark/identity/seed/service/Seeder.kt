package org.ladhark.identity.seed.service

import jakarta.validation.Valid
import kotlinx.coroutines.*
import kotlinx.coroutines.reactor.asCoroutineContext
import org.ladhark.identity.SeedCompletionHelper
import org.ladhark.identity.api.dto.application.ApplicationDetails
import org.ladhark.identity.api.dto.aspect.AspectDetails
import org.ladhark.identity.api.dto.aspect.AspectTypeDetails
import org.ladhark.identity.api.dto.client.ClientDetails
import org.ladhark.identity.api.dto.policy.PolicyConditionDetails
import org.ladhark.identity.api.dto.policy.PolicyDetails
import org.ladhark.identity.api.dto.policy.PolicySetDetails
import org.ladhark.identity.api.dto.policy.ResourcePolicyDetails
import org.ladhark.identity.api.dto.region.RegionDetails
import org.ladhark.identity.api.dto.resource.ResourceActionDetails
import org.ladhark.identity.api.dto.resource.ResourceDetails
import org.ladhark.identity.api.dto.role.RoleDetails
import org.ladhark.identity.api.dto.user.RegistrationDetails
import org.ladhark.identity.api.dto.user.UserRegionDetails
import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.entity.role.Role
import org.ladhark.identity.entity.user.Name
import org.ladhark.identity.entity.user.User
import org.ladhark.identity.exception.InvalidStateException
import org.ladhark.identity.repository.user.UserJWTRepository
import org.ladhark.identity.security.AuthenticationToken
import org.ladhark.identity.security.LadharkAuthenticatedPrincipal
import org.ladhark.identity.security.context.SERVICE_CONTEXT_KEY
import org.ladhark.identity.security.context.ServiceContext
import org.ladhark.identity.security.context.ServiceContextHolder
import org.ladhark.identity.seed.model.Model
import org.ladhark.identity.service.application.ApplicationService
import org.ladhark.identity.service.aspect.AspectService
import org.ladhark.identity.service.aspect.AspectTypeService
import org.ladhark.identity.service.client.ClientService
import org.ladhark.identity.service.region.RegionService
import org.ladhark.identity.service.resource.ResourceActionService
import org.ladhark.identity.service.resource.ResourceService
import org.ladhark.identity.service.resource.policy.PolicyConditionService
import org.ladhark.identity.service.resource.policy.PolicyService
import org.ladhark.identity.service.resource.policy.PolicySetService
import org.ladhark.identity.service.resource.policy.ResourcePolicyService
import org.ladhark.identity.service.role.RoleService
import org.ladhark.identity.service.user.RegisterUserService
import org.ladhark.identity.service.user.UserService
import org.ladhark.kotlin.extension.s
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.annotation.Validated
import reactor.util.context.Context
import java.util.*
import kotlin.time.Duration.Companion.milliseconds


@Service
@Validated
class Seeder {

    private val log: Logger = LoggerFactory.getLogger(Seeder::class.java)

    @Autowired
    private lateinit var applicationService: ApplicationService

    @Autowired
    private lateinit var userJWTRepository: UserJWTRepository

    @Autowired
    private lateinit var clientService: ClientService

    @Autowired
    private lateinit var regionService: RegionService

    @Autowired
    private lateinit var roleService: RoleService

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var registerUserService: RegisterUserService

    @Autowired
    private lateinit var resourceService: ResourceService

    @Autowired
    private lateinit var resourceActionService: ResourceActionService

    @Autowired
    private lateinit var resourcePolicyService: ResourcePolicyService

    @Autowired
    private lateinit var policySetService: PolicySetService

    @Autowired
    private lateinit var policyService: PolicyService

    @Autowired
    private lateinit var policyConditionService: PolicyConditionService

    @Autowired
    private lateinit var aspectTypeService: AspectTypeService

    @Autowired
    private lateinit var aspectService: AspectService


    private fun prepareContext(context: Context, user: User): Context {
        val p = org.ladhark.identity.policy.context.model.User.fromUserEntityModel(
            user,
            emptySet(),
            emptySet(),
            emptySet(),
            emptySet()
        )
        val lp = LadharkAuthenticatedPrincipal(p)
        val a = AuthenticationToken(lp, "seed_credentials")
        val sc = ServiceContext(true, a, Optional.empty(), Optional.empty(), Optional.empty())
        val sch = ServiceContextHolder(Optional.of(sc))
        return context.put(SERVICE_CONTEXT_KEY, sch)
    }

    suspend fun start(@Valid model: Model) {
        log.info("Seeding model")

        val seedUser = seedSystemUser(model)

        val p = org.ladhark.identity.policy.context.model.User.fromUserEntityModel(
            seedUser,
            emptySet(),
            emptySet(),
            emptySet(),
            emptySet()
        )
        val lp = LadharkAuthenticatedPrincipal(p)
        val a = AuthenticationToken(lp, "seed_credentials")
        val sc = ServiceContext(true, a, Optional.empty(), Optional.empty(), Optional.empty())
        val sch = ServiceContextHolder(Optional.of(sc))

        withContext(Context.of(SERVICE_CONTEXT_KEY, sch).asCoroutineContext()) {
            seedModelData(model)
        }

    }

    @Transactional(rollbackFor = [Exception::class])
    suspend fun seedSystemUser(model: Model): User {

        val systemUser = model.systemUser ?: throw InvalidStateException("System user not specified.")

        log.info("Saving system user with email {}", systemUser.email)

        return userJWTRepository.findUserByEmail(systemUser.email) ?: run {
            log.info("No system user with email {} found, saving fresh", systemUser.email)
            val user = User()
            user.email = systemUser.email
            user.name = Name(
                systemUser.name.first,
                systemUser.name.middle,
                systemUser.name.last
            )
            user.active = false
            user.locked = true
            user.system = true
            val sefTeamUUID = BaseEntity.generateUuid()
            val exclusiveTeamUUID = BaseEntity.generateUuid()
            user.initOnCreateTeams(sefTeamUUID, exclusiveTeamUUID)
            val userSaved = userJWTRepository.save(user)
            log.info("Saved system user by email {}", userSaved.email)
            userSaved
        }

    }

    suspend fun seedModelData(model: Model) {
        coroutineScope {

            awaitAll(
                async {
                    seedApplication(model)
                },
                async {
                    seedClient(model)
                },
                async {
                    seedRegion(model)
                },
                async {
                    seedRole(model)
                }
            )

            seedUser(model)

            seedResource(model)

            seedResourcePolicy(model)

            seedResourcePolicySet(model)

            seedAspectTypes(model)

            seedAspects(model)

            SeedCompletionHelper.markEnd()
        }
    }

    @Transactional(rollbackFor = [Exception::class])
    suspend fun seedApplication(model: Model) {
        model.apps.forEach { app ->
            applicationService.findByName(app.name) ?: run {
                val applicationDetails =
                    ApplicationDetails(app.name, app.external, app.description, app.pattern, app.locations.map { it.location }.toSet())

                applicationService.createApplication(applicationDetails)
                    .also {
                        log.info("Saved application by name {} with uuid {}", it.name, it.uuid)
                    }
            }
        }
    }

    @Transactional(rollbackFor = [Exception::class])
    suspend fun seedRole(model: Model) {
        model.roles.forEach { role ->
            roleService
                .findByName(role.name) ?: run {
                val roleDetails = RoleDetails(role.name, role.description)
                roleService
                    .createRole(roleDetails)
                    .also {
                        log.info("Saved role by name {} with uuid {}", it.name, it.uuid)
                    }
            }
        }

    }

    @Transactional(rollbackFor = [Exception::class])
    suspend fun seedRegion(model: Model) {
        model.regions.forEach { region ->
            regionService
                .findByName(region.name) ?: run {
                val regionDetails = RegionDetails(region.name, description = region.description)
                regionService
                    .createRegion(regionDetails)
                    .also {
                        log.info("Saved region by name {} with uuid {}", it.name, it.uuid)
                    }
            }
        }
    }

    @Transactional(rollbackFor = [Exception::class])
    suspend fun seedClient(model: Model) {
        model.clients.forEach { client ->
            clientService
                .findByName(client.name) ?: run {
                val clientDetails = ClientDetails(client.name, client.email, client.description)
                clientService
                    .createClient(clientDetails)
                    .also {
                        log.info("Saved client by name {} with uuid {}", it.name, it.uuid)
                    }
            }
        }
    }

    @Transactional(rollbackFor = [Exception::class])
    suspend fun seedUser(model: Model) {
        model.users.forEach { user ->
            userService
                .findByEmail(user.email)
                ?: run {

                    coroutineScope {
                        val deferredRegions = user.regions.map { region ->
                            async {
                                regionService.findByName(region.name)
                            }
                        }

                        val regionList = deferredRegions.awaitAll().map {
                            UserRegionDetails(regionUuid = it!!.uuid.s)
                        }

                        log.info("Creating user with email {}", user.email)

                        val nameForDetails = org.ladhark.identity.api.dto.user.Name(
                            user.name.first,
                            user.name.middle, user.name.last
                        )
                        val registrationDetails = RegistrationDetails(
                            user.email, user.password,
                            nameForDetails, regionList.toTypedArray()
                        )

                        val userCreated = registerUserService.createUser(registrationDetails)

                        delay(300.milliseconds)

                        user.roles.forEach { userRole ->
                            val roleName = userRole.name
                            val regionName = userRole.region

                            val records = awaitAll(
                                async {
                                    roleService.findByName(roleName)
                                },
                                async {
                                    regionService.findByName(regionName)
                                }
                            )

                            val role = records[0] as Role
                            val region = records[1] as Region

                            log.info(
                                "Adding role {} in region {} to user with email {}",
                                roleName,
                                regionName,
                                userCreated.email
                            )
                            userService.addRoleForRegion(userCreated.uuid.s, role.uuid.s, region.uuid.s)
                        }
                    }
            }
        }
    }

    @Transactional(rollbackFor = [Exception::class])
    suspend fun seedResource(model: Model) {
        model.resources.forEach { resource ->

            resourceService
                .findByName(resource.name) ?: run {
                applicationService
                    .findByName(resource.applicationName)
                    ?.also { application ->

                        val resourceDetails = ResourceDetails(resource.name, resource.description, application.uuid.s)
                        val resourceSaved = resourceService.createResource(resourceDetails)

                        log.info("Saved Resource by name {} with uuid {}", resourceSaved.name, resourceSaved.uuid)

                        coroutineScope {
                            val resourceActionDeferred = resource.actions.map { resourceAction ->
                                async {
                                    resourceActionService
                                        .findByName(resourceAction.name)
                                        ?: run {
                                            val resourceActionDetails = ResourceActionDetails(
                                                resourceAction.name,
                                                resourceAction.scope, resourceAction.description,
                                                resourceAction.uri, resourceSaved.uuid.s
                                            )

                                            val resourceActionSaved = resourceActionService
                                                .createResourceAction(resourceActionDetails)

                                            log.info(
                                                "Saved Resource Action by name {} with uuid {}",
                                                resourceActionSaved.name, resourceActionSaved.uuid
                                            )
                                        }
                                }
                            }

                            resourceActionDeferred.awaitAll()
                        }

                    }
                    ?: throw IllegalStateException("Application not found ${resource.applicationName}")

            }
        }
    }

    @Transactional(rollbackFor = [Exception::class])
    suspend fun seedResourcePolicy(model: Model) {
        model.resourcePolicies.forEach { resourcePolicy ->
            resourcePolicyService
                .findByName(resourcePolicy.name)
                ?: run {

                    resourceActionService
                        .findByName(resourcePolicy.resourceActionName)
                        ?.let { resourceAction ->
                            val resourcePolicyDetails = ResourcePolicyDetails(
                                resourcePolicy.name,
                                resourcePolicy.description,
                                resourceAction.uuid.s
                            )
                            resourcePolicyService.create(resourcePolicyDetails)
                                .also { resourcePolicySaved ->
                                    log.info(
                                        "Saved Resource Policy by name {} with uuid {}",
                                        resourcePolicySaved.name, resourcePolicySaved.uuid
                                    )
                                }

                        } ?: throw IllegalStateException("Resource action not found ${resourcePolicy.resourceActionName}")
            }
        }
    }

    @Transactional(rollbackFor = [Exception::class])
    suspend fun seedResourcePolicySet(model: Model) {
        model.policySet.forEach { policySet ->
            policySetService
                .findByName(policySet.name)
                ?: run {

                    resourcePolicyService.findByName(policySet.resourcePolicyName)?.let { resourcePolicy ->
                        val policySetDetails = PolicySetDetails(
                            policySet.name,
                            policySet.description,
                            policySet.policyEvaluationType,
                            resourcePolicy.uuid.s
                        )
                        val policySetSaved = policySetService.create(policySetDetails)
                        log.info("Saved Policy Set by name {} with uuid {}", policySetSaved.name, policySetSaved.uuid)

                        coroutineScope {
                            val policySetDeferred = policySet.policies.map { policy ->
                                async {
                                    policyService
                                        .findByName(policy.name)
                                        ?: run {

                                            val policyDetails = PolicyDetails(
                                                policy.name, policy.description,
                                                policy.policyAccessType, policySetSaved.uuid.s
                                            )

                                            val policySaved = policyService.create(policyDetails)

                                            log.info(
                                                "Saved Policy by name {} with uuid {}",
                                                policySaved.name, policySaved.uuid
                                            )

                                            val policyConditions = policy.conditions

                                            policyConditions.forEach { pc ->
                                                val policyConditionDetails = PolicyConditionDetails(
                                                    pc.policyConditionType,
                                                    pc.description,
                                                    pc.key,
                                                    pc.value,
                                                    policySaved.uuid.s
                                                )
                                                log.info(
                                                    "Creating policy conditions by key {} for policy {}",
                                                    pc.key,
                                                    policy.name
                                                )
                                                policyConditionService.create(policyConditionDetails)
                                            }
                                        }
                                }
                            }

                            policySetDeferred.awaitAll()
                        }
                    } ?: throw IllegalStateException("Resource policy not found ${policySet.resourcePolicyName}")
                }
        }
    }

    @Transactional(rollbackFor = [Exception::class])
    suspend fun seedAspectTypes(model: Model) {
        model.aspectTypes.forEach { aspectType ->
            aspectTypeService.findByName(aspectType.name) ?: run {
                val aspectTypeDetails = AspectTypeDetails(aspectType.name, aspectType.description)
                aspectTypeService.createAspectType(aspectTypeDetails).also { aspectTypeFromDb ->
                    log.info("Creating aspect type by name {}", aspectTypeFromDb.name)
                }
            }
        }
    }

    @Transactional(rollbackFor = [Exception::class])
    suspend fun seedAspects(model: Model) {
        model.filteraspects.forEach { aspect ->
            aspectTypeService.findByName(aspect.typeName)?.let { aspectType ->

                var regionUuid: String? = null

                aspect.regionName?.let { regionName ->
                    regionService.findByName(regionName)?.let { region ->
                        regionUuid = region.uuid.s
                    }
                }

                resourceActionService.findByName(aspect.resourceActionName)?.let { resourceAction ->
                    val aspectDetails = AspectDetails(aspect.name,
                        aspect.description,
                        aspectType.uuid.s,
                        regionUuid,
                        resourceAction.uuid.s,
                        aspect.enforceRegion,
                        aspect.order,
                        aspect.properties)

                    aspectService.createAspect(aspectDetails).also { aspectFromDb ->
                        log.info("Creating aspect by name {}", aspectFromDb.name)
                    }
                } ?: throw IllegalStateException("Resource action not found ${aspect.resourceActionName}")



            } ?: throw IllegalStateException("Aspect type not found ${aspect.typeName}")
        }
    }
}