package org.ladhark.identity.seed.service

import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.ladhark.identity.exception.InvalidArgumentException
import org.ladhark.identity.seed.model.Model
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.nio.file.Files
import java.nio.file.Path

@Service
class ModelReader {

    private val log: Logger = LoggerFactory.getLogger(ModelReader::class.java)

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var seeder: Seeder

    suspend fun start(args: Array<String>) {
        val path = checkIsFileAndExists(args)
        val model = objectMapper.readValue(withContext(Dispatchers.IO) {
            Files.newInputStream(path)
        }, Model::class.java)
        seeder.start(model)
    }

    private fun checkIsFileAndExists(args: Array<String>): Path {
        if (args.isEmpty()) {
            throw InvalidArgumentException("No seed file specified.")
        } else if (args.size > 1) {
            throw InvalidArgumentException("Expecting only one argument for file location.")
        }

        val fileLocation = args[0];

        val path = Path.of(fileLocation)

        if (!Files.exists(path) || !Files.isRegularFile(path)) {
            throw InvalidArgumentException("File does not exists or is not a file.")
        }

        log.info("Seed file {} found valid.", path.toAbsolutePath())

        return path;
    }
}