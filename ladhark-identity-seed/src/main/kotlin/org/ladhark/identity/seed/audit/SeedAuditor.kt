package org.ladhark.identity.seed.audit

import org.ladhark.identity.repository.extension.suspendedResultToMono
import org.ladhark.identity.repository.user.UserRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.ReactiveAuditorAware
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Mono
import java.util.*

@Component
class SeedAuditor : ReactiveAuditorAware<UUID> {

    private val log: Logger = LoggerFactory.getLogger(SeedAuditor::class.java)

    @Autowired
    private lateinit var userRepository: UserRepository

    @Transactional(readOnly = true)
    override fun getCurrentAuditor(): Mono<UUID> {
        return suspendedResultToMono {
                userRepository
                    .findSystemUser()
            }
            .map { userFromDb ->
                userFromDb?.uuid ?: throw IllegalStateException("No audit system user found.")
            }
            .doOnNext {
                log.debug("Auditing with user uuid {}", it)
            }
    }
}