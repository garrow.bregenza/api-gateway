package org.ladhark.identity.seed.model

import jakarta.validation.Valid
import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotEmpty
import org.springframework.validation.annotation.Validated

@Validated
class Model {

    @field:Valid
    var systemUser: SystemUser? = null

    @field:Valid
    var clients = emptyList<Client>()

    @field:Valid
    var apps = emptyList<App>()

    @field:Valid
    var aspectTypes = emptyList<AspectType>()

    @field:Valid
    var filteraspects = emptyList<FilterAspect>()

    @field:Valid
    var regions = emptyList<Region>()

    @field:Valid
    var roles = emptyList<Role>()

    @field:Valid
    var users = emptyList<User>()

    @field:Valid
    var resources = emptyList<Resource>()

    @field:Valid
    var policySet = emptyList<PolicySet>()

    @field:Valid
    var resourcePolicies = emptyList<ResourcePolicy>()
}

class App(
    @field:NotEmpty var name: String = "",
    @field:NotEmpty var description: String = "",
    @field:NotEmpty var pattern: String = "",
    @field:Valid var locations: List<HostingLocation> = emptyList(),
    @field:Valid var cstore: CredentialStore? = null,
    var external: Boolean = true
)

class Client(
    @field:NotEmpty var name: String = "",
    @field:NotEmpty var email: String = "",
    @field:NotEmpty var description: String = ""
)

class HostingLocation(@field:NotEmpty var location: String = "")

class CredentialStore(
    @field:NotEmpty var name: String = "", @field:NotEmpty var type: String = "",
    var properties: Map<String, String> = emptyMap()
)

class AspectType(
    @field:NotEmpty var name: String = "",
    var description: String = ""
)

class FilterAspect(
    @field:NotEmpty var name: String = "",
    @field:NotEmpty var typeName: String = "",
    var description: String = "",
    var regionName: String? = null,
    @field:NotEmpty var resourceActionName: String = "",
    var enforceRegion: Boolean = false,
    var order: Int = 0,
    var properties: Map<String, String> = emptyMap()
)

class SystemUser(
    @field:Email @field:NotEmpty var email: String = "",
    var locked: Boolean = false,
    var active: Boolean = true,
    @field:Valid var name: Name = Name()
)

class User(
    @field:Email @field:NotEmpty var email: String = "",
    var locked: Boolean = false,
    var active: Boolean = true,
    @field:NotEmpty var password: String = "",
    @field:Valid var name: Name = Name(),
    @field:Valid var regions: List<Region> = emptyList(),
    @field:Valid var roles: List<UserRole> = emptyList()
)

class UserRole(@field:NotEmpty var name: String = "", var region: String = "")

class Name(
    @field:NotEmpty var first: String = "",
    var middle: String = "",
    @field:NotEmpty var last: String = ""
)

class Region(@field:NotEmpty var name: String = "", var description: String = "")

class Role(@field:NotEmpty var name: String = "", @field:NotEmpty var description: String = "")

class Resource(
    @field:NotEmpty var name: String = "",
    var description: String = "", @field:NotEmpty var applicationName: String = "",
    @field:Valid var actions: List<ResourceAction> = emptyList()
)

class ResourceAction(
    @field:NotEmpty var name: String = "",
    var description: String = "",
    @field:NotEmpty var scope: String = "",
    @field:NotEmpty var uri: String = ""
)

class PolicyCondition(
    @field:NotEmpty var policyConditionType: String = "",
    var description: String = "",
    @field:NotEmpty var key: String = "",
    @field:NotEmpty var value: String = ""
)

class Policy(
    @field:NotEmpty var name: String = "",
    var description: String = "",
    @field:NotEmpty var policyAccessType: String = "",
    @field:Valid var conditions: List<PolicyCondition> = emptyList()
)

class PolicySet(
    @field:NotEmpty var name: String = "",
    var description: String = "",
    @field:NotEmpty var policyEvaluationType: String = "",
    @field:NotEmpty var resourcePolicyName: String = "",
    @field:Valid var policies: List<Policy> = emptyList()
)

class ResourcePolicy(
    @field:NotEmpty var name: String = "",
    var description: String = "",
    @field:NotEmpty var resourceActionName: String = ""
)