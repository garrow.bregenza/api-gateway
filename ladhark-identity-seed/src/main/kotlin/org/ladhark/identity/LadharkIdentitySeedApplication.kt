package org.ladhark.identity

import kotlinx.coroutines.runBlocking
import org.ladhark.identity.seed.service.ModelReader
import org.neo4j.driver.Driver
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.data.neo4j.config.EnableReactiveNeo4jAuditing
import org.springframework.data.neo4j.core.ReactiveDatabaseSelectionProvider
import org.springframework.data.neo4j.core.transaction.ReactiveNeo4jTransactionManager
import org.springframework.data.neo4j.repository.config.EnableReactiveNeo4jRepositories
import org.springframework.data.neo4j.repository.config.ReactiveNeo4jRepositoryConfigurationExtension
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.transaction.ReactiveTransactionManager
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor
import java.util.concurrent.CountDownLatch


@SpringBootApplication
@EnableReactiveNeo4jRepositories(value = ["org.ladhark.identity.repository"])
@EnableReactiveNeo4jAuditing
class LadharkIdentitySeedApplication {

    @Bean(ReactiveNeo4jRepositoryConfigurationExtension.DEFAULT_TRANSACTION_MANAGER_BEAN_NAME)
    fun transactionManager(
        driver: Driver,
        databaseNameProvider: ReactiveDatabaseSelectionProvider
    ): ReactiveTransactionManager {
        return ReactiveNeo4jTransactionManager(driver, databaseNameProvider)
    }

    @Bean
    fun validationPostProcessor(): MethodValidationPostProcessor {
        return MethodValidationPostProcessor()
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }


}

fun main(args: Array<String>) {
    //ReactorDebugAgent.init()
    //BlockHound.install()

    val ctx = runApplication<LadharkIdentitySeedApplication>(*args)

    runBlocking {
        val modelReader = ctx.getBean(ModelReader::class.java)
        modelReader.start(args)
    }

    SeedCompletionHelper.await()
}

object SeedCompletionHelper {

    private val latch = CountDownLatch(1)

    fun await() {
        latch.await()
    }

    fun markEnd() {
        latch.countDown()
    }
}