package org.ladhark.identity.api.dto.client

import jakarta.validation.constraints.NotEmpty

data class ClientDetails(
    @field:NotEmpty val name: String,
    @field:NotEmpty val email: String,
    @field:NotEmpty val description: String
)

// no validation as this will go out of system only
data class ClientCredentialDetailsResponse(val uuid: String, val accessToken: String, val refreshToken: String) {

    override fun toString(): String {
        return "ClientCredentialDetailsResponse@${hashCode()}"
    }

}