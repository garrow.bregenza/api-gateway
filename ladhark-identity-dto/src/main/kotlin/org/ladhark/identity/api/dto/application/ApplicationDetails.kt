package org.ladhark.identity.api.dto.application

import jakarta.validation.constraints.NotEmpty
import org.ladhark.identity.api.dto.store.credential.CredentialStoreDetails

data class ApplicationDetails(
    @field:NotEmpty val name: String, val external: Boolean = true, val description:String = "",
    val servicePattern: String = "",
    val hostingLocations: Set<String> = emptySet(),
    var credentialStore: CredentialStoreDetails? = null,
    val balancerConfig: BalancerConfig = BalancerConfig()
)

data class BalancerConfig(val type: BalancerType = BalancerType.ROUND_ROBIN, val poolSize: Int = 10,
        val properties: MutableMap<String, Any> = HashMap())

// Note: same enum is in entity package
enum class BalancerType {

    RANDOM, FAIL_OVER, ROUND_ROBIN;
    companion object {
        val STRING_SET = BalancerType.values().map { v -> v.toString() }.toSet()
    }
}