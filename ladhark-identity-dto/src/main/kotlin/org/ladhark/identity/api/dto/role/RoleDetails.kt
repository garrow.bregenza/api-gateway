package org.ladhark.identity.api.dto.role

import jakarta.validation.constraints.NotEmpty

data class RoleDetails(@field:NotEmpty val name: String, val description: String? = null)