package org.ladhark.identity.api.dto.policy

import jakarta.validation.constraints.NotEmpty

data class ResourcePolicyDetails(
    @field:NotEmpty val name: String,
    val description: String,
    @field:NotEmpty val resourceActionUuid: String
)

data class PolicySetDetails(
    @field:NotEmpty val name: String,
    val description: String,
    @field:NotEmpty val policyEvaluationType: String,
    @field:NotEmpty val resourcePolicyUuid: String
)

data class PolicyDetails(
    @field:NotEmpty val name: String,
    val description: String,
    @field:NotEmpty val policyAccessType: String,
    @field:NotEmpty val policySetUuid: String
)

data class PolicyConditionDetails(
    @field:NotEmpty val policyConditionType: String,
    val description: String,
    @field:NotEmpty val key: String,
    @field:NotEmpty val value: String,
    @field:NotEmpty val policyUuid: String
)