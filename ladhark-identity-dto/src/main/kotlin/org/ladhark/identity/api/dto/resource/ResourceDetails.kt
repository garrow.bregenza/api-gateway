package org.ladhark.identity.api.dto.resource

import jakarta.validation.constraints.NotEmpty

data class ResourceDetails(
    @field:NotEmpty val name: String,
    val description: String,
    @field:NotEmpty val applicationUuid: String
)

data class ResourceActionDetails(
    @field:NotEmpty val name: String,
    @field:NotEmpty val scope: String,
    val description: String,
    @field:NotEmpty val uriPattern: String,
    @field:NotEmpty val resourceUuid: String
)