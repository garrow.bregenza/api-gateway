package org.ladhark.identity.api.dto.aspect

import jakarta.validation.constraints.NotEmpty

data class AspectDetails(
    @field:NotEmpty val name: String,
    val description: String? = null,
    @field:NotEmpty val typeUuid: String,
    val regionUuid: String? = null,
    @field:NotEmpty val resourceActionUuid: String,
    val enforceRegion: Boolean = false,
    val order: Int = 0,
    val properties: Map<String, Any> = HashMap()
)

data class AspectTypeDetails(
    @field:NotEmpty val name: String,
    val description: String? = null
)