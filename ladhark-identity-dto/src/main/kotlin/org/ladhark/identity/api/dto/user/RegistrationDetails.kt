package org.ladhark.identity.api.dto.user

import jakarta.validation.Valid
import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotEmpty

data class RegistrationDetails(
    @field:Email val email: String, @field:NotEmpty val password: String, @field:Valid val name: Name,
    @field:Valid val regionUuids: Array<UserRegionDetails> = emptyArray()
) {
    fun valid(): Boolean {
        return !inValid()
    }

    // TODO: add check for system user
    fun inValid(): Boolean {
        return email.isBlank()
                // || email == SystemUser.sysUser.email
                || !email.contains('@')
                || password.isBlank()
                || name.inValid()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RegistrationDetails

        if (email != other.email) return false
        if (password != other.password) return false
        if (name != other.name) return false
        if (!regionUuids.contentDeepEquals(other.regionUuids)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = email.hashCode()
        result = 31 * result + password.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + regionUuids.contentDeepHashCode()
        return result
    }

    override fun toString(): String {
        return "RegistrationDetails(email='$email', password='******************', name=$name, regionUuids=${regionUuids.contentToString()})"
    }


}

data class Name(
    @field:NotEmpty val first: String = "",
    val middle: String = "",
    @field:NotEmpty val last: String = ""
) {
    fun valid(): Boolean {
        return !inValid()
    }

    // TODO add check for system user
    fun inValid(): Boolean {
        return first.isBlank() // || first == SystemUser.sysUser.name.first || StringUtils.isBlank(last) || last == SystemUser.sysUser.name.last
    }
}

data class UserRegionDetails(
    val primary: Boolean = false,
    val childrenAccess: Boolean = false,
    @field:NotEmpty val regionUuid: String
)