package org.ladhark.identity.api.dto.store.credential

import jakarta.validation.constraints.NotEmpty

data class CredentialStoreDetails(
    @field:NotEmpty val name: String,
    @field:NotEmpty val type: String,
    val properties: Map<String, Any> = HashMap()
)