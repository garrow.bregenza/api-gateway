package org.ladhark.identity.api.dto.region

import jakarta.validation.constraints.NotEmpty

data class RegionDetails(
    @field:NotEmpty val name: String,
    val global: Boolean = false,
    val public: Boolean = true,
    val parentRegionUuid: String? = null,
    val description: String? = null
)