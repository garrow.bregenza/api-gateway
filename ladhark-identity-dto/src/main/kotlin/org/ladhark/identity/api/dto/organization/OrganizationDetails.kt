package org.ladhark.identity.api.dto.organization

import jakarta.validation.constraints.NotEmpty

data class OrganizationDetails(@field:NotEmpty val name: String)