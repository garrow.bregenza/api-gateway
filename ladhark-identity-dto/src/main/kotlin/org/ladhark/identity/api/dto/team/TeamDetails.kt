package org.ladhark.identity.api.dto.team

import jakarta.validation.constraints.NotEmpty

data class TeamDetails(
    @field:NotEmpty val name: String,
    val parentTeamUuid: String = "",
    val regionUuids: Array<String> = emptyArray()
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TeamDetails

        if (name != other.name) return false
        if (parentTeamUuid != other.parentTeamUuid) return false
        if (!regionUuids.contentEquals(other.regionUuids)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + parentTeamUuid.hashCode()
        result = 31 * result + regionUuids.contentHashCode()
        return result
    }
}