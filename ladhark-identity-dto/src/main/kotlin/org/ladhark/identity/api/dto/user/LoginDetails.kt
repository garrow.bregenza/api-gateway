package org.ladhark.identity.api.dto.user

import jakarta.validation.constraints.NotEmpty

data class LoginDetails(
    @field:NotEmpty val email: String,
    @field:NotEmpty val password: String,
    val regionUuid: String = "",
    val applications: Set<String> = emptySet()
) {
    fun valid(): Boolean {
        return !inValid()
    }

    fun inValid(): Boolean {
        return email.isBlank() or password.isBlank()
    }

    override fun toString(): String {
        return "LoginDetails(email='$email', regionUuid='$regionUuid', applications=$applications)"
    }


}

data class LoginResponse(val access: String, val refresh: String)