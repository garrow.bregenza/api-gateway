package org.ladhark.kotlin.extension

import java.util.*

val String.uuid: UUID
    get() = UUID.fromString(this)

val UUID.s: String
    get() = this.toString()