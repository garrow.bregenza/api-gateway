package org.ladhark.kotlin.extension

import reactor.core.publisher.Mono
import java.util.*

val <T : Any> Mono<T>.o: Mono<Optional<T>>
    get() = this.map { if (it == null) Optional.empty() else Optional.of(it) }.defaultIfEmpty(Optional.empty())

val <T : Any> Mono<T>.toOptional: Mono<Optional<T>>
    get() = this.o