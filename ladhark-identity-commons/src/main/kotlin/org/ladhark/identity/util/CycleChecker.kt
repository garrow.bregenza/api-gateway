package org.ladhark.identity.util

import org.ladhark.identity.exception.InvalidStateException
import java.util.*

interface CycleChecker {
    fun checkAgainst(): String
    fun children(): Iterator<CycleChecker>
}

object CycleCheckerUtil {

    private val cycleAndDepthValidator = CycleAndDepthValidator(4)
    private val cycleValidator = CycleValidator()

    fun checkCycleAndDepth(cycleChecker: CycleChecker) {
        val stack = Stack<String>()
        check(cycleChecker, stack, cycleAndDepthValidator)
    }

    fun checkCycleAndDepth(cycleChecker: CycleChecker, depth: Int) {
        val stack = Stack<String>()
        check(cycleChecker, stack, CycleAndDepthValidator(depth))
    }

    fun checkCycle(cycleChecker: CycleChecker) {
        val stack = Stack<String>()
        check(cycleChecker, stack, cycleValidator)
    }

    private fun check(cycleChecker: CycleChecker, stack: Stack<String>, validator: Validator) {

        val target = cycleChecker.checkAgainst()

        validator.check(target, stack)

        stack.push(target)

        cycleChecker.children().forEach {
            check(it, stack, validator)
            stack.pop()
        }
    }
}

internal interface Validator {
    fun check(target: String, stack: Stack<String>)
}

internal open class CycleValidator : Validator {

    override fun check(target: String, stack: Stack<String>) {
        if (stack.search(target) != -1) {
            throw InvalidStateException("Path cycle detected with $target")
        }
    }

}

internal class CycleAndDepthValidator(private val depth: Int) : Validator, CycleValidator() {

    override fun check(target: String, stack: Stack<String>) {

        if (stack.size > depth) {
            throw InvalidStateException("Depth of more than $depth not supported.")
        }

        super.check(target, stack)
    }

}