package org.ladhark.identity.exception

interface ApplicationException {
    fun getCode(): String
    fun getMsg(): String
    fun getWrappedException(): Exception?
}

abstract class BaseException(private val code: String, private val msg: String, private val exception: Exception?) :
    RuntimeException("$code: $msg", exception), ApplicationException {
    override fun getCode(): String {
        return code
    }

    override fun getMsg(): String {
        return msg
    }

    override fun getWrappedException(): Exception? {
        return exception
    }
}

class EntityNotFoundException(msg: String, exception: Exception? = null) : BaseException("LA-404", msg, exception)

class EntityAlreadyPresentException(msg: String, exception: Exception? = null) : BaseException("LA-412", msg, exception)

class AuthorityEvaluationFailedException(msg: String, exception: Exception? = null) :
    BaseException("LA-403", msg, exception)

class AuthorizationFailedException(msg: String, exception: Exception? = null) : BaseException("LA-403", msg, exception)

class InvalidArgumentException(msg: String, exception: Exception? = null) : BaseException("LA-400", msg, exception)

class InvalidStateException(msg: String, exception: Exception? = null) : BaseException("LA-500", msg, exception)

class OperationLockAcquireFailed(msg: String, exception: Exception? = null) : BaseException("LA-500", msg, exception)

class InvalidCredentialException(private val msg: String, private val exception: Exception? = null) :
    ApplicationException {

    override fun getCode(): String {
        return code
    }

    override fun getMsg(): String {
        return msg
    }

    override fun getWrappedException(): Exception? {
        return exception
    }

    companion object {
        const val code = "SA-403"
    }
}