package org.ladhark.identity.repository.client

import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.ladhark.identity.entity.client.Client
import org.ladhark.identity.entity.client.ClientCredentials
import org.ladhark.identity.repository.base.AppCoroutineSortingRepository
import org.ladhark.identity.repository.model.role.RoleAuthorityResult
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository
import org.springframework.data.neo4j.repository.query.Query
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

@Repository
internal interface InternalClientRepository : ReactiveNeo4jRepository<Client, Long> {
    fun findClientByUuid(uuid: UUID): Mono<Client>
    fun findClientByName(name: String): Mono<Client>
    fun findClientByEmail(email: String): Mono<Client>
}

@Repository
class ClientRepository : AppCoroutineSortingRepository<Client, Long>() {

    @Autowired
    private lateinit var _repository: InternalClientRepository
    override fun repository() = _repository

    suspend fun findClientByUuid(uuid: UUID) = repository().findClientByUuid(uuid).awaitSingleOrNull()
    suspend fun findClientByName(name: String) = repository().findClientByName(name).awaitSingleOrNull()
    suspend fun findClientByEmail(email: String) = repository().findClientByEmail(email).awaitSingleOrNull()
}

@Repository
interface InternalClientJWTRepository : ReactiveNeo4jRepository<Client, Long> {

    fun findClientByUuid(uuid: UUID): Mono<Client>

    @Query(
        " MATCH (client: Client {uuid: $0}) -[:HAS_ROLES]-> (role: Role)" +
                " OPTIONAL MATCH (role) -[:HAS_AUTHORITIES*1..]-> (authority: Authority)" +
                " OPTIONAL MATCH (authority) -[:IMPLIES_AUTHORITIES*1..]-> (implies: Authority)" +
                " RETURN role as role, authority as authority, COLLECT(implies) as implies"
    )
    fun findRoleAuthoritiesForClient(uuid: UUID): Flux<RoleAuthorityResult>
}

@Repository
class ClientJWTRepository : AppCoroutineSortingRepository<Client, Long>() {

    @Autowired
    private lateinit var _repository: InternalClientJWTRepository
    override fun repository() = _repository

    suspend fun findClientByUuid(uuid: UUID) = repository().findClientByUuid(uuid).awaitSingleOrNull()

    fun findRoleAuthoritiesForClient(uuid: UUID) = repository().findRoleAuthoritiesForClient(uuid).asFlow()
}

@Repository
interface InternalClientCredentialsRepository : ReactiveNeo4jRepository<ClientCredentials, Long> {
    @Query(
        " MATCH (clientCredentials: ClientCredentials { active: $0 }) " +
                " RETURN clientCredentials" +
                " ORDER BY clientCredentials.createdAt DESC" +
                " LIMIT 1"
    )
    fun findLatestActiveCredentials(active: Boolean): Mono<ClientCredentials>
}

@Repository
class ClientCredentialsRepository : AppCoroutineSortingRepository<ClientCredentials, Long>() {

    @Autowired
    private lateinit var _repository: InternalClientCredentialsRepository
    override fun repository() = _repository
    suspend fun findLatestActiveCredentials(active: Boolean) =
        repository().findLatestActiveCredentials(active).awaitSingleOrNull()
}