package org.ladhark.identity.repository.resource.policy

import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.ladhark.identity.entity.resource.policy.Policy
import org.ladhark.identity.entity.resource.policy.PolicyCondition
import org.ladhark.identity.entity.resource.policy.PolicySet
import org.ladhark.identity.entity.resource.policy.ResourcePolicy
import org.ladhark.identity.repository.base.AppCoroutineSortingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository
import org.springframework.data.neo4j.repository.query.Query
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import java.util.*

@Repository
internal interface InternalResourcePolicyRepository : ReactiveNeo4jRepository<ResourcePolicy, Long> {

    //@Depth(value = 3)
    //override fun findAll(): MutableIterable<ResourcePolicy>

    //@Depth(value = 3)
    fun findByResourceActionUuid(uuid: UUID): Mono<ResourcePolicy>

    //@Depth(value = 3)
    fun findResourcePolicyByUuid(uuid: UUID): Mono<ResourcePolicy>

    //@Depth(value = 3)
    fun findResourcePolicyByName(name: String): Mono<ResourcePolicy>

    fun findResourcePolicyByPolicySetUuid(uuid: UUID): Mono<ResourcePolicy>

}

@Repository
class ResourcePolicyRepository : AppCoroutineSortingRepository<ResourcePolicy, Long>() {

    @Autowired
    private lateinit var _repository: InternalResourcePolicyRepository
    override fun repository() = _repository

    suspend fun findByResourceActionUuid(uuid: UUID) = repository().findByResourceActionUuid(uuid).awaitSingleOrNull()

    suspend fun findResourcePolicyByUuid(uuid: UUID) = repository().findResourcePolicyByUuid(uuid).awaitSingleOrNull()

    suspend fun findResourcePolicyByName(name: String) = repository().findResourcePolicyByName(name).awaitSingleOrNull()

    suspend fun findResourcePolicyByPolicySetUuid(uuid: UUID) =
        repository().findResourcePolicyByPolicySetUuid(uuid).awaitSingleOrNull()
}

@Repository
internal interface InternalPolicySetRepository : ReactiveNeo4jRepository<PolicySet, Long> {

    //@Depth(value = 2)
    // override fun findAll(): MutableIterable<PolicySet>

    //@Depth(value = 2)
    fun findPolicySetByUuid(uuid: UUID): Mono<PolicySet>

    //@Depth(value = 2)
    fun findPolicySetByName(name: String): Mono<PolicySet>
}

@Repository
class PolicySetRepository : AppCoroutineSortingRepository<PolicySet, Long>() {

    @Autowired
    private lateinit var _repository: InternalPolicySetRepository
    override fun repository() = _repository

    suspend fun findPolicySetByUuid(uuid: UUID) = repository().findPolicySetByUuid(uuid).awaitSingleOrNull()

    suspend fun findPolicySetByName(name: String) = repository().findPolicySetByName(name).awaitSingleOrNull()
}

@Repository
internal interface InternalPolicyRepository : ReactiveNeo4jRepository<Policy, Long> {

    fun findPolicyByUuid(uuid: UUID): Mono<Policy>
    fun findPolicyByName(name: String): Mono<Policy>

    @Query(
        "MATCH (policySet: PolicySet) -[:HAS_POLICIES]-> (policy: Policy)" +
                " WHERE policy.uuid = $0 RETURN policySet"
    )
    fun findPolicySetByPolicyUuid(uuid: UUID): Mono<PolicySet>

    @Query(
        "MATCH (resourcePolicy: ResourcePolicy) -[:HAS_POLICY_SET]-> (policySet: PolicySet) -[:HAS_POLICIES]-> (policy: Policy)" +
                " WHERE policy.uuid = $0 RETURN resourcePolicy"
    )
    fun findResourcePolicyByPolicyUuid(uuid: UUID): Mono<ResourcePolicy>
}

@Repository
class PolicyRepository : AppCoroutineSortingRepository<Policy, Long>() {

    @Autowired
    private lateinit var _repository: InternalPolicyRepository
    override fun repository() = _repository
    suspend fun findPolicyByUuid(uuid: UUID) = repository().findPolicyByUuid(uuid).awaitSingleOrNull()
    suspend fun findPolicyByName(name: String) = repository().findPolicyByName(name).awaitSingleOrNull()
}

@Repository
internal interface InternalPolicyConditionRepository : ReactiveNeo4jRepository<PolicyCondition, Long> {

    fun findPolicyConditionByUuid(uuid: UUID): Mono<PolicyCondition>

    @Query(
        "MATCH (policySet: PolicySet) -[:HAS_POLICIES]-> (policy: Policy)" +
                " -[:HAS_POLICY_CONDITIONS]-> (policyCondition: PolicyCondition) WHERE policyCondition.uuid = $0 RETURN policySet"
    )
    fun findPolicySetByPolicyConditionUuid(uuid: UUID): Mono<PolicySet>

    @Query(
        "MATCH (resourcePolicy: ResourcePolicy) -[:HAS_POLICY_SET]-> (policySet: PolicySet) -[:HAS_POLICIES]-> (policy: Policy)" +
                " -[:HAS_POLICY_CONDITIONS]-> (policyCondition: PolicyCondition) WHERE policyCondition.uuid = $0 RETURN resourcePolicy"
    )
    fun findResourcePolicyByPolicyConditionUuid(uuid: UUID): Mono<ResourcePolicy>
}

@Repository
class PolicyConditionRepository : AppCoroutineSortingRepository<PolicyCondition, Long>() {

    @Autowired
    private lateinit var _repository: InternalPolicyConditionRepository
    override fun repository() = _repository
    suspend fun findPolicyConditionByUuid(uuid: UUID) = repository().findPolicyConditionByUuid(uuid).awaitSingleOrNull()
    suspend fun findPolicySetByPolicyConditionUuid(uuid: UUID) =
        repository().findPolicySetByPolicyConditionUuid(uuid).awaitSingleOrNull()

    suspend fun findResourcePolicyByPolicyConditionUuid(uuid: UUID) =
        repository().findResourcePolicyByPolicyConditionUuid(uuid).awaitSingleOrNull()
}