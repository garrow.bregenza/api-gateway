package org.ladhark.identity.repository.user

import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.ladhark.identity.entity.team.Team
import org.ladhark.identity.entity.user.User
import org.ladhark.identity.entity.user.UserCredentials
import org.ladhark.identity.entity.user.UserRoleRegion
import org.ladhark.identity.entity.user.UserTeamRegion
import org.ladhark.identity.repository.base.AppCoroutineSortingRepository
import org.ladhark.identity.repository.model.role.InferredAuthorities
import org.ladhark.identity.repository.model.team.MappedInheritedTeam
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository
import org.springframework.data.neo4j.repository.query.Query
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

@Repository
internal interface InternalUserCredentialsRepository : ReactiveNeo4jRepository<UserCredentials, Long> {
    fun findByUserUuid(uuid: UUID): Mono<UserCredentials>
}

@Repository
class UserCredentialsRepository : AppCoroutineSortingRepository<UserCredentials, Long>() {

    @Autowired
    private lateinit var _repository: InternalUserCredentialsRepository
    override fun repository() = _repository
    suspend fun findByUserUuid(uuid: UUID) = repository().findByUserUuid(uuid).awaitSingleOrNull()

}

@Repository
internal interface InternalUserRepository : ReactiveNeo4jRepository<User, Long> {

    @Query(
        "MATCH (n:`User`) " +
                "WHERE n.email = $0 " +
                "RETURN " +
                "n"
    )
    fun findUserByEmail(email: String): Mono<User>

    @Query(
        "MATCH (n:`User`) " +
                "WHERE n.uuid = $0 " +
                "RETURN " +
                "n"
    )
    fun findUserByUuid(uuid: UUID): Mono<User>

    @Query(
        "MATCH (n:`User`) " +
                "WHERE n.system = true " +
                "RETURN " +
                "n"
    )
    fun findSystemUser(): Mono<User>
}

@Repository
class UserRepository : AppCoroutineSortingRepository<User, Long>() {

    @Autowired
    private lateinit var _repository: InternalUserRepository
    override fun repository() = _repository
    suspend fun findUserByEmail(email: String) = repository().findUserByEmail(email).awaitSingleOrNull()
    suspend fun findUserByUuid(uuid: UUID) = repository().findUserByUuid(uuid).awaitSingleOrNull()
    suspend fun findSystemUser() = repository().findSystemUser().awaitSingleOrNull()

}

@Repository
internal interface InternalUserJWTRepository : ReactiveNeo4jRepository<User, Long> {

    @Query(
        "MATCH (n:User {email: $0}) " +
                "MATCH (n)-[r_a1:`HAS_EXCLUSIVE_TEAM`]->(t1:`Team`) " +
                "MATCH (n)-[r_b1:`HAS_SELF_TEAM`]->(t2:`Team`) " +
                "OPTIONAL MATCH (n)-[r_c1:`HAS_EXCLUSIVE_ROLE`]->(r1:`Role`) " +
                "OPTIONAL MATCH (n)-[r_d1:`HAS_REGIONS`]->(r2:`Region`) " +
                "RETURN " +
                "n, collect(r_a1), collect(t1), collect(r_b1), collect(t2), collect(r_c1), collect(r1), collect(r_d1), collect(r2)"
    )
    fun findUserByEmail(email: String): Mono<User>

    @Query(
        "MATCH (n:User {uuid: $0}) " +
                "MATCH (n)-[r_a1:`HAS_EXCLUSIVE_TEAM`]->(t1:`Team`) " +
                "MATCH (n)-[r_b1:`HAS_SELF_TEAM`]->(t2:`Team`) " +
                "OPTIONAL MATCH (n)-[r_c1:`HAS_EXCLUSIVE_ROLE`]->(r1:`Role`) " +
                "OPTIONAL MATCH (n)-[r_d1:`HAS_REGIONS`]->(r2:`Region`) " +
                "RETURN " +
                "n, collect(r_a1), collect(t1), collect(r_b1), collect(t2), collect(r_c1), collect(r1), collect(r_d1), collect(r2)"
    )
    fun findUserByUuid(uuid: UUID): Mono<User>


    @Query(
        " MATCH (user: User {uuid: $0}) <-[:HAS_USER]- (userRoleRegion: UserRoleRegion) -[:HAS_REGION]-> (region: Region) where region.uuid in $1" +
                " MATCH (userRoleRegion) -[:HAS_ROLE]-> (role: Role) " +
                " OPTIONAL MATCH (role) -[:HAS_AUTHORITIES]-> (authority: Authority) -[:BELONGS_TO_RESOURCE]-> (resource: Resource) -[:BELONGS_TO_APPLICATION]-> (application: Application)" +
                " OPTIONAL MATCH (authority) -[:IMPLIES_AUTHORITIES*1..]-> (implies: Authority)" +
                " RETURN role as role, authority as authority, COLLECT(implies) as implies, resource as resource, region as region, application as application, user as source, false as inherited"
    )
    fun findUserLocalOnlyAuthoritiesByRegionAndApplication(
        userUuid: UUID,
        regionUuids: Collection<UUID>
    ): Flux<InferredAuthorities>

    @Query(
        " MATCH (user: User {uuid: $0}) <-[:HAS_USER]- (userRoleRegion: UserRoleRegion) -[:HAS_REGION]-> (region: Region) where region.uuid in $1" +
                " MATCH (userRoleRegion) -[:HAS_ROLE]-> (role: Role) " +
                " OPTIONAL MATCH (role) -[:HAS_AUTHORITIES]-> (authority: Authority) -[:BELONGS_TO_RESOURCE]-> (resource: Resource) -[:BELONGS_TO_APPLICATION]-> (application: Application)" +
                " OPTIONAL MATCH (authority) -[:IMPLIES_AUTHORITIES*1..]-> (implies: Authority)" +
                " RETURN role as role, authority as authority, COLLECT(implies) as implies, resource as resource, region as region, application as application, user as source, false as inherited" +
                " UNION " +
                " MATCH (region: Region) <-[:PARENT_REGION*1..]- (inheritedRegion: Region) where region.uuid in $1" +
                " WITH inheritedRegion" +
                " MATCH (user: User {uuid: $0}) <-[:HAS_USER]- (userRoleRegion: UserRoleRegion) -[:HAS_REGION]-> (inheritedRegion)" +
                " MATCH (userRoleRegion) -[:HAS_ROLE]-> (role: Role) " +
                " OPTIONAL MATCH (role) -[:HAS_AUTHORITIES]-> (authority: Authority) -[:BELONGS_TO_RESOURCE]-> (resource: Resource) -[:BELONGS_TO_APPLICATION]-> (application: Application)" +
                " OPTIONAL MATCH (authority) -[:IMPLIES_AUTHORITIES*1..]-> (implies: Authority)" +
                " RETURN role as role, authority as authority, COLLECT(implies) as implies, resource as resource, inheritedRegion as region, application as application, user as source, true as inherited"
    )
    fun findUserLocalAndInheritedAuthoritiesByRegionAndApplication(
        userUuid: UUID,
        regionUuids: Collection<UUID>
    ): Flux<InferredAuthorities>
}

@Repository
class UserJWTRepository : AppCoroutineSortingRepository<User, Long>() {

    @Autowired
    private lateinit var _repository: InternalUserJWTRepository
    override fun repository() = _repository
    suspend fun findUserByEmail(email: String) = repository().findUserByEmail(email).awaitSingleOrNull()
    suspend fun findUserByUuid(uuid: UUID) = repository().findUserByUuid(uuid).awaitSingleOrNull()
    fun findUserLocalOnlyAuthoritiesByRegionAndApplication(userUuid: UUID, regionUuids: Collection<UUID>) =
        repository()
            .findUserLocalOnlyAuthoritiesByRegionAndApplication(userUuid, regionUuids)
            .asFlow()

    fun findUserLocalAndInheritedAuthoritiesByRegionAndApplication(userUuid: UUID, regionUuids: Collection<UUID>) =
        repository()
            .findUserLocalAndInheritedAuthoritiesByRegionAndApplication(userUuid, regionUuids)
            .asFlow()
}

@Repository
internal interface InternalUserRoleRegionRepository : ReactiveNeo4jRepository<UserRoleRegion, Long> {
    fun findByUserUuidAndRoleUuidAndRegionUuid(userUuid: UUID, roleUuid: UUID, regionUuid: UUID): Mono<UserRoleRegion>
}

@Repository
class UserRoleRegionRepository : AppCoroutineSortingRepository<UserRoleRegion, Long>() {

    @Autowired
    private lateinit var _repository: InternalUserRoleRegionRepository

    override fun repository() = _repository
    suspend fun findByUserUuidAndRoleUuidAndRegionUuid(userUuid: UUID, roleUuid: UUID, regionUuid: UUID) =
        repository()
            .findByUserUuidAndRoleUuidAndRegionUuid(userUuid, roleUuid, regionUuid)
            .awaitSingleOrNull()
}

@Repository
internal interface InternalUserTeamRegionRepository : ReactiveNeo4jRepository<UserTeamRegion, Long> {

    @Query(
        "MATCH (user: User {uuid: $0}) <-[:HAS_USER]- (userTeamRegion: UserTeamRegion) -[:HAS_REGION]-> (region: Region) where region.uuid in $1" +
                " MATCH (userTeamRegion) -[:HAS_TEAM]-> (team: Team) " +
                " RETURN team"
    )
    fun findTeamsForUserInRegions(userUuid: UUID, regionUuids: Collection<UUID>): Flux<Team>

    @Query(
        "MATCH (user: User {uuid: $0}) <-[:HAS_USER]- (userTeamRegion: UserTeamRegion) -[:HAS_REGION]-> (region: Region) where region.uuid in $1" +
                " MATCH (userTeamRegion) -[:HAS_TEAM]-> (team: Team) " +
                " RETURN team as team, team as child, region as region, false as inheritedRegion" +
                " UNION " +
                " MATCH (user: User {uuid: $0}) <-[:HAS_USER]- (userTeamRegion: UserTeamRegion) -[:HAS_REGION]-> (region: Region) where region.uuid in $1" +
                " MATCH (userTeamRegion) -[:HAS_TEAM]-> (team: Team) " +
                " MATCH (region) <-[:BELONGS_TO_REGIONS]- (inheritedTeam: Team) <-[:PARENT_TEAM*1..]- (team)" +
                " WITH inheritedTeam, team, region" +
                " RETURN inheritedTeam as team, team as child, region as region, false as inheritedRegion"
    )
    fun findAllTeamsWithInheritanceForUserInRegions(
        userUuid: UUID,
        regionUuids: Collection<UUID>
    ): Flux<MappedInheritedTeam>

    /**
     * 1. Team - Region
     * 2. Parent Team - Region
     * 3. Team - Child Region
     * 4. Parent Team - Child Region
     */
    @Query(
        "MATCH (user: User {uuid: $0}) <-[:HAS_USER]- (userTeamRegion: UserTeamRegion) -[:HAS_REGION]-> (region: Region) where region.uuid in $1" +
                " MATCH (userTeamRegion) -[:HAS_TEAM]-> (team: Team) " +
                " RETURN team as team, team as child, region as region, false as inheritedRegion" +
                " UNION " +
                " MATCH (user: User {uuid: $0}) <-[:HAS_USER]- (userTeamRegion: UserTeamRegion) -[:HAS_REGION]-> (region: Region) where region.uuid in $1" +
                " MATCH (userTeamRegion) -[:HAS_TEAM]-> (team: Team) " +
                " MATCH (region) <-[:BELONGS_TO_REGIONS]- (inheritedTeam: Team) <-[:PARENT_TEAM*1..]- (team)" +
                " WITH inheritedTeam, team, region" +
                " RETURN inheritedTeam as team, team as child, region as region, false as inheritedRegion" +
                " UNION " +
                " MATCH (region: Region) <-[:PARENT_REGION*1..]- (inheritedRegion: Region) where region.uuid in $1" +
                " MATCH (user: User {uuid: $0}) <-[:HAS_USER]- (userTeamRegion: UserTeamRegion) -[:HAS_REGION]-> (inheritedRegion)" +
                " MATCH (userTeamRegion) -[:HAS_TEAM]-> (team: Team) " +
                " RETURN team as team, team as child, inheritedRegion as region, true as inheritedRegion" +
                " UNION " +
                " MATCH (region: Region) <-[:PARENT_REGION*1..]- (inheritedRegion: Region) where region.uuid in $1" +
                " MATCH (user: User {uuid: $0}) <-[:HAS_USER]- (userTeamRegion: UserTeamRegion) -[:HAS_REGION]-> (inheritedRegion)" +
                " MATCH (userTeamRegion) -[:HAS_TEAM]-> (team: Team) " +
                " MATCH (inheritedRegion) <-[:BELONGS_TO_REGIONS]- (inheritedTeam: Team) <-[:PARENT_TEAM*1..]- (team)" +
                " WITH inheritedTeam, team, inheritedRegion" +
                " RETURN inheritedTeam as team, team as child, inheritedRegion as region, true as inheritedRegion"
    )
    fun findAllTeamsWithInheritanceForUserWithInheritedRegions(
        userUuid: UUID,
        regionUuids: Collection<UUID>
    ): Flux<MappedInheritedTeam>
}

@Repository
class UserTeamRegionRepository : AppCoroutineSortingRepository<UserTeamRegion, Long>() {

    @Autowired
    private lateinit var _repository: InternalUserTeamRegionRepository

    override fun repository() = _repository

    fun findTeamsForUserInRegions(userUuid: UUID, regionUuids: Collection<UUID>) = repository()
        .findTeamsForUserInRegions(userUuid, regionUuids)
        .asFlow()

    fun findAllTeamsWithInheritanceForUserInRegions(userUuid: UUID, regionUuids: Collection<UUID>) = repository()
        .findAllTeamsWithInheritanceForUserInRegions(userUuid, regionUuids)
        .asFlow()

    fun findAllTeamsWithInheritanceForUserWithInheritedRegions(userUuid: UUID, regionUuids: Collection<UUID>) =
        repository()
            .findAllTeamsWithInheritanceForUserWithInheritedRegions(userUuid, regionUuids)
            .asFlow()
}