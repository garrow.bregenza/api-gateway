package org.ladhark.identity.repository.application

import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.ladhark.identity.entity.application.Application
import org.ladhark.identity.repository.base.AppCoroutineSortingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

@Repository
internal interface InternalApplicationRepository : ReactiveNeo4jRepository<Application, Long> {
    fun findApplicationByName(name: String): Mono<Application>
    fun findApplicationByUuid(uuid: UUID): Mono<Application>
    fun findAllByExternal(external: Boolean): Flux<Application>
}

@Repository
class ApplicationRepository : AppCoroutineSortingRepository<Application, Long>() {

    @Autowired
    private lateinit var _repository: InternalApplicationRepository
    override fun repository() = _repository
    suspend fun findApplicationByName(name: String) = repository().findApplicationByName(name).awaitSingleOrNull()
    suspend fun findApplicationByUuid(uuid: UUID) = repository().findApplicationByUuid(uuid).awaitSingleOrNull()
    fun findAllByExternal(external: Boolean) = repository().findAllByExternal(external).asFlow()
}