package org.ladhark.identity.repository.extension

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.reactor.mono
import reactor.core.publisher.Mono


fun <T> suspendedResultToMono(supplier: suspend () -> T): Mono<T> {
    return mono(Dispatchers.Unconfined) {
        supplier.invoke()
    }
}