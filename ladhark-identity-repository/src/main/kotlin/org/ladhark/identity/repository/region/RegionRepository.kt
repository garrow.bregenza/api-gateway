package org.ladhark.identity.repository.region

import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.repository.base.AppCoroutineSortingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository
import org.springframework.data.neo4j.repository.query.Query
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

@Repository
internal interface InternalRegionRepository : ReactiveNeo4jRepository<Region, Long> {
    fun findRegionByName(name: String): Mono<Region>
    fun findRegionByUuid(uuid: UUID): Mono<Region>
    fun findRegionByGlobal(global: Boolean): Mono<Region>
    fun findByParentRegionName(parentRegionName: String): Flux<Region> // find all children

    // ordered ????
    @Query("MATCH (parent: Region) <-[:PARENT_REGION*1..]- (region: Region) WHERE region.name IN $0 RETURN parent")
    fun findAllParentRegions(vararg name: String): Flux<Region>

    // ordered ????
    @Query("MATCH (region: Region) <-[:PARENT_REGION*1..]- (child: Region) WHERE region.name IN $0 RETURN child")
    fun findAllChildrenRegions(vararg name: String): Flux<Region>

    @Query("MATCH (region: Region) WHERE region.public = true RETURN region")
    fun findAllPublicRegions(): Flux<Region>

    @Query("MATCH (region: Region) WHERE region.uuid IN $0 RETURN region")
    fun findAllRegions(vararg uuids: String): Flux<Region>

    // ordered ???
    @Query(
        "MATCH (n: `Region`) " +
                "MATCH (n)-[r_p1:`PARENT_REGION`]->(r1:`Region`) " +
                "RETURN " +
                "n, collect(r_p1), collect(r1)"
    )
    fun findAllWithParentRegion(): Flux<Region>

    @Query("MATCH (parent: Region) <-[:PARENT_REGION*1..]- (region: Region) WHERE parent.name = $0 RETURN parent")
    fun findParentInChainByName(name: String): Mono<Region>

    @Query(
        "MATCH (parent: Region) <-[:PARENT_REGION*1..]- (region: Region) " +
                "WHERE parent.name = $0 and region.name = $1 " +
                "RETURN region"
    )
    fun findChildInChainByName(parentName: String, childName: String): Mono<Region>


    @Query("MATCH (region: Region) WHERE region.name = $0 and region.name != $1 RETURN region")
    fun findRegionExistsForUpdateWithName(name: String, updatingEntityName: String): Mono<Region>
}

@Repository
class RegionRepository : AppCoroutineSortingRepository<Region, Long>() {

    @Autowired
    private lateinit var _repository: InternalRegionRepository
    override fun repository() = _repository

    suspend fun findRegionByName(name: String) = repository().findRegionByName(name).awaitSingleOrNull()
    suspend fun findRegionByUuid(uuid: UUID) = repository().findRegionByUuid(uuid).awaitSingleOrNull()
    suspend fun findRegionByGlobal(global: Boolean) = repository().findRegionByGlobal(global).awaitSingleOrNull()

    // find all children
    fun findByParentRegionName(parentRegionName: String) =
        repository().findByParentRegionName(parentRegionName).asFlow()

    fun findAllParentRegions(vararg name: String) = repository().findAllParentRegions(*name).asFlow()
    fun findAllChildrenRegions(vararg name: String) = repository().findAllChildrenRegions(*name).asFlow()
    fun findAllPublicRegions() = repository().findAllPublicRegions().asFlow()
    fun findAllWithParentRegion() = repository().findAllWithParentRegion().asFlow()
    fun findAllRegions(vararg uuids: String) = repository().findAllRegions(*uuids).asFlow()
    suspend fun findParentInChainByName(name: String) = repository().findParentInChainByName(name).awaitSingleOrNull()
    suspend fun findChildInChainByName(parentName: String, childName: String) =
        repository().findChildInChainByName(parentName, childName).awaitSingleOrNull()

    suspend fun findRegionExistsForUpdateWithName(name: String, updatingEntityName: String) =
        repository().findRegionExistsForUpdateWithName(name, updatingEntityName).awaitSingleOrNull()
}