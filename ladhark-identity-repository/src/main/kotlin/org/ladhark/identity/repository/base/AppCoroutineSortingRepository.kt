package org.ladhark.identity.repository.base

import kotlinx.coroutines.Dispatchers.Unconfined
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.asPublisher
import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.springframework.data.domain.Sort
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.data.repository.kotlin.CoroutineSortingRepository
import reactor.core.publisher.Flux

abstract class AppCoroutineSortingRepository<T : Any, ID : Number> : CoroutineCrudRepository<T, ID>, CoroutineSortingRepository<T, ID> {

    internal abstract fun repository(): ReactiveNeo4jRepository<T, ID>

    override suspend fun count(): Long = repository().count().awaitSingle()

    override suspend fun deleteAll() {
        repository().deleteAll().awaitSingle()
    }

    override fun findAll(sort: Sort) = repository().findAll(sort).asFlow()

    override fun findAll() = repository().findAll().asFlow()

    suspend fun findAllList(): List<T> = repository().findAll().collectList().awaitSingle()

    fun findAllFlux(): Flux<T> = repository().findAll()

    override fun <S : T> saveAll(entityStream: Flow<S>) =
        repository().saveAll(entityStream.asPublisher(Unconfined)).asFlow()

    override fun <S : T> saveAll(entities: Iterable<S>) = repository().saveAll(entities).asFlow()

    override suspend fun <S : T> save(entity: S): T = repository().save(entity).awaitSingle()

    override suspend fun findById(id: ID) = repository().findById(id).awaitSingleOrNull()

    override fun findAllById(ids: Flow<ID>) = repository().findAllById(ids.asPublisher(Unconfined)).asFlow()

    override fun findAllById(ids: Iterable<ID>) = repository().findAllById(ids).asFlow()

    override suspend fun existsById(id: ID): Boolean = repository().existsById(id).awaitSingle()

    override suspend fun deleteById(id: ID) {
        repository().deleteById(id).awaitSingle()
    }

    override suspend fun deleteAllById(ids: Iterable<ID>) {
        repository().deleteAllById(ids).awaitSingle()
    }

    override suspend fun <S : T> deleteAll(entityStream: Flow<S>) {
        repository().deleteAll(entityStream.asPublisher(Unconfined)).asFlow()
    }

    override suspend fun deleteAll(entities: Iterable<T>) {
        repository().deleteAll(entities).awaitSingle()
    }

    override suspend fun delete(entity: T) {
        repository().delete(entity).awaitSingle()
    }

    fun subscribeAndDoOnAll(block: (entity: T) -> Unit) {
        findAllFlux().doOnNext {
            block(it)
        }.subscribe()
    }
}