package org.ladhark.identity.repository.team

import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.ladhark.identity.entity.team.Team
import org.ladhark.identity.entity.team.TeamRoleRegion
import org.ladhark.identity.repository.base.AppCoroutineSortingRepository
import org.ladhark.identity.repository.model.role.InferredAuthorities
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository
import org.springframework.data.neo4j.repository.query.Query
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

@Repository
internal interface InternalTeamRepository : ReactiveNeo4jRepository<Team, Long> {
    fun findTeamByName(name: String): Mono<Team>
    fun findTeamByUuid(uuid: UUID): Mono<Team>
    fun findTeamByRegionsName(name: String): Flux<Team>
}

@Repository
class TeamRepository : AppCoroutineSortingRepository<Team, Long>() {

    @Autowired
    private lateinit var _repository: InternalTeamRepository
    override fun repository() = _repository
    suspend fun findTeamByName(name: String) = repository().findTeamByName(name).awaitSingleOrNull()
    suspend fun findTeamByUuid(uuid: UUID) = repository().findTeamByUuid(uuid).awaitSingleOrNull()
    fun findTeamByRegionsName(name: String) = repository().findTeamByRegionsName(name).asFlow()

}


@Repository
internal interface InternalTeamRoleRegionRepository : ReactiveNeo4jRepository<TeamRoleRegion, Long> {
    /**
     * Even though here we are using in, typical usage is you find users all teams with region details from
     * UserTeamRegion, loop over its result sending pair of team and region to get exact authorities.
     */
    @Query(
        " MATCH (team: Team) <-[:HAS_TEAM]- (teamRoleRegion: TeamRoleRegion) -[:HAS_REGION]-> (region: Region) where team.uuid in $0 and region.uuid in $1" +
                " MATCH (teamRoleRegion) -[:HAS_ROLE]-> (role: Role) " +
                " OPTIONAL MATCH (role) -[:HAS_AUTHORITIES]-> (authority: Authority) -[:BELONGS_TO_RESOURCE]-> (resource: Resource) -[:BELONGS_TO_APPLICATION]-> (application: Application)" +
                " OPTIONAL MATCH (authority) -[:IMPLIES_AUTHORITIES*1..]-> (implies: Authority)" +
                " RETURN role as role, authority as authority, COLLECT(implies) as implies, resource as resource, region as region, application as application, team as source, false as inherited"
    )
    fun findTeamLocalOnlyAuthoritiesByRegionAndApplication(
        teamUuids: Collection<UUID>,
        regionUuids: Collection<UUID>
    ): Flux<InferredAuthorities>

    /**
     * /**
     * So far no usage as such
    */
    @Query("MATCH (team: Team) -[:BELONGS_TO_REGIONS]-> (region: Region) where team.uuid in $0 and region.uuid in $1" +
    " MATCH (team) <-[:HAS_TEAM]- (teamRoleRegion: TeamRoleRegion) -[:HAS_REGION]-> (region)" +
    " MATCH (teamRoleRegion) -[:HAS_ROLE]-> (role: Role) " +
    " MATCH (role) -[:HAS_AUTHORITIES]-> (authority: Authority) -[:HAS_RESOURCE]-> (resource: Resource) -[:BELONGS_TO_APPLICATION]-> (application: Application {name: $2})" +
    " OPTIONAL MATCH (authority) -[:IMPLIES_AUTHORITIES*1..]-> (implies: Authority)" +
    " RETURN role.name as role, authority.name as authority, COLLECT(implies.name) as implies, resource.name as resource, region.name as region, 'User' as source" +
    " UNION " +
    " MATCH (region: Region) <-[:PARENT_REGION*1..]- (inheritedRegion) where region.uuid in $1" +
    " WITH inheritedRegion" +
    " MATCH (inheritedTeam: Team) <-[:HAS_TEAM]- (teamRoleRegion: TeamRoleRegion) -[:HAS_REGION]-> (inheritedRegion)" +
    " MATCH (inheritedTeam) -[:HAS_ROLE]-> (role: Role) " +
    " MATCH (role) -[:HAS_AUTHORITIES]-> (authority: Authority) -[:HAS_RESOURCE]-> (resource: Resource) -[:BELONGS_TO_APPLICATION]-> (application: Application {name: $2})" +
    " OPTIONAL MATCH (authority) -[:IMPLIES_AUTHORITIES*1..]-> (implies: Authority)" +
    " RETURN role.name as role, authority.name as authority, COLLECT(implies.name) as implies, resource.name as resource, region.name as region, 'User' as source"
    )
     */
}

@Repository
class TeamRoleRegionRepository : AppCoroutineSortingRepository<TeamRoleRegion, Long>() {

    @Autowired
    private lateinit var _repository: InternalTeamRoleRegionRepository
    override fun repository() = _repository
    fun findTeamLocalOnlyAuthoritiesByRegionAndApplication(teamUuids: Collection<UUID>, regionUuids: Collection<UUID>) =
        repository()
            .findTeamLocalOnlyAuthoritiesByRegionAndApplication(teamUuids, regionUuids)
            .asFlow()

}