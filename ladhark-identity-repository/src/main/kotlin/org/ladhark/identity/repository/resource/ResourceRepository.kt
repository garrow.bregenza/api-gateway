package org.ladhark.identity.repository.resource

import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.ladhark.identity.entity.application.Application
import org.ladhark.identity.entity.resource.Resource
import org.ladhark.identity.entity.resource.ResourceAction
import org.ladhark.identity.entity.resource.Scope
import org.ladhark.identity.repository.base.AppCoroutineSortingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository
import org.springframework.data.neo4j.repository.query.Query
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import java.util.*

@Repository
internal interface InternalResourceRepository : ReactiveNeo4jRepository<Resource, Long> {
    fun findResourceByName(name: String): Mono<Resource>
    fun findResourceByUuid(uuid: UUID): Mono<Resource>
}

@Repository
class ResourceRepository : AppCoroutineSortingRepository<Resource, Long>() {

    @Autowired
    private lateinit var _repository: InternalResourceRepository
    override fun repository() = _repository

    suspend fun findResourceByName(name: String) = repository().findResourceByName(name).awaitSingleOrNull()

    suspend fun findResourceByUuid(uuid: UUID) = repository().findResourceByUuid(uuid).awaitSingleOrNull()
}

@Repository
internal interface InternalResourceActionRepository : ReactiveNeo4jRepository<ResourceAction, Long> {

    fun findResourceActionByName(name: String): Mono<ResourceAction>
    fun findResourceActionByUuid(uuid: UUID): Mono<ResourceAction>
    fun findByUriPattern(uriPattern: String): Mono<ResourceAction>
    fun findByUriPatternAndScope(uriPattern: String, scope: Scope): Mono<ResourceAction>

    // TODO Query is not working !!!!!!!!!!!!!!!
    @Query(
        "MATCH (resourceAction: ResourceAction) -[:BELONGS_TO_RESOURCE]-> (resource: Resource) -[:BELONGS_TO_APPLICATION]-> (application: Application)" +
                " WHERE resourceAction.uriPattern = $0 and resourceAction.scope = $1 RETURN application"
    )
    fun findApplicationForUriPatternAndScope(uriPattern: String, scope: Scope): Mono<Application>
}

@Repository
class ResourceActionRepository : AppCoroutineSortingRepository<ResourceAction, Long>() {

    @Autowired
    private lateinit var _repository: InternalResourceActionRepository
    override fun repository() = _repository
    suspend fun findResourceActionByName(name: String) = repository().findResourceActionByName(name).awaitSingleOrNull()
    suspend fun findResourceActionByUuid(uuid: UUID) = repository().findResourceActionByUuid(uuid).awaitSingleOrNull()
    suspend fun findByUriPattern(uriPattern: String) = repository().findByUriPattern(uriPattern).awaitSingleOrNull()
    suspend fun findByUriPatternAndScope(uriPattern: String, scope: Scope) =
        repository().findByUriPatternAndScope(uriPattern, scope).awaitSingleOrNull()

    suspend fun findApplicationForUriPatternAndScope(uriPattern: String, scope: Scope) =
        repository().findApplicationForUriPatternAndScope(uriPattern, scope).awaitSingleOrNull()
}