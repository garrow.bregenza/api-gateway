package org.ladhark.identity.repository.organization

import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.ladhark.identity.entity.organization.Organization
import org.ladhark.identity.repository.base.AppCoroutineSortingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import java.util.*

@Repository
internal interface InternalOrganizationRepository : ReactiveNeo4jRepository<Organization, Long> {
    fun findOrganizationByName(name: String): Mono<Organization>
    fun findOrganizationByUuid(uuid: UUID): Mono<Organization>
}

@Repository
class OrganizationRepository : AppCoroutineSortingRepository<Organization, Long>() {

    @Autowired
    private lateinit var _repository: InternalOrganizationRepository
    override fun repository() = _repository
    suspend fun findOrganizationByName(name: String) = repository().findOrganizationByName(name).awaitSingleOrNull()
    suspend fun findOrganizationByUuid(uuid: UUID) = repository().findOrganizationByUuid(uuid).awaitSingleOrNull()
}