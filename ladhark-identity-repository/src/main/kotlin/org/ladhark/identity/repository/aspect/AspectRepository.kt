package org.ladhark.identity.repository.aspect

import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.ladhark.identity.entity.aspect.Aspect
import org.ladhark.identity.entity.aspect.AspectType
import org.ladhark.identity.repository.base.AppCoroutineSortingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository
import org.springframework.data.neo4j.repository.query.Query
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

@Repository
internal interface InternalAspectTypeRepository : ReactiveNeo4jRepository<AspectType, Long> {
    fun findAspectTypeByName(name: String): Mono<AspectType>

    fun findAspectTypeByUuid(uuid: UUID): Mono<AspectType>
}

@Repository
class AspectTypeRepository : AppCoroutineSortingRepository<AspectType, Long>() {

    @Autowired
    private lateinit var _repository: InternalAspectTypeRepository
    override fun repository() = _repository
    suspend fun findAspectTypeByName(name: String) = repository().findAspectTypeByName(name).awaitSingleOrNull()
    suspend fun findAspectTypeByUuid(uuid: UUID) = repository().findAspectTypeByUuid(uuid).awaitSingleOrNull()
}

@Repository
internal interface InternalAspectRepository : ReactiveNeo4jRepository<Aspect, Long> {
    fun findAspectByName(name: String): Mono<Aspect>

    fun findAspectByUuid(uuid: UUID): Mono<Aspect>

    @Query(
        "MATCH (aspect: Aspect) -[:BELONGS_TO_RESOURCE_ACTION]-> (resourceAction: ResourceAction) " +
                " WHERE resourceAction.name = $0 RETURN aspect"
    )
    fun findAspectByResourceActionName(name: String): Mono<Aspect>

    @Query(
        "MATCH (n:`Aspect`) -[:BELONGS_TO_RESOURCE_ACTION]-> (resourceAction: ResourceAction) " +
                "MATCH (n)-[r_b1:`BELONGS_TO_RESOURCE_ACTION`]->(ra1:`ResourceAction`) " +
                "MATCH (n)-[r_c1:`CREATED_BY`]-(u1:`User`) " +
                "MATCH (n)-[r_m1:`MODIFIED_BY`]-(u2:`User`) " +
                "MATCH (n)-[r_b2:`BELONGS_TO_REGION`]->(r1:`Region`) " +
                "MATCH (n)-[r_h1:`HAS_ASPECT_TYPE`]->(a1:`AspectType`) " +
                "WHERE resourceAction.uuid = $0 " +
                "WITH n " +
                "RETURN " +
                "n, collect(r_b1), collect(ra1), collect(r_c1), collect(u1), collect(r_m1), collect(u2), collect(r_b2), collect(r1), collect(r_h1), collect(a1)"
    )
    fun findAspectByResourceActionUuid(uuid: UUID): Flux<Aspect>
}

@Repository
class AspectRepository : AppCoroutineSortingRepository<Aspect, Long>() {

    @Autowired
    private lateinit var _repository: InternalAspectRepository
    override fun repository() = _repository
    suspend fun findAspectByName(name: String) = repository().findAspectByName(name).awaitSingleOrNull()
    suspend fun findAspectByUuid(uuid: UUID) = repository().findAspectByUuid(uuid).awaitSingleOrNull()
    suspend fun findAspectByResourceActionName(name: String) =
        repository().findAspectByResourceActionName(name).awaitSingleOrNull()

    suspend fun findAspectsByResourceActionUuid(uuid: UUID) =
        repository().findAspectByResourceActionUuid(uuid).asFlow()
}