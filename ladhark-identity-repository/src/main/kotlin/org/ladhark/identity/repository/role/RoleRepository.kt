package org.ladhark.identity.repository.role

import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.ladhark.identity.entity.role.Role
import org.ladhark.identity.repository.base.AppCoroutineSortingRepository
import org.ladhark.identity.repository.model.role.RoleAuthorityResult
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository
import org.springframework.data.neo4j.repository.query.Query
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

@Repository
internal interface InternalRoleRepository : ReactiveNeo4jRepository<Role, Long> {
    fun findRoleByName(name: String): Mono<Role>
    fun findRoleByUuid(uuid: UUID): Mono<Role>

    @Query(
        " MATCH (role: Role {uuid: $0}) -[:HAS_AUTHORITIES]-> (authority: Authority)" +
                " OPTIONAL MATCH (authority) -[:IMPLIES_AUTHORITIES*1..]-> (implies: Authority) -[:BELONGS_TO_RESOURCE]-> (resource: Resource) -[:BELONGS_TO_APPLICATION]-> (application: Application)" +
                " RETURN role as role, authority as authority, COLLECT(implies) as implies, resource as resource,  application as application"
    )
    fun findAuthoritiesForRole(uuid: UUID): Flux<RoleAuthorityResult>
}

@Repository
class RoleRepository : AppCoroutineSortingRepository<Role, Long>() {

    @Autowired
    private lateinit var _repository: InternalRoleRepository
    override fun repository() = _repository
    suspend fun findRoleByName(name: String) = repository().findRoleByName(name).awaitSingleOrNull()
    suspend fun findRoleByUuid(uuid: UUID) = repository().findRoleByUuid(uuid).awaitSingleOrNull()
    fun findAuthoritiesForRole(uuid: UUID) = repository().findAuthoritiesForRole(uuid).asFlow()

}