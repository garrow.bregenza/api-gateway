package org.ladhark.identity.repository.authority

import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.ladhark.identity.entity.authority.Authority
import org.ladhark.identity.repository.base.AppCoroutineSortingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import java.util.*

@Repository
internal interface InternalAuthorityRepository : ReactiveNeo4jRepository<Authority, Long> {

    fun findAuthorityByName(name: String): Mono<Authority>
    fun findAuthorityByUuid(uuid: UUID): Mono<Authority>

}

@Repository
class AuthorityRepository : AppCoroutineSortingRepository<Authority, Long>() {

    @Autowired
    private lateinit var _repository: InternalAuthorityRepository
    override fun repository() = _repository
    suspend fun findAuthorityByName(name: String) = repository().findAuthorityByName(name).awaitSingleOrNull()
    suspend fun findAuthorityByUuid(uuid: UUID) = repository().findAuthorityByUuid(uuid).awaitSingleOrNull()
}