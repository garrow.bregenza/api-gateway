package org.ladhark.identity.repository.model.team

import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.entity.team.Team

data class MappedInheritedTeam(val team: Team, val child: Team?, val region: Region, val inheritedRegion: Boolean)