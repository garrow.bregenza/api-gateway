package org.ladhark.identity.repository.credential

import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.ladhark.identity.entity.credential.CredentialStore
import org.ladhark.identity.repository.base.AppCoroutineSortingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository
import org.springframework.data.neo4j.repository.query.Query
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import java.util.*

@Repository
internal interface InternalCredentialStoreRepository : ReactiveNeo4jRepository<CredentialStore, Long> {
    fun findCredentialStoreByName(name: String): Mono<CredentialStore>
    fun findCredentialStoreByUuid(uuid: UUID): Mono<CredentialStore>

    @Query(
        "MATCH (application: Application) -[:HAS_CREDENTIAL_STORE]-> (credentialStore: CredentialStore)" +
                " WHERE application.name = $0 RETURN credentialStore"
    )
    fun findCredentialStoreByApplicationName(name: String): Mono<CredentialStore>
}

@Repository
class CredentialStoreRepository : AppCoroutineSortingRepository<CredentialStore, Long>() {

    @Autowired
    private lateinit var _repository: InternalCredentialStoreRepository
    override fun repository() = _repository
    suspend fun findCredentialStoreByName(name: String) =
        repository().findCredentialStoreByName(name).awaitSingleOrNull()

    suspend fun findCredentialStoreByUuid(uuid: UUID) = repository().findCredentialStoreByUuid(uuid).awaitSingleOrNull()
    suspend fun findCredentialStoreByApplicationName(name: String) =
        repository().findCredentialStoreByApplicationName(name).awaitSingleOrNull()
}