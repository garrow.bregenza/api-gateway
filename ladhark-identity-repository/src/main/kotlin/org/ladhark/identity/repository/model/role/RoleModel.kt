package org.ladhark.identity.repository.model.role

import org.ladhark.identity.entity.BaseEntity
import org.ladhark.identity.entity.application.Application
import org.ladhark.identity.entity.authority.Authority
import org.ladhark.identity.entity.region.Region
import org.ladhark.identity.entity.resource.Resource
import org.ladhark.identity.entity.role.Role

data class RoleAuthorityResult(
    val role: Role,
    val authority: Authority?,
    val implies: Set<Authority> = emptySet(),
    val application: Application?
)

data class InferredAuthorities(
    val role: Role, val authority: Authority?, val implies: Set<Authority> = emptySet(), val resource: Resource?,
    val region: Region, val application: Application?, val source: BaseEntity, val inherited: Boolean = false
)