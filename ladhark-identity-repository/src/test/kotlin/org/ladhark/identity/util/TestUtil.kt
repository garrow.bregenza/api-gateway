package org.ladhark.identity.util

import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.util.*
import javax.imageio.ImageIO

object TestUtil {

    fun email(): String {
        return "jhon_doe_${random()}@ladhark.com"
    }

    fun regionName(): String {
        return "India_${random()}"
    }

    fun teamName(): String {
        return "Testing_Team_${random()}"
    }

    fun addRandom(value: String): String {
        return "${value}_${random()}"
    }

    fun generateImageWithText(text: String): ByteArray {
        val image = BufferedImage(64, 64, BufferedImage.TYPE_INT_RGB)
        image.createGraphics().drawString(text, 1, 10)
        val imageData = ByteArrayOutputStream()
        ImageIO.write(image, "png", imageData)
        return imageData.toByteArray()
    }

    fun clientName(): String {
        return "Testing_Client_${random()}"
    }

    fun applicationName(): String {
        return "App_${random()}"
    }

    fun policyName(): String {
        return "Policy_${random()}"
    }

    private fun random(): String {
        return UUID.randomUUID().toString().replace("-", "").lowercase(Locale.getDefault())
    }
}

data class Record(
    val role: String,
    val resource: String,
    val region: String,
    val authority: String,
    val implies: Collection<String> = emptySet()
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Record

        if (role != other.role) return false
        if (resource != other.resource) return false
        if (region != other.region) return false
        if (authority != other.authority) return false
        if (!implies.containsAll(other.implies)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = role.hashCode()
        result = 31 * result + resource.hashCode()
        result = 31 * result + region.hashCode()
        result = 31 * result + authority.hashCode()
        result = 31 * result + implies.joinToString("").hashCode()
        return result
    }
}