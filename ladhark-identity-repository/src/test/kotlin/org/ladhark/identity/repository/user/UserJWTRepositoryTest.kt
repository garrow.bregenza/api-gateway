package org.ladhark.identity.repository.user

import org.ladhark.identity.repository.RepositoryTestHelper
import org.ladhark.identity.repository.application.ApplicationRepository
import org.ladhark.identity.repository.authority.AuthorityRepository
import org.ladhark.identity.repository.region.RegionRepository
import org.ladhark.identity.repository.resource.ResourceRepository
import org.ladhark.identity.repository.role.RoleRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.neo4j.DataNeo4jTest
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional

@DataNeo4jTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
class UserJWTRepositoryTest : RepositoryTestHelper {

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var userJwtRepository: UserJWTRepository

    @Autowired
    lateinit var regionRepository: RegionRepository

    @Autowired
    lateinit var roleRepository: RoleRepository

    @Autowired
    lateinit var applicationRepository: ApplicationRepository

    @Autowired
    lateinit var authorityRepository: AuthorityRepository

    @Autowired
    lateinit var userRoleRegionRepository: UserRoleRegionRepository

    @Autowired
    lateinit var resourceRepository: ResourceRepository

    /*
        @Test
        fun `it should link roles and authorities to a user`() {

            val REGION_INDIA = TestUtil.addRandom("India")

            val app = Application()
            app.name = "school"

            val dbOp = applicationRepository
                .save(app)
                .flatMapMany { application ->

                    val schoolResource = Resource()
                    schoolResource.name = "school"
                    schoolResource.application = application
                    schoolResource.description = "School Resource"

                    resourceRepository
                        .save(schoolResource)
                        .flatMapMany {
                            val studentResource = Resource()
                            studentResource.name = "student"
                            studentResource.application = application
                            studentResource.description = "Student Resource"

                            ReactiveUtils.combine(it, resourceRepository.save(studentResource))
                        }.flatMap {

                            val teacherResource = Resource()
                            teacherResource.name = "teacher"
                            teacherResource.application = application
                            teacherResource.description = "Teacher Resource"

                            ReactiveUtils.combine(it, resourceRepository.save(teacherResource))
                        }
                }
                .flatMap {
                    val schoolResource = it.t1
                    val studentResource = it.t2
                    val teacherResource = it.t3

                    val authoritySchoolCreate = Authority("create", schoolResource)

                    authorityRepository
                        .save(authoritySchoolCreate)
                        .flatMapMany { asc ->
                            val authoritySchoolRead = Authority("read", schoolResource)
                            ReactiveUtils.combine(asc, authorityRepository.save(authoritySchoolRead))
                        }.flatMap { tp ->
                            val authorityTeacherRead = Authority("read", teacherResource)
                            ReactiveUtils.combine(tp, authorityRepository.save(authorityTeacherRead))
                        }.flatMap { tp ->
                            val authorityStudentRead = Authority("read", studentResource)
                            ReactiveUtils.combine(tp, authorityRepository.save(authorityStudentRead))
                        }.flatMap { tp ->
                            val authorityStudentCreate = Authority("create", studentResource, tp.t4)
                            ReactiveUtils.combine(tp, authorityRepository.save(authorityStudentCreate))
                        }
                }
                .flatMap {

                    val authoritySchoolCreate = it.t1
                    val authoritySchoolRead = it.t2
                    val authorityTeacherRead = it.t3
                    val authorityStudentRead = it.t4
                    val authorityStudentCreate = it.t5

                    val adminRole = Role("Admin")
                    adminRole.authorities.add(authoritySchoolCreate)
                    adminRole.authorities.add(authorityStudentCreate)

                    roleRepository
                        .save(adminRole)
                        .flatMapMany { r ->
                            val teacherRole = Role("Teacher")
                            teacherRole.authorities.add(authorityTeacherRead)
                            ReactiveUtils.combine(r, roleRepository.save(teacherRole))
                        }.flatMap { r ->
                            val studentRole = Role("Student")
                            studentRole.authorities.add(authorityStudentRead)
                            ReactiveUtils.combine(r, roleRepository.save(studentRole))
                        }
                }
                .flatMap {

                    val adminRole = it.t1
                    val teacherRole = it.t2
                    val studentRole = it.t3

                    val regionIndia = Region()
                    regionIndia.name = REGION_INDIA

                    regionRepository
                        .save(regionIndia)
                        .flatMapMany {
                            val email = TestUtil.email()
                            val user = User(email, Name("garrow", "", "bregenza"), locked = false, active = true)
                            user.initOnCreateTeams(UUID.randomUUID(), UUID.randomUUID())

                            val userSave = userRepository.save(user)

                            val emailAgra = TestUtil.email()
                            val userAgra = User(emailAgra, Name("john", "", "doe"), locked = false, active = true)
                            userAgra.initOnCreateTeams(UUID.randomUUID(), UUID.randomUUID())

                            val userAgraSave = userRepository.save(userAgra)

                            Flux.zip(userSave, userAgraSave, Mono.just(it))

                        }.flatMap { data ->

                            val user = data.t1
                            val userAgra = data.t2
                            val india = data.t3

                            val userRoleAdminRegionIndia = UserRoleRegion()
                            userRoleAdminRegionIndia.user = user
                            userRoleAdminRegionIndia.role = adminRole
                            userRoleAdminRegionIndia.region = india

                            userRoleRegionRepository
                                .save(userRoleAdminRegionIndia)
                                .flatMapMany { urr ->
                                    val userRoleTeacherRegionIndia = UserRoleRegion()
                                    userRoleTeacherRegionIndia.user = user
                                    userRoleTeacherRegionIndia.role = teacherRole
                                    userRoleTeacherRegionIndia.region = india

                                    ReactiveUtils.combine(urr, userRoleRegionRepository.save(userRoleTeacherRegionIndia))
                                }.flatMap { urr ->
                                    val userRoleTeacherRegionAgra = UserRoleRegion()
                                    userRoleTeacherRegionAgra.user = userAgra
                                    userRoleTeacherRegionAgra.role = studentRole
                                    userRoleTeacherRegionAgra.region = india

                                    ReactiveUtils.combine(urr, userRoleRegionRepository.save(userRoleTeacherRegionAgra))
                                }.flatMap { urr ->
                                    Flux.zip(
                                        Mono.just(urr.t1),
                                        Mono.just(urr.t2),
                                        Mono.just(urr.t3),
                                        Mono.just(user),
                                        Mono.just(regionIndia)
                                    )
                                }
                        }

                }
                .flatMap {
                    val user = it.t4
                    val regionIndia = it.t5

                    userJwtRepository
                        .findUserLocalOnlyAuthoritiesByRegionAndApplication(user.uuid, listOf(regionIndia.uuid))
                }
                .map {
                    Record(
                        it.role.name,
                        it.resource!!.name,
                        it.region.name,
                        it.authority!!.name,
                        it.implies.map { i -> i.name })
                }
                .collectList()



            StepVerifier.create(dbOp).assertNext { authorityRecords ->
                assertThat(authorityRecords)
                    .containsExactlyInAnyOrder(
                        Record("Admin", "school", REGION_INDIA, "create"),
                        Record("Admin", "student", REGION_INDIA, "create", setOf("read")),
                        Record("Teacher", "teacher", REGION_INDIA, "read")
                    )
            }
                .verifyComplete()

        }


        @Test
        fun `it should link roles and authorities to a user from inherited region`() {

            val REGION_INDIA = TestUtil.addRandom("India")
            val REGION_DELHI = TestUtil.addRandom("Delhi")
            val REGION_AGRA = TestUtil.addRandom("Agra")

            val app = Application()
            app.name = "school"

            val dbOp = applicationRepository
                .save(app)
                .flatMapMany { application ->

                    val schoolResource = Resource()
                    schoolResource.name = "school"
                    schoolResource.application = application
                    schoolResource.description = "School Resource"

                    resourceRepository
                        .save(schoolResource)
                        .flatMapMany {
                            val studentResource = Resource()
                            studentResource.name = "student"
                            studentResource.application = application
                            studentResource.description = "Student Resource"

                            ReactiveUtils.combine(it, resourceRepository.save(studentResource))
                        }.flatMap {

                            val teacherResource = Resource()
                            teacherResource.name = "teacher"
                            teacherResource.application = application
                            teacherResource.description = "Teacher Resource"

                            ReactiveUtils.combine(it, resourceRepository.save(teacherResource))
                        }
                }
                .flatMap {
                    val schoolResource = it.t1
                    val studentResource = it.t2
                    val teacherResource = it.t3

                    val authoritySchoolCreate = Authority("create", schoolResource)

                    authorityRepository
                        .save(authoritySchoolCreate)
                        .flatMapMany { asc ->
                            val authoritySchoolRead = Authority("read", schoolResource)
                            ReactiveUtils.combine(asc, authorityRepository.save(authoritySchoolRead))
                        }.flatMap { tp ->
                            val authorityTeacherRead = Authority("read", teacherResource)
                            ReactiveUtils.combine(tp, authorityRepository.save(authorityTeacherRead))
                        }.flatMap { tp ->
                            val authorityStudentRead = Authority("read", studentResource)
                            ReactiveUtils.combine(tp, authorityRepository.save(authorityStudentRead))
                        }.flatMap { tp ->
                            val authorityStudentCreate = Authority("create", studentResource, tp.t4)
                            ReactiveUtils.combine(tp, authorityRepository.save(authorityStudentCreate))
                        }
                }
                .flatMap {

                    val authoritySchoolCreate = it.t1
                    val authoritySchoolRead = it.t2
                    val authorityTeacherRead = it.t3
                    val authorityStudentRead = it.t4
                    val authorityStudentCreate = it.t5

                    val adminRole = Role("Admin")
                    adminRole.authorities.add(authoritySchoolCreate)
                    adminRole.authorities.add(authorityStudentCreate)

                    roleRepository
                        .save(adminRole)
                        .flatMapMany { r ->
                            val teacherRole = Role("Teacher")
                            teacherRole.authorities.add(authorityTeacherRead)
                            ReactiveUtils.combine(r, roleRepository.save(teacherRole))
                        }.flatMap { r ->
                            val studentRole = Role("Student")
                            studentRole.authorities.add(authorityStudentRead)
                            ReactiveUtils.combine(r, roleRepository.save(studentRole))
                        }
                }
                .flatMap {
                    val adminRole = it.t1
                    val teacherRole = it.t2
                    val studentRole = it.t3

                    val regionIndia = Region()
                    regionIndia.name = REGION_INDIA

                    regionRepository
                        .save(regionIndia)
                        .flatMapMany { india ->
                            val regionDelhi = Region()
                            regionDelhi.name = REGION_DELHI
                            regionDelhi.parentRegion = india

                            ReactiveUtils.combine(india, regionRepository.save(regionDelhi))
                        }
                        .flatMap { regions ->
                            val india = regions.t1
                            val delhi = regions.t2

                            val regionAgra = Region()
                            regionAgra.name = REGION_AGRA
                            regionAgra.parentRegion = india

                            ReactiveUtils.combine(regions, regionRepository.save(regionAgra))
                        }
                        .flatMap { regions ->
                            val india = regions.t1
                            val delhi = regions.t2
                            val agra = regions.t3

                            val email = TestUtil.email()
                            val user = User(email, Name("garrow", "", "bregenza"), locked = false, active = true)
                            user.initOnCreateTeams(UUID.randomUUID(), UUID.randomUUID())


                            userRepository
                                .save(user)
                                .flatMapMany { user ->

                                    val emailAgra = TestUtil.email()
                                    val userAgra = User(emailAgra, Name("john", "", "doe"), locked = false, active = true)
                                    userAgra.initOnCreateTeams(UUID.randomUUID(), UUID.randomUUID())

                                    ReactiveUtils.combine(user, userRepository.save(userAgra))
                                }
                                .flatMap { users ->
                                    Flux.zip(
                                        Mono.just(india),
                                        Mono.just(delhi),
                                        Mono.just(agra),
                                        Mono.just(users.t1),
                                        Mono.just(users.t2)
                                    )
                                }

                        }
                        .flatMap { data ->

                            val india = data.t1
                            val delhi = data.t2
                            val agra = data.t3
                            val userDelhi = data.t4
                            val userAgra = data.t5

                            val userRoleAdminRegionIndia = UserRoleRegion()
                            userRoleAdminRegionIndia.user = userDelhi
                            userRoleAdminRegionIndia.role = adminRole
                            userRoleAdminRegionIndia.region = india

                            userRoleRegionRepository
                                .save(userRoleAdminRegionIndia)
                                .flatMapMany { data ->
                                    val userRoleTeacherRegionDelhi = UserRoleRegion()
                                    userRoleTeacherRegionDelhi.user = userDelhi
                                    userRoleTeacherRegionDelhi.role = teacherRole
                                    userRoleTeacherRegionDelhi.region = delhi

                                    ReactiveUtils.combine(data, userRoleRegionRepository.save(userRoleTeacherRegionDelhi))
                                }
                                .flatMap { data ->
                                    val userRoleTeacherRegionAgra = UserRoleRegion()
                                    userRoleTeacherRegionAgra.user = userAgra
                                    userRoleTeacherRegionAgra.role = studentRole
                                    userRoleTeacherRegionAgra.region = agra

                                    ReactiveUtils.combine(data, userRoleRegionRepository.save(userRoleTeacherRegionAgra))
                                }
                                .flatMap {
                                    userJwtRepository
                                        .findUserLocalAndInheritedAuthoritiesByRegionAndApplication(
                                            userDelhi.uuid,
                                            listOf(india.uuid)
                                        )
                                }
                        }
                }
                .map {
                    Record(
                        it.role.name,
                        it.resource!!.name,
                        it.region.name,
                        it.authority!!.name,
                        it.implies.map { i -> i.name })
                }
                .collectList()



            StepVerifier.create(dbOp).assertNext { authorityRecords ->
                assertThat(authorityRecords)
                    .containsExactlyInAnyOrder(
                        Record("Admin", "school", REGION_INDIA, "create"),
                        Record("Admin", "student", REGION_INDIA, "create", setOf("read")),
                        Record("Teacher", "teacher", REGION_DELHI, "read")
                    )
            }
                .verifyComplete()

        }

        @Test
        fun `it should save a user with self team and exclusive team`() {

            val regionIndia = Region()
            regionIndia.name = "India"

            val roleDb = Role()
            roleDb.name = "ExclusiveRole"

            val application = Application()
            application.name = "BookApp"

            val resource = Resource()
            resource.name = "Book"
            resource.application = application

            val authorityDb = Authority()
            authorityDb.name = "Book_Read"
            authorityDb.resource = resource

            val dbOp = Mono
                        .zip(roleRepository.save(roleDb), regionRepository.save(regionIndia))
                        .flatMap {
                            val role = it.t1
                            val region = it.t2

                            val user = User()
                            user.email = "test@ladhark.org"
                            user.name= Name("test","", "user")

                            val userHasRegion = UserHasRegion()
                            userHasRegion.region = region

                            user.hasRegions.add(userHasRegion)

                            user.initOnCreateTeams(UUID.randomUUID(), UUID.randomUUID())

                            user.exclusiveRole = role

                            userRepository.save(user)
                        }
                        .flatMap {
                            userJwtRepository.findUserByEmail(it.email)
                        }


            StepVerifier.create(dbOp).assertNext { userRecord ->
                assertThat(userRecord.exclusiveTeam.name).isEqualTo("_exclusive_test@ladhark.org")
                assertThat(userRecord.selfTeam.name).isEqualTo("_self_test@ladhark.org")
                assertThat(userRecord.exclusiveRole?.name).isEqualTo("ExclusiveRole")
                assertThat(userRecord.hasRegions).anyMatch {
                    it.region.name == "India"
                }
                assertThat(userRecord.name).isEqualTo(Name("test","", "user"))
            }.verifyComplete()

        }
    */
}