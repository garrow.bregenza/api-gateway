package org.ladhark.identity.repository.role

import kotlinx.coroutines.flow.count
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.ladhark.identity.repository.RepositoryTestHelper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import org.testcontainers.junit.jupiter.Testcontainers
import java.util.*

@Testcontainers
@SpringBootTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
class RoleRepositoryTest : RepositoryTestHelper {

    @Autowired
    lateinit var roleRepository: RoleRepository

    @Test
    fun `it should give empty result if no authority present for a role`() {

        runBlocking {
            val count = roleRepository
                .findAuthoritiesForRole(UUID.randomUUID())
                .count()

            Assertions.assertThat(count).isEqualTo(0)
        }

    }

}