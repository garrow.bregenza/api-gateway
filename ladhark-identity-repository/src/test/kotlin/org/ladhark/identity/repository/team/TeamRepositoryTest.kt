package org.ladhark.identity.repository.team

import org.ladhark.identity.repository.RepositoryTestHelper
import org.ladhark.identity.repository.region.RegionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.data.neo4j.DataNeo4jTest
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional

@DataNeo4jTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
class TeamRepositoryTest : RepositoryTestHelper {

    @Autowired
    lateinit var teamRepository: TeamRepository

    @Autowired
    lateinit var regionRepository: RegionRepository

    /*
        @Test
        fun `it should save team`() {
            val teamName = TestUtil.teamName()
            val team = Team(teamName)

            val fromDb = teamRepository
                .save(team)
                .flatMap {
                    teamRepository.findTeamByName(teamName)
                }

            StepVerifier.create(fromDb).assertNext { t ->
                Assertions.assertThat(t.name).isEqualTo(team.name)
                Assertions.assertThat(t.uuid).isEqualTo(team.uuid)
            }.verifyComplete()

        }

        @Test
        fun `it should find team by name`() {

            val teamName = TestUtil.teamName()
            val team = Team(teamName)

            val fromDb = teamRepository
                .save(team)
                .flatMap {
                    teamRepository.findTeamByName(teamName)
                }

            StepVerifier.create(fromDb).assertNext { t ->
                Assertions.assertThat(t.name).isEqualTo(team.name)
                Assertions.assertThat(t.uuid).isEqualTo(team.uuid)
            }.verifyComplete()
        }

        @Test
        fun `it should find team by uuid`() {

            val teamName = TestUtil.teamName()
            val team = Team(teamName)

            val fromDb = teamRepository
                .save(team)
                .flatMap {
                    teamRepository.findTeamByUuid(team.uuid)
                }

            StepVerifier.create(fromDb).assertNext { t ->
                Assertions.assertThat(t.name).isEqualTo(team.name)
                Assertions.assertThat(t.uuid).isEqualTo(team.uuid)
            }.verifyComplete()
        }

        @Test
        fun `it should find team for a region`() {
            val north = Region("North")

            val fromDb = regionRepository
                .save(north)
                .flatMapMany {
                    val south = Region("South")
                    ReactiveUtils.combine(it, regionRepository.save(south))
                }.flatMap {
                    val north = it.t1
                    val south = it.t2

                    val team1 = Team("Team1")
                    team1.regions.add(north)
                    teamRepository
                        .save(team1)
                        .flatMapMany { t1 ->
                            val team2 = Team("Team2")
                            team2.regions.add(south)
                            ReactiveUtils.combine(t1, teamRepository.save(team2))
                        }
                        .flatMap {
                            teamRepository.findTeamByRegionsName("North")
                        }
                        .collectList()
                }


            StepVerifier.create(fromDb).assertNext { teams ->
                Assertions.assertThat(teams).isNotEmpty

                val tnames = teams.map { t -> t.name }
                Assertions.assertThat(tnames).hasSameElementsAs(listOf("Team1"))
            }.verifyComplete()

        }
    */
}