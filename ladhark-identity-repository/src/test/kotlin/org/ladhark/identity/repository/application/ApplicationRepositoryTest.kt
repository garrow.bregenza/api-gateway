package org.ladhark.identity.repository.application

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.ladhark.identity.entity.application.Application
import org.ladhark.identity.entity.application.HostingLocation
import org.ladhark.identity.repository.RepositoryTestHelper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import org.testcontainers.junit.jupiter.Testcontainers

@Testcontainers
@SpringBootTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
class ApplicationRepositoryTest : RepositoryTestHelper {

    @Autowired
    lateinit var applicationRepository: ApplicationRepository

    @Test
    fun `it should save application`() {

        runBlocking {

            val application = Application()
            application.name = "school"

            applicationRepository.save(application)
            val applicationFromDb = applicationRepository.findApplicationByName(application.name)

            Assertions.assertNotNull(applicationFromDb!!.id)
            Assertions.assertEquals("school", applicationFromDb.name)
        }

    }

    @Test
    fun `it should save application with hosting locations`() {

        runBlocking {

            val application = Application()
            application.name = "school22"

            val h1 = HostingLocation()
            h1.location = "https://1.test"

            val h2 = HostingLocation()
            h2.location = "https://2.test"

            application.hostingLocations.add(h1)
            application.hostingLocations.add(h2)

            applicationRepository.save(application)

            val applicationFromDb = applicationRepository.findApplicationByName(application.name)

            Assertions.assertNotNull(applicationFromDb!!.id)
            Assertions.assertEquals("school22", applicationFromDb.name)
        }

    }


}