package org.ladhark.identity.repository

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.Neo4jContainer

interface RepositoryTestHelper {

    companion object {
        private var neo4jContainer: Neo4jContainer<*>? = null

        @BeforeAll
        @JvmStatic
        fun initializeNeo4j() {
            neo4jContainer = Neo4jContainer("neo4j:latest")
                .withAdminPassword("matrix")
            neo4jContainer!!.start()
        }

        @AfterAll
        @JvmStatic
        fun stopNeo4j() {
            neo4jContainer!!.close()
        }

        @DynamicPropertySource
        @JvmStatic
        fun neo4jProperties(registry: DynamicPropertyRegistry) {
            registry.add("spring.neo4j.uri") { neo4jContainer!!.boltUrl }
            registry.add("spring.neo4j.authentication.username") { "neo4j" }
            registry.add("spring.neo4j.authentication.password") { neo4jContainer!!.adminPassword }
        }
    }
}